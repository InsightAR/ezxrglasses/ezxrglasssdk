﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;
using UnityEngine.Rendering;
using Wheels.Unity;

public partial class IssueFix : EditorWindow
{
    static bool orientationLandscapeLeft;

    static partial void CheckAllIssues_X1()
    {
        if (PlayerSettings.defaultInterfaceOrientation != UIOrientation.LandscapeLeft)
        {
            orientationLandscapeLeft = false;
            allFixed &= orientationLandscapeLeft;
        }
        else
        {
            orientationLandscapeLeft = true;
            allFixed &= orientationLandscapeLeft;
        }
    }

    static partial void Fix_X1()
    {
        FixDefaultOrientation();
    }

    static void FixDefaultOrientation()
    {
        PlayerSettings.defaultInterfaceOrientation = UIOrientation.LandscapeLeft;
    }

    partial void OnGUI_X1()
    {
        if (!orientationLandscapeLeft)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Default Orientation: ");
            GUILayout.FlexibleSpace();
            GUILayout.Label("Default Orientation must be 'LandscapeLeft'", label_Red);
            if (GUILayout.Button("Fix"))
            {
                FixDefaultOrientation();
            }
            GUILayout.EndHorizontal();
        }
    }
}