﻿using EZXR.Glass.Inputs;
using EZXR.Glass.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HandTrackingDemo : MonoBehaviour
{
    public ARHandManager arHand;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("SamplesMenu");
        }
    }
    public void OnExitButton() {
        SceneManager.LoadScene("SamplesMenu");
    }
}
