using EZXR.Glass.Inputs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandTouchUIDemo : MonoBehaviour
{
    public Text keyTipsText;
    public Slider sliderView;
    public Text sliderValueText;

    public GameObject canvasHandler;

    // Start is called before the first frame update
    void Start()
    {
        if (canvasHandler != null)
        {
            canvasHandler.GetComponent<CanvasHandler>().enabled = true;
            canvasHandler.GetComponent<CanvasTouchHandler>().enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && canvasHandler != null)
        {
            if (canvasHandler.GetComponent<CanvasHandler>().enabled)
            {
                canvasHandler.GetComponent<CanvasHandler>().enabled = false;
                canvasHandler.GetComponent<CanvasTouchHandler>().enabled = true;
            }
            else
            {
                canvasHandler.GetComponent<CanvasHandler>().enabled = true;
                canvasHandler.GetComponent<CanvasTouchHandler>().enabled = false;
            }
        }
    }

    public void OnSiliderChanged()
    {
        sliderValueText.text = sliderView.value.ToString();
    }

    public void OnButtonClicked()
    {
        keyTipsText.text = $"{Time.frameCount}";
    }
}
