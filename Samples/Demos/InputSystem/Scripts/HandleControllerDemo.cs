using System.Collections;
using System.Collections.Generic;
using EZXR.Glass.Inputs;
using EZXR.Glass.Runtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HandleControllerDemo : BaseGlassSDKDemo
{
    public Text keyCodeText;
    public Text sliderValueText;
    public Slider sliderView;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    new void Update()
    {
        keyCodeText.text = string.Empty;

        if (XRMan.Input.Type == InputType.Controller)
        {
            keyCodeText.text += GetButtonInfo(HandType.Left);
            keyCodeText.text += GetButtonInfo(HandType.Right);
        }
        base.Update();
    }

    private string GetButtonInfo(HandType handType)
    {
        string text_info = string.Empty;
        string text_hand = handType == HandType.Left ? "左手柄" : "右手柄";

        if (HandleControllerManager.Instance.GetButton(handType, HandleKeyCode.Grid))
            text_info = $"检测到{text_hand}【侧握键】";
        if (HandleControllerManager.Instance.GetButton(handType, HandleKeyCode.Home))
            text_info = $"检测到{text_hand}【Home键】";
        if (HandleControllerManager.Instance.GetButton(handType, HandleKeyCode.Primary))
            text_info = $"检测到{text_hand}【{(handType == HandType.Left ? "X" : "A")}键】";
        if (HandleControllerManager.Instance.GetButton(handType, HandleKeyCode.Back))
            text_info = $"检测到{text_hand}【返回键】";
        if (HandleControllerManager.Instance.GetButton(handType, HandleKeyCode.Rocker))
            text_info = $"检测到{text_hand}【摇杆键】";
        if (HandleControllerManager.Instance.GetButton(handType, HandleKeyCode.Secondary))
            text_info = $"检测到{text_hand}【{(handType == HandType.Left ? "Y" : "B")}键】";
        if (HandleControllerManager.Instance.GetButton(handType, HandleKeyCode.Trigger))
            text_info = $"检测到{text_hand}【扳机键】";

        var axis = HandleControllerManager.Instance.GetAxis2D(handType);
        if (axis != Vector2.zero)
        {
            if (!string.IsNullOrEmpty(text_info)) text_info += "\n";
            text_info += $"{text_hand}({axis.x:F1},  {axis.y:F1})";
        }

        if (!string.IsNullOrEmpty(text_info)) text_info += "\n";

        return text_info;
    }

    public void OnSiliderChanged()
    {
        sliderValueText.text = sliderView.value.ToString();
    }

    public void OnVibrateHandle(int handType)
    {
        HandleControllerManager.Instance.VibrateHandle((HandType)handType, 6, 500);
    }
}
