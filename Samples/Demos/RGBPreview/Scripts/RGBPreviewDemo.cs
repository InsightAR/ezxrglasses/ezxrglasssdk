using EZXR.Glass;
using EZXR.Glass.Device;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RGBPreviewDemo : BaseGlassSDKDemo
{

    #region params

    public Text labelPreview;
    
    public RGBCameraPreview previewQuad;

    #endregion

    #region unity functions

    private void OnEnable()
    {
        if (labelPreview != null)
        {
            labelPreview.text = "StopPreview";
        }
    }
    private void OnDisable()
    {
    }
    
    #endregion

    #region custom functions


    public void OnPreviewButton()
    {
        if (previewQuad == null) {
            return;
        }
        previewQuad.enablePreview = !previewQuad.enablePreview;
        
        if (labelPreview != null)
        {
            labelPreview.text = previewQuad.enablePreview ? "StopPreview" : "StartPreview";
        }
    }
    public void OnExitButton()
    {
        SceneManager.LoadScene("SamplesMenu");
    }
    #endregion
}
