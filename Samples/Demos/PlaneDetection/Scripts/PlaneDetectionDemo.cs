﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using EZXR.Glass.SixDof;
using EZXR.Glass.Plane;

public class PlaneDetectionDemo : BaseGlassSDKDemo
{
    Dictionary<ulong, GameObject> m_allPlanes = new Dictionary<ulong, GameObject>();

    // Update is called once per frame
    new void Update()
    {

        Dictionary<ulong, PlaneDetectionManager.PlaneInfo> planes = PlaneDetectionManager.Instance.GetPlanes();

        foreach (KeyValuePair<ulong, PlaneDetectionManager.PlaneInfo> kvp in planes)
        {
            ulong key = kvp.Key;
            PlaneDetectionManager.PlaneInfo value = kvp.Value;
            if (m_allPlanes.ContainsKey(key))
            {
                GameObject planeObj = m_allPlanes[key];
                planeObj.transform.position = value.position;
                planeObj.transform.rotation = value.rotation;
                planeObj.transform.localScale = value.scale * 0.1f;
            }
            else
            {
                CreatePlane(key, value);
            }
        }
        base.Update();
    }

    void CreatePlane(ulong key, PlaneDetectionManager.PlaneInfo planeObj)
    {
        GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
        plane.transform.position = planeObj.position;
        plane.transform.rotation = planeObj.rotation;
        plane.transform.localScale = planeObj.scale * 0.1f;
        Material material = Resources.Load<Material>("Shader/Wireframe/Examples/Materials/Wireframe-Transparent");
        plane.GetComponent<MeshRenderer>().material = material;
        m_allPlanes.Add(key, plane);
    }
}
