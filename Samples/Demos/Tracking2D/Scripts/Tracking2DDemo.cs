﻿using System.Collections;
using System.Collections.Generic;
using EZXR.Glass.Tracking2D;
using EZXR.Glass.UI;
using EZXR.Glass.SixDof;
using UnityEngine;
using UnityEngine.SceneManagement;
using EZXR.Glass;
using EZXR.Glass.Device;
using EZXR.Glass.Runtime;

public class Tracking2DDemo : BaseGlassSDKDemo
{
    public Tracking2DManager m_tracking2DManager;
    public CopyStreamingAssets m_copyStreamingAssets;

    private bool m_isDetectAuto = false;

    private void OnEnable()
    {
        if (m_copyStreamingAssets != null)
        {
            m_copyStreamingAssets.OnCopyCompleted += OnCopyCompleted;
        }
    }
    private void OnDisable()
    {
        if (m_copyStreamingAssets != null)
        {
            m_copyStreamingAssets.OnCopyCompleted -= OnCopyCompleted;
        }
    }
    void Start()
    {
    }


    public void OnDetectButton()
    {
        if (m_tracking2DManager != null)
        {
            m_tracking2DManager.DetectImageOnce();
        }
    }

    public void OnDetectAutoButton(GameObject obj_DetectAuto)
    {
        SpatialButton button_DetectAuto = obj_DetectAuto == null?null:obj_DetectAuto.GetComponent<SpatialButton>();
        if (m_tracking2DManager != null)
        {
            if (m_isDetectAuto == false)
            {
                m_isDetectAuto = true;
                if (button_DetectAuto != null)
                {
                    button_DetectAuto.Text = "DetectAuto: ON";
                }
            }
            else
            {
                m_isDetectAuto = false;
                if (button_DetectAuto != null)
                {
                    button_DetectAuto.Text = "DetectAuto: OFF";
                }
            }
            m_tracking2DManager.DetectImageAuto(m_isDetectAuto);
        }
    }

    void OnCopyCompleted(string assetPath)
    {
        if (m_tracking2DManager != null)
        {
            m_tracking2DManager.startTrackSession(assetPath);
        }
    }
}
