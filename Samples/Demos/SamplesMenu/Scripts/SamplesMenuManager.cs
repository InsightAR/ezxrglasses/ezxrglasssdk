﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SamplesMenuManager : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    public void onMenuButton(GameObject button)
    {
        string scene = "";
        bool isValidScene = true;
        bool isQuit = false;
        switch (button.name)
        {
            case "SpatialTracking":
                // scene += "SpatialTracking/SpatialTracking";
                scene += "SpatialTracking";
                break;
            case "InputSystem":
                // scene += "SpatialTracking/SpatialTracking";
                scene += "InputSystem_Dynamic";
                break;
            case "SpatialPositioning":
                // scene += "SpatialPositioning/SpatialPositioning";
                scene += "SpatialPositioning";
                break;
            case "Tracking3D":
                // scene += "Tracking3D/Tracking3D";
                scene += "Tracking3D";
                break;
            case "PlaneDetection":
                // scene += "PlaneDetection/PlaneDetection";
                scene += "PlaneDetection";
                break;
            case "RGBPreview":
                // scene += "RGBPreview/RGBPreview";
                scene += "RGBPreview";
                break;
            case "ApiTest":
                // scene += "APITestSample/ApiTestSample";
                scene += "ApiTestSample";
                break;
            case "Tracking2D":
                // scene += "Tracking2D/Scenes/Tracking2D";
                scene += "Tracking2D";
                break;
            case "SpatialComputing":
                // scene += "SpatialComputingSample/Scenes/EZXRSpatialComputingSample";
                scene += "EZXRSpatialComputingSample";
                break;
            case "MeshDetection":
                scene += "SpatialMesh";
                break;
            case "QRScanner":
                scene += "QRScanner";
                break;
            case "Exit":
                isQuit = true;
                isValidScene = false;
                break;
            default:
                isValidScene = false;
                break;
        }
        if (isQuit) {
            Application.Quit();
        }
        if (isValidScene)
            SceneManager.LoadScene(scene);
    }
}
