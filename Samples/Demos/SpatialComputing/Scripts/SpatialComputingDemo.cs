using System;
using System.IO;
using System.IO.Compression;
using EZXR.Glass.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EZXR.Glass.SpatialComputing;
using EZXR.Glass.Runtime;

public class SpatialComputingDemo : BaseGlassSDKDemo
{
    //public SpatialPanel locResultPanel;
    public Text locResultLabel;

    public GameObject displayModel;
    private bool modelShowing = false;

    public CopyStreamingAssets m_CopyStreamingAssets;

    // Start is called before the first frame update
    void Start()
    {
        if(displayModel) {
            displayModel.SetActive(false);
        }
        modelShowing = false;

        InvokeRepeating("checkingLocResults", 1.0f, 1.0f);

        if (m_CopyStreamingAssets != null)
        {
            m_CopyStreamingAssets.OnCopyCompleted += OnCopyCompleted;
        }
    }

    private void OnDestroy()
    {
        if (m_CopyStreamingAssets != null)
        {
            m_CopyStreamingAssets.OnCopyCompleted -= OnCopyCompleted;
        }
    }
    // Update is called once per frame
    new void Update()
    {
        base.Update();
    }

    public void onTrigerApplyLocResultImmediately()
    {
        if (EZXRSpatialComputingManager.Instance != null)
            EZXRSpatialComputingManager.Instance.TrigerApplyLocResultImmediately();
    }

    public void useImgUndistortion(GameObject objectButton) {
        if (EZXRSpatialComputingManager.Instance != null) {
            EZXRSpatialComputingManager.Instance.UseImgUndistortion = !EZXRSpatialComputingManager.Instance.UseImgUndistortion;

            if (objectButton != null)
            {
                Button btn = objectButton.GetComponent<Button>();
                btn.GetComponentInChildren<Text>().text =
                    EZXRSpatialComputingManager.Instance.UseImgUndistortion?"ImgUndist Off": "ImgUndist On";
            }
        }
    }

    private void checkingLocResults() {
        showLocResultStatus();
        showModel();
    }

    private void showLocResultStatus() {
        if (locResultLabel != null && EZXRSpatialComputingManager.Instance != null)
        {
            var status = EZXRSpatialComputingManager.Instance.currentVpsResultState.vps_result_status;
            var timestamp = EZXRSpatialComputingManager.Instance.currentVpsResultState.t_s;
            locResultLabel.text = $"LocResult : {status}\n    时间戳：{timestamp:F3}";
        }
    }

    private void showModel() {
        if(EZXRSpatialComputingManager.Instance != null && displayModel != null && !modelShowing) 
        {
            if(EZXRSpatialComputingManager.Instance.currentVpsResultState.vps_result_status == LOCSTATUS.SUCCESS){
                displayModel.SetActive(true);
                modelShowing = true;
            }
        }
    }

    void OnCopyCompleted(string assetPath)
    {
        EZXRSpatialComputingManager.Instance.startSCLocSession(assetPath);
    }

}
