using EZXR.Glass.SpatialMesh;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class SpatialMeshDemo : BaseGlassSDKDemo
{

    private bool isShowSmoothedMesh = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    new void Update()
    {
        if (isShowSmoothedMesh)
        {
            SpatialMeshManager.Instance.ShowSmoothedMeshes();
        }
        else
        {
            SpatialMeshManager.Instance.ShowIncrementalMeshes();
        }
        base.Update();
    }


    public void OnBtnClick_MeshDetection(Button button) {
        if (SpatialMeshManager.Instance.IsMeshDetecting)
        {
            SpatialMeshManager.Instance.pauseMeshDetecting();
            button.GetComponentInChildren<Text>().text = "Resume\nMesh Detection";            
        }
        else {
            SpatialMeshManager.Instance.resumeMeshDetecting();
            button.GetComponentInChildren<Text>().text = "Pause\nMesh Detection";
        }
    }
    public void OnBtnClick_ShowSmoothedMesh(Button button)
    {
        if (isShowSmoothedMesh)
        {//to show incremental meshes
            isShowSmoothedMesh = false;
            SpatialMeshManager.Instance.incrementalMeshVisible = true;
            SpatialMeshManager.Instance.smoothedMeshVisible = false;
            button.GetComponentInChildren<Text>().text = "To Show\nSmoothedMesh";
            CancelInvoke("updateSmoothMesh");
        }
        else
        {//to show smoothed meshes
            SpatialMeshManager.Instance.incrementalMeshVisible = false;
            SpatialMeshManager.Instance.smoothedMeshVisible = true;
            isShowSmoothedMesh = true;
            button.GetComponentInChildren<Text>().text = "To Show\nIncrementalMesh";
            InvokeRepeating("updateSmoothMesh", 0.01f, 2.0f);
        }
    }
    private void updateSmoothMesh() {
        if (isShowSmoothedMesh) {
            SpatialMeshManager.Instance.UpdateSmoothedMeshes();
        }
    }

    public void OnBtnClick_ExportMesh()
    {
        new Thread(() =>
        {
            SpatialMeshManager.Instance.SaveBackendSmoothedMesh();
        }).Start();
    }

}
