// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Dissolve"
{
	Properties
	{
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_Dissolve("Dissolve", Range( -2 , 2)) = -0.32
		_EmissivePower("Emissive Power", Float) = 1
		_Color0("Color 0", Color) = (1,0.8735534,0,0)
		[Toggle(_USEFRESHNEL_ON)] _UseFreshnel("Use Freshnel?", Float) = 1
		_TextureSample1("Texture Sample 1", 2D) = "white" {}
		_NoiseUVscale("Noise UV scale", Float) = 1
		_FreshnelDensity("Freshnel Density", Float) = 1
		_Opacity("Opacity", Float) = 0

	}
	
	SubShader
	{
		
		
		Tags { "RenderType"="Opaque" }
	LOD 100

		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend Off
		AlphaToMask Off
		Cull Back
		ColorMask RGBA
		ZWrite On
		ZTest LEqual
		Offset 0 , 0
		
		
		
		Pass
		{
			Name "Unlit"
			Tags { "LightMode"="ForwardBase" }
			CGPROGRAM

			

			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			//only defining to not throw compilation error over Unity 5.5
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			#define ASE_NEEDS_FRAG_POSITION
			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#pragma shader_feature_local _USEFRESHNEL_ON


			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float4 ase_texcoord : TEXCOORD0;
				float3 ase_normal : NORMAL;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 worldPos : TEXCOORD0;
				#endif
				float4 ase_texcoord1 : TEXCOORD1;
				float4 ase_texcoord2 : TEXCOORD2;
				float3 ase_normal : NORMAL;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			uniform sampler2D _TextureSample1;
			SamplerState sampler_TextureSample1;
			uniform float _Opacity;
			uniform float4 _Color0;
			uniform sampler2D _TextureSample0;
			SamplerState sampler_TextureSample0;
			uniform float _NoiseUVscale;
			uniform float _Dissolve;
			uniform float _FreshnelDensity;
			uniform float _EmissivePower;

			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				o.ase_texcoord1.xy = v.ase_texcoord.xy;
				o.ase_texcoord2 = v.vertex;
				o.ase_normal = v.ase_normal;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord1.zw = 0;
				float3 vertexValue = float3(0, 0, 0);
				#if ASE_ABSOLUTE_VERTEX_POS
				vertexValue = v.vertex.xyz;
				#endif
				vertexValue = vertexValue;
				#if ASE_ABSOLUTE_VERTEX_POS
				v.vertex.xyz = vertexValue;
				#else
				v.vertex.xyz += vertexValue;
				#endif
				o.vertex = UnityObjectToClipPos(v.vertex);

				#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				#endif
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
				fixed4 finalColor;
				#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 WorldPosition = i.worldPos;
				#endif
				float2 texCoord270 = i.ase_texcoord1.xy * float2( 1,1 ) + float2( 0,0 );
				float4 tex2DNode271 = tex2D( _TextureSample1, texCoord270 );
				float4 appendResult322 = (float4(tex2DNode271.r , tex2DNode271.g , tex2DNode271.b , _Opacity));
				float2 appendResult223 = (float2(i.ase_texcoord2.xyz.x , i.ase_texcoord2.xyz.y));
				float temp_output_208_0 = ( i.ase_texcoord2.xyz.y + _Dissolve );
				float clampResult246 = clamp( ( ( ( tex2D( _TextureSample0, ( appendResult223 * _NoiseUVscale ) ).r - temp_output_208_0 ) * 3.93 ) * (0.14 + (_Dissolve - -2.0) * (6.1 - 0.14) / (2.0 - -2.0)) ) , 0.0 , 1.0 );
				float clampResult296 = clamp( clampResult246 , 0.0 , 1.0 );
				float4 lerpResult224 = lerp( appendResult322 , ( _Color0 * appendResult322 ) , clampResult296);
				float clampResult319 = clamp( ( clampResult296 - temp_output_208_0 ) , 0.0 , 1.0 );
				float4 color248 = IsGammaSpace() ? float4(0,0,0,1) : float4(0,0,0,1);
				float4 lerpResult247 = lerp( color248 , _Color0 , clampResult296);
				float3 ase_worldViewDir = UnityWorldSpaceViewDir(WorldPosition);
				ase_worldViewDir = normalize(ase_worldViewDir);
				float clampResult262 = clamp( saturate( ( 1.0 - _Dissolve ) ) , 0.0 , 1.0 );
				float fresnelNdotV249 = dot( i.ase_normal, ase_worldViewDir );
				float fresnelNode249 = ( 0.0 + clampResult262 * pow( 1.0 - fresnelNdotV249, _FreshnelDensity ) );
				float4 lerpResult251 = lerp( float4( 0,0,0,0 ) , ( lerpResult247 * fresnelNode249 ) , fresnelNode249);
				#ifdef _USEFRESHNEL_ON
				float4 staticSwitch267 = ( lerpResult251 * _EmissivePower );
				#else
				float4 staticSwitch267 = lerpResult247;
				#endif
				
				
				finalColor = ( ( lerpResult224 + ( ( clampResult296 - clampResult319 ) * _Color0 ) ) + staticSwitch267 );
				return finalColor;
			}
			ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=18600
-457;1034;1659;682;-2400.263;924.5611;2.264211;True;True
Node;AmplifyShaderEditor.PosVertexDataNode;198;1976.499,517.1544;Inherit;True;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;309;2206.778,294.8452;Inherit;False;Property;_NoiseUVscale;Noise UV scale;6;0;Create;True;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;223;2289.068,487.3518;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;308;2436.059,273.2387;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;163;2081.325,881.3934;Inherit;False;Property;_Dissolve;Dissolve;1;0;Create;False;0;0;False;0;False;-0.32;-2;-2;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;200;2577.837,347.2195;Inherit;True;Property;_TextureSample0;Texture Sample 0;0;0;Create;True;0;0;False;0;False;-1;None;7d2ee8772c68e9e438ea8ab52f8ff023;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;208;3021.569,570.8812;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0.48;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;231;3276.083,377.4654;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;293;3499.047,559.8597;Inherit;False;5;0;FLOAT;-2;False;1;FLOAT;-2;False;2;FLOAT;2;False;3;FLOAT;0.14;False;4;FLOAT;6.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;295;3533.755,404.4954;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;3.93;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;168;3750.64,512.5699;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;258;3654.148,1274.512;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;259;3920.223,1273.598;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;246;3903.703,433.7365;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;296;4238.804,350.9589;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;310;4156.716,1181.65;Inherit;False;Property;_FreshnelDensity;Freshnel Density;7;0;Create;True;0;0;False;0;False;1;-0.04;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;248;4003.669,162.9113;Inherit;False;Constant;_Color2;Color 2;3;0;Create;True;0;0;False;0;False;0,0,0,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NormalVertexDataNode;250;4168.631,828.2186;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;225;4006.128,-29.07494;Inherit;False;Property;_Color0;Color 0;3;0;Create;True;0;0;False;0;False;1,0.8735534,0,0;1,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;262;4197.216,1271.083;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;266;4176.633,998.3741;Inherit;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.TextureCoordinatesNode;270;3654.542,-255.9105;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FresnelNode;249;4626.063,916.5195;Inherit;True;Standard;WorldNormal;ViewDir;False;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0.14;False;3;FLOAT;4.21;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;271;3965.099,-284.5305;Inherit;True;Property;_TextureSample1;Texture Sample 1;5;0;Create;True;0;0;False;0;False;-1;None;41b55e74e2f2061499476d87b84aac4b;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;324;4277.206,-122.0814;Inherit;False;Property;_Opacity;Opacity;8;0;Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;247;4758.61,302.0146;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;316;4517.553,539.6946;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;322;4359.869,-388.286;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ClampOpNode;319;4836.477,170.8219;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;263;5028.663,460.6475;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;272;4449.786,-40.01112;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;318;5078.276,47.3219;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;253;5293.283,729.9967;Inherit;False;Property;_EmissivePower;Emissive Power;2;0;Create;True;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;251;5275.226,485.4479;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;224;5148.405,-257.5111;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;321;5400.176,53.49248;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;252;5600.448,484.6866;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;320;5637.276,25.22208;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;267;5892.689,316.7535;Inherit;True;Property;_UseFreshnel;Use Freshnel?;4;0;Create;True;0;0;False;0;False;0;1;1;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;315;6237.33,31.55833;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;292;2926.921,120.9939;Inherit;True;SmoothStep;-1;;1;;0;0;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;313;6612.139,19.914;Float;False;True;-1;2;ASEMaterialInspector;100;1;Dissolve;0770190933193b94aaa3065e307002fa;True;Unlit;0;0;Unlit;2;True;0;1;False;-1;0;False;-1;0;1;False;-1;0;False;-1;True;0;False;-1;0;False;-1;False;False;False;False;False;False;True;0;False;-1;True;0;False;-1;True;True;True;True;True;0;False;-1;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;1;RenderType=Opaque=RenderType;True;2;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;LightMode=ForwardBase;False;0;;0;0;Standard;1;Vertex Position,InvertActionOnDeselection;1;0;1;True;False;;False;0
WireConnection;223;0;198;1
WireConnection;223;1;198;2
WireConnection;308;0;223;0
WireConnection;308;1;309;0
WireConnection;200;1;308;0
WireConnection;208;0;198;2
WireConnection;208;1;163;0
WireConnection;231;0;200;1
WireConnection;231;1;208;0
WireConnection;293;0;163;0
WireConnection;295;0;231;0
WireConnection;168;0;295;0
WireConnection;168;1;293;0
WireConnection;258;0;163;0
WireConnection;259;0;258;0
WireConnection;246;0;168;0
WireConnection;296;0;246;0
WireConnection;262;0;259;0
WireConnection;249;0;250;0
WireConnection;249;4;266;0
WireConnection;249;2;262;0
WireConnection;249;3;310;0
WireConnection;271;1;270;0
WireConnection;247;0;248;0
WireConnection;247;1;225;0
WireConnection;247;2;296;0
WireConnection;316;0;296;0
WireConnection;316;1;208;0
WireConnection;322;0;271;1
WireConnection;322;1;271;2
WireConnection;322;2;271;3
WireConnection;322;3;324;0
WireConnection;319;0;316;0
WireConnection;263;0;247;0
WireConnection;263;1;249;0
WireConnection;272;0;225;0
WireConnection;272;1;322;0
WireConnection;318;0;296;0
WireConnection;318;1;319;0
WireConnection;251;1;263;0
WireConnection;251;2;249;0
WireConnection;224;0;322;0
WireConnection;224;1;272;0
WireConnection;224;2;296;0
WireConnection;321;0;318;0
WireConnection;321;1;225;0
WireConnection;252;0;251;0
WireConnection;252;1;253;0
WireConnection;320;0;224;0
WireConnection;320;1;321;0
WireConnection;267;1;247;0
WireConnection;267;0;252;0
WireConnection;315;0;320;0
WireConnection;315;1;267;0
WireConnection;313;0;315;0
ASEEND*/
//CHKSM=3169B95983E65C8AE0681FBAE61808744BCD750C