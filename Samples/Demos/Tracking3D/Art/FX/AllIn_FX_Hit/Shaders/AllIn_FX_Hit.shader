// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "AllIn/AllIn_FX_Hit"
{
	Properties
	{
		_Color0("Color 0", Color) = (0,0,0,0)
		_Offset("Offset", Range( 0 , 1)) = 0.14782
		_Width("Width", Float) = 0.23
		_Opacity("Opacity", Range( 0 , 1)) = 1
		_Smoothness("Smoothness", Range( 0 , 0.2)) = 0.1658824
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#pragma target 3.0
		#pragma multi_compile_instancing
		#pragma only_renderers d3d11 glcore gles gles3 metal 
		#pragma surface surf Unlit alpha:fade keepalpha noshadow noambient novertexlights nolightmap  nodynlightmap nodirlightmap nofog nometa noforwardadd 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Color0;
		uniform float _Offset;
		uniform float _Width;
		uniform float _Smoothness;
		uniform float _Opacity;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			o.Emission = _Color0.rgb;
			float temp_output_9_0 = ( _Offset + _Width );
			float2 uv_TexCoord2 = i.uv_texcoord + float2( -0.5,-0.5 );
			float temp_output_10_0_g1 = length( uv_TexCoord2 );
			float temp_output_5_0_g1 = _Offset;
			float temp_output_6_0_g1 = temp_output_9_0;
			float temp_output_35_0_g1 = ( step( temp_output_5_0_g1 , temp_output_10_0_g1 ) * step( temp_output_10_0_g1 , temp_output_6_0_g1 ) );
			float temp_output_14_0_g1 = ( temp_output_10_0_g1 * temp_output_35_0_g1 );
			float temp_output_3_0 = temp_output_14_0_g1;
			float smoothstepResult5 = smoothstep( _Offset , ( temp_output_9_0 + -_Smoothness ) , temp_output_3_0);
			float smoothstepResult6 = smoothstep( temp_output_9_0 , ( _Offset + _Smoothness ) , temp_output_3_0);
			o.Alpha = ( smoothstepResult5 * smoothstepResult6 * _Opacity );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18935
92;51;1646;823;2136.813;589.823;2.182184;True;False
Node;AmplifyShaderEditor.TextureCoordinatesNode;2;-1102,56.5;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;-0.5,-0.5;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;8;-955.5999,331.5;Inherit;False;Property;_Width;Width;2;0;Create;True;0;0;0;False;0;False;0.23;0.23;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-1006.3,171.8;Inherit;False;Property;_Offset;Offset;1;0;Create;True;0;0;0;False;0;False;0.14782;0.14782;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;18;-945.7251,438.8851;Inherit;False;Property;_Smoothness;Smoothness;4;0;Create;True;0;0;0;False;0;False;0.1658824;0.15;0;0.2;0;1;FLOAT;0
Node;AmplifyShaderEditor.LengthOpNode;1;-851,67.5;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;9;-731,292.5;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;19;-613.7251,390.8851;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;16;-509.7251,287.8851;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;-0.21;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;17;-499.7251,449.8851;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.21;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;3;-548.5823,47.74368;Inherit;False;JieQu 截取;-1;;1;3aa22aee9a140c8479ec5a3d9fe7775d;5,8,1,28,1,34,1,9,1,31,1;3;10;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;1;False;2;FLOAT;0;FLOAT;37
Node;AmplifyShaderEditor.SmoothstepOpNode;5;-208,125.2;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;6;-189.9,342.5;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;12;74.24963,412.2303;Inherit;False;Property;_Opacity;Opacity;3;0;Create;True;0;0;0;False;0;False;1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;11;52.3497,-156.0697;Inherit;False;Property;_Color0;Color 0;0;0;Create;True;0;0;0;False;0;False;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;165,196.5;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;506.6,16.1;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;AllIn/AllIn_FX_Hit;False;False;False;False;True;True;True;True;True;True;True;True;False;False;True;False;True;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;All;5;d3d11;glcore;gles;gles3;metal;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;1;0;2;0
WireConnection;9;0;7;0
WireConnection;9;1;8;0
WireConnection;19;0;18;0
WireConnection;16;0;9;0
WireConnection;16;1;19;0
WireConnection;17;0;7;0
WireConnection;17;1;18;0
WireConnection;3;10;1;0
WireConnection;3;5;7;0
WireConnection;3;6;9;0
WireConnection;5;0;3;0
WireConnection;5;1;7;0
WireConnection;5;2;16;0
WireConnection;6;0;3;0
WireConnection;6;1;9;0
WireConnection;6;2;17;0
WireConnection;10;0;5;0
WireConnection;10;1;6;0
WireConnection;10;2;12;0
WireConnection;0;2;11;0
WireConnection;0;9;10;0
ASEEND*/
//CHKSM=A2E4FCABEAD0D2CD0F999BD9C6F34E10787919A9