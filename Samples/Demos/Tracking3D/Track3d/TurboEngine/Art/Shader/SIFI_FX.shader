// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "SIFI_FX"
{
	Properties
	{
		_MainText("MainText", 2D) = "white" {}
		_Color0("Color 0", Color) = (0.8023214,0.6745283,1,0)
		_Color1("Color 1", Color) = (0.504717,0.9536285,1,0)
		_Num_Y("Num_Y", Float) = 10
		_UseTexture("UseTexture", Range( 0 , 1)) = 0
		_ArcUV("ArcUV", Range( 0 , 1)) = 1
		_TimeScale("TimeScale", Float) = 1
		_CullNum("CullNum", Float) = 3
		_Rotate("Rotate", Float) = -1
		_Num_X("Num_X", Float) = 10
		_Clip_X("Clip_X", Range( 0 , 0.25)) = 0.1
		_Clip_Y("Clip_Y", Range( 0 , 0.25)) = 0.1
		_Seed("Seed", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Unlit alpha:fade keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Color1;
		uniform float4 _Color0;
		uniform float _ArcUV;
		uniform float _Num_Y;
		uniform float _Seed;
		uniform sampler2D _MainText;
		uniform float4 _MainText_ST;
		uniform float _UseTexture;
		uniform float _Rotate;
		uniform float _TimeScale;
		uniform float _Clip_Y;
		uniform float _CullNum;
		uniform float _Clip_X;
		uniform float _Num_X;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float2 _Vector1 = float2(0.5,0.5);
			float2 break44 = ( i.uv_texcoord - _Vector1 );
			float temp_output_45_0 = distance( i.uv_texcoord , _Vector1 );
			float2 appendResult48 = (float2(( ( atan2( break44.x , break44.y ) / ( 2.0 * UNITY_PI ) ) + 0.5 ) , min( ( temp_output_45_0 + temp_output_45_0 ) , 1.0 )));
			float2 lerpResult42 = lerp( i.uv_texcoord , appendResult48 , _ArcUV);
			float2 UV34 = lerpResult42;
			float2 break38 = UV34;
			float temp_output_3_0 = ( break38.y * _Num_Y );
			float temp_output_2_0 = floor( temp_output_3_0 );
			float2 temp_cast_0 = (( temp_output_2_0 + _Seed )).xx;
			float dotResult4_g1 = dot( temp_cast_0 , float2( 12.9898,78.233 ) );
			float lerpResult10_g1 = lerp( 0.0 , 1.0 , frac( ( sin( dotResult4_g1 ) * 43758.55 ) ));
			float temp_output_6_0 = lerpResult10_g1;
			float4 lerpResult25 = lerp( _Color1 , _Color0 , temp_output_6_0);
			float2 uv_MainText = i.uv_texcoord * _MainText_ST.xy + _MainText_ST.zw;
			float4 lerpResult36 = lerp( lerpResult25 , tex2D( _MainText, uv_MainText ) , _UseTexture);
			o.Emission = lerpResult36.rgb;
			float temp_output_74_0 = frac( ( break38.x + ( _Time.y * _Rotate ) ) );
			float temp_output_12_0 = abs( ( frac( ( temp_output_6_0 + ( ( _Time.y * _TimeScale ) * ( ( temp_output_6_0 * 0.1 ) + 0.1 ) ) ) ) - 0.5 ) );
			float temp_output_16_0 = frac( temp_output_3_0 );
			float temp_output_65_0 = frac( ( break38.x * _Num_X ) );
			o.Alpha = ( step( temp_output_74_0 , ( temp_output_12_0 + temp_output_12_0 ) ) * step( _Clip_Y , ( ( 1.0 - temp_output_16_0 ) * temp_output_16_0 ) ) * temp_output_74_0 * step( _CullNum , temp_output_2_0 ) * step( _Clip_X , ( ( 1.0 - temp_output_65_0 ) * temp_output_65_0 ) ) );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18703
-1150;543;1181;544;786.9248;1155.136;1.677688;True;False
Node;AmplifyShaderEditor.TextureCoordinatesNode;1;35.0732,-896.3539;Inherit;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;46;136.9597,-614.2513;Inherit;False;Constant;_Vector1;Vector 1;3;0;Create;True;0;0;False;0;False;0.5,0.5;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleSubtractOpNode;39;320.9749,-662.4707;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.BreakToComponentsNode;44;447.9595,-658.2513;Inherit;False;FLOAT2;1;0;FLOAT2;0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.ATan2OpNode;43;560.9592,-657.2513;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PiNode;50;370.3406,-800.1076;Inherit;False;1;0;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;45;345.3927,-1021.818;Inherit;True;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;49;563.2119,-938.2155;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;47;562.2686,-1038.128;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;51;703.5679,-1046.701;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;52;751.54,-931.3847;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;48;951.6767,-1040.608;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;41;710.5936,-707.3034;Inherit;False;Property;_ArcUV;ArcUV;5;0;Create;True;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;42;1057.761,-896.7054;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;34;1237.883,-903.8591;Inherit;True;UV;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;33;-992.3145,172.3389;Inherit;False;34;UV;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;4;-828.3569,307.7769;Inherit;False;Property;_Num_Y;Num_Y;3;0;Create;True;0;0;False;0;False;10;100;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;38;-809.4334,157.0767;Inherit;False;FLOAT2;1;0;FLOAT2;0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;3;-643.411,273.8108;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FloorOpNode;2;-419.6637,525.1799;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;77;-406.1421,742.5469;Inherit;False;Property;_Seed;Seed;12;0;Create;True;0;0;False;0;False;0;3.74;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;76;-266.1422,641.5469;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;28;-295.4009,828.9983;Inherit;False;Constant;_Float2;Float 2;0;0;Create;True;0;0;False;0;False;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;6;-165.6637,537.18;Inherit;True;Random Range;-1;;1;7b754edb8aebbfb4a9ace907af661cfc;0;3;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;54;-197.4305,1115.728;Inherit;False;Property;_TimeScale;TimeScale;6;0;Create;True;0;0;False;0;False;1;0.2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-131.0138,762.0677;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;9;-232.2847,967.679;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;27;27.28828,793.5953;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;60;-20.17858,1007.976;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;15;81.12445,885.8243;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;8;122.68,617.0208;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;70;-797.2239,-115.2183;Inherit;False;Property;_Num_X;Num_X;9;0;Create;True;0;0;False;0;False;10;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;59;-709.8734,455.9742;Inherit;False;Property;_Rotate;Rotate;8;0;Create;True;0;0;False;0;False;-1;-0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;72;-751.1031,390.5408;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;69;-632.8047,-134.3721;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;13;200.6801,747.0208;Inherit;False;Constant;_Float1;Float 1;0;0;Create;True;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;10;233.6801,671.0208;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;11;364.68,699.0208;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;58;-549.1851,425.9678;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;16;-323.6048,-104.9018;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;65;-310.003,-340.9949;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.AbsOpNode;12;498.68,702.0208;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;73;-326.1414,142.2952;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;17;-86.77017,14.41124;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;66;8.394882,-347.9714;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;18;198.1767,-42.49216;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;22;765.3609,-156.9598;Inherit;False;Property;_Color0;Color 0;1;0;Create;True;0;0;False;0;False;0.8023214,0.6745283,1,0;0.5683467,0.25,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;75;138.3597,-429.266;Inherit;False;Property;_Clip_X;Clip_X;10;0;Create;True;0;0;False;0;False;0.1;0;0;0.25;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;53;635.8249,693.5128;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;74;-95.50688,161.2104;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;56;-302.8292,381.039;Inherit;False;Property;_CullNum;CullNum;7;0;Create;True;0;0;False;0;False;3;20;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;78;129.7861,169.0245;Inherit;False;Property;_Clip_Y;Clip_Y;11;0;Create;True;0;0;False;0;False;0.1;0.1;0;0.25;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;29;763.8932,16.10201;Inherit;False;Property;_Color1;Color 1;2;0;Create;True;0;0;False;0;False;0.504717,0.9536285,1,0;0.504717,0.9945575,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;67;202.9464,-308.8297;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;35;959.6341,-190.7699;Inherit;True;Property;_MainText;MainText;0;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;37;983.6342,-277.7699;Inherit;False;Property;_UseTexture;UseTexture;4;0;Create;True;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;68;427.2075,-309.3948;Inherit;True;2;0;FLOAT;0.2;False;1;FLOAT;0.9;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;25;1010.472,5.802811;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StepOpNode;7;787.8812,623.5789;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;55;-68.87507,409.9473;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;19;432.7373,-40.17412;Inherit;True;2;0;FLOAT;0.1;False;1;FLOAT;0.9;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;1046.079,233.0467;Inherit;True;5;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;36;1303.634,-161.7699;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;71;1028.989,-547.0087;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;31;1462.71,-202.8731;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;SIFI_FX;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;39;0;1;0
WireConnection;39;1;46;0
WireConnection;44;0;39;0
WireConnection;43;0;44;0
WireConnection;43;1;44;1
WireConnection;45;0;1;0
WireConnection;45;1;46;0
WireConnection;49;0;43;0
WireConnection;49;1;50;0
WireConnection;47;0;45;0
WireConnection;47;1;45;0
WireConnection;51;0;47;0
WireConnection;52;0;49;0
WireConnection;48;0;52;0
WireConnection;48;1;51;0
WireConnection;42;0;1;0
WireConnection;42;1;48;0
WireConnection;42;2;41;0
WireConnection;34;0;42;0
WireConnection;38;0;33;0
WireConnection;3;0;38;1
WireConnection;3;1;4;0
WireConnection;2;0;3;0
WireConnection;76;0;2;0
WireConnection;76;1;77;0
WireConnection;6;1;76;0
WireConnection;26;0;6;0
WireConnection;26;1;28;0
WireConnection;27;0;26;0
WireConnection;27;1;28;0
WireConnection;60;0;9;0
WireConnection;60;1;54;0
WireConnection;15;0;60;0
WireConnection;15;1;27;0
WireConnection;8;0;6;0
WireConnection;8;1;15;0
WireConnection;69;0;38;0
WireConnection;69;1;70;0
WireConnection;10;0;8;0
WireConnection;11;0;10;0
WireConnection;11;1;13;0
WireConnection;58;0;72;0
WireConnection;58;1;59;0
WireConnection;16;0;3;0
WireConnection;65;0;69;0
WireConnection;12;0;11;0
WireConnection;73;0;38;0
WireConnection;73;1;58;0
WireConnection;17;0;16;0
WireConnection;66;0;65;0
WireConnection;18;0;17;0
WireConnection;18;1;16;0
WireConnection;53;0;12;0
WireConnection;53;1;12;0
WireConnection;74;0;73;0
WireConnection;67;0;66;0
WireConnection;67;1;65;0
WireConnection;68;0;75;0
WireConnection;68;1;67;0
WireConnection;25;0;29;0
WireConnection;25;1;22;0
WireConnection;25;2;6;0
WireConnection;7;0;74;0
WireConnection;7;1;53;0
WireConnection;55;0;56;0
WireConnection;55;1;2;0
WireConnection;19;0;78;0
WireConnection;19;1;18;0
WireConnection;20;0;7;0
WireConnection;20;1;19;0
WireConnection;20;2;74;0
WireConnection;20;3;55;0
WireConnection;20;4;68;0
WireConnection;36;0;25;0
WireConnection;36;1;35;0
WireConnection;36;2;37;0
WireConnection;31;2;36;0
WireConnection;31;9;20;0
ASEEND*/
//CHKSM=90E57F53CC10D936C95A63D0D46E8377EDDF3DE1