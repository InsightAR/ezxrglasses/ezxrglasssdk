// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "UI_FlipBooK"
{
	Properties
	{
		_Color0("Color 0", Color) = (1,1,1,1)
		_Opacity("Opacity", Range( 0 , 1)) = 1
		_Digit(" Digit", Vector) = (1,1,1,0)
		_Step("Step", Float) = 0
		_Columns("Columns", Float) = 0
		_Rows("Rows", Float) = 0
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_TimeScale("TimeScale", Float) = 3
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "ForceNoShadowCasting" = "True" "DisableBatching" = "True" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma multi_compile_instancing
		#pragma surface surf Unlit alpha:fade keepalpha noshadow noambient novertexlights nolightmap  nodynlightmap nodirlightmap nofog nometa noforwardadd 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Color0;
		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform float _Columns;
		uniform float _Rows;
		uniform float _Step;
		uniform float _TimeScale;
		uniform float3 _Digit;
		SamplerState sampler_TextureSample0;
		uniform float _Opacity;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			o.Emission = _Color0.rgb;
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float2 appendResult361 = (float2(3.0 , 1.0));
			float2 temp_output_352_0 = ( uv_TextureSample0 * appendResult361 );
			float2 temp_output_353_0 = frac( temp_output_352_0 );
			float mulTime387 = _Time.y * _TimeScale;
			float temp_output_386_0 = ( _Step + mulTime387 );
			float temp_output_350_0 = ( temp_output_386_0 * 0.1 );
			// *** BEGIN Flipbook UV Animation vars ***
			// Total tiles of Flipbook Texture
			float fbtotaltiles349 = _Columns * _Rows;
			// Offsets for cols and rows of Flipbook Texture
			float fbcolsoffset349 = 1.0f / _Columns;
			float fbrowsoffset349 = 1.0f / _Rows;
			// Speed of animation
			float fbspeed349 = _Time[ 1 ] * 0.0;
			// UV Tiling (col and row offset)
			float2 fbtiling349 = float2(fbcolsoffset349, fbrowsoffset349);
			// UV Offset - calculate current tile linear index, and convert it to (X * coloffset, Y * rowoffset)
			// Calculate current tile linear index
			float fbcurrenttileindex349 = round( fmod( fbspeed349 + floor( ( temp_output_350_0 + 0.05 ) ), fbtotaltiles349) );
			fbcurrenttileindex349 += ( fbcurrenttileindex349 < 0) ? fbtotaltiles349 : 0;
			// Obtain Offset X coordinate from current tile linear index
			float fblinearindextox349 = round ( fmod ( fbcurrenttileindex349, _Columns ) );
			// Multiply Offset X by coloffset
			float fboffsetx349 = fblinearindextox349 * fbcolsoffset349;
			// Obtain Offset Y coordinate from current tile linear index
			float fblinearindextoy349 = round( fmod( ( fbcurrenttileindex349 - fblinearindextox349 ) / _Columns, _Rows ) );
			// Reverse Y to get tiles from Top to Bottom
			fblinearindextoy349 = (int)(_Rows-1) - fblinearindextoy349;
			// Multiply Offset Y by rowoffset
			float fboffsety349 = fblinearindextoy349 * fbrowsoffset349;
			// UV Offset
			float2 fboffset349 = float2(fboffsetx349, fboffsety349);
			// Flipbook UV
			half2 fbuv349 = temp_output_353_0 * fbtiling349 + fboffset349;
			// *** END Flipbook UV Animation vars ***
			float temp_output_374_0 = (temp_output_352_0).x;
			float temp_output_354_0 = step( temp_output_374_0 , 1.0 );
			float temp_output_369_0 = step( temp_output_374_0 , 2.0 );
			float fbtotaltiles359 = _Columns * _Rows;
			float fbcolsoffset359 = 1.0f / _Columns;
			float fbrowsoffset359 = 1.0f / _Rows;
			float fbspeed359 = _Time[ 1 ] * 0.0;
			float2 fbtiling359 = float2(fbcolsoffset359, fbrowsoffset359);
			float fbcurrenttileindex359 = round( fmod( fbspeed359 + floor( ( temp_output_350_0 * 0.1 ) ), fbtotaltiles359) );
			fbcurrenttileindex359 += ( fbcurrenttileindex359 < 0) ? fbtotaltiles359 : 0;
			float fblinearindextox359 = round ( fmod ( fbcurrenttileindex359, _Columns ) );
			float fboffsetx359 = fblinearindextox359 * fbcolsoffset359;
			float fblinearindextoy359 = round( fmod( ( fbcurrenttileindex359 - fblinearindextox359 ) / _Columns, _Rows ) );
			fblinearindextoy359 = (int)(_Rows-1) - fblinearindextoy359;
			float fboffsety359 = fblinearindextoy359 * fbrowsoffset359;
			float2 fboffset359 = float2(fboffsetx359, fboffsety359);
			half2 fbuv359 = temp_output_353_0 * fbtiling359 + fboffset359;
			float fbtotaltiles344 = _Columns * _Rows;
			float fbcolsoffset344 = 1.0f / _Columns;
			float fbrowsoffset344 = 1.0f / _Rows;
			float fbspeed344 = _Time[ 1 ] * 0.0;
			float2 fbtiling344 = float2(fbcolsoffset344, fbrowsoffset344);
			float fbcurrenttileindex344 = round( fmod( fbspeed344 + temp_output_386_0, fbtotaltiles344) );
			fbcurrenttileindex344 += ( fbcurrenttileindex344 < 0) ? fbtotaltiles344 : 0;
			float fblinearindextox344 = round ( fmod ( fbcurrenttileindex344, _Columns ) );
			float fboffsetx344 = fblinearindextox344 * fbcolsoffset344;
			float fblinearindextoy344 = round( fmod( ( fbcurrenttileindex344 - fblinearindextox344 ) / _Columns, _Rows ) );
			fblinearindextoy344 = (int)(_Rows-1) - fblinearindextoy344;
			float fboffsety344 = fblinearindextoy344 * fbrowsoffset344;
			float2 fboffset344 = float2(fboffsetx344, fboffsety344);
			half2 fbuv344 = temp_output_353_0 * fbtiling344 + fboffset344;
			o.Alpha = ( ( ( tex2D( _TextureSample0, fbuv349 ).r * ( 1.0 - temp_output_354_0 ) * temp_output_369_0 * _Digit.y ) + ( tex2D( _TextureSample0, fbuv359 ).r * temp_output_354_0 * _Digit.x ) + ( ( 1.0 - temp_output_369_0 ) * tex2D( _TextureSample0, fbuv344 ).r * _Digit.z ) ) * _Opacity * _Color0.a );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18703
587;200;1080;689;124.8142;207.3332;1;True;False
Node;AmplifyShaderEditor.RangedFloatNode;388;309.1858,154.6668;Inherit;False;Property;_TimeScale;TimeScale;7;0;Create;True;0;0;False;0;False;3;3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;387;538.1858,149.6668;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;315;559.605,59.2967;Float;False;Property;_Step;Step;3;0;Create;True;0;0;False;0;False;0;3.21;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;386;731.3246,57.69061;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;351;666.9337,350.6737;Float;False;Constant;_Float0;Float 0;6;0;Create;True;0;0;False;0;False;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;377;772.2606,757.8863;Float;False;Constant;_Float3;Float 3;8;0;Create;True;0;0;False;0;False;3;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;304;800.147,610.3874;Inherit;False;0;342;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;361;922.9958,743.4355;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;385;798.1167,215.2066;Float;False;Constant;_Float4;Float 4;7;0;Create;True;0;0;False;0;False;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;350;818.577,296.4192;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;384;1040.117,254.2066;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;360;1159.502,350.7927;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;352;1100.898,616.4272;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;2,1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;346;1144.636,89.46508;Float;False;Property;_Columns;Columns;4;0;Create;True;0;0;False;0;False;0;10;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;353;1309.253,441.8323;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FloorOpNode;379;1173.614,268.5643;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;368;1519.719,833.0062;Float;False;Constant;_Float2;Float 2;7;0;Create;True;0;0;False;0;False;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;374;1456.84,631.3145;Inherit;False;True;False;True;True;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;365;1516.408,718.1385;Float;False;Constant;_Float1;Float 1;7;0;Create;True;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FloorOpNode;380;1306.687,351.911;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;347;1154.182,182.9098;Float;False;Property;_Rows;Rows;5;0;Create;True;0;0;False;0;False;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;354;1702.137,618.9447;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCFlipBookUVAnimation;359;1484.596,408.514;Inherit;False;0;0;6;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TFHCFlipBookUVAnimation;344;1487.624,16.56606;Inherit;False;0;0;6;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.StepOpNode;369;1699.898,836.5267;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;10;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCFlipBookUVAnimation;349;1486.888,214.7205;Inherit;False;0;0;6;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.OneMinusNode;375;1976.963,725.6522;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;342;1771.969,-14.71696;Inherit;True;Property;_TextureSample0;Texture Sample 0;6;0;Create;True;0;0;False;0;False;-1;None;ef39ac7705cd106498929a6cecdfbd7c;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector3Node;376;1959.956,854.9277;Float;False;Property;_Digit; Digit;2;0;Create;True;0;0;False;0;False;1,1,1;1,1,1;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;364;1763.793,417.61;Inherit;True;Property;_TextureSample2;Texture Sample 2;6;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Instance;342;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;348;1770.349,203.2001;Inherit;True;Property;_TextureSample1;Texture Sample 1;6;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Instance;342;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;373;1971.375,647.6118;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;356;2356.528,308.7917;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;355;2356.151,424.8179;Inherit;False;4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;370;2356.035,559.6617;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;14;2343.866,26.29197;Float;False;Property;_Color0;Color 0;0;0;Create;True;0;0;False;0;False;1,1,1,1;1,1,1,1;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;135;2277.477,213.0021;Float;False;Property;_Opacity;Opacity;1;0;Create;True;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;358;2544.712,327.948;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;345;2677.372,242.6788;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;10;2854.475,8.105847;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;UI_FlipBooK;False;False;False;False;True;True;True;True;True;True;True;True;False;True;True;True;True;False;False;False;False;Off;2;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;0;4;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;1;False;-1;1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Absolute;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;387;0;388;0
WireConnection;386;0;315;0
WireConnection;386;1;387;0
WireConnection;361;0;377;0
WireConnection;350;0;386;0
WireConnection;350;1;351;0
WireConnection;384;0;350;0
WireConnection;384;1;385;0
WireConnection;360;0;350;0
WireConnection;360;1;351;0
WireConnection;352;0;304;0
WireConnection;352;1;361;0
WireConnection;353;0;352;0
WireConnection;379;0;384;0
WireConnection;374;0;352;0
WireConnection;380;0;360;0
WireConnection;354;0;374;0
WireConnection;354;1;365;0
WireConnection;359;0;353;0
WireConnection;359;1;346;0
WireConnection;359;2;347;0
WireConnection;359;4;380;0
WireConnection;344;0;353;0
WireConnection;344;1;346;0
WireConnection;344;2;347;0
WireConnection;344;4;386;0
WireConnection;369;0;374;0
WireConnection;369;1;368;0
WireConnection;349;0;353;0
WireConnection;349;1;346;0
WireConnection;349;2;347;0
WireConnection;349;4;379;0
WireConnection;375;0;369;0
WireConnection;342;1;344;0
WireConnection;364;1;359;0
WireConnection;348;1;349;0
WireConnection;373;0;354;0
WireConnection;356;0;364;1
WireConnection;356;1;354;0
WireConnection;356;2;376;1
WireConnection;355;0;348;1
WireConnection;355;1;373;0
WireConnection;355;2;369;0
WireConnection;355;3;376;2
WireConnection;370;0;375;0
WireConnection;370;1;342;1
WireConnection;370;2;376;3
WireConnection;358;0;355;0
WireConnection;358;1;356;0
WireConnection;358;2;370;0
WireConnection;345;0;358;0
WireConnection;345;1;135;0
WireConnection;345;2;14;4
WireConnection;10;2;14;0
WireConnection;10;9;345;0
ASEEND*/
//CHKSM=7CE7119B08FD28EA89DF6609BFF11080233481A2