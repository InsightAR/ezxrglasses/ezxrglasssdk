﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaseController : MonoBehaviour
{
    /// <summary>
    /// 四方瓶的animator
    /// </summary>
    public Animator animator_Vase;
    /// <summary>
    /// 步骤的ui
    /// </summary>
    public GameObject[] ui_Step;

    private void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine(PlayAnimation());
    }

    void ResetUI()
    {
        foreach (GameObject item in ui_Step)
        {
            item.SetActive(false);
        }
    }

    IEnumerator PlayAnimation()
    {
        ResetUI();
        ui_Step[0].SetActive(true);

        yield return new WaitForSeconds(8);
        animator_Vase.speed = 0;

        yield return new WaitForSeconds(2);
        ResetUI();
        ui_Step[1].SetActive(true);
        yield return new WaitForSeconds(2);

        animator_Vase.speed = 1;

        yield return new WaitForSeconds(6);
        animator_Vase.speed = 0;

        yield return new WaitForSeconds(2);
        ResetUI();
        ui_Step[2].SetActive(true);
        yield return new WaitForSeconds(2);

        animator_Vase.speed = 1;

        yield return new WaitForSeconds(5f);
        animator_Vase.speed = 0;

        yield return new WaitForSeconds(2);
        ResetUI();
        ui_Step[3].SetActive(true);
        yield return new WaitForSeconds(2);

        animator_Vase.speed = 1;
    }
}
