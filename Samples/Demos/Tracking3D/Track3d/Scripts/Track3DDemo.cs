﻿using EZXR.Glass.SixDof;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZXR.Glass.Runtime;
using System.Runtime.InteropServices;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;
using EZXR.Glass.Tracking3D;

[Serializable]
public class KVPair
{
    public GameObject objToShow;
    public string algModelPath;
}
[Serializable]
public enum ObjectType
{
    /// <summary>
    /// 大引擎
    /// </summary>
    TurbineBig,
    /// <summary>
    /// 小引擎
    /// </summary>
    TurbineSmall,
}
[Serializable]
public class ObjectInfo
{
    public ObjectType objectType;
    public KVPair infoPair;
}
public class Track3DDemo : MonoBehaviour
{


    /// <summary>
    /// 算法识别模型所在的路径以及对应要展示的3D资源
    /// </summary>
    public ObjectInfo[] objPathPair;
    /// <summary>
    /// 要进行识别的模型的类型
    /// </summary>
    public ObjectType targetTypeToShow;
    /// <summary>
    /// 场景中的3DPlacement
    /// </summary>
    public GameObject target3DObjectAnchor;


    private bool isAssetsCopied = false;

    public Tracking3DManager tracking3DManager;

    /// <summary>
    /// 当前正在识别的3D效果
    /// </summary>
    //public GameObject tips_Detecting;
    public Renderer tips_DetectTarget;
    public Texture2D[] detectTargetsImage;
    
    GameObject curObjToShow;
    public GameObject ui;
    public GameObject tips_Success;

    // Start is called before the first frame update
    private void Awake()
    {
        isAssetsCopied = false;
        StartCoroutine(copyTrackAsset());
    }

    private void Start()
    {
        ShowObj((int)targetTypeToShow);
    }

    private void OnEnable()
    {
    }
    private void OnDisable()
    {

        foreach (ObjectInfo itm in objPathPair)
        {
            itm.infoPair.objToShow.SetActive(false);
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("SamplesMenu");
        }
    }
    private void LateUpdate()
    {
        //UpdateCameraImage();
        RelocByTrack3d();

    }

    private void RelocByTrack3d()
    {
        bool isRelocSucc = false;
        //Debug.Log("-10001-RelocByTrack3d GetLocPose start");
        if (target3DObjectAnchor != null && tracking3DManager != null)
        {
            //Debug.Log("-10001-RelocByTrack3d GetLocPose start 1");
            if (tracking3DManager.IsLocationSuccess)
            {
                //Debug.Log("-10001-RelocByTrack3d GetLocPose success");
                curObjToShow.SetActive(true);
                if (ui != null)
                {
                    ui.SetActive(false);
                }
                //Debug.Log("-10001-LateUpdate GettLocalizedAnchorPose successed");
                target3DObjectAnchor.transform.position = tracking3DManager.AnchorPose.position;
                target3DObjectAnchor.transform.rotation = tracking3DManager.AnchorPose.rotation;
            }
            else
            {
                curObjToShow.SetActive(false);
                if (ui != null)
                {
                    ui.SetActive(true);
                }
                //Debug.Log("-10001-RelocByTrack3d GetLocPose Failed");
            }
        }
        if (isRelocSucc)
        {
            //tips_Detecting.SetActive(false);
            //tips_Scan.SetActive(false);
            //ui.SetActive(true);
        }
    }
    private IEnumerator copyTrackAsset()
    {
        yield return null;
        AndroidJavaClass ezxrAssetUtilCls = new AndroidJavaClass("com.ezxr.ezglassarsdk.utils.AssetUtils");
        AndroidJavaClass unityActivityCls = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityActivityCls.GetStatic<AndroidJavaObject>("currentActivity");

        foreach (ObjectInfo itm in objPathPair)
        {
            ezxrAssetUtilCls.CallStatic<bool>("copyAssetsToApplicationDir", unityActivity, itm.infoPair.algModelPath);
        }
        isAssetsCopied = true;
        Debug.Log("copyTrackAsset isAssetsCopied=" + isAssetsCopied);
    }

    public void ShowObj(int id)
    {
        Debug.Log("-10001-  ShowObj id=" + id + ",tips_DetectTarget=" + tips_DetectTarget + ",ui=" + ui);
        if (tips_DetectTarget != null)
        {
            tips_DetectTarget.material.SetTexture("_MainTex", detectTargetsImage[id]);
        }
        //tips_Scan.SetActive(true);
        if (ui != null) {
            ui.SetActive(false);
        }
        //tips_Success.SetActive(true);
        foreach (ObjectInfo itm in objPathPair)
        {
            itm.infoPair.objToShow.SetActive(false);
        }
        curObjToShow = objPathPair[id].infoPair.objToShow;
        
        targetTypeToShow = (ObjectType)id;
        StartCoroutine(startTracking());
    }

    private IEnumerator startTracking(){
        yield return new WaitUntil(() => isAssetsCopied);

        tracking3DManager.enableTracking(objPathPair[(int)targetTypeToShow].infoPair.algModelPath);
    }


}
