using System.Collections;
using System.Collections.Generic;
using EZXR.Glass.Runtime;
using EZXR.Glass.QRScanner;
using UnityEngine;
using UnityEngine.UI;

public class QRScannerDemo : BaseGlassSDKDemo
{
    public Text tipsText;
    public GameObject userView;

    private void Awake()
    {
    }
    private void OnEnable()
    {
        QRScannerManager.RegisterStartListener(OnStarted);
        QRScannerManager.RegisterCompleteListener(OnComplete);
    }
    private void OnDisable()
    {
        QRScannerManager.UnregisterStartListener(OnStarted);
        QRScannerManager.UnregisterCompleteListener(OnComplete);
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause) QRScannerManager.RequestClose();
    }

    public void OnScanning()
    {
        QRScannerManager.RequestOpen();
    }

    private void OnStarted()
    {
        Debug.Log("QRScannerDemo, Start Scanning");
        userView.SetActive(false);
    }

    private void OnComplete(bool status, string content)
    {
        userView.SetActive(true);
        Debug.Log("QRScannerDemo, Scanning Complete: " + content);

        if (status)
        {
            tipsText.text = content;
        }
        else
        {
            tipsText.text = "未识别到二维码";
        }
    }
}
