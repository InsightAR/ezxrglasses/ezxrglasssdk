﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using EZXR.Glass.SixDof;

public class BaseGlassSDKDemo : MonoBehaviour
{

    // Update is called once per frame
    protected void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (SceneManager.GetSceneByName("SamplesMenu") != null)
                SceneManager.LoadScene("SamplesMenu");
            else {
                Application.Quit();
            }
        }
    }

    public void OnExitButton()
    {
        if (SceneManager.GetSceneByName("SamplesMenu") != null)
            SceneManager.LoadScene("SamplesMenu");
        else
        {
            Application.Quit();
        }
    }
}
