﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using EZXR.Glass.SixDof;
using EZXR.Glass.Inputs;
using System.Runtime.InteropServices;
using System;
using EZXR.Glass.Runtime;

namespace EZXR.Glass.Simulator.Hand
{
    public class HandSimulator : MonoBehaviour
    {
        bool useLeftHand = true;
        public float moveSpeed = 0.1f;
        public static IntPtr ptr_HandTrackingData;
        public static IntPtr ptr_HandTrackingData0;
        int handTrackingDataSize;
        NativeSwapManager.HandTrackingData[] handTrackingData;
        Vector3 offset;
        NativeSwapManager nativeSwapManager;

        // Start is called before the first frame update
        void Start()
        {
            nativeSwapManager = GetComponent<NativeSwapManager>();

            //为了接收算法的结果，在非托管内存区域开辟长度为2个HandTrackingData的内存
            handTrackingDataSize = Marshal.SizeOf(typeof(NativeSwapManager.HandTrackingData));
            ptr_HandTrackingData = Marshal.AllocHGlobal(handTrackingDataSize * 2);
            ptr_HandTrackingData0 = new IntPtr(ptr_HandTrackingData.ToInt64() + handTrackingDataSize);

            //用于将Marshal开辟的非托管区域的所有值清零以避免内存区域原数据造成的影响
            NativeSwapManager.HandTrackingData temp = new NativeSwapManager.HandTrackingData();
            Marshal.StructureToPtr(temp, ptr_HandTrackingData, false);
            Marshal.StructureToPtr(temp, ptr_HandTrackingData0, false);

            ChangeHandGestureType(GestureType.OpenHand);
        }

        // Update is called once per frame
        void Update()
        {
            if (nativeSwapManager != null)
            {
                nativeSwapManager.enabled = false;
            }

#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
            {
                ChangeHandGestureType(GestureType.Pinch, true);
            }
            if (Input.GetMouseButtonUp(0))
            {
                ChangeHandGestureType(GestureType.OpenHand, true);
            }

            if (Input.GetKey(KeyCode.A))
            {
                SetJointPose(Vector3.left);
            }

            if (Input.GetKey(KeyCode.D))
            {
                SetJointPose(Vector3.right);
            }

            if (Input.GetKey(KeyCode.W))
            {
                SetJointPose(Vector3.up);
            }

            if (Input.GetKey(KeyCode.S))
            {
                SetJointPose(Vector3.down);
            }
#endif
        }

        void ChangeHandGestureType(GestureType gestureType, bool doNotChangePose = false)
        {
            byte[] leftHandData = Resources.Load<TextAsset>("RecordHandData/leftHand_" + gestureType.ToString()).bytes;
            byte[] rightHandData = Resources.Load<TextAsset>("RecordHandData/rightHand_" + gestureType.ToString()).bytes;
            Marshal.Copy(leftHandData, 0, ptr_HandTrackingData, leftHandData.Length);
            Marshal.Copy(rightHandData, 0, ptr_HandTrackingData0, rightHandData.Length);

            handTrackingData = new NativeSwapManager.HandTrackingData[2];
            //if (useLeftHand)
            //{
            handTrackingData[0] = (NativeSwapManager.HandTrackingData)Marshal.PtrToStructure(ptr_HandTrackingData, typeof(NativeSwapManager.HandTrackingData));
            handTrackingData[0].isTracked = useLeftHand;
            //}
            //else
            //{
            handTrackingData[1] = (NativeSwapManager.HandTrackingData)Marshal.PtrToStructure(ptr_HandTrackingData0, typeof(NativeSwapManager.HandTrackingData));
            handTrackingData[1].isTracked = !useLeftHand;
            //}

            ARHandManager.Instance.UpdateHandTrackingDataManually(handTrackingData);

            if (doNotChangePose)
            {
                for (int i = 0; i < 25; i++)
                {
                    HandJointType handJointType = (HandJointType)i;
                    Pose temp = (useLeftHand ? XRMan.Hands.Left : XRMan.Hands.Right).GetJointData(handJointType);
                    (useLeftHand ? XRMan.Hands.Left : XRMan.Hands.Right).jointsPose[handJointType] = new Pose(temp.position + offset, temp.rotation);
                }
            }
        }

        void SetJointPose(Vector3 dir)
        {
            offset += dir * Time.deltaTime * moveSpeed;
            for (int i = 0; i < 25; i++)
            {
                HandJointType handJointType = (HandJointType)i;
                Pose temp = (useLeftHand ? XRMan.Hands.Left : XRMan.Hands.Right).GetJointData(handJointType);
                (useLeftHand ? XRMan.Hands.Left : XRMan.Hands.Right).jointsPose[handJointType] = new Pose(temp.position + dir * Time.deltaTime * moveSpeed, temp.rotation);
            }
        }
    }
}