﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using EZXR.Glass.SixDof;
using EZXR.Glass.Inputs;
using Wheels.Unity;
using EZXR.Glass.Runtime;

namespace EZXR.Glass.UI
{
    public class UGUIHandler : MonoBehaviour
    {
        #region singleton
        private static UGUIHandler instance;
        public static UGUIHandler Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        Camera eventCamera;

        private void Awake()
        {
            instance = this;
        }

        // Start is called before the first frame update
        void Start()
        {
            GetComponent<EventSystem>().pixelDragThreshold = 80;

            eventCamera = Application.isEditor ? XRMan.Eyes.Center : XRMan.Eyes.Left;
        }

        // Update is called once per frame
        void Update()
        {
            //左手
            if (XRMan.Input.Left && XRMan.Input.Left.Exist)
            {
                if (XRMan.Input.Left.isCloseContactingUGUI)//近距离交互
                {//当前只有手有近距离交互，所以此处直接用ARHandManager
                    if (XRMan.Hands.Left.InLeftView(HandJointType.Index_4))
                    {
                        eventCamera = XRMan.Eyes.Left;
                    }
                    else if (XRMan.Hands.Left.InRightView(HandJointType.Index_4))
                    {
                        eventCamera = XRMan.Eyes.Right;
                    }
                    foreach (Canvas canvas in CanvasHandler.canvases)
                    {
                        canvas.worldCamera = eventCamera;
                    }

                    HandInputModule.touches[0].position = eventCamera.WorldToScreenPoint(XRMan.Hands.Left.GetJointData(HandJointType.Index_4).position);
                }
                else//远距离射线交互
                {
                    if (XRMan.Input.Left.rayPoint_End.position.InView(XRMan.Eyes.Left))
                    {
                        eventCamera = XRMan.Eyes.Left;
                    }
                    else if (XRMan.Input.Left.rayPoint_End.position.InView(XRMan.Eyes.Right))
                    {
                        eventCamera = XRMan.Eyes.Right;
                    }
                    foreach (Canvas canvas in CanvasHandler.canvases)
                    {
                        canvas.worldCamera = eventCamera;
                    }

                    HandInputModule.touches[0].position = eventCamera.WorldToScreenPoint(XRMan.Input.Left.rayPoint_End.position);
                }
            }
            else
            {
                HandInputModule.touches[0].position = Vector2.zero;
            }

            //右手
            if (XRMan.Input.Right && XRMan.Input.Right.Exist)
            {
                if (XRMan.Input.Right.isCloseContactingUGUI)//近距离交互
                {//当前只有手有近距离交互，所以此处直接用ARHandManager
                    if (XRMan.Hands.Right.InLeftView(HandJointType.Index_4))
                    {
                        eventCamera = XRMan.Eyes.Left;
                    }
                    else if (XRMan.Hands.Right.InRightView(HandJointType.Index_4))
                    {
                        eventCamera = XRMan.Eyes.Right;
                    }
                    foreach (Canvas canvas in CanvasHandler.canvases)
                    {
                        canvas.worldCamera = eventCamera;
                    }

                    HandInputModule.touches[1].position = eventCamera.WorldToScreenPoint(XRMan.Hands.Right.GetJointData(HandJointType.Index_4).position);
                }
                else//远距离射线交互
                {
                    if (XRMan.Input.Right.rayPoint_End.position.InView(XRMan.Eyes.Left))
                    {
                        eventCamera = XRMan.Eyes.Left;
                    }
                    else if (XRMan.Input.Right.rayPoint_End.position.InView(XRMan.Eyes.Right))
                    {
                        eventCamera = XRMan.Eyes.Right;
                    }
                    foreach (Canvas canvas in CanvasHandler.canvases)
                    {
                        canvas.worldCamera = eventCamera;
                    }

                    HandInputModule.touches[1].position = eventCamera.WorldToScreenPoint(XRMan.Input.Right.rayPoint_End.position);
                }
            }
            else
            {
                HandInputModule.touches[1].position = Vector2.zero;
            }

            //if (Input.GetKeyDown(KeyCode.Q))
            //    Debug.Log($"HandControllerSession, ctrlTouch: ({HandInputModule.touches[1].position.x}, {HandInputModule.touches[1].position.y})");

        }

        //public void ConfigAllCanvas()
        //{
        //    eventCamera = Application.isEditor ? XRMan.Eyes.Center : XRMan.Eyes.Left;

        //    canvas = FindObjectsOfType<Canvas>();
        //    foreach (Canvas item in canvas)
        //    {
        //        if (item.transform.parent != null && item.transform.parent.GetComponent<PhoneUIRenderer>() != null)
        //        {
        //            continue;
        //        }
        //        if (item.gameObject.GetComponent<BoxCollider>() == null)
        //        {
        //            item.gameObject.AddComponent<BoxCollider>().size = item.GetComponent<RectTransform>().sizeDelta;
        //        }
        //        item.worldCamera = eventCamera;
        //    }
        //}
    }
}