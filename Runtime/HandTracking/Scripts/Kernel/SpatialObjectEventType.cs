﻿namespace EZXR.Glass.Inputs
{
    public enum SpatialObjectEventType
    {
        /// <summary>
        /// 当手近距离触碰到或者远距离射线碰到物体时触发
        /// </summary>
        OnEnter,
        /// <summary>
        /// 当手近距离或者远距离射线持续触碰物体时会持续触发
        /// </summary>
        OnStay,
        /// <summary>
        /// 当手结束近距离或者远距离射线触碰时触发
        /// </summary>
        OnExit,
        /// <summary>
        /// 当手近距离或者远距离射线抓取物体时触发
        /// </summary>
        OnGrab,
        /// <summary>
        /// 当手近距离或者远距离射线松开物体时触发
        /// </summary>
        OnRelease,
        /// <summary>
        /// 当手近距离触碰到物体时触发
        /// </summary>
        OnHandEnter,
        /// <summary>
        /// 当手近距离持续触碰物体时会持续触发
        /// </summary>
        OnHandStay,
        /// <summary>
        /// 当手结束近距离触碰时触发
        /// </summary>
        OnHandExit,
        /// <summary>
        /// 当手近距离抓取物体时触发
        /// </summary>
        OnHandGrab,
        /// <summary>
        /// 当手近距离松开物体时触发
        /// </summary>
        OnHandRelease,
        /// <summary>
        /// 当手远距离射线碰到物体时触发
        /// </summary>
        OnRayEnter,
        /// <summary>
        /// 当手远距离射线持续触碰物体时会持续触发
        /// </summary>
        OnRayStay,
        /// <summary>
        /// 当手结束远距离射线触碰时触发
        /// </summary>
        OnRayExit,
        /// <summary>
        /// 当手远距离射线抓取物体时触发
        /// </summary>
        OnRayGrab,
        /// <summary>
        /// 当手远距离射线松开物体时触发
        /// </summary>
        OnRayRelease,
    }
}