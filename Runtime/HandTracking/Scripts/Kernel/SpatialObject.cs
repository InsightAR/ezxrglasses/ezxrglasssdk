﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace EZXR.Glass.Inputs
{
    public class SpatialObject : MonoBehaviour
    {
        public bool isStaticObject;
        public bool freezePosition_X, freezePosition_Y, freezePosition_Z;
        public bool freezeRotation;
        public bool freezeScale_X, freezeScale_Y, freezeScale_Z;

        /// <summary>
        /// 允许的交互类型
        /// </summary>
        public InteractionType allowInteractionType;

        private static Dictionary<Collider, SpatialObject> All = new Dictionary<Collider, SpatialObject>();

        [Serializable]
        public class SpatialObjectEvent : UnityEvent
        { }

        [Serializable]
        public class Entry
        {
            public SpatialObjectEventType eventID = SpatialObjectEventType.OnEnter;
            public SpatialObjectEvent callback = new SpatialObjectEvent();
        }

        [FormerlySerializedAs("delegates")]
        [SerializeField]
        private List<Entry> m_Delegates;
        public List<Entry> triggers
        {
            get
            {
                if (m_Delegates == null)
                    m_Delegates = new List<Entry>();
                return m_Delegates;
            }
            set { m_Delegates = value; }
        }

        private void Execute(SpatialObjectEventType id)
        {
            var triggerCount = triggers.Count;

            for (int i = 0, imax = triggers.Count; i < imax; ++i)
            {
                var ent = triggers[i];
                if (ent.eventID == id && ent.callback != null)
                    ent.callback.Invoke();
            }
        }

        ////TODO: 改为EventTrigger "Add Event"形式
        //[SerializeField]
        //private UnityEvent OnHandEnter;
        //[SerializeField]
        //private UnityEvent OnHandStay;
        //[SerializeField]
        //private UnityEvent OnHandExit;
        //[SerializeField]
        //private UnityEvent OnHandGrab;
        //[SerializeField]
        //private UnityEvent OnHandRelease;
        //[SerializeField]
        //private UnityEvent OnHandTriggerEnter;
        //[SerializeField]
        //private UnityEvent OnHandTriggerStay;
        //[SerializeField]
        //private UnityEvent OnHandTriggerExit;
        //[SerializeField]
        //private UnityEvent OnHandTriggerGrab;
        //[SerializeField]
        //private UnityEvent OnHandTriggerRelease;
        //[SerializeField]
        //private UnityEvent OnHandRayEnter;
        //[SerializeField]
        //private UnityEvent OnHandRayStay;
        //[SerializeField]
        //private UnityEvent OnHandRayExit;
        //[SerializeField]
        //private UnityEvent OnHandRayGrab;
        //[SerializeField]
        //private UnityEvent OnHandRayRelease;

        private void Awake()
        {
            gameObject.tag = "SpatialObject";
            All.Add(GetComponent<Collider>(), this);
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void LateUpdate()
        {

        }

        Vector3 newPos;
        public void SetPosition(Vector3 pos, InteractionType interactionType)
        {
            if (!isStaticObject && (allowInteractionType == InteractionType.All || interactionType == allowInteractionType))
            {
                newPos.Set(freezePosition_X ? transform.position.x : pos.x, freezePosition_Y ? transform.position.y : pos.y, freezePosition_Z ? transform.position.z : pos.z);
                transform.position = newPos;
            }
        }

        Quaternion newQ;
        public void SetRotation(Quaternion rot, InteractionType interactionType)
        {
            if (!isStaticObject && !freezeRotation && (allowInteractionType == InteractionType.All || interactionType == allowInteractionType))
            {
                newQ = rot;
                transform.rotation = newQ;
            }
        }

        Vector3 newScale;
        public void SetScale(Vector3 scale, InteractionType interactionType)
        {
            if (!isStaticObject && (allowInteractionType == InteractionType.All || interactionType == allowInteractionType))
            {
                newScale.Set(freezeScale_X ? transform.localScale.x : scale.x, freezeScale_Y ? transform.localScale.y : scale.y, freezeScale_Z ? transform.localScale.z : scale.z);
                transform.localScale = newScale;
            }
        }

        public void SetTransform(Vector3 pos, Quaternion rot, Vector3 scale, InteractionType interactionType)
        {
            if (!isStaticObject && (allowInteractionType == InteractionType.All || interactionType == allowInteractionType))
            {
                transform.position = pos;
                transform.rotation = rot;
                transform.localScale = scale;
            }
        }

        #region HandTriggerEvent
        public static void PerformOnHandTriggerEnter(Collider other)
        {
            Debug.Log("SpatialObject--> PerformOnHandTriggerEnter: " + other.name);
            if (All.ContainsKey(other))
            {
                //All[other].OnHandTriggerEnter.Invoke();
                //All[other].OnHandEnter.Invoke();

                All[other].Execute(SpatialObjectEventType.OnHandEnter);
                All[other].Execute(SpatialObjectEventType.OnEnter);
            }
        }

        public static void PerformOnHandTriggerStay(Collider other)
        {
            //Debug.Log("SpatialObject--> PerformOnHandTriggerStay: " + other.name);
            if (All.ContainsKey(other))
            {
                //All[other].OnHandTriggerStay.Invoke();
                //All[other].OnHandStay.Invoke();

                All[other].Execute(SpatialObjectEventType.OnHandStay);
                All[other].Execute(SpatialObjectEventType.OnStay);
            }
        }

        public static void PerformOnHandTriggerExit(Collider other)
        {
            Debug.Log("SpatialObject--> PerformOnHandTriggerExit: " + other.name);
            if (All.ContainsKey(other))
            {
                //All[other].OnHandTriggerExit.Invoke();
                //All[other].OnHandExit.Invoke();

                All[other].Execute(SpatialObjectEventType.OnHandExit);
                All[other].Execute(SpatialObjectEventType.OnExit);
            }
        }

        public static void PerformOnHandTriggerGrab(Collider other)
        {
            Debug.Log("SpatialObject--> PerformOnHandTriggerGrab: " + other.name);
            if (All.ContainsKey(other))
            {
                //All[other].OnHandTriggerGrab.Invoke();
                //All[other].OnHandGrab.Invoke();

                All[other].Execute(SpatialObjectEventType.OnHandGrab);
                All[other].Execute(SpatialObjectEventType.OnGrab);
            }
        }

        public static void PerformOnHandTriggerRelease(Collider other)
        {
            Debug.Log("SpatialObject--> PerformOnHandTriggerRelease: " + other.name);
            if (All.ContainsKey(other))
            {
                //All[other].OnHandTriggerRelease.Invoke();
                //All[other].OnHandRelease.Invoke();

                All[other].Execute(SpatialObjectEventType.OnHandRelease);
                All[other].Execute(SpatialObjectEventType.OnRelease);
            }
        }
        #endregion

        #region HandRayEvent
        public static void PerformOnHandRayEnter(Collider other)
        {
            Debug.Log("SpatialObject--> PerformOnHandRayEnter: " + other.name);
            if (All.ContainsKey(other))
            {
                //All[other].OnHandRayEnter.Invoke();
                //All[other].OnHandEnter.Invoke();

                All[other].Execute(SpatialObjectEventType.OnRayEnter);
                All[other].Execute(SpatialObjectEventType.OnEnter);
            }
        }

        public static void PerformOnHandRayStay(Collider other)
        {
            //Debug.Log("SpatialObject--> PerformOnHandRayStay: " + other.name);
            if (All.ContainsKey(other))
            {
                //All[other].OnHandRayStay.Invoke();
                //All[other].OnHandStay.Invoke();

                All[other].Execute(SpatialObjectEventType.OnRayStay);
                All[other].Execute(SpatialObjectEventType.OnStay);
            }
        }

        public static void PerformOnHandRayExit(Collider other)
        {
            Debug.Log("SpatialObject--> PerformOnHandRayExit: " + other.name);
            if (All.ContainsKey(other))
            {
                //All[other].OnHandRayExit.Invoke();
                //All[other].OnHandExit.Invoke();

                All[other].Execute(SpatialObjectEventType.OnRayExit);
                All[other].Execute(SpatialObjectEventType.OnExit);
            }
        }

        public static void PerformOnHandRayGrab(Collider other)
        {
            Debug.Log("SpatialObject--> PerformOnHandRayGrab: " + other.name);
            if (All.ContainsKey(other))
            {
                //All[other].OnHandRayGrab.Invoke();
                //All[other].OnHandGrab.Invoke();

                All[other].Execute(SpatialObjectEventType.OnRayGrab);
                All[other].Execute(SpatialObjectEventType.OnGrab);
            }
        }

        public static void PerformOnHandRayRelease(Collider other)
        {
            Debug.Log("SpatialObject--> PerformOnHandRayRelease: " + other.name);
            if (All.ContainsKey(other))
            {
                //All[other].OnHandRayRelease.Invoke();
                //All[other].OnHandRelease.Invoke();

                All[other].Execute(SpatialObjectEventType.OnRayRelease);
                All[other].Execute(SpatialObjectEventType.OnRelease);
            }
        }
        #endregion
    }
}