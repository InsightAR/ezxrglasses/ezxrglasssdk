﻿using EZXR.Glass.Inputs;
using EZXR.Glass.SixDof;
using EZXR.Glass.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.Inputs
{
    public class CanvasHandler : MonoBehaviour
    {
        /// <summary>
        /// 所有可交互的canvas
        /// </summary>
        public static List<Canvas> canvases = new List<Canvas>();
        public bool canvasCollider = true;
        /// <summary>
        /// 允许近距离交互
        /// </summary>
        public bool proximityInteraction = true;
        /// <summary>
        /// 两次点击的最小时间间隔
        /// </summary>
        float clickInterval = 0.2f;
        /// <summary>
        /// 上次点击的时刻
        /// </summary>
        float timer_LastClick;

        private Camera eventCamera;

        private bool startPinchChanged = false;
        private bool endPinchChanged = false;

        private void Awake()
        {
            eventCamera = Application.isEditor ? XRMan.Eyes.Center : XRMan.Eyes.Left;

            Canvas canvas = GetComponent<Canvas>();
            canvas.renderMode = UnityEngine.RenderMode.WorldSpace;
            canvas.worldCamera = eventCamera;

            canvases.Add(canvas);
        }
        private void OnDestroy()
        {
            if (canvases != null)
                canvases.Clear();
        }
        private void OnEnable()
        {
            if (canvasCollider)
            {
                var depth = 0.02f / transform.localScale.z;
                var sizeDelta = gameObject.GetComponent<RectTransform>().sizeDelta;

                if (gameObject.GetComponent<BoxCollider>() == null)
                {
                    gameObject.AddComponent<BoxCollider>().size = new Vector3(sizeDelta.x, sizeDelta.y, depth);
                    gameObject.GetComponent<BoxCollider>().center = new Vector3(0, 0, 0.005f);
                    //canvas.gameObject.GetComponent<BoxCollider>().isTrigger = true;
                }
                else
                {
                    gameObject.GetComponent<BoxCollider>().size = new Vector3(sizeDelta.x, sizeDelta.y, depth);
                    gameObject.GetComponent<BoxCollider>().center = new Vector3(0, 0, 0.005f);
                    //canvas.gameObject.GetComponent<BoxCollider>().isTrigger = true;
                }
            }
        }

        private void Update()
        {
            if (proximityInteraction)
            {
                if (timer_LastClick < clickInterval)
                {
                    timer_LastClick += Time.deltaTime;
                }
                else
                {
                    UpdatePinching();
                }
            }
        }

        /// <summary>
        /// 手势(食指尖)触摸Enter
        /// </summary>
        /// <param name="other">(食)指尖Collider</param>
        private void OnTriggerEnter(Collider other)
        {
            Debug.Log($"CanvasTouchHandler, OnTriggerEnter {other.name} ({Time.frameCount})");

            if (timer_LastClick >= clickInterval)
            {
                if (other.name.Contains("Left_Index_4"))
                {
                    float angle = Vector3.Dot(Vector3.Project(other.transform.position - transform.position, -transform.forward).normalized, -transform.forward);
                    Debug.Log("the angle: " + angle);
                    if (1 - angle < 0.1f)
                    {
                        startPinchChanged = true;
                        XRMan.Input.Left.isCloseContactingUGUI = true;
                        //InputSystem.leftHand.RaycastInteraction = false;
                    }
                }
                else if (other.name.Contains("Right_Index_4"))
                {
                    float angle = Vector3.Dot(Vector3.Project(other.transform.position - transform.position, -transform.forward).normalized, -transform.forward);
                    Debug.Log("the angle: " + angle);
                    if (1 - angle < 0.1f)
                    {
                        startPinchChanged = true;
                        XRMan.Input.Right.isCloseContactingUGUI = true;
                        //InputSystem.rightHand.RaycastInteraction = false;
                    }
                }
            }
        }

        /// <summary>
        /// 手势(食指尖)触摸Exit
        /// </summary>
        /// <param name="other">(食)指尖Collider</param>
        private void OnTriggerExit(Collider other)
        {
            Debug.Log($"CanvasTouchHandler, OnTriggerExit {other.name} ({Time.frameCount})");

            if (timer_LastClick >= clickInterval)
            {
                if (other.name.Contains("Left_Index_4"))
                {
                    endPinchChanged = true;
                    //ControllerManager.leftHand.isCloseContactingUGUI = false;
                }
                else if (other.name.Contains("Right_Index_4"))
                {
                    endPinchChanged = true;
                    //ControllerManager.rightHand.isCloseContactingUGUI = false;
                }
            }
        }

        private void UpdatePinching()
        {
            if (XRMan.Input.Left.isCloseContactingUGUI)
            {
                if (XRMan.Input.Left.startPinch)
                    XRMan.Input.Left.startPinch = false;
                if (XRMan.Input.Left.endPinch)
                {
                    XRMan.Input.Left.endPinch = false;
                    XRMan.Input.Left.isCloseContactingUGUI = false;
                }
            }

            if (XRMan.Input.Right.isCloseContactingUGUI)
            {
                if (XRMan.Input.Right.startPinch)
                    XRMan.Input.Right.startPinch = false;
                if (XRMan.Input.Right.endPinch)
                {
                    XRMan.Input.Right.endPinch = false;
                    XRMan.Input.Right.isCloseContactingUGUI = false;
                }
            }

            if (startPinchChanged)
            {
                startPinchChanged = false;

                if (XRMan.Input.Left.isCloseContactingUGUI)
                    XRMan.Input.Left.startPinch = true;
                if (XRMan.Input.Right.isCloseContactingUGUI)
                    XRMan.Input.Right.startPinch = true;
            }

            if (endPinchChanged)
            {
                endPinchChanged = false;

                if (XRMan.Input.Left.isCloseContactingUGUI)
                {
                    XRMan.Input.Left.endPinch = true;
                    XRMan.Input.Left.isCloseContactingUGUI = false;
                    //InputSystem.leftHand.enableRayInteraction = true; //未起作用
                }
                if (XRMan.Input.Right.isCloseContactingUGUI)
                {
                    XRMan.Input.Right.endPinch = true;
                    XRMan.Input.Right.isCloseContactingUGUI = false;
                    //InputSystem.rightHand.enableRayInteraction = true; //未起作用
                }

                timer_LastClick = 0;
                Debug.Log("endPinchChanged reset");
            }
        }
    }
}
