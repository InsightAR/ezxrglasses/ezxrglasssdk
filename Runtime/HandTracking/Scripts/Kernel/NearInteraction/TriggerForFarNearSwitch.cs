﻿using EZXR.Glass.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.Inputs
{
    [ScriptExecutionOrder(-47)]
    public class TriggerForFarNearSwitch : MonoBehaviour
    {
        //检测Trigger的长度
        public static float length = 0.24f;
        HandInfo handInfo;

        bool exit = false;

        private void Awake()
        {
            //transform.localScale = Vector3.one * radius * 2;
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (handInfo.PreCloseContactingTarget != null && !handInfo.PreCloseContactingTarget.gameObject.activeInHierarchy)
            {
                OnTriggerExit(handInfo.PreCloseContactingTarget.GetComponent<Collider>());
            }

            if (handInfo != null && handInfo.Exist)
            {
                Pose pose = handInfo.GetJointData(HandJointType.Palm);
                transform.position = pose.position;
                transform.rotation = pose.rotation;

                if (!handInfo.isPinching && exit)
                {
                    exit = false;
                    handInfo.RI_Internal = true;
                    handInfo.PreCloseContactingTarget = null;
                }
            }
            else
            {
                exit = false;
                handInfo.RI_Internal = true;
                handInfo.PreCloseContactingTarget = null;
            }
        }

        private void OnDisable()
        {
            exit = false;
            handInfo.RI_Internal = true;
            handInfo.PreCloseContactingTarget = null;
            handInfo.Last_PreCloseContactingTarget = null;
        }

        public void SetUp(Transform handRoot)
        {
            handInfo = handRoot.GetComponent<HandInfo>();
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject.layer != gameObject.layer)
            {
                if (handInfo != null)
                {
                    if (other.tag == "SpatialObject" || other.tag == "SpatialUI" || other.tag == "SpatialHandler")
                    {
                        //“当用户的手是up状态”且“当前手没有进入其他Trigger”时
                        if (!handInfo.isPinching)
                        {
                            exit = false;
                            if (!handInfo.IsCloseContacting)
                            {
                                handInfo.RI_Internal = false;
                                handInfo.preCloseContacting = true;
                                if (handInfo.PreCloseContactingTarget == null || Vector3.Distance(transform.position, other.transform.position) < Vector3.Distance(transform.position, handInfo.PreCloseContactingTarget.position))
                                {
                                    //handInfo.Last_PreCloseContactingTarget = handInfo.PreCloseContactingTarget;
                                    handInfo.PreCloseContactingTarget = other.transform;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.layer != gameObject.layer)
            {
                if (handInfo != null)
                {
                    if (other.tag == "SpatialObject" || other.tag == "SpatialUI" || other.tag == "SpatialHandler")
                    {
                        exit = true;
                        //“当用户的手是up状态”且“当前手没有进入其他Trigger”时
                        if (!handInfo.isPinching)
                        {
                            if (!handInfo.IsCloseContacting)
                            {
                                if (handInfo.PreCloseContactingTarget == other.transform)
                                {
                                    handInfo.RI_Internal = true;
                                    //handInfo.Last_PreCloseContactingTarget = handInfo.PreCloseContactingTarget;
                                    handInfo.PreCloseContactingTarget = null;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}