﻿using EZXR.Glass.Runtime;
using EZXR.Glass.SixDof;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.UIElements;
using Wheels.Unity;

namespace EZXR.Glass.Inputs
{
    public struct HandInfoUpdateJob : IJobParallelFor
    {
        [ReadOnly]
        public NativeArray<NativeSwapManager.Mat4f> Matrices;
        public NativeArray<Pose> Poses;

        public void Execute(int index)
        {
            NativeSwapManager.Mat4f mat4 = Matrices[index];
            Matrix4x4 m = new Matrix4x4(
                new Vector4(mat4.col0.x, mat4.col0.y, mat4.col0.z, mat4.col0.w),
                new Vector4(mat4.col1.x, mat4.col1.y, mat4.col1.z, mat4.col1.w),
                new Vector4(mat4.col2.x, mat4.col2.y, mat4.col2.z, mat4.col2.w),
                new Vector4(mat4.col3.x, mat4.col3.y, mat4.col3.z, mat4.col3.w)
            );

            if (m.ValidTRS())
            {
                Pose pose = new Pose();
                pose.rotation = m.rotation;
                pose.position = m.GetColumn(3);
                Poses[index] = pose;
            }
        }
    }

    public enum GestureType
    {
        Unknown = -1,
        OpenHand = 0,
        Grab,
        Pinch,
        Point,
        Victory,
        Call,
    }

    public enum HandJointType
    {
        Thumb_0,
        Thumb_1,
        Thumb_2,
        /// <summary>
        /// 拇指尖
        /// </summary>
        Thumb_3,
        /// <summary>
        /// 食指根节点
        /// </summary>
        Index_1,
        Index_2,
        Index_3,
        /// <summary>
        /// 食指尖
        /// </summary>
        Index_4,
        /// <summary>
        /// 中指根节点
        /// </summary>
        Middle_1,
        Middle_2,
        Middle_3,
        /// <summary>
        /// 中指指尖
        /// </summary>
        Middle_4,
        Ring_1,
        Ring_2,
        Ring_3,
        /// <summary>
        /// 无名指指尖
        /// </summary>
        Ring_4,
        Pinky_0,
        /// <summary>
        /// 小指根节点
        /// </summary>
        Pinky_1,
        Pinky_2,
        Pinky_3,
        /// <summary>
        /// 小指指尖
        /// </summary>
        Pinky_4,
        /// <summary>
        /// 掌心点
        /// </summary>
        Palm,
        /// <summary>
        /// 手腕横切线，靠近拇指根节点的点
        /// </summary>
        Wrist_Thumb,
        /// <summary>
        /// 手腕横切线，靠近小指根节点的点
        /// </summary>
        Wrist_Pinky,
        /// <summary>
        /// 手腕横切线，中点
        /// </summary>
        Wrist_Middle,
    }


    /// <summary>
    /// 包含单个手的各种状态信息，是个基础信息类，供其他类进行状态获取和协同
    /// </summary>
    [ScriptExecutionOrder(-48)]
    public class HandInfo : InputInfoBase
    {
        /// <summary>
        /// 手的所有关节等物体的父节点
        /// </summary>
        public Transform root
        {
            get
            {
                return transform;
            }
        }
        HandVisualization handVisualization;
        HandsVisualizationForModel handsVisualizationForModel;
        InputRaycast handRaycast;
        HandTouch handTouch;
        HandCollision handCollision;
        Func<InputInfoBase, bool> _EnableRaycastByExternal;
        public Func<InputInfoBase, bool> EnableRaycastByExternal
        {
            get
            {
                return _EnableRaycastByExternal;
            }
            set
            {
                _EnableRaycastByExternal = value;
                RefreshRaycastExternal();
            }
        }


        /// <summary>
        /// 初始化，设置当前输入设备的类型以及所属的手是左手还是右手
        /// </summary>
        /// <param name="inputType">输入设备类型：头控、手势、手柄</param>
        /// <param name="handType">0为左手，1为右手</param>
        public override void Init(InputType inputType, HandType handType)
        {
            base.Init(inputType, handType);

            handVisualization = GetComponent<HandVisualization>();
            handsVisualizationForModel = GetComponent<HandsVisualizationForModel>();
            handRaycast = GetComponent<InputRaycast>();
            handTouch = GetComponent<HandTouch>();
            handCollision = GetComponent<HandCollision>();

#if EZXRForMRTK
            handVisualization.enabled = false;
            handsVisualizationForModel.enabled = false;
            handRaycast.enabled = false;
            handTouch.enabled = false;
            handCollision.enabled = false;
#endif

            //设定肩部点，用于射线计算
            shoulderPoint = new Vector3(handType == HandType.Left ? -0.15f : 0.15f, -0.15f, -0.1f);

            palm = new GameObject("Palm").transform;
            palm.parent = transform;
        }

        private NativeArray<NativeSwapManager.Mat4f> matrices;
        private NativeArray<Pose> poses;
        int length = 0;
        public void UpdateHandInfoData(NativeSwapManager.HandTrackingData handTrackingData)
        {
            this.handTrackingData = handTrackingData;

            if (matrices == null || matrices.Length == 0)
            {
                // 假设 handTrackingData 和 handTrackingData.handJointData 已正确初始化
                length = handTrackingData.handJointData.Length;
                matrices = new NativeArray<NativeSwapManager.Mat4f>(length, Allocator.TempJob);
                poses = new NativeArray<Pose>(length, Allocator.TempJob);
            }

            for (int i = 0; i < length; i++)
            {
                matrices[i] = handTrackingData.handJointData[i].handJointPose;
            }

            var job = new HandInfoUpdateJob()
            {
                Matrices = matrices,
                Poses = poses
            };

            JobHandle handle = job.Schedule(length, 8);
            handle.Complete();

            // 更新关节位姿字典
            for (int i = 0; i < length; i++)
            {
                HandJointType handJointType = (HandJointType)i;
                if (poses[i].position != Vector3.zero) // 用位置信息判断是否有效
                {
                    jointsPose[handJointType] = poses[i];
                }
            }

            //if (handTrackingData.handJointData != null)
            //{
            //    Pose pose;
            //    for (int i = 0; i < handTrackingData.handJointData.Length; i++)
            //    {
            //        NativeSwapManager.Mat4f mat4 = handTrackingData.handJointData[i].handJointPose;
            //        Matrix4x4 m = new Matrix4x4();
            //        m.SetColumn(0, new Vector4(mat4.col0.x, mat4.col0.y, mat4.col0.z, mat4.col0.w));
            //        m.SetColumn(1, new Vector4(mat4.col1.x, mat4.col1.y, mat4.col1.z, mat4.col1.w));
            //        m.SetColumn(2, new Vector4(mat4.col2.x, mat4.col2.y, mat4.col2.z, mat4.col2.w));
            //        m.SetColumn(3, new Vector4(mat4.col3.x, mat4.col3.y, mat4.col3.z, mat4.col3.w));
            //        if (!m.ValidTRS())
            //        {
            //            return;
            //        }
            //        pose.rotation = m.rotation;// XRMan.Instance == null ? m.rotation : (XRMan.Instance.transform.rotation * m.rotation);
            //        Vector3 pos = m.GetColumn(3);
            //        pose.position = pos;// XRMan.Instance == null ? pos : (XRMan.Instance.transform.TransformPoint(pos));
            //        HandJointType handJointType = (HandJointType)i;
            //        if (jointsPose.ContainsKey(handJointType))
            //        {
            //            jointsPose[handJointType] = pose;
            //        }
            //        else
            //        {
            //            jointsPose.Add(handJointType, pose);
            //        }
            //    }
            //}
        }

        protected override void Awake()
        {
            base.Awake();

            Debug.Log("handray=> Awake");
            _EnableRaycastByExternal = Internal_EnableRaycastByExternal;
        }

        bool Internal_EnableRaycastByExternal(InputInfoBase inputInfo)
        {
            //Debug.Log("EnableRaycastByExternal: " + !IsPalmFacingHead());
            return !IsPalmFacingHead();
        }

        protected void OnEnable()
        {
            //Debug.Log("handray=> OnEnable");
            //在OnEnable中执行，是因为需要在切换到对应的控制器的时候将射线逻辑替换为当前控制器的射线逻辑
            RefreshRaycastExternal();
        }

        /// <summary>
        /// 更新了_EnableRaycastByExternal之后需要Refresh以生效
        /// </summary>
        void RefreshRaycastExternal()
        {
            InputRaycast.EnableRaycastByExternal = _EnableRaycastByExternal;
        }

        protected override void Update()
        {
            if (Exist)
            {
                base.Update();

                palm.position = GetJointData(HandJointType.Palm).position;
                palm.rotation = GetJointData(HandJointType.Palm).rotation;

                //掌心面向的方向
                palmNormal = GetJointData(HandJointType.Palm).TransformDirection(Vector3.forward);

                //手掌指尖朝向
                palmDirection = GetJointData(HandJointType.Palm).TransformDirection(Vector3.up);

                //射线方向
                if (!(ARHandManager.SetRayDirByExternal != null && ARHandManager.SetRayDirByExternal(this, ref rayDirection)))
                {
                    rayDirection = (GetJointData(HandJointType.Index_1).position - ARHandManager.head.TransformPoint(shoulderPoint)).normalized;
                }

                //射线起点
                if (!(ARHandManager.SetRayStartPointByExternal != null && ARHandManager.SetRayStartPointByExternal(this, ref rayPoint_Start)))
                {
                    rayPoint_Start = GetJointData(HandJointType.Index_1).position + rayDirection * rayStartDistance;
                }

                ////用于双手操作，得到左手加滤波之后的掌心点位置
                //NativeSwapManager.Point3 temp = new NativeSwapManager.Point3(rayPoint_Start);
                //NativeSwapManager.filterPoint(ref temp, transform.GetInstanceID());
                //Vector3 tarPos = new Vector3(temp.x, temp.y, temp.z);
                //rayPoint_Start_Left = tarPos;

                //从算法得到的预估的射线终点位置
                //rayPoint_End = new Vector3(ray[1].x, Main.ray[1].y, ray[1].z);
            }
        }

        protected override void UpdatePinching()
        {
            bool curPinchState = handTrackingData.isTracked && (handTrackingData.gestureType == GestureType.Pinch || handTrackingData.gestureType == GestureType.Grab);
            if (isPinching)
            {
                if (curPinchState)
                {
                    startPinch = false;
                }
                else
                {
                    endPinch = true;
                }
            }
            else
            {
                if (curPinchState)
                {
                    startPinch = true;
                }
                else
                {
                    endPinch = false;
                }
            }
            isPinching = curPinchState;
        }

        #region 交互能力开关
        /// <summary>
        /// 全局手开关，与Enabled互斥，由外部递交的逻辑函数控制，如果置为false的话，Visibility和RaycastInteraction将会失效，且被置为false
        /// </summary>
        public static event Func<bool> EnableByExternal;
        private bool m_Enabled = true;
        /// <summary>
        /// 全局手开关，与EnableByExternal互斥，由外部直接修改，如果置为false的话，Visibility和RaycastInteraction将会失效，且被置为false
        /// </summary>
        public bool Enabled
        {
            get
            {
                return m_Enabled;
            }
            set
            {
                m_Enabled = value;
                Visibility &= value;
                RaycastInteraction &= value;
                TouchInteraction &= value;
                PhysicsInteraction &= value;
            }
        }

        /// <summary>
        /// 设置或获得当前手的显示状态（仅仅影响显示，并不影响交互），如果Enabled为false则Visibility也将为false
        /// </summary>
        public override bool Visibility
        {
            get
            {
                return handsVisualizationForModel ? handsVisualizationForModel.Visibility : false;
            }
            set
            {
#if EZXRForMRTK
                handsVisualizationForModel.Visibility = false;
#else        
                if (handsVisualizationForModel != null)
                {
                    handsVisualizationForModel.Visibility = m_Enabled && (EnableByExternal == null ? true : EnableByExternal.Invoke()) && value;
                }
#endif
            }
        }
        private bool raycastInteractionByUser = true;
        /// <summary>
        /// 设置或获得远距离射线交互状态，如果Enabled为false则Visibility也将为false
        /// </summary>
        public override bool RaycastInteraction
        {
            get
            {
                return handRaycast ? handRaycast.enabled : false;
            }
            set
            {
                raycastInteractionByUser = value;
                UpdateRaycastInteractionState();
            }
        }
        private bool raycastInteractionByInternal = true;
        public override bool RI_Internal
        {
            get
            {
                return handRaycast ? handRaycast.enabled : false;
            }
            set
            {
                raycastInteractionByInternal = value;
                UpdateRaycastInteractionState();
            }
        }

        private void UpdateRaycastInteractionState()
        {
            if (handRaycast != null)
            {
#if EZXRForMRTK
                    handRaycast.enabled = false;
#else
                //Debug.Log("HandInfo => RaycastInteraction: " + value);
                handRaycast.enabled = m_Enabled && (EnableByExternal == null ? true : EnableByExternal.Invoke()) && raycastInteractionByInternal && raycastInteractionByUser;
#endif
            }
        }

        /// <summary>
        /// 设置或获得近距离交互状态，如果Enabled为false则Visibility也将为false
        /// </summary>
        public override bool TouchInteraction
        {
            get
            {
                return handTouch ? handTouch.enabled : false;
            }
            set
            {
#if EZXRForMRTK
                handTouch.enabled = false;
#else
                if (handTouch != null)
                {
                    handTouch.enabled = m_Enabled && (EnableByExternal == null ? true : EnableByExternal.Invoke()) && value;
                }
#endif
            }
        }

        /// <summary>
        /// 设置或获得手部的物理碰撞交互状态
        /// </summary>
        public override bool PhysicsInteraction
        {
            get
            {
                return handCollision ? handCollision.enabled : false;
            }
            set
            {
#if EZXRForMRTK
                handCollision.enabled = false;
#else
                if (handCollision != null)
                {
                    handCollision.enabled = m_Enabled && (EnableByExternal == null ? true : EnableByExternal.Invoke()) && value;
                }
#endif
            }
        }
        #endregion

        #region 单手信息
        /// <summary>
        /// 当前手节点是否在视野内
        /// </summary>
        /// <param name="jointType"></param>
        /// <returns></returns>
        public bool InView(HandJointType jointType)
        {
            if (Application.isEditor)
            {
                Vector3 viewportPoint = XRMan.Eyes.Center.WorldToViewportPoint(GetJointData(jointType).position);
                //Debug.Log(viewportPoint.ToString("0.##"));
                return viewportPoint.x > 0 && viewportPoint.x < 1 && viewportPoint.y > 0 && viewportPoint.y < 1 && viewportPoint.z > 0;
            }
            else
            {
                Vector3 viewportPoint_L = XRMan.Eyes.Left.WorldToViewportPoint(GetJointData(jointType).position);
                Vector3 viewportPoint_R = XRMan.Eyes.Right.WorldToViewportPoint(GetJointData(jointType).position);
                return (viewportPoint_L.x > 0 && viewportPoint_L.x < 1 && viewportPoint_L.y > 0 && viewportPoint_L.y < 1 && viewportPoint_L.z > 0) || (viewportPoint_R.x > 0 && viewportPoint_R.x < 1 && viewportPoint_R.y > 0 && viewportPoint_R.y < 1 && viewportPoint_R.z > 0);
            }
        }
        public bool InLeftView(HandJointType jointType)
        {
            Vector3 viewportPoint_L = XRMan.Eyes.Left.WorldToViewportPoint(GetJointData(jointType).position);
            return (viewportPoint_L.x > 0 && viewportPoint_L.x < 1 && viewportPoint_L.y > 0 && viewportPoint_L.y < 1 && viewportPoint_L.z > 0);
        }
        public bool InRightView(HandJointType jointType)
        {
            Vector3 viewportPoint_R = XRMan.Eyes.Right.WorldToViewportPoint(GetJointData(jointType).position);
            return (viewportPoint_R.x > 0 && viewportPoint_R.x < 1 && viewportPoint_R.y > 0 && viewportPoint_R.y < 1 && viewportPoint_R.z > 0);
        }

        /// <summary>
        /// 眼镜当前是否检测到这只手（不在视野内也是可以检测到的，如果需要在视野内的判定请调用InView方法）
        /// </summary>
        public override bool Exist
        {
            get
            {
                return handTrackingData.isTracked;
            }
        }

        /// <summary>
        /// 手掌正在朝向头部，如果手掌朝向和手掌与头的连线的夹角小于angle则认为手掌是朝向了头部
        /// </summary>
        /// <param name="angle">默认90</param>
        /// <returns></returns>
        public override bool isPalmFacingHead(float angle = 90)
        {
            Pose palm = GetJointData(HandJointType.Palm);
            Vector3 palmNormal = palm.TransformDirection(Vector3.forward * 0.1f);
            Vector3 palmToHead = XRMan.Head.position - palm.position;
            //求palmNormal和palmToHead的夹角
            float includedAngle = Vector3.Angle(palmNormal, palmToHead);

            //Debug.Log("EnableRaycastByExternal: palmNormal: " + palmNormal.ToString("#.###") + ", palmToHead: " + palmToHead.ToString("#.###"));
            //Debug.Log("EnableRaycastByExternal: includedAngle: " + includedAngle + ", angle: " + angle);
            if (includedAngle < angle)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public override bool IsPalmFacingHead(float angle = 90)
        {
            return isPalmFacingHead(angle);
        }

        /// <summary>
        /// 从算法得到的此手的原始关节点数据，单位为mm
        /// </summary>
        public NativeSwapManager.HandTrackingData handTrackingData = new NativeSwapManager.HandTrackingData();

        /// <summary>
        /// 自行定义的一个肩部点，相对于头部坐标系的
        /// </summary>
        Vector3 shoulderPoint;

        /// <summary>
        /// 手的动作
        /// </summary>
        public GestureType gestureType
        {
            get
            {
                return handTrackingData.gestureType;
            }
        }

        /// <summary>
        /// 所有手指关节点的Pose
        /// </summary>
        public Dictionary<HandJointType, Pose> jointsPose = new Dictionary<HandJointType, Pose>();
        /// <summary>
        /// 手掌面向的方向
        /// </summary>
        [HideInInspector]
        public Vector3 palmNormal;
        /// <summary>
        /// 手掌指尖朝向（从手腕点到中指和无名指根节点中间点的朝向，世界坐标系）
        /// </summary>
        [HideInInspector]
        public Vector3 palmDirection;
        [HideInInspector]
        public Transform palm;


        public bool JointDataExist(HandJointType handJointType)
        {
            return jointsPose.ContainsKey(handJointType);
        }

        public Pose GetJointData(HandJointType handJointType)
        {
            if (jointsPose.ContainsKey(handJointType))
            {
                return jointsPose[handJointType];
            }
            return Pose.identity;
        }

        public Pose GetJointData(int handJointTypeID)
        {
            return GetJointData((HandJointType)handJointTypeID);
        }
        #endregion

        #region 当前预触碰的物体信息
        /// <summary>
        /// 在手的近距离交互预判区内是否存在物体，只是在手的触发区域内但是还没有碰到食指指尖（主要用于避免在手即将触摸到旋转握把的时候会出现射线射到另外一个握把或者缩放角）
        /// </summary>
        [HideInInspector]
        public bool preCloseContacting;
        /// <summary>
        /// 手部近距离交互预判区（TriggerForFarNearSwitch中定义区域大小，此区域内则认为用户要近距离抓取）内存在的距离最近的物体
        /// </summary>
        Transform preCloseContactingTarget;
        /// <summary>
        /// 手部近距离交互预判区内存在的距离最近的物体
        /// </summary>
        public Transform PreCloseContactingTarget
        {
            get
            {
                return preCloseContactingTarget;
            }
            set
            {
                //if (preCloseContactingTarget != value)
                {
                    //记录上一帧食指指尖的近距离交互预判区内存在的距离最近的物体
                    Last_PreCloseContactingTarget = preCloseContactingTarget;

                    preCloseContactingTarget = value;
                    preCloseContactingTarget_Renderer = (preCloseContactingTarget == null ? null : preCloseContactingTarget.GetComponent<Renderer>());
                    if (preCloseContactingTarget_Renderer != null)
                    {
                        if (preCloseContactingTarget_PropertyBlock == null)
                        {
                            preCloseContactingTarget_PropertyBlock = new MaterialPropertyBlock();
                        }
                        preCloseContactingTarget_Renderer.GetPropertyBlock(preCloseContactingTarget_PropertyBlock);
                    }

                    if (preCloseContactingTarget == null)
                    {
                        preCloseContacting = false;
                    }
                    else
                    {
                        preCloseContacting = true;
                    }
                }
            }
        }
        [HideInInspector]
        public Renderer preCloseContactingTarget_Renderer;
        [HideInInspector]
        public MaterialPropertyBlock preCloseContactingTarget_PropertyBlock;

        /// <summary>
        /// 上一帧手部近距离交互预判区内存在的距离最近的物体
        /// </summary>
        Transform last_PreCloseContactingTarget;
        /// <summary>
        /// 上一帧手部近距离交互预判区内存在的距离最近的物体
        /// </summary>
        public Transform Last_PreCloseContactingTarget
        {
            get
            {
                return last_PreCloseContactingTarget;
            }
            set
            {
                if (last_PreCloseContactingTarget != value)
                {
                    last_PreCloseContactingTarget = value;
                    last_PreCloseContactingTarget_Renderer = (last_PreCloseContactingTarget == null ? null : last_PreCloseContactingTarget.GetComponent<Renderer>());
                    if (last_PreCloseContactingTarget_Renderer != null)
                    {
                        if (last_PreCloseContactingTarget_PropertyBlock == null)
                        {
                            last_PreCloseContactingTarget_PropertyBlock = new MaterialPropertyBlock();
                        }
                        last_PreCloseContactingTarget_Renderer.GetPropertyBlock(last_PreCloseContactingTarget_PropertyBlock);
                    }
                }
            }
        }
        [HideInInspector]
        public Renderer last_PreCloseContactingTarget_Renderer;
        [HideInInspector]
        public MaterialPropertyBlock last_PreCloseContactingTarget_PropertyBlock;
        #endregion

        #region 当前正在触碰的物体信息
        protected bool isCloseContacting;
        /// <summary>
        /// 指示手是否正在近距离触碰某个物体（被拇指和食指指尖的连线穿过）
        /// </summary>
        public bool IsCloseContacting
        {
            get
            {
                return isCloseContacting;
            }
            set
            {
                isCloseContacting = value;
            }
        }

        Collider curCloseContactingTarget;
        /// <summary>
        /// 当前正在被近距离触碰的物体
        /// </summary>
        public Collider CurCloseContactingTarget
        {
            get
            {
                return curCloseContactingTarget;
            }
            set
            {
                if (curCloseContactingTarget != null && value != null)
                {
                    if (curCloseContactingTarget != value)
                    {
                        SpatialObject.PerformOnHandTriggerExit(curCloseContactingTarget);
                        SpatialObject.PerformOnHandTriggerEnter(curCloseContactingTarget);
                    }
                    else
                    {
                        SpatialObject.PerformOnHandTriggerStay(value);
                    }
                }
                else if (curCloseContactingTarget == null && value != null)
                {
                    SpatialObject.PerformOnHandTriggerEnter(value);
                }
                else if (curCloseContactingTarget != null && value == null)
                {
                    SpatialObject.PerformOnHandTriggerExit(curCloseContactingTarget);
                }

                curCloseContactingTarget = value;
                if (curCloseContactingTarget == null)
                {
                    IsCloseContacting = false;
                }
                else
                {
                    IsCloseContacting = true;
                }
            }
        }

        /// <summary>
        /// 指示手是否正在近距离触碰某个物体或者射线是否正射在某个物体上
        /// </summary>
        public bool IsContacting
        {
            get
            {
                return IsCloseContacting | isRayContacting;
            }
        }
        /// <summary>
        /// 当前正在被触碰的物体（无论射线还是近距离）
        /// </summary>
        public Collider CurContactingTarget
        {
            get
            {
                if (CurCloseContactingTarget == null)
                {
                    if (curRayContactingTarget != null)
                    {
                        return curRayContactingTarget;
                    }
                }
                else
                {
                    return CurCloseContactingTarget;
                }
                return null;
            }
        }
        #endregion

    }
}
