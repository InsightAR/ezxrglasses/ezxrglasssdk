﻿using EZXR.Glass.Runtime;
using SuperSystems.ImageEffects;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EZXR.Glass.Inputs
{
    /// <summary>
    /// 可视化手部信息：关节、关节点等等
    /// </summary>
    [ScriptExecutionOrder(-8)]
    public class HandsVisualizationForModel : MonoBehaviour
    {
        #region singleton
        private static HandsVisualizationForModel _instance_Left;
        private static HandsVisualizationForModel _instance_Right;
        public static HandsVisualizationForModel Left
        {
            get
            {
                return _instance_Left;
            }
        }
        public static HandsVisualizationForModel Right
        {
            get
            {
                return _instance_Right;
            }
        }
        #endregion

        //public bool bone = true;
        public bool outline = false;
        public bool isLeft;
        bool visibility = true;
        bool lastVisibility = true;
        public Transform handRoot;
        public Transform wrist;
        /// <summary>
        /// 和HandJointType顺序一致
        /// </summary>
        public List<Transform> bones = new List<Transform>();
        /// <summary>
        /// 针对每个
        /// </summary>
        Vector3 corrction;

        HandInfo handInfo;

        #region 变量声明，灰度相关
        /// <summary>
        /// 左灰度相机的左手关节点的信息
        /// </summary>
        RectTransform[] keyPoints2D_LeftFish_LeftHand = new RectTransform[21];
        /// <summary>
        /// 左灰度相机的右手关节点的信息
        /// </summary>
        RectTransform[] keyPoints2D_LeftFish_RightHand = new RectTransform[21];
        /// <summary>
        /// 右灰度相机的左手关节点的信息
        /// </summary>
        RectTransform[] keyPoints2D_RightFish_LeftHand = new RectTransform[21];
        /// <summary>
        /// 右灰度相机的右手关节点的信息
        /// </summary>
        RectTransform[] keyPoints2D_RightFish_RightHand = new RectTransform[21];

        /// <summary>
        /// 左灰度相机的左手关节线的信息
        /// </summary>
        Dictionary<int, RectTransform> fingerBones2D_LeftFish_LeftHand = new Dictionary<int, RectTransform>();
        /// <summary>
        /// 左灰度相机的右手关节线的信息
        /// </summary>
        Dictionary<int, RectTransform> fingerBones2D_LeftFish_RightHand = new Dictionary<int, RectTransform>();
        /// <summary>
        /// 右灰度相机的左手关节线的信息
        /// </summary>
        Dictionary<int, RectTransform> fingerBones2D_RightFish_LeftHand = new Dictionary<int, RectTransform>();
        /// <summary>
        /// 右灰度相机的右手关节线的信息
        /// </summary>
        Dictionary<int, RectTransform> fingerBones2D_RightFish_RightHand = new Dictionary<int, RectTransform>();

        /// <summary>
        /// 左灰度相机图像上用于绘制关节点和关节线的物体
        /// </summary>
        Transform canvasRoot_Left;
        /// <summary>
        /// 右灰度相机图像上用于绘制关节点和关节线的物体
        /// </summary>
        Transform canvasRoot_Right;
        #endregion

        public Vector3 ModelFingerPointing = new Vector3(0, 0, 0);
        public Vector3 ModelPalmFacing = new Vector3(0, 0, 0);

        float width;
        float height;

        /// <summary>
        /// 0是默认都开启，1是禁用了手的长宽缩放，2是禁用了手的长宽缩放和关节点位置
        /// </summary>
        static int mode = 0;

        Vector3 defaultScale;
        public Vector3 defaultPos;
        public Quaternion defaultRot;
        public List<Vector3> defaultBonesPos = new List<Vector3>();
        public List<Quaternion> defaultBonesRot = new List<Quaternion>();
        public Renderer handRenderer;
        /// <summary>
        /// 0是模型手，1是轮廓手
        /// </summary>
        public Material[] materials;


        public static int Mode
        {
            get
            {
                return mode;
            }
            set
            {
                Debug.Log("Mode:a");
                mode = value;
                Left.wrist.localScale = Left.defaultScale;
                Right.wrist.localScale = Right.defaultScale;
                Left.wrist.localPosition = Left.defaultPos;
                Right.wrist.localPosition = Right.defaultPos;
                Left.wrist.localRotation = Left.defaultRot;
                Right.wrist.localRotation = Right.defaultRot;
                Debug.Log("Mode:b");

                Debug.Log("Mode:Left.bones.Count: " + Left.bones.Count + " , Left.defaultBonesPos: " + Left.defaultBonesPos.Count);
                foreach (HandJointType key in Left.handInfo.jointsPose.Keys)
                {
                    int id = (int)key;
                    if (id < Left.bones.Count)
                    {
                        Left.bones[id].localPosition = Left.defaultBonesPos[id];
                        Left.bones[id].localRotation = Left.defaultBonesRot[id];
                    }
                }
                Debug.Log("Mode:Right.bones.Count: " + Right.bones.Count + " , Right.defaultBonesPos: " + Right.defaultBonesPos.Count);
                foreach (HandJointType key in Right.handInfo.jointsPose.Keys)
                {
                    int id = (int)key;
                    if (id < Right.bones.Count)
                    {
                        Right.bones[id].localPosition = Right.defaultBonesPos[id];
                        Right.bones[id].localRotation = Right.defaultBonesRot[id];
                    }
                }
            }
        }

        private void Awake()
        {
            //编辑器内手动禁用此组件的时候需要将对应的模型手一并禁用
            handRoot.gameObject.SetActive(enabled);

            if (isLeft)
            {
                _instance_Left = this;
            }
            else
            {
                _instance_Right = this;
            }

            width = Vector3.Distance(bones[4].position, bones[16].position);
            height = Vector3.Distance(bones[8].position, wrist.position);

            handInfo = GetComponent<HandInfo>();

            string _handType = isLeft ? "L" : "R";
            corrction = isLeft ? new Vector3(-90, 0, 0) : new Vector3(-90, 0, 0);

            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Thumb" + _handType + "_JNT1"));
            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Thumb" + _handType + "_JNT1" + "/" + "Thumb" + _handType + "_JNT2"));
            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Thumb" + _handType + "_JNT1" + "/" + "Thumb" + _handType + "_JNT2" + "/" + "Thumb" + _handType + "_JNT3"));
            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Thumb" + _handType + "_JNT1" + "/" + "Thumb" + _handType + "_JNT2" + "/" + "Thumb" + _handType + "_JNT3"));

            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Point" + _handType + "_JNT" + "/" + "Point" + _handType + "_JNT1"));
            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Point" + _handType + "_JNT" + "/" + "Point" + _handType + "_JNT1" + "/" + "Point" + _handType + "_JNT2"));
            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Point" + _handType + "_JNT" + "/" + "Point" + _handType + "_JNT1" + "/" + "Point" + _handType + "_JNT2" + "/" + "Point" + _handType + "_JNT3"));
            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Point" + _handType + "_JNT" + "/" + "Point" + _handType + "_JNT1" + "/" + "Point" + _handType + "_JNT2" + "/" + "Point" + _handType + "_JNT3"));

            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Middle" + _handType + "_JNT" + "/" + "Middle" + _handType + "_JNT1"));
            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Middle" + _handType + "_JNT" + "/" + "Middle" + _handType + "_JNT1" + "/" + "Middle" + _handType + "_JNT2"));
            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Middle" + _handType + "_JNT" + "/" + "Middle" + _handType + "_JNT1" + "/" + "Middle" + _handType + "_JNT2" + "/" + "Middle" + _handType + "_JNT3"));
            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Middle" + _handType + "_JNT" + "/" + "Middle" + _handType + "_JNT1" + "/" + "Middle" + _handType + "_JNT2" + "/" + "Middle" + _handType + "_JNT3"));

            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Ring" + _handType + "_JNT" + "/" + "Ring" + _handType + "_JNT1"));
            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Ring" + _handType + "_JNT" + "/" + "Ring" + _handType + "_JNT1" + "/" + "Ring" + _handType + "_JNT2"));
            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Ring" + _handType + "_JNT" + "/" + "Ring" + _handType + "_JNT1" + "/" + "Ring" + _handType + "_JNT2" + "/" + "Ring" + _handType + "_JNT3"));
            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Ring" + _handType + "_JNT" + "/" + "Ring" + _handType + "_JNT1" + "/" + "Ring" + _handType + "_JNT2" + "/" + "Ring" + _handType + "_JNT3"));

            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Pinky" + _handType + "_JNT"));
            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Pinky" + _handType + "_JNT" + "/" + "Pinky" + _handType + "_JNT1"));
            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Pinky" + _handType + "_JNT" + "/" + "Pinky" + _handType + "_JNT1" + "/" + "Pinky" + _handType + "_JNT2"));
            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Pinky" + _handType + "_JNT" + "/" + "Pinky" + _handType + "_JNT1" + "/" + "Pinky" + _handType + "_JNT2" + "/" + "Pinky" + _handType + "_JNT3"));
            //bones.Add(handRoot.Find("Wrist" + _handType + "_JNT" + "/" + "Pinky" + _handType + "_JNT" + "/" + "Pinky" + _handType + "_JNT1" + "/" + "Pinky" + _handType + "_JNT2" + "/" + "Pinky" + _handType + "_JNT3"));

            ////bones.Add(temp);
            ////bones.Add(temp);
            ////bones.Add(temp);
            ////bones.Add(handRoot.Find("Wrist" + _handType + "_JNT"));
        }

        private void Start()
        {
            defaultScale = wrist.localScale;
            defaultPos = wrist.localPosition;
            defaultRot = wrist.localRotation;
            for (int id = 0; id < Left.bones.Count; id++)
            {
                if (id < bones.Count)
                {
                    Left.defaultBonesPos.Add(Left.bones[id].localPosition);
                    Left.defaultBonesRot.Add(Left.bones[id].localRotation);
                }
            }
            for (int id = 0; id < Right.bones.Count; id++)
            {
                if (id < bones.Count)
                {
                    Right.defaultBonesPos.Add(Right.bones[id].localPosition);
                    Right.defaultBonesRot.Add(Right.bones[id].localRotation);
                }
            }
        }

        void LateUpdate()
        {
            if (lastVisibility != visibility)
            {
                lastVisibility = visibility;
                handRoot.gameObject.SetActive(visibility);
            }

            if (handInfo.Exist)
            {
                wrist.position = handInfo.GetJointData(HandJointType.Wrist_Middle).position;
                wrist.rotation = handInfo.GetJointData(HandJointType.Wrist_Middle).rotation * Quaternion.Euler(corrction) * Reorientation();

                //Debug.Log("handInfo.handTrackingData.model_scale: " + handInfo.handTrackingData.model_scale);
                //Debug.Log("handInfo.handTrackingData.fingler_curl: " + handInfo.handTrackingData.fingler_curl[0] + ", " + handInfo.handTrackingData.fingler_curl[1] + ", " + handInfo.handTrackingData.fingler_curl[2] + ", " + handInfo.handTrackingData.fingler_curl[3] + ", " + handInfo.handTrackingData.fingler_curl[4]);
                if (Mode == 0)
                {
                    //float curWidth = Vector3.Distance(bones[4].position, bones[16].position);
                    //float curHeight = Vector3.Distance(bones[8].position, wrist.position);
                    //float ratioWidth = curWidth / width;
                    //float ratioHeight = curHeight / height;
#if UNITY_EDITOR
                    wrist.localScale = Vector3.one;
#else
                    wrist.localScale = new Vector3(handInfo.handTrackingData.model_scale, handInfo.handTrackingData.model_scale, handInfo.handTrackingData.model_scale);
#endif
                }
                else
                {
                    wrist.localScale = Vector3.one;
                }

                foreach (HandJointType key in handInfo.jointsPose.Keys)
                {
                    int id = (int)key;

                    if (id < bones.Count)
                    {
                        if (Mode == 0 || Mode == 1)
                        {
                            bones[id].position = handInfo.GetJointData(key).position;
                        }
                        bones[id].rotation = handInfo.GetJointData(key).rotation * Quaternion.Euler(corrction) * Reorientation();
                    }
                }

                //handInfo.Visibility = bone;

                if (outline)
                {
                    if (handRenderer.sharedMaterial != materials[1])
                    {
                        Debug.Log("change to outline");
                        handRenderer.sharedMaterial = materials[1];
                    }
                }
                else
                {
                    if (handRenderer.sharedMaterial != materials[0])
                    {
                        Debug.Log("change to model");
                        handRenderer.sharedMaterial = materials[0];
                    }
                }
            }
        }

        public bool Visibility
        {
            get
            {
                return visibility;
            }
            set
            {
                visibility = value;
                //这里直接调用Show会出现keyPoints数组虽然长度为25，但是每个元素都是"null"，非常神奇的bug，只要从其他脚本中调用Visibility中的Show来隐藏球棍手就一定会出现这个问题，所以改为了在此脚本的update中调用Show
                //Show(value);
            }
        }

        private Quaternion Reorientation()
        {
            return Quaternion.Inverse(Quaternion.LookRotation(ModelFingerPointing, -ModelPalmFacing));
        }
    }
}