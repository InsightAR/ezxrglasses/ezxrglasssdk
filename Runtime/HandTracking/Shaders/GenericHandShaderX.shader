﻿Shader "Ultraleap/GenericHandShaderX"
{
    Properties
    {

        
        [HDR]_OutlineColor("Outline Color", Color) = (0,0,0,1)
        _Outline("Outline width", Range(0,.01)) = .002
        _width("width", Range(-.01,.01)) = .01
        
        _GradationThreshold ("Gradation Threshold", Range(0, 1)) = 0.5
		_GradationSmooth ("Gradation Smooth", Range(0, 0.5)) = 0.5
    }

    CGINCLUDE

    #include "UnityCG.cginc"   // for & UNITY_VERTEX_OUTPUT_STEREO UnityObjectToWorldNormal() 
    #include "AutoLight.cginc" // for UNITY_SHADOW_COORDS() & UNITY_TRANSFER_SHADOW()
    float _width;
    float _Outline;
    float4 _OutlineColor;

    fixed _GradationThreshold;
	fixed _GradationSmooth;

    struct v2f
    {
        float2 uv : TEXCOORD0;
        float4 pos : POSITION;
        float3 worldNormal : NORMAL;
        float3 viewDir : TEXCOORD1;
        fixed4 diff : COLOR0;

        UNITY_VERTEX_OUTPUT_STEREO
        UNITY_SHADOW_COORDS(2) // Uses TEXCOORD2
    };

    ENDCG

    SubShader
    {

        ZWrite On
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
             Tags
            {
                "Queue" = "Geometry-1" "IgnoreProjector" = "True" "RenderType" = "Opaque" "LightMode" = "ForwardBase"
            }
                Cull Back
                Blend Zero One
                        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            v2f vert(appdata_base v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_OUTPUT(v2f, o);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

                o.pos = UnityObjectToClipPos(v.vertex);
                float3 norm = mul((float3x3)UNITY_MATRIX_IT_MV, v.normal);
                float2 offset = TransformViewToProjection(norm.xy);
                o.uv = v.texcoord;
                o.pos.xy =  o.pos.xy + offset * _width ;
                return o;
            }
                                    half4 frag(v2f i) :COLOR
            {

                return fixed4(1,1,1,1);
            }
                        ENDCG
            }

        Pass
        {
            
            Tags
            {
                "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" "LightMode" = "ForwardBase"
            }
            Cull Front

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            v2f vert(appdata_base v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_OUTPUT(v2f, o);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

                o.pos = UnityObjectToClipPos(v.vertex);
                float3 norm = mul((float3x3)UNITY_MATRIX_IT_MV, v.normal);
                float2 offset = TransformViewToProjection(norm.xy);
                o.uv = v.texcoord;
                o.pos.xy =  o.pos.xy + +offset * _width+offset * _Outline ;
                return o;
            }
            
            half LinearStep(half minValue, half maxValue, half In)
			{
			    return saturate((In-minValue) / (maxValue - minValue));
			}

            half4 frag(v2f i) :COLOR
            {
                fixed4 col = _OutlineColor;
                fixed jianb=LinearStep(_GradationThreshold-_GradationSmooth,_GradationThreshold+_GradationSmooth,i.uv.y);
                return fixed4(col.xyz,jianb*_OutlineColor.a);
            }
            ENDCG
        }

    }

}