Shader "Shaders Study/Alpha Blend ZWrite"
{
	Properties
	{
		_Color ("Main Tint", Color) = (1, 1, 1, 1)
		_MainTex ("Main Tex", 2D) = "white" {}
		_AlphaScale ("Alpha Scale", Range(0, 1)) = 1
		_FresnelScale ("Fresnel Scale", Range(0, 5)) = 0.5
		_FresnelPow ("Fresnel Pow", Range(0, 10)) = 5
		_GradationThreshold ("Gradation Threshold", Range(0, 1)) = 0.5
		_GradationSmooth ("Gradation Smooth", Range(0, 0.5)) = 0.5
		
	}
	Subshader
	{
		Tags
        {
            "Queue" = "Geometry-1" "IgnoreProjector" = "True" "RenderType" = "Transparent" "LightMode" = "ForwardBase"
        }

        ZWrite On
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            Cull Back
            Blend Zero One
        }

		pass
		{
			Tags { "LightMode" = "ForwardBase"}


			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#include "Lighting.cginc"

			fixed4 _Color;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed _AlphaScale;
			fixed _GradationThreshold;
			fixed _GradationSmooth;
			fixed _FresnelScale;
			fixed _FresnelPow;

			struct a2v
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
			};
			struct v2f 
			{
				float4 pos : SV_POSITION;
				float3 worldNormal : TEXCOORD0;
				float3 worldPos : TEXCOORD1;
				float2 uv : TEXCOORD2;
				fixed3 worldViewDir : TEXCOORD3;
			};

			v2f vert(a2v v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);

				o.worldNormal = UnityObjectToWorldNormal(v.normal);

				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);

				o.worldViewDir = UnityWorldSpaceViewDir(o.worldPos);

				return o;
			}

			half LinearStep(half minValue, half maxValue, half In)
			{
			    return saturate((In-minValue) / (maxValue - minValue));
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed3 worldNormal = normalize(i.worldNormal);
				fixed3 worldLightDir = normalize(UnityWorldSpaceLightDir(i.worldPos));

				fixed4 texColor = tex2D(_MainTex, i.uv);

				fixed3 albedo = texColor.rgb * _Color.rgb;

				fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.xyz * albedo;

				
				
				fixed3 worldViewDir = normalize(i.worldViewDir);
				
				fixed fresnel =  _FresnelScale * saturate(pow(1 - dot(worldViewDir, worldNormal), _FresnelPow));

				fixed3 diffuse = _LightColor0.rgb * albedo * (max(0, dot(worldNormal, worldLightDir))*0.5+0.5);

				fixed jianb=LinearStep(_GradationThreshold-_GradationSmooth,_GradationThreshold+_GradationSmooth,i.uv.y);

				fixed a=texColor.a * _AlphaScale*jianb+fresnel*jianb;
				fixed3 col=saturate((ambient + diffuse+fresnel*_Color));

				return fixed4(col, a);
			}
			ENDCG
		}
	}
}