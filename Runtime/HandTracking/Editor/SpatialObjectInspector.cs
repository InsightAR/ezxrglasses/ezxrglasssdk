using EZXR.Glass.Inputs;
using System;
using System.Drawing;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

namespace EZXR.Glass.Inputs
{
    [CustomEditor(typeof(SpatialObject), true)]
    public class EventTriggerEditor : Editor
    {
        SerializedProperty isStaticObject;
        bool constraints;
        SerializedProperty freezePosition_X, freezePosition_Y, freezePosition_Z;
        SerializedProperty freezeRotation;
        SerializedProperty freezeScale_X, freezeScale_Y, freezeScale_Z;
        SerializedProperty allowInteractionType;

        SerializedProperty m_DelegatesProperty;

        GUIContent m_IconToolbarMinus;
        GUIContent m_EventIDName;
        GUIContent[] m_EventTypes;
        GUIContent m_AddButonContent;

        protected virtual void OnEnable()
        {
            isStaticObject = serializedObject.FindProperty("isStaticObject");

            freezePosition_X = serializedObject.FindProperty("freezePosition_X");
            freezePosition_Y = serializedObject.FindProperty("freezePosition_Y");
            freezePosition_Z = serializedObject.FindProperty("freezePosition_Z");
            freezeRotation = serializedObject.FindProperty("freezeRotation");
            freezeScale_X = serializedObject.FindProperty("freezeScale_X");
            freezeScale_Y = serializedObject.FindProperty("freezeScale_Y");
            freezeScale_Z = serializedObject.FindProperty("freezeScale_Z");

            allowInteractionType = serializedObject.FindProperty("allowInteractionType");

            m_DelegatesProperty = serializedObject.FindProperty("m_Delegates");
            m_AddButonContent = EditorGUIUtility.TrTextContent("Add New Event Type");
            m_EventIDName = new GUIContent("");
            // Have to create a copy since otherwise the tooltip will be overwritten.
            m_IconToolbarMinus = new GUIContent(EditorGUIUtility.IconContent("Toolbar Minus"));
            m_IconToolbarMinus.tooltip = "Remove all events in this list.";

            string[] eventNames = Enum.GetNames(typeof(SpatialObjectEventType));
            m_EventTypes = new GUIContent[eventNames.Length];
            for (int i = 0; i < eventNames.Length; ++i)
            {
                m_EventTypes[i] = new GUIContent(eventNames[i]);
            }
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            int toBeRemovedEntry = -1;

            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(isStaticObject);

            constraints = EditorGUILayout.BeginFoldoutHeaderGroup(constraints, new GUIContent("Constraints"));
            if (constraints)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(100));
                EditorGUILayout.PrefixLabel("Freeze Position");
                EditorGUIUtility.labelWidth = 10f;
                EditorGUILayout.PropertyField(freezePosition_X, new GUIContent("X"));
                EditorGUILayout.PropertyField(freezePosition_Y, new GUIContent("Y"));
                EditorGUILayout.PropertyField(freezePosition_Z, new GUIContent("Z"));
                EditorGUIUtility.labelWidth = 0;
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(100));
                EditorGUILayout.PrefixLabel("Freeze Rotation");
                EditorGUIUtility.labelWidth = 40f;
                EditorGUILayout.PropertyField(freezeRotation, new GUIContent("XYZW"));
                EditorGUIUtility.labelWidth = 0;
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(100));
                EditorGUILayout.PrefixLabel("Freeze Scale");
                EditorGUIUtility.labelWidth = 10f;
                EditorGUILayout.PropertyField(freezeScale_X, new GUIContent("X"));
                EditorGUILayout.PropertyField(freezeScale_Y, new GUIContent("Y"));
                EditorGUILayout.PropertyField(freezeScale_Z, new GUIContent("Z"));
                EditorGUIUtility.labelWidth = 0;
                EditorGUILayout.EndHorizontal();
                EditorGUI.indentLevel--;
            }
            EditorGUILayout.EndFoldoutHeaderGroup();

            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(allowInteractionType, new GUIContent("AllowInteractionType"));

            EditorGUILayout.Space();

            Vector2 removeButtonSize = GUIStyle.none.CalcSize(m_IconToolbarMinus);

            for (int i = 0; i < m_DelegatesProperty.arraySize; ++i)
            {
                SerializedProperty delegateProperty = m_DelegatesProperty.GetArrayElementAtIndex(i);
                SerializedProperty eventProperty = delegateProperty.FindPropertyRelative("eventID");
                SerializedProperty callbacksProperty = delegateProperty.FindPropertyRelative("callback");
                m_EventIDName.text = eventProperty.enumDisplayNames[eventProperty.enumValueIndex];

                EditorGUILayout.PropertyField(callbacksProperty, m_EventIDName);
                Rect callbackRect = GUILayoutUtility.GetLastRect();

                Rect removeButtonPos = new Rect(callbackRect.xMax - removeButtonSize.x - 8, callbackRect.y + 1, removeButtonSize.x, removeButtonSize.y);
                if (GUI.Button(removeButtonPos, m_IconToolbarMinus, GUIStyle.none))
                {
                    toBeRemovedEntry = i;
                }

                EditorGUILayout.Space();
            }

            if (toBeRemovedEntry > -1)
            {
                RemoveEntry(toBeRemovedEntry);
            }

            Rect btPosition = GUILayoutUtility.GetRect(m_AddButonContent, GUI.skin.button);
            const float addButonWidth = 200f;
            btPosition.x = btPosition.x + (btPosition.width - addButonWidth) / 2;
            btPosition.width = addButonWidth;
            if (GUI.Button(btPosition, m_AddButonContent))
            {
                ShowAddTriggermenu();
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void RemoveEntry(int toBeRemovedEntry)
        {
            m_DelegatesProperty.DeleteArrayElementAtIndex(toBeRemovedEntry);
        }

        void ShowAddTriggermenu()
        {
            // Now create the menu, add items and show it
            GenericMenu menu = new GenericMenu();
            for (int i = 0; i < m_EventTypes.Length; ++i)
            {
                bool active = true;

                // Check if we already have a Entry for the current eventType, if so, disable it
                for (int p = 0; p < m_DelegatesProperty.arraySize; ++p)
                {
                    SerializedProperty delegateEntry = m_DelegatesProperty.GetArrayElementAtIndex(p);
                    SerializedProperty eventProperty = delegateEntry.FindPropertyRelative("eventID");
                    if (eventProperty.enumValueIndex == i)
                    {
                        active = false;
                    }
                }
                if (active)
                    menu.AddItem(m_EventTypes[i], false, OnAddNewSelected, i);
                else
                    menu.AddDisabledItem(m_EventTypes[i]);
            }
            menu.ShowAsContext();
            Event.current.Use();
        }

        private void OnAddNewSelected(object index)
        {
            int selected = (int)index;

            m_DelegatesProperty.arraySize += 1;
            SerializedProperty delegateEntry = m_DelegatesProperty.GetArrayElementAtIndex(m_DelegatesProperty.arraySize - 1);
            SerializedProperty eventProperty = delegateEntry.FindPropertyRelative("eventID");
            eventProperty.enumValueIndex = selected;
            serializedObject.ApplyModifiedProperties();
        }
    }
}

