﻿using EZXR.Glass.Runtime;
using System;
using System.Collections;
using UnityEngine;

namespace EZXR.Glass.SixDof
{
    [ExecuteInEditMode]
    [ScriptExecutionOrder(-60)]
    public class HMDPoseTracker : MonoBehaviour
    {
        #region singleton
        private static HMDPoseTracker _instance;
        public static HMDPoseTracker Instance
        {
            get
            {
                return _instance;
            }
        }
        #endregion

        #region params
        public enum DegreeOfFreedom
        {
            /// <summary>
            /// 6 Dof
            /// </summary>

            SixDof,
            /// <summary>
            /// 3 Dof
            /// </summary>
            ThreeDof,
            /// <summary>
            /// 0 Dof
            /// </summary>
            ZeroDof,
        }
        [SerializeField]
        private DegreeOfFreedom m_dof = DegreeOfFreedom.SixDof;
        /// <summary>
        /// Head的自由度
        /// </summary>
        public DegreeOfFreedom degreeOfFreedom
        {
            get
            {
                return m_dof;
            }
            set
            {
                if (value != m_dof)
                {
                    m_dof = value;
                    PosetrackerChangedListener?.Invoke(m_dof);
                }
            }
        }
        public bool UseLocalPose
        {
            get
            {
                return m_UseLocalPose;
            }
            set
            {
                m_UseLocalPose = value;
            }
        }

        //private bool isInit = false;
        /// <summary>
        /// invoked while dof changed
        /// </summary>
        /// <param name="dof"> current dof of HMDPoseTracker </param>
        public delegate void PosePoseTrackerDofChanged(DegreeOfFreedom dof);
        /// <summary>
        /// PoseTracker changed listener
        /// </summary>
        public event PosePoseTrackerDofChanged PosetrackerChangedListener;

        [Obsolete("Use \"XRMan.Eyes.Left\" instead!")]
        public Camera leftCamera;
        [Obsolete("Use \"XRMan.Eyes.Right\" instead!")]
        public Camera rightCamera;
        [Obsolete("Use \"XRMan.Eyes.Center\" instead!")]
        public Camera centerCamera;

        [SerializeField]
        private bool m_UseLocalPose = true;

        private static OSTMatrices ost_m = new OSTMatrices();
        float[] left_rgb;
        float[] right_rgb;

        [Obsolete("Use \"XRMan.Head\" instead!")]
        public Transform Head
        {
            get
            {
                return transform;
            }
        }
        #endregion

        #region unity functions
        private void Awake()
        {
            Debug.Log("=============Unity Log===============   HMDPoseTracker:" + this.GetInstanceID() + " -- Awake");

            _instance = this;

#if !EZXRForMRTK
            if (centerCamera != null && centerCamera.gameObject != null)
                centerCamera.gameObject.SetActive(Application.isEditor ? true : false);
#endif
            if (Application.isPlaying && !Application.isEditor)
            {
                leftCamera.gameObject.SetActive(false);
                rightCamera.gameObject.SetActive(false);
            }
        }

        private void OnEnable()
        {
            _instance = this;
        }

        private void Start()
        {
            if (Application.isPlaying)
            {
                Debug.Log("=============Unity Log===============   HMDPoseTracker:" + this.GetInstanceID() + " -- Start");

                StartCoroutine(InitParam());
            }
        }

        private void Update()
        {
            if (Application.isPlaying)
            {
                //Debug.Log("=============Unity Log===============   HMDPoseTracker -- Update");

                if (!SessionManager.Instance.IsInited) return;

                if (ARFrame.SessionStatus < EZVIOState.EZVIOCameraState_Detecting) return;

                //if (!isInit)
                //{
                //    InitializeCameraPose();
                //    //InitializeCameraFieldOfViews();
                //    isInit = true;
                //}

                UpdatePoseByTrackingType();
            }
        }

        private void OnDestroy()
        {
            if (Application.isPlaying)
            {
                Debug.Log("=============Unity Log===============   HMDPoseTracker:" + this.GetInstanceID() + " -- OnDestroy");
                //_instance = null;
            }
        }

        #endregion

        #region custom functions

        private IEnumerator InitParam()
        {
            yield return new WaitUntil(() => SessionManager.Instance.IsInited);
            yield return new WaitUntil(() => ARFrame.SessionStatus == EZVIOState.EZVIOCameraState_Tracking);
            ost_m = new OSTMatrices();

            NativeTracking.GetARServiceVersionName();
            if (Application.isEditor)
            {
                centerCamera.gameObject.SetActive(true);
                leftCamera.gameObject.SetActive(false);
                rightCamera.gameObject.SetActive(false);

                ost_m.T_TrackCam_Head = new float[16];
                ost_m.T_RightEye_Head = new float[16];
                ost_m.T_LeftEye_Head = new float[16];
            }
            else
            {
#if !EZXRForMRTK
                centerCamera.gameObject.SetActive(false);
#endif
                leftCamera.gameObject.SetActive(true);
                rightCamera.gameObject.SetActive(true);

                NativeTracking.GetOSTParams(ref ost_m);
            }
            left_rgb = ost_m.T_LeftEye_Head;
            right_rgb = ost_m.T_RightEye_Head;

            if (!Application.isEditor)
            {
                InitializeCameraPose();
            }
        }
        private void InitializeCameraPose()
        {
            Matrix4x4 left_head_matrix = new Matrix4x4();
            Matrix4x4 right_head_matrix = new Matrix4x4();

            for (int i = 0; i < 16; i++)
            {
                left_head_matrix[i] = left_rgb[i];
                right_head_matrix[i] = right_rgb[i];
            }

            Matrix4x4 head_left_matrix = left_head_matrix.transpose.inverse;
            Matrix4x4 head_right_matrix = right_head_matrix.transpose.inverse;

            Vector3 eyePoseLeft = head_left_matrix.GetColumn(3);
            Vector3 eyePoseRight = head_right_matrix.GetColumn(3);
            Quaternion eyeRotLeft = head_left_matrix.rotation;
            Quaternion eyeRotRight = head_right_matrix.rotation;

            if (leftCamera != null)
            {
                leftCamera.transform.localPosition = eyePoseLeft;
                leftCamera.transform.localRotation = eyeRotLeft;
            }

            // right

            if (rightCamera != null)
            {
                rightCamera.transform.localPosition = eyePoseRight;
                rightCamera.transform.localRotation = eyeRotRight;
            }

            //isInit = true;
        }

        private void InitializeCameraFieldOfViews()
        {
            double[] fovs = ARFrame.CameraParams.fov;
            int imageWidth = ARFrame.CameraParams.width;
            int imageHeight = ARFrame.CameraParams.height;
            int screenWidth = Screen.width;
            int screenHeight = Screen.height;

            Debug.Log("==========UNITY LOG Screen Size and fov : " + Screen.width + " " + Screen.height + " " + SessionManager.Instance.RenderMode);

            if (SessionManager.Instance.RenderMode == Rendering.RenderMode.Bino) screenWidth /= 2;
            float[] screenFov = CameraUtility.CalculateFov(imageWidth, imageHeight, screenWidth, screenHeight,
                new float[] { (float)fovs[0], (float)fovs[1] });
            float cameraFov = screenFov[1];

            if (leftCamera != null)
            {
                leftCamera.fieldOfView = cameraFov;
            }

            if (rightCamera != null)
            {
                rightCamera.fieldOfView = cameraFov;
            }

            Debug.Log("==========UNITY LOG FOV : " + cameraFov + " " + fovs[0] + " " + fovs[1]);

            if (centerCamera != null)
            {
                centerCamera.fieldOfView = cameraFov;
            }
        }

        private void UpdatePoseByTrackingType()
        {
            if (ARFrame.SessionStatus != EZVIOState.EZVIOCameraState_Tracking) return;

            Pose headPose = ARFrame.HeadPose;

            if (!XRMan.Exist)
            {
                if (m_UseLocalPose)
                {

                    if (degreeOfFreedom != DegreeOfFreedom.ZeroDof)
                    {
                        if (degreeOfFreedom == DegreeOfFreedom.SixDof)
                        {
                            transform.localPosition = headPose.position;
                        }
                        transform.localRotation = headPose.rotation;
                    }
                }
                else
                {

                    if (degreeOfFreedom != DegreeOfFreedom.ZeroDof)
                    {
                        if (degreeOfFreedom == DegreeOfFreedom.SixDof)
                        {
                            transform.position = headPose.position;
                        }
                        transform.rotation = headPose.rotation;
                    }

                }
            }
            else
            {
                if (degreeOfFreedom != DegreeOfFreedom.ZeroDof)
                {
                    if (degreeOfFreedom == DegreeOfFreedom.SixDof)
                    {
                        XRMan.position_SpatialTracking = headPose.position;
                    }
                    transform.localRotation = headPose.rotation;
                    //XRMan.rotation_SpatialTracking = headPose.rotation;
                }
            }

        }
        #endregion
    }
}
