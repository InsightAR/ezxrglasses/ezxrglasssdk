﻿using UnityEngine;
using System.Collections;
using EZXR.Glass.SixDof;
using System;
using System.Collections.Generic;
using EZXR.Glass.Runtime;

namespace EZXR.Glass.Plane
{
    public class PlaneDetectionManager : MonoBehaviour
    {
        #region singleton
        private static PlaneDetectionManager _instance;
        public static PlaneDetectionManager Instance
        {
            get
            {
                return _instance;
            }
        }
        #endregion

        public struct PlaneInfo
        {
            public Vector3 position;
            public Quaternion rotation;
            public Vector3 scale;
        }
        Dictionary<ulong, PlaneInfo> planeInfos = new Dictionary<ulong, PlaneInfo>();

        private ulong m_FrameCount = 0;
        /// <summary>
        /// 一运行就开始检测
        /// </summary>
        public bool detectOnAwake = true;

        /// <summary>
        /// 开始检测平面
        /// </summary>
        public void StartDetection()
        {
            detectOnAwake = true;
        }

        /// <summary>
        /// 停止检测平面
        /// </summary>
        public void StopDetection()
        {
            detectOnAwake = false;
        }

        /// <summary>
        /// 得到所有检测到的Plane的信息
        /// </summary>
        /// <returns></returns>
        public Dictionary<ulong, PlaneInfo> GetPlanes()
        {
            return planeInfos;
        }

        private void Awake()
        {
            _instance = this;
        }

        private void OnDestroy()
        {
            ARConfig.DefaultConfig.PlaneFindingMode = PlaneFindingMode.Disable;
        }

        // Use this for initialization
        void OnEnable()
        {
            ARFrame.trackableManager.freshPlanes();
            SessionManager.Instance.ARInitConfig.PlaneFindingMode = PlaneFindingMode.Enable;
            SessionManager.Instance.ResumeSession();
        }
        private void OnDisable()
        {
            SessionManager.Instance.ARInitConfig.PlaneFindingMode = PlaneFindingMode.Disable;
            SessionManager.Instance.ResumeSession();
        }

        EZVIOPlane planeInfo = new EZVIOPlane();
        PlaneInfo planeObj;
        // Update is called once per frame
        void Update()
        {
            if (!detectOnAwake)
            {
                //Debug.Log("=============Unity Log===============   PlaneExtractor -- Update   idling, no extracting planes");
                return;
            }

            m_FrameCount++;
            if (m_FrameCount % 30 == 0)
            {
                if (m_FrameCount > 10000)//避免长时间运行越界
                {
                    m_FrameCount = 0;
                }

                //Debug.Log("=============Unity Log===============   PlaneExtractor -- Update   time to extract plane");

                if (ARFrame.SessionStatus == EZVIOState.EZVIOCameraState_Tracking)
                {
                    bool res = NativeTracking.GetPlane(ref planeInfo);

                    if (res)
                    {
                        ARFrame.trackableManager.UpdatePlane(planeInfo);
                        //Debug.Log("=============Unity Log===============   PlaneExtractor -- Update   extract plane, id: " + planeInfo.id);
                    }
                    else
                    {
                        //Debug.Log("=============Unity Log===============   PlaneExtractor -- Update   failed to extract plane cuz no plane got");
                    }

                    foreach (ulong key in ARFrame.trackableManager.planesTrackable.Keys)
                    {

                        EZVIOPlane plane = ARFrame.trackableManager.planesTrackable[key];
                        Matrix4x4 tr = new Matrix4x4();
                        for (int i = 0; i < 16; i++)
                        {
                            tr[i] = plane.transform_unity[i];
                        }

                        Matrix4x4 tr_tp = tr.transpose;

                        planeObj.position = tr_tp.GetColumn(3);
                        planeObj.rotation = tr_tp.rotation;

                        // scale
                        {
                            Vector3 l1 = new Vector3(plane.rect[0 * 3 + 0] - plane.rect[1 * 3 + 0], plane.rect[0 * 3 + 1] - plane.rect[1 * 3 + 1], plane.rect[0 * 3 + 2] - plane.rect[1 * 3 + 2]);
                            Vector3 l2 = new Vector3(plane.rect[1 * 3 + 0] - plane.rect[2 * 3 + 0], plane.rect[1 * 3 + 1] - plane.rect[2 * 3 + 1], plane.rect[1 * 3 + 2] - plane.rect[2 * 3 + 2]);

                            float d1 = l1.magnitude;
                            float d2 = l2.magnitude;

                            if (d1 > d2)
                            {
                                planeObj.scale = new Vector3(d1, 0.005f, d2);
                            }
                            else
                            {
                                planeObj.scale = new Vector3(d2, 0.005f, d1);
                            }
                        }

                        if (planeInfos.ContainsKey(key))
                        {
                            planeInfos[key] = planeObj;
                        }
                        else
                        {
                            planeInfos.Add(key, planeObj);
                        }
                    }

                }
                else
                {
                    //Debug.Log("=============Unity Log===============   PlaneExtractor -- Update   failed to extract plane cuz VIO state is not OK");
                }
            }
        }
    }
}

