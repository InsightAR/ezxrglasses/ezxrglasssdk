﻿using EZXR.Glass.Plane;
using EZXR.Glass.SixDof;
using UnityEditor;
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    public partial class ARAbilities : MonoBehaviour
    {
        #region SpatialTracking
        [MenuItem("GameObject/XR Abilities/SpatialTracking", false, 20)]
        public static void EnableSpatialTracking()
        {
            if (FindObjectOfType<HMDPoseTracker>() == null)
            {
                string filePath = "Assets/EZXRGlassSDK/Rumtime/SixDof/Prefabs/CameraRig.prefab";
                if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                {
                    filePath = AssetDatabase.GUIDToAssetPath("06bbeec2a98d8bd47a5d23c6d228eb60");
                }
                Runtime.PrefabUtility.InstantiatePrefabWithUndoAndSelection(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
            }
        }
        #endregion

        #region PlaneDetection
        [MenuItem("GameObject/XR Abilities/PlaneDetection", false, 20)]
        public static void EnablePlaneDetection()
        {
            if (FindObjectOfType<PlaneDetectionManager>() == null)
            {
                string filePath = "Assets/EZXRGlassSDK/Rumtime/SixDof/Prefabs/PlaneDetectionManager.prefab";
                if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                {
                    filePath = AssetDatabase.GUIDToAssetPath("1a44cb9e3dd5f5046a7333d3057af165");
                }
                Runtime.PrefabUtility.InstantiatePrefabWithUndoAndSelection(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
            }
        }
        #endregion
    }
}