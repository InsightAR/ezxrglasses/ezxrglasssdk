using EZXR.Glass.Plane;
#if SpatialComputing
using EZXR.Glass.SpatialComputing;
#endif
#if SpatialMesh
using EZXR.Glass.SpatialMesh;
#endif
#if Tracking2D
using EZXR.Glass.Tracking2D;
#endif
#if Tracking3D
using EZXR.Glass.Tracking3D;
#endif
using EZXR.Glass.Runtime;
using UnityEditor;
using UnityEngine;
using EZXR.Glass.Inputs;
using UnityEditor.PackageManager.Requests;
using UnityEditor.PackageManager;

[CustomEditor(typeof(XRMan))]
[CanEditMultipleObjects]
public class XRManInspector : Editor
{
    bool toggle_PlaneDetection = false;
    bool toggle_PlaneDetection_Last = false;
    bool toggle_SpatialMesh = false;
    bool toggle_SpatialMesh_Last = false;
    bool toggle_SpatialComputing = false;
    bool toggle_SpatialComputing_Last = false;
    bool toggle_ImageDetection = false;
    bool toggle_ImageDetection_Last = false;
    bool toggle_ObjectDetection = false;
    bool toggle_ObjectDetection_Last = false;

    XRMan xrMan;
    SerializedProperty showModelInEditor;
    SerializedProperty simulateInEditor;
    bool lastValue_SimulateInEditor = true;
    SerializedProperty remoteDebug;
    bool lastValue_RemoteDebug = false;
    SerializedProperty editorIP;
    SerializedProperty supportsMultipleLanguages;
    bool lastValue_supportsMultipleLanguages = false;


    private void OnEnable()
    {
        xrMan = (XRMan)target;

        showModelInEditor = serializedObject.FindProperty("showModelInEditor");

        simulateInEditor = serializedObject.FindProperty("simulateInEditor");
        remoteDebug = serializedObject.FindProperty("remoteDebug");
        editorIP = serializedObject.FindProperty("editorIP");

        supportsMultipleLanguages = serializedObject.FindProperty("supportsMultipleLanguages");
        string symbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android);
        if (!symbols.Contains("USE_LOCALIZATION"))
        {
            supportsMultipleLanguages.boolValue = false;
        }
        lastValue_supportsMultipleLanguages = supportsMultipleLanguages.boolValue;

        toggle_PlaneDetection = FindObjectOfType<PlaneDetectionManager>() != null;
        toggle_PlaneDetection_Last = toggle_PlaneDetection;

#if SpatialMesh
        toggle_SpatialMesh = FindObjectOfType<SpatialMeshManager>() != null;
        toggle_SpatialMesh_Last = toggle_SpatialMesh;
#endif

#if SpatialComputing
        toggle_SpatialComputing = FindObjectOfType<EZXRSpatialComputingManager>() != null;
        toggle_SpatialComputing_Last = toggle_SpatialComputing;
#endif

#if Tracking2D
        toggle_ImageDetection = FindObjectOfType<Tracking2DManager>() != null;
        toggle_ImageDetection_Last = toggle_ImageDetection;
#endif

#if Tracking3D
        toggle_ObjectDetection = FindObjectOfType<Tracking3DManager>() != null;
        toggle_ObjectDetection_Last = toggle_ObjectDetection;
#endif

        lastValue_SimulateInEditor = simulateInEditor.boolValue;
        lastValue_RemoteDebug = remoteDebug.boolValue;

#if EZXRForMRTK
        GameObject eventSystem = xrMan.attachments.Find("EventSystem").gameObject;
        eventSystem.SetActive(false);
#endif
    }

    GUIContent content_Dof = new GUIContent("SpatialTracking", "基础组件，不可以被关闭！");
    GUIContent content_InputSystem = new GUIContent("InputSystem", "基础组件，不可以被关闭！");
    GUIContent content_SpatialMesh = new GUIContent("SpatialMesh", "启用后将会开启空间网格生成，有一定的性能开销！");
    GUIContent content_ShowModelInEditor = new GUIContent("ShowModelInEditor", "运行时不显示模型（通常情况下为true）！");
    GUIContent content_SimulateInEditor = new GUIContent("SimulateInEditor", "在Editor中通过键盘鼠标模拟操作！");
    GUIContent content_RemoteDebug = new GUIContent("RemoteDebug", "启用远程调试功能！");
    GUIContent content_EditorIP = new GUIContent("EditorIP", "请输入Editor所在的电脑IP！");
    GUIContent content_SupportsMultipleLanguages = new GUIContent("SupportsMultipleLanguages", "启用多国语言支持！");
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        GUIStyle box = GUI.skin.box;

        #region SpatialTracking
        EditorGUILayout.BeginVertical(box);
        {
            EditorGUILayout.BeginToggleGroup(content_Dof, true);
            {
                //EditorGUILayout.PropertyField(m_dof, new GUIContent("DegreeOfFreedom"));
                toggle_SpatialMesh = EditorGUILayout.Toggle(content_SpatialMesh, toggle_SpatialMesh);
            }
            EditorGUILayout.EndToggleGroup();
        }
        EditorGUILayout.EndVertical();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        #endregion

        #region InputSystem
        EditorGUILayout.BeginVertical(box);
        {
            EditorGUILayout.BeginToggleGroup(content_InputSystem, true);
            {

            }
            EditorGUILayout.EndToggleGroup();
        }
        EditorGUILayout.EndVertical();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        #endregion

        #region PlaneDetection
        EditorGUILayout.BeginVertical(box);
        {
            toggle_PlaneDetection = EditorGUILayout.BeginToggleGroup("PlaneDetection", toggle_PlaneDetection);
            if (toggle_PlaneDetection_Last != toggle_PlaneDetection)
            {
                if (toggle_PlaneDetection)
                {
                    if (FindObjectOfType<PlaneDetectionManager>() == null)
                    {
                        string filePath = "Assets/EZXRGlassSDK/Runtime/SixDof/Prefabs/PlaneDetectionManager.prefab";
                        if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                        {
                            filePath = AssetDatabase.GUIDToAssetPath("1a44cb9e3dd5f5046a7333d3057af165");
                        }
                        EZXR.Glass.Runtime.PrefabUtility.InstantiatePrefabWithUndo(AssetDatabase.LoadAssetAtPath<GameObject>(filePath), xrMan.attachments);
                    }
                }
                else
                {
                    PlaneDetectionManager planeDetectionManager = FindObjectOfType<PlaneDetectionManager>();
                    if (planeDetectionManager != null)
                    {
                        DestroyImmediate(planeDetectionManager.gameObject);
                    }
                }
            }
            toggle_PlaneDetection_Last = toggle_PlaneDetection;
            EditorGUILayout.EndToggleGroup();
        }
        EditorGUILayout.EndVertical();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        #endregion

        #region ImageDetection
#if Tracking2D
        EditorGUILayout.BeginVertical(box);
        {
            toggle_ImageDetection = EditorGUILayout.BeginToggleGroup("ImageDetection", toggle_ImageDetection);
            if (toggle_ImageDetection_Last != toggle_ImageDetection)
            {
                if (toggle_ImageDetection)
                {
                    if (FindObjectOfType<Tracking2DManager>() == null)
                    {
                        string filePath = "Assets/EZXRGlassSDK/Runtime/Tracking2D/Prefabs/Tracking2DManager.prefab";
                        if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                        {
                            filePath = AssetDatabase.GUIDToAssetPath("c1ccc76a41cf54cdfb14a3580ad173f2");
                        }
                        EZXR.Glass.Runtime.PrefabUtility.InstantiatePrefabWithUndo(AssetDatabase.LoadAssetAtPath<GameObject>(filePath), xrMan.attachments);
                    }
                }
                else
                {
                    Tracking2DManager imageDetectionManager = FindObjectOfType<Tracking2DManager>();
                    if (imageDetectionManager != null)
                    {
                        DestroyImmediate(imageDetectionManager.gameObject);
                    }
                }
            }
            toggle_ImageDetection_Last = toggle_ImageDetection;
            EditorGUILayout.EndToggleGroup();
        }
        EditorGUILayout.EndVertical();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
#endif
        #endregion

        #region ObjectDetection
        //EditorGUILayout.BeginVertical(box);
        //{
        //    toggle_ObjectDetection = EditorGUILayout.BeginToggleGroup("ObjectDetection", toggle_ObjectDetection);
        //    if (toggle_ObjectDetection_Last != toggle_ObjectDetection)
        //    {
        //        if (toggle_ObjectDetection)
        //        {
        //            //if (FindObjectOfType<Tracking2DManager>() == null)
        //            //{
        //            //    string filePath = "Assets/EZXRGlassSDK/Core/Tracking2D/Prefabs/Tracking2DManager.prefab";
        //            //    if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
        //            //    {
        //            //        filePath = AssetDatabase.GUIDToAssetPath("c1ccc76a41cf54cdfb14a3580ad173f2");
        //            //    }
        //            //    EZXR.Glass.Runtime.PrefabUtility.InstantiatePrefabWithUndo(AssetDatabase.LoadAssetAtPath<GameObject>(filePath), xrMan.attachments);
        //            //}
        //        }
        //        else
        //        {

        //        }
        //    }
        //    toggle_ObjectDetection_Last = toggle_ObjectDetection;
        //    EditorGUILayout.EndToggleGroup();
        //}
        //EditorGUILayout.EndVertical();
        //EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        #endregion

        #region SpatialComputing
#if SpatialComputing
        EditorGUILayout.BeginVertical(box);
        {
            toggle_SpatialComputing = EditorGUILayout.BeginToggleGroup("SpatialComputing", toggle_SpatialComputing);
            if (toggle_SpatialComputing_Last != toggle_SpatialComputing)
            {
                if (toggle_SpatialComputing)
                {
                    if (FindObjectOfType<EZXRSpatialComputingManager>() == null)
                    {
                        string filePath = "Assets/EZXRGlassSDK/Runtime/SpatialComputing/Prefabs/SpatialComputingManager.prefab";
                        if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                        {
                            filePath = AssetDatabase.GUIDToAssetPath("2ff96ba100da74917a4b639f15c46cae");
                        }
                        EZXR.Glass.Runtime.PrefabUtility.InstantiatePrefabWithUndo(AssetDatabase.LoadAssetAtPath<GameObject>(filePath), xrMan.attachments);
                    }
                }
                else
                {
                    EZXRSpatialComputingManager spatialComputingManager = FindObjectOfType<EZXRSpatialComputingManager>();
                    if (spatialComputingManager != null)
                    {
                        DestroyImmediate(spatialComputingManager.gameObject);
                    }
                }
            }
            toggle_SpatialComputing_Last = toggle_SpatialComputing;
            EditorGUILayout.EndToggleGroup();
        }
        EditorGUILayout.EndVertical();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
#endif
        #endregion
        #region SpatialMesh
#if SpatialMesh
        EditorGUILayout.BeginVertical(box);
        {
            toggle_SpatialMesh = EditorGUILayout.BeginToggleGroup("SpatialMesh", toggle_SpatialMesh);
            if (toggle_SpatialMesh_Last != toggle_SpatialMesh)
            {
                if (toggle_SpatialMesh)
                {
                    if (FindObjectOfType<SpatialMeshManager>() == null)
                    {
                        string filePath = "Assets/EZXRGlassSDK/Runtime/SpatialMesh/Prefabs/SpatialMeshManager.prefab";
                        if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                        {
                            filePath = AssetDatabase.GUIDToAssetPath("9cb4ff0104470844eafdb0f39509e0b2");
                        }
                        EZXR.Glass.Runtime.PrefabUtility.InstantiatePrefabWithUndo(AssetDatabase.LoadAssetAtPath<GameObject>(filePath), xrMan.attachments);
                    }
                }
                else
                {
                    SpatialMeshManager spatialMeshManager = FindObjectOfType<SpatialMeshManager>();
                    if (spatialMeshManager != null)
                    {
                        DestroyImmediate(spatialMeshManager.gameObject);
                    }
                }
            }
            toggle_SpatialMesh_Last = toggle_SpatialMesh;
            EditorGUILayout.EndToggleGroup();
        }
        EditorGUILayout.EndVertical();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
#endif
        #endregion
        #region Extras
        EditorGUILayout.BeginVertical(box);
        {
            EditorGUILayout.BeginToggleGroup("Extras", true);
            {
                EditorGUILayout.BeginVertical(box);
                {
                    EditorGUILayout.PropertyField(showModelInEditor, content_ShowModelInEditor);
                    if (showModelInEditor.boolValue != xrMan.model_Head.activeSelf)
                    {
                        xrMan.model_Head.SetActive(showModelInEditor.boolValue);
                        xrMan.model_Body.SetActive(showModelInEditor.boolValue);
                        xrMan.model_Glass.SetActive(showModelInEditor.boolValue);
                    }
                }
                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical(box);
                {
                    EditorGUILayout.PropertyField(simulateInEditor, content_SimulateInEditor);
                    if (simulateInEditor.boolValue != lastValue_SimulateInEditor)
                    {
                        xrMan.handManager.enableHandSimulatorInEditor = simulateInEditor.boolValue;
                        EditorUtility.SetDirty(xrMan.handManager);

                        lastValue_SimulateInEditor = simulateInEditor.boolValue;
                        if (simulateInEditor.boolValue)
                        {
                            remoteDebug.boolValue = false;
                        }
                    }
                }
                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical(box);
                {
                    EditorGUILayout.PropertyField(remoteDebug, content_RemoteDebug);
                    if (remoteDebug.boolValue != lastValue_RemoteDebug)
                    {
                        lastValue_RemoteDebug = remoteDebug.boolValue;
                        xrMan.go_RemoteDebug.SetActive(remoteDebug.boolValue);

                        if (remoteDebug.boolValue)
                        {
                            simulateInEditor.boolValue = false;
                        }
                    }

                    if (remoteDebug.boolValue)
                    {
                        EditorGUI.BeginChangeCheck();
                        EditorGUILayout.PropertyField(editorIP, content_EditorIP);
                        if (EditorGUI.EndChangeCheck())
                        {
                            SyncGameObjects syncGameObjects = xrMan.go_RemoteDebug.GetComponent<SyncGameObjects>();
                            if (syncGameObjects != null)
                            {
                                syncGameObjects.editorIP = editorIP.stringValue;
                                EditorUtility.SetDirty(syncGameObjects);
                            }
                            SyncHandData syncHandData = xrMan.go_RemoteDebug.GetComponent<SyncHandData>();
                            if (syncHandData != null)
                            {
                                syncHandData.editorIP = editorIP.stringValue;
                                EditorUtility.SetDirty(syncHandData);
                            }
                        }
                    }
                }
                EditorGUILayout.EndVertical();


                EditorGUILayout.BeginVertical(box);
                {
                    EditorGUILayout.PropertyField(supportsMultipleLanguages, content_SupportsMultipleLanguages);
                    if (supportsMultipleLanguages.boolValue != lastValue_supportsMultipleLanguages)
                    {
                        if (supportsMultipleLanguages.boolValue == true)
                        {
                            enableMultiLanguages();
                        }
                        else {
                            disableMultiLanguages();
                        }
                        lastValue_supportsMultipleLanguages = supportsMultipleLanguages.boolValue;
                    }
                    
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndToggleGroup();
        }
        EditorGUILayout.EndVertical();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        #endregion

        serializedObject.ApplyModifiedProperties();
    }

    private void disableMultiLanguages() {
        string symbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android);
      
        if (symbols.Contains("USE_LOCALIZATION;"))
        {
            symbols = symbols.Replace("USE_LOCALIZATION;", "");
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, symbols);
        }
        else if (symbols.Contains("USE_LOCALIZATION"))
        {
            symbols = symbols.Replace("USE_LOCALIZATION", "");
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, symbols);
        }
    }
    private void enableMultiLanguages() {
        string symbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android);
        var packageInfo = UnityEditor.PackageManager.PackageInfo.FindForAssetPath("Packages/com.unity.localization");
        if (packageInfo != null)
        {
            if (!symbols.Contains("USE_LOCALIZATION"))
            {
                symbols += ";USE_LOCALIZATION";
                PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, symbols);
            }
        }
        else
        {
            importLocationPackage();
        }
    }
    private static AddRequest addRequest;
    private void importLocationPackage() {

        if (EditorUtility.DisplayDialog("Importing Package", "Start Import Packages: \ncom.unity.localization \n\nPlease make sure that this will not make conflicts!", "Yes"))
        {
            EditorApplication.update += importPackageProgress;

            EditorUtility.DisplayProgressBar("Importing Package", "Is importing package, please wait...", 0);

            addRequest = Client.Add("com.unity.localization");
            AssetDatabase.SaveAssets();
        }
    }

    private static void importPackageProgress()
    {
        if (addRequest != null && addRequest.IsCompleted)
        {
            switch (addRequest.Status)
            {
                case StatusCode.Failure:
                    EditorUtility.ClearProgressBar();
                    Debug.LogError("Couldn't add package '" + "': " + addRequest.Error.message);
                    break;

                case StatusCode.InProgress:
                    break;

                case StatusCode.Success:
                    EditorUtility.ClearProgressBar();
                    Debug.Log("Added package: " + addRequest.Result.name);
                    var symbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android);
                    if (!symbols.Contains("USE_LOCALIZATION"))
                    {
                        symbols += ";USE_LOCALIZATION";
                        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, symbols);
                    }
                    UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene());
                    AssetDatabase.SaveAssets();
                    break;
            }

            EditorApplication.update -= importPackageProgress;
        }
    }
    //private SpatialUIElement _target_SpatialUIElement;

    //SerializedProperty interactable;
    //SerializedProperty positionType;
    //SerializedProperty size;
    //SerializedProperty text;
    //SerializedProperty textColor;
    //SerializedProperty color;
    //SerializedProperty texture;
    //SerializedProperty sortingOrder;

    //protected virtual void OnEnable()
    //{
    //    _target_SpatialUIElement = target as SpatialUIElement;

    //    interactable = serializedObject.FindProperty("interactable");
    //    positionType = serializedObject.FindProperty("positionType");
    //    size = serializedObject.FindProperty("size");
    //    text = serializedObject.FindProperty("text");
    //    textColor = serializedObject.FindProperty("textColor");
    //    color = serializedObject.FindProperty("color");
    //    texture = serializedObject.FindProperty("texture");
    //    sortingOrder = serializedObject.FindProperty("sortingOrder");
    //}

    //public override void OnInspectorGUI()
    //{
    //    serializedObject.Update();

    //    EditorGUILayout.PropertyField(interactable, new GUIContent("Interactable", "是否可以与射线或者手进行交互"));
    //    EditorGUILayout.PropertyField(positionType);
    //    EditorGUILayout.PropertyField(size, new GUIContent("Size（单位：m）"));
    //    EditorGUILayout.PropertyField(text);
    //    EditorGUILayout.PropertyField(textColor);
    //    EditorGUILayout.PropertyField(color);
    //    EditorGUILayout.BeginHorizontal();
    //    {
    //        EditorGUILayout.PropertyField(texture);
    //        if (GUILayout.Button(new GUIContent("Set Physical Size", "按照SpatialUIController.unitsPerPixel中设定的“像素-UnityUnit转换比例”将Texture以换算后的物理尺寸绘制在空间中")))
    //        {
    //            Undo.RecordObject(_target_SpatialUIElement, "Set Physical Size");
    //            _target_SpatialUIElement.size = new Vector3(_target_SpatialUIElement.Texture.width * _target_SpatialUIElement.curUnitsPerPixel, _target_SpatialUIElement.Texture.height * _target_SpatialUIElement.curUnitsPerPixel, _target_SpatialUIElement.size.z);
    //            //_target_SpatialUIElement.curUnitsPerPixel = SpatialUIController.Instance.unitsPerPixel;
    //            EditorApplication.QueuePlayerLoopUpdate();
    //            //SceneView.RepaintAll();
    //        }
    //    }
    //    EditorGUILayout.EndHorizontal();

    //    EditorGUI.BeginChangeCheck();
    //    _target_SpatialUIElement.sortingOrder = EditorGUILayout.DelayedIntField("SortingOrder", _target_SpatialUIElement.sortingOrder);
    //    if (EditorGUI.EndChangeCheck())
    //    {
    //        if (_target_SpatialUIElement._text != null && _target_SpatialUIElement._text.GetComponent<MeshRenderer>() != null)
    //        {
    //            _target_SpatialUIElement._text.GetComponent<MeshRenderer>().sortingOrder = _target_SpatialUIElement.sortingOrder + 1;
    //        }
    //    }

    //    serializedObject.ApplyModifiedProperties();

    //    SceneView.RepaintAll();
    //}
}
