﻿using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

namespace EZXR.Glass.Runtime
{

    public partial class ARAbilities : MonoBehaviour
    {
        #region XRMan
        [MenuItem("ARSDK/Migrate To XRMan", false, 50)]
        public static void MigrateToXRMan() {

            if (FindObjectOfType<EZXR.Glass.Runtime.XRMan>() != null)
            {
                return;
            }
            if (FindObjectOfType<SixDof.HMDPoseTracker>() != null)
            {
                SixDof.HMDPoseTracker hmdPoseTracker = FindObjectOfType<SixDof.HMDPoseTracker>();
                if (hmdPoseTracker != null)
                {
                    DestroyImmediate(hmdPoseTracker.gameObject);
                }
            }
            if (FindObjectOfType<Inputs.InputSystem>() != null)
            {
                Inputs.InputSystem inputSystem = FindObjectOfType<Inputs.InputSystem>();
                if (inputSystem != null)
                {
                    DestroyImmediate(inputSystem.gameObject);
                }
            }
            if (FindObjectOfType<SystemManager>() != null)
            {
                SystemManager systemManager = FindObjectOfType<SystemManager>();
                if (systemManager != null)
                {
                    DestroyImmediate(systemManager.gameObject);
                }
            }
            if (FindObjectOfType<UI.SpatialUIController>() != null)
            {
                UI.SpatialUIController uIController = FindObjectOfType<UI.SpatialUIController>();
                if (uIController != null)
                {
                    DestroyImmediate(uIController.gameObject);
                }
            }
            if (FindObjectOfType<UI.SpatialUIEventSystem>() != null)
            {
                UI.SpatialUIEventSystem uIEventSystem = FindObjectOfType<UI.SpatialUIEventSystem>();
                if (uIEventSystem != null)
                {
                    DestroyImmediate(uIEventSystem.gameObject);
                }
            }
            if (FindObjectOfType<EventSystem>() != null)
            {
                EventSystem eventSystem = FindObjectOfType<EventSystem>();
                if (eventSystem != null)
                {
                    DestroyImmediate(eventSystem.gameObject);
                }
            }
            if (FindObjectOfType<EZXR.Glass.Runtime.XRMan>() == null)
            {
                string filePath = "Assets/EZXRGlassSDK/Runtime/XRMan/Prefabs/XRMan.prefab";
                if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                {
                    filePath = AssetDatabase.GUIDToAssetPath("1e6336cd06fea764b954b9511d91b102");
                }
                Runtime.PrefabUtility.InstantiatePrefabWithUndoAndSelection(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
            }
        }
        [MenuItem("GameObject/XR Abilities/XRMan", false, 20)]
        public static void EnableXRMan()
        {
            if (FindObjectOfType<EZXR.Glass.Runtime.XRMan>() != null) {
                return;
            }
            if (FindObjectOfType<EZXR.Glass.Runtime.XRMan>() == null)
            {
                string filePath = "Assets/EZXRGlassSDK/Runtime/XRMan/Prefabs/XRMan.prefab";
                if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                {
                    filePath = AssetDatabase.GUIDToAssetPath("1e6336cd06fea764b954b9511d91b102");
                }
                Runtime.PrefabUtility.InstantiatePrefabWithUndoAndSelection(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
            }
        }
        #endregion
    }
}