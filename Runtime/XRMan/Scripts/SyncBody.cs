using EZXR.Glass.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyncBody : MonoBehaviour
{
    Vector3 bodyForward;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        bodyForward = Vector3.ProjectOnPlane(XRMan.Head.forward, Vector3.up);
        XRMan.Body.rotation = Quaternion.LookRotation(bodyForward, Vector3.up);
    }
}
