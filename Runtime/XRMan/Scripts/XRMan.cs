using EZXR.Glass.Inputs;
using EZXR.Glass.SixDof;
#if SpatialComputing
using EZXR.Glass.SpatialComputing;
#endif
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    [ScriptExecutionOrder(-58)]
    public class XRMan : MonoBehaviour
    {
        #region 单例
        private static XRMan instance;
        //public static XRMan Instance
        //{
        //    get
        //    {
        //        return instance;
        //    }
        //}

        /// <summary>
        /// XRMan组件是否存在
        /// </summary>
        public static bool Exist
        {
            get
            {
                return instance != null;
            }
        }
        #endregion

        #region Encapsulation
        /// <summary>
        /// Head
        /// </summary>
        public static Transform Head
        {
            get
            {
                return HMDPoseTracker.Instance.Head;
            }
        }

        /// <summary>
        /// Eyes
        /// </summary>
        public static class Eyes
        {
            public static Camera Left
            {
                get
                {
                    return HMDPoseTracker.Instance.leftCamera;
                }
            }
            public static Camera Right
            {
                get
                {
                    return HMDPoseTracker.Instance.rightCamera;
                }
            }
            public static Camera Center
            {
                get
                {
                    return HMDPoseTracker.Instance.centerCamera;
                }
            }
        }


        /// <summary>
        /// 手（无论当前是否活动）
        /// </summary>
        public static class Hands
        {
            /// <summary>
            /// 记录了左手柄的所有信息
            /// </summary>
            public static HandInfo Left
            {
                get
                {
                    return InputSystem.LeftHand;
                }
            }
            /// <summary>
            /// 记录了右手柄的所有信息
            /// </summary>
            public static HandInfo Right
            {
                get
                {
                    return InputSystem.RightHand;
                }
            }
        }

        /// <summary>
        /// 手柄（无论当前是否活动）
        /// </summary>
        public static class Controllers
        {
            /// <summary>
            /// 记录了左手柄的所有信息
            /// </summary>
            public static ControllerInfo Left
            {
                get
                {
                    return InputSystem.LeftController;
                }
            }
            /// <summary>
            /// 记录了右手柄的所有信息
            /// </summary>
            public static ControllerInfo Right
            {
                get
                {
                    return InputSystem.RightController;
                }
            }
        }



        /// <summary>
        /// 当前活动的输入设备
        /// </summary>
        public static class Input
        {
            /// <summary>
            /// 当前活动的输入设备类型
            /// </summary>
            public static InputType Type
            {
                get
                {
                    return InputSystem.CurrentActiveControllerType;
                }
            }

            /// <summary>
            /// 记录了左手柄的所有信息
            /// </summary>
            public static InputInfoBase Left
            {
                get
                {
                    return InputSystem.Left;
                }
            }
            /// <summary>
            /// 记录了右手柄的所有信息
            /// </summary>
            public static InputInfoBase Right
            {
                get
                {
                    return InputSystem.Right;
                }
            }

            ///// <summary>
            ///// 手（无论当前是否活动）
            ///// </summary>
            //public static class Hands
            //{
            //    /// <summary>
            //    /// 记录了左手柄的所有信息
            //    /// </summary>
            //    public static HandInfo Left
            //    {
            //        get
            //        {
            //            return InputSystem.LeftHand;
            //        }
            //    }
            //    /// <summary>
            //    /// 记录了右手柄的所有信息
            //    /// </summary>
            //    public static HandInfo Right
            //    {
            //        get
            //        {
            //            return InputSystem.RightHand;
            //        }
            //    }
            //}

            ///// <summary>
            ///// 手柄（无论当前是否活动）
            ///// </summary>
            //public static class Controllers
            //{
            //    /// <summary>
            //    /// 记录了左手柄的所有信息
            //    /// </summary>
            //    public static ControllerInfo Left
            //    {
            //        get
            //        {
            //            return InputSystem.LeftController;
            //        }
            //    }
            //    /// <summary>
            //    /// 记录了右手柄的所有信息
            //    /// </summary>
            //    public static ControllerInfo Right
            //    {
            //        get
            //        {
            //            return InputSystem.RightController;
            //        }
            //    }
            //}
        }

        /// <summary>
        /// 身体
        /// </summary>
        public static Transform Body
        {
            get
            {
                return instance.model_Body.transform.parent;
            }
        }
        #endregion

        /// <summary>
        /// 是否显示模型（头、身体、眼镜）
        /// </summary>
        public bool showModelInEditor = true;
        public GameObject model_Head;
        public GameObject model_Body;
        public GameObject model_Glass;

        public ARHandManager handManager;

        /// <summary>
        /// 在Editor中通过键盘鼠标模拟操作（与remoteDebug互斥）
        /// </summary>
        public bool simulateInEditor;
        /// <summary>
        /// 开启远程调试（与simulateInEditor互斥）
        /// </summary>
        public bool remoteDebug;
        public static bool RemoteDebug
        {
            get
            {
                return instance == null ? false : instance.remoteDebug;
            }
        }
        public GameObject go_RemoteDebug;
        public string editorIP;

        [Tooltip("支持多国语言")]
        public bool supportsMultipleLanguages;

        /// <summary>
        /// 专门用于放置各种附加组件
        /// </summary>
        public Transform attachments;

        [HideInInspector]
        public static Vector3 position_SpatialTracking;
        [HideInInspector]
        public static Quaternion rotation_SpatialTracking;
        [HideInInspector]
        public static Vector3 position_SpatialComputing;
        [HideInInspector]
        public static Quaternion rotation_SpatialComputing;
        [HideInInspector]
        public static Matrix4x4 offset_SpatialComputing = Matrix4x4.identity;

        Vector3 tempPosition = Vector3.zero;
        Quaternion tempRotation = Quaternion.identity;
        Matrix4x4 tempMatrix;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Debug.LogError("场景中存在多个XRMan实例！请检查并移除多余实例！");
            }

            if (attachments == null)
            {
                attachments = transform.Find("Attachments");
            }
        }

        // Start is called before the first frame update
        void Start()
        {
#if UNITY_EDITOR
            model_Head.SetActive(showModelInEditor);
            model_Body.SetActive(showModelInEditor);
            model_Glass.SetActive(showModelInEditor);
#else
            model_Head.SetActive(false);
            model_Body.SetActive(false);
            model_Glass.SetActive(false);
#endif
            InvokeRepeating("DoFramerate", 0, 1);
        }

        void DoFramerate()
        {
            Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "DebugInfo", (1 / Time.smoothDeltaTime).ToString("##.#"));
        }

        // Update is called once per frame
        void Update()
        {
            if (HMDPoseTracker.Instance != null)
            {
                tempPosition = position_SpatialTracking;
                //tempRotation = rotation_SpatialTracking;
                //使用空值
                tempRotation = Quaternion.identity;
            }
#if SpatialComputing
            if (EZXRSpatialComputingManager.Instance != null)
            {
                tempMatrix = offset_SpatialComputing * Matrix4x4.TRS(tempPosition, tempRotation, Vector3.one);
                tempPosition = tempMatrix.GetColumn(3);
                tempRotation = tempMatrix.rotation;
            }
#endif
#if !UNITY_EDITOR
            transform.position = tempPosition;
            transform.rotation = tempRotation;
#endif
        }
    }
}