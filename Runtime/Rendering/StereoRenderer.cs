﻿using EZXR.Glass.SixDof;
using AOT;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using EZXR.Glass.Runtime;
using EZXR.Glass.Device;
using EZXR.Glass.Rendering;

namespace EZXR.Glass.Rendering
{
    public class StereoRenderer : MonoBehaviour
    {
        public enum RenderMode
        {
            Native,
            Unity,
        }
        public RenderMode renderMode;

        private static DisplaySize ds = new DisplaySize();
        private static DisplaySize targetRTSize = new DisplaySize();
        private static CamParams cp = new CamParams();


        private static StereoRenderer currentInstance = null;

        // 解决屏幕的问题
        // 备用，防止Native渲染链路无法建立。
        private static int mainDisplayIdx = -1;
        private static int targetDisplayIdx = -1;

        /// <summary> Renders the event delegate described by eventID. </summary>
        /// <param name="eventID"> Identifier for the event.</param>
        private delegate void RenderEventDelegate(int eventID);
        /// <summary> Handle of the render thread. </summary>
        private static RenderEventDelegate RenderThreadHandle = new RenderEventDelegate(RunOnRenderThread);
        /// <summary> The render thread handle pointer. </summary>
        private static IntPtr RenderThreadHandlePtr = Marshal.GetFunctionPointerForDelegate(RenderThreadHandle);

        private const int SETRENDERTEXTUREEVENT = 0x0001;
        private const int STARTNATIVERENDEREVENT = 0x0002;
        private const int RESUMENATIVERENDEREVENT = 0x0003;
        private const int PAUSENATIVERENDEREVENT = 0x0004;
        private const int STOPNATIVERENDEREVENT = 0x0005;
        private const int RECHANGENATIVERENDEREVENT = 0x0006;

        /// <summary> Gets or sets the native renderring. </summary>
        /// <value> The m native renderring. </value>
        private static EZXRNativeRenderring m_NativeRenderring;
        static EZXRNativeRenderring NativeRenderring
        {
            get
            {
                if (m_NativeRenderring == null)
                {
                    m_NativeRenderring = new EZXRNativeRenderring();
                }

                return m_NativeRenderring;
            }
            set
            {
                m_NativeRenderring = value;
            }
        }


        /// <summary> Values that represent eyes. </summary>
        public enum Eyes
        {
            /// <summary> An enum constant representing the left option. </summary>
            Left = 0,
            /// <summary> An enum constant representing the right option. </summary>
            Right = 1,
            /// <summary> An enum constant representing the count option. </summary>
            Count = 2
        }

        private static int _TextureBufferSize = 4;
        /// <summary> Number of eye textures. </summary>
        private static int EyeTextureCount = _TextureBufferSize * (int)Eyes.Count;
        /// <summary> The eye textures. </summary>
        private RenderTexture[] eyeTextures;
        /// <summary> Dictionary of rights. </summary>
        private Dictionary<RenderTexture, IntPtr> m_RTDict = new Dictionary<RenderTexture, IntPtr>();

        /// <summary> Values that represent renderer states. </summary>
        public enum RendererState
        {
            UnInitialized = 0,
            InitializedFailed = 1,
            Initialized = 2,
            Running = 3,
            Paused = 4,
            Destroyed = 5,
            Unknown = 6
        }

        /// <summary> The current state. </summary>
        internal RendererState m_CurrentState = RendererState.UnInitialized;

        /// <summary> Gets the current state. </summary>
        /// <value> The current state. </value>
        public RendererState currentState
        {
            get
            {
                return m_CurrentState;
            }
        }

        private int currentEyeTextureIdx = 0;

        private void Awake()
        {
            currentInstance = this;

            Debug.Log("=============Unity Log===============   StereoRenderer:" + this.GetInstanceID() + " -- Awake   display length " + Display.displays.Length);

            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            for (int i = 0; i < Display.displays.Length; i++)
            {
                Display.displays[i].Activate();
            }

            if (!Application.isEditor)
            {
                if (renderMode == RenderMode.Unity)
                {
                    ARConfig.DefaultConfig.unsetMTPMode_openMTP();
                    //StartUp_Unity();
                    StartCoroutine(StartUp_Unity());
                }
            }

            m_CurrentState = RendererState.UnInitialized;

            Camera.onPreRender += OnPreRenderCallback;
        }

        private void Start()
        {
            Debug.Log("=============Unity Log===============   StereoRenderer:" + this.GetInstanceID() + " -- Start");

            if (!Application.isEditor)
            {
                if (renderMode == RenderMode.Native)
                {
                    StartCoroutine(StartUp_Native());
                }
            }
        }

        private void Update()
        {
            if (!Application.isEditor)
            {
                if (m_CurrentState == RendererState.Running && ARFrame.SessionStatus == EZVIOState.EZVIOCameraState_Tracking)
                {
                    if (renderMode == RenderMode.Native)
                    {
                        XRMan.Eyes.Left.targetTexture = eyeTextures[currentEyeTextureIdx];
                        XRMan.Eyes.Right.targetTexture = eyeTextures[currentEyeTextureIdx + 1];
                        currentEyeTextureIdx = ((currentEyeTextureIdx + 2) % EyeTextureCount + EyeTextureCount) % EyeTextureCount;
                    }

                    Matrix4x4 leftProj = new Matrix4x4();
                    Matrix4x4 rightProj = new Matrix4x4();
                    for (int i = 0; i < 4; i++)
                        for (int j = 0; j < 4; j++)
                        {
                            leftProj[i, j] = cp.leftProjection[i * 4 + j];
                            rightProj[i, j] = cp.rightProjection[i * 4 + j];
                        }
                    //Debug.Log("=============Unity Log===============   StereoRenderer Update,  left proj "
                    //    + leftProj[0, 0] + " " + leftProj[0, 1] + " " + leftProj[0, 2] + " " + leftProj[0, 3] + " "
                    //     + leftProj[1, 0] + " " + leftProj[1, 1] + " " + leftProj[1, 2] + " " + leftProj[1, 3] + " "
                    //      + leftProj[2, 0] + " " + leftProj[2, 1] + " " + leftProj[2, 2] + " " + leftProj[2, 3] + " "
                    //       + leftProj[3, 0] + " " + leftProj[3, 1] + " " + leftProj[3, 2] + " " + leftProj[3, 3]);
                    //Debug.Log("=============Unity Log===============   StereoRenderer Update,  right proj "
                    //    + rightProj[0, 0] + " " + rightProj[0, 1] + " " + rightProj[0, 2] + " " + rightProj[0, 3] + " "
                    //     + rightProj[1, 0] + " " + rightProj[1, 1] + " " + rightProj[1, 2] + " " + rightProj[1, 3] + " "
                    //      + rightProj[2, 0] + " " + rightProj[2, 1] + " " + rightProj[2, 2] + " " + rightProj[2, 3] + " "
                    //       + rightProj[3, 0] + " " + rightProj[3, 1] + " " + rightProj[3, 2] + " " + rightProj[3, 3]);

                    XRMan.Eyes.Left.projectionMatrix = leftProj;
                    XRMan.Eyes.Right.projectionMatrix = rightProj;

                    //Debug.Log("=============Unity Log===============   StereoRenderer Update,  XRMan.Eyes.Left.projectionMatrix "
                    //    + XRMan.Eyes.Left.projectionMatrix[0, 0] + " " + XRMan.Eyes.Left.projectionMatrix[0, 1] + " " + XRMan.Eyes.Left.projectionMatrix[0, 2] + " " + XRMan.Eyes.Left.projectionMatrix[0, 3] + " "
                    //     + XRMan.Eyes.Left.projectionMatrix[1, 0] + " " + XRMan.Eyes.Left.projectionMatrix[1, 1] + " " + XRMan.Eyes.Left.projectionMatrix[1, 2] + " " + XRMan.Eyes.Left.projectionMatrix[1, 3] + " "
                    //      + XRMan.Eyes.Left.projectionMatrix[2, 0] + " " + XRMan.Eyes.Left.projectionMatrix[2, 1] + " " + XRMan.Eyes.Left.projectionMatrix[2, 2] + " " + XRMan.Eyes.Left.projectionMatrix[2, 3] + " "
                    //       + XRMan.Eyes.Left.projectionMatrix[3, 0] + " " + XRMan.Eyes.Left.projectionMatrix[3, 1] + " " + XRMan.Eyes.Left.projectionMatrix[3, 2] + " " + XRMan.Eyes.Left.projectionMatrix[3, 3]);
                    //Debug.Log("=============Unity Log===============   StereoRenderer Update,  right proj "
                    //    + XRMan.Eyes.Right.projectionMatrix[0, 0] + " " + XRMan.Eyes.Right.projectionMatrix[0, 1] + " " + XRMan.Eyes.Right.projectionMatrix[0, 2] + " " + XRMan.Eyes.Right.projectionMatrix[0, 3] + " "
                    //     + XRMan.Eyes.Right.projectionMatrix[1, 0] + " " + XRMan.Eyes.Right.projectionMatrix[1, 1] + " " + XRMan.Eyes.Right.projectionMatrix[1, 2] + " " + XRMan.Eyes.Right.projectionMatrix[1, 3] + " "
                    //      + XRMan.Eyes.Right.projectionMatrix[2, 0] + " " + XRMan.Eyes.Right.projectionMatrix[2, 1] + " " + XRMan.Eyes.Right.projectionMatrix[2, 2] + " " + XRMan.Eyes.Right.projectionMatrix[2, 3] + " "
                    //       + XRMan.Eyes.Right.projectionMatrix[3, 0] + " " + XRMan.Eyes.Right.projectionMatrix[3, 1] + " " + XRMan.Eyes.Right.projectionMatrix[3, 2] + " " + XRMan.Eyes.Right.projectionMatrix[3, 3]);

                    //Debug.Log("=============Unity Log===============   StereoRenderer Update,  leftCamera.fieldOfView "
                    //    + XRMan.Eyes.Left.fieldOfView);
                    //Debug.Log("=============Unity Log===============   StereoRenderer Update,  rightCamera.fieldOfView "
                    //    + XRMan.Eyes.Right.fieldOfView);
                }
            }
        }


        private double cachedPreRenderTime = 0.0f;
        void OnPreRenderCallback(Camera cam)
        {
            if (cam.name == "LeftCamera")
            {
                cachedPreRenderTime = NativeTracking.GetSystemTime() / 1e9;
                //Debug.Log("=============Unity Log===============   StereoRenderer: + OnPreRenderCallback() cursystime: " + NativeTracking.GetSystemTime() + " cachedPreRenderTime: " + cachedPreRenderTime);
            }
        }

        private void OnDestroy()
        {
            Debug.Log("=============Unity Log===============   StereoRenderer:" + this.GetInstanceID() + " -- OnDestroy begins");

            currentInstance = null;

            if (renderMode == RenderMode.Native)
            {
                if (m_CurrentState != RendererState.Destroyed)
                {
                    //GL.IssuePluginEvent(RenderThreadHandlePtr, STOPNATIVERENDEREVENT);
                    NativeRenderring?.Stop();
                    NativeRenderring = null;

                    m_CurrentState = RendererState.Destroyed;
                }
            }

            Camera.onPreRender -= OnPreRenderCallback;

            Debug.Log("=============Unity Log===============   StereoRenderer:" + this.GetInstanceID() + " -- OnDestroy ends");
        }

        //private void StartUp_Unity()
        private IEnumerator StartUp_Unity()
        {
            //XRMan.Eyes.Left.rect = new Rect(0, 0, 0.5f, 1);
            //XRMan.Eyes.Right.rect = new Rect(0.5f, 0, 0.5f, 1);

            //yield return new WaitForEndOfFrame();
            //yield return new WaitForEndOfFrame();
            //yield return new WaitForEndOfFrame();

            while (!NativeTracking.GetIsARSessionInited())
            {
                yield return new WaitForEndOfFrame();
            }

            Debug.Log("=============Unity Log===============   StereoRenderer -- StartUp_Unity   get started");

            NativeTracking.GetCameraParams(ref cp);

            Debug.Log("=============Unity Log===============   StereoRenderer -- StartUp_Unity   CameraParams: " + cp.width + ", " + cp.height + ", fov=(" + cp.fov[0] + ", " + cp.fov[1] + ")");

            m_CurrentState = RendererState.Running;
        }

        /// <summary> Prepares this object for use. </summary>
        /// <returns> An IEnumerator. </returns>
        private IEnumerator StartUp_Native()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();

            while (!NativeTracking.GetIsARSessionInited())
            {
                yield return new WaitForEndOfFrame();
            }

            Debug.Log("=============Unity Log===============   StereoRenderer -- StartUp   get started");

            NativeTracking.GetGlassDisplaySize(ref ds);
            NativeTracking.GetTargetRenderSize(ref targetRTSize);
            NativeTracking.GetCameraParams(ref cp);

            Debug.Log("=============Unity Log===============   StereoRenderer -- StartUp   desired glass size: " + ds.width + " " + ds.height + " target RenderTexture Size:(" + targetRTSize.width + "," + targetRTSize.height + ")");
            Debug.Log("=============Unity Log===============   StereoRenderer -- StartUp   desired CameraParams: " + cp.width + ", " + cp.height + ", fov=(" + cp.fov[0] + ", " + cp.fov[1] + ")");

            CreateRenderTextures();

            StartCoroutine(RenderCoroutine());

            GL.IssuePluginEvent(RenderThreadHandlePtr, STARTNATIVERENDEREVENT);
        }

        /// <summary> Creates render textures. </summary>
        private void CreateRenderTextures()
        {
            EyeTextureCount = _TextureBufferSize * (int)Eyes.Count;
            eyeTextures = new RenderTexture[EyeTextureCount];
            for (int i = 0; i < EyeTextureCount; i++)
            {
                eyeTextures[i] = EZGlassARRendererUtility.CreateRenderTexture(targetRTSize.width, targetRTSize.height, 24, RenderTextureFormat.DefaultHDR, false);  // default may ARGB
                m_RTDict.Add(eyeTextures[i], eyeTextures[i].GetNativeTexturePtr());
            }

            XRMan.Eyes.Left.targetTexture = eyeTextures[0];
            XRMan.Eyes.Right.targetTexture = eyeTextures[1];
        }

        /// <summary> Renders the coroutine. </summary>
        /// <returns> An IEnumerator. </returns>
        private IEnumerator RenderCoroutine()
        {
            WaitForEndOfFrame delay = new WaitForEndOfFrame();
            WaitForSecondsRealtime delayMoment = new WaitForSecondsRealtime(0.2f);
            yield return delay;
            bool isWaitMoreTime = false;

            while (true)
            {
                if (isWaitMoreTime)
                {
                    isWaitMoreTime = false;
                    yield return delayMoment;
                }
                else
                {
                    yield return delay;
                }

                //Debug.Log("=============Unity Log===============   StereoRenderer -- RenderCoroutine  m_CurrentState " + m_CurrentState);

                if (m_CurrentState == RendererState.Initialized)
                {
                    Debug.Log("=============Unity Log===============   StereoRenderer -- RenderCoroutine  renderer state would come from Initialized into Running");
                    m_CurrentState = RendererState.Running;
                }

                if (m_CurrentState != RendererState.Running)
                {
                    Debug.Log("=============Unity Log===============   StereoRenderer -- RenderCoroutine  m_CurrentState is not Running, passed");
                    isWaitMoreTime = true;
                    continue;
                }

                //Debug.Log("=============Unity Log===============   StereoRenderer -- RenderCoroutine  SessionStatus " + ARFrame.SessionStatus);
                if (ARFrame.SessionStatus != EZVIOState.EZVIOCameraState_Tracking)
                {
                    Debug.Log("=============Unity Log===============   StereoRenderer -- RenderCoroutine  ARFrame.SessionStatus is not Tracking, passed");
                    isWaitMoreTime = true;
                    continue;
                }

                FrameInfo info = new FrameInfo(IntPtr.Zero, IntPtr.Zero, ARFrame.OriginVIOResult.Twc, ARFrame.OriginVIOResult.timestamp, cachedPreRenderTime, ARFrame.VIOResultFetchTimeNS / 1e9);

                IntPtr left_target, right_target;
                if (!m_RTDict.TryGetValue(XRMan.Eyes.Left.targetTexture, out left_target)) continue;
                if (!m_RTDict.TryGetValue(XRMan.Eyes.Right.targetTexture, out right_target)) continue;
                info.leftTex = left_target;
                info.rightTex = right_target;

                //Debug.Log("=============Unity Log===============   UICamController -- RenderCoroutine  before SetRenderFrameInfo");

                SetRenderFrameInfo(info);
            }
        }

        /// <summary> Sets render frame information. </summary>
        /// <param name="frame"> The frame.</param>
        private static void SetRenderFrameInfo(FrameInfo frame)
        {
            //Debug.Log("=============Unity Log===============   UICamController -- SetRenderFrameInfo frameinfo " + frame.leftTex + " " + frame.rightTex + " " + frame.Twc + " " + frame.TwcTime);

            NativeRenderring.WriteFrameData(frame);
            GL.IssuePluginEvent(RenderThreadHandlePtr, SETRENDERTEXTUREEVENT);
        }

        public static void OnHMDPoseTrackerDofChanged(HMDPoseTracker.DegreeOfFreedom dof)
        {
            EZXRRendererHotConfig cfg = new EZXRRendererHotConfig(HMDPoseTracker.Instance.degreeOfFreedom != HMDPoseTracker.DegreeOfFreedom.ZeroDof);
            NativeRenderring.WriteRendererHotConfig(cfg);
            GL.IssuePluginEvent(RenderThreadHandlePtr, RECHANGENATIVERENDEREVENT);
        }

        /// <summary> Executes the 'on render thread' operation. </summary>
        /// <param name="eventID"> Identifier for the event.</param>
        [MonoPInvokeCallback(typeof(RenderEventDelegate))]
        private static void RunOnRenderThread(int eventID)
        {
            if (eventID == STARTNATIVERENDEREVENT)
            {
                int result = NativeRenderring.Start();
                Debug.Log("=============Unity Log===============   StereoRenderer -- RunOnRenderThread STARTNATIVERENDEREVENT result: " + result);

                if (result != 0 && result != -3)
                {
                    Debug.Log("=============Unity Log===============   StereoRenderer -- RunOnRenderThread STARTNATIVERENDEREVENT start failed");
                    if (StereoRenderer.currentInstance != null)
                        StereoRenderer.currentInstance.m_CurrentState = RendererState.InitializedFailed;
                    return;
                }
                if (StereoRenderer.currentInstance != null)
                    StereoRenderer.currentInstance.m_CurrentState = RendererState.Initialized;

                // @xunighao: assumed openMTP is set on at now;
                if (SessionManager.Instance.ARInitConfig.getMTPMode_warping_1() ==
                    (HMDPoseTracker.Instance.degreeOfFreedom == HMDPoseTracker.DegreeOfFreedom.SixDof ||
                    HMDPoseTracker.Instance.degreeOfFreedom == HMDPoseTracker.DegreeOfFreedom.ThreeDof))
                {
                    Debug.Log("=============Unity Log===============   StereoRenderer -- RunOnRenderThread STARTNATIVERENDEREVENT, no need to do hotupdate to EZXRRenderer; " +
                        " ARInitConfig‘s warp: " + SessionManager.Instance.ARInitConfig.getMTPMode_warping_1() +
                        " camera instance dof: " + HMDPoseTracker.Instance.degreeOfFreedom);
                }
                else
                {
                    Debug.Log("=============Unity Log===============   StereoRenderer -- RunOnRenderThread STARTNATIVERENDEREVENT, to do hotupdate to EZXRRenderer; " +
        " ARInitConfig‘s warp: " + SessionManager.Instance.ARInitConfig.getMTPMode_warping_1() +
        " camera instance dof: " + HMDPoseTracker.Instance.degreeOfFreedom);

                    EZXRRendererHotConfig cfg = new EZXRRendererHotConfig(HMDPoseTracker.Instance.degreeOfFreedom != HMDPoseTracker.DegreeOfFreedom.ZeroDof);
                    NativeRenderring.WriteRendererHotConfig(cfg);
                    NativeRenderring.HotUpdateRenderer();
                }

                HMDPoseTracker.Instance.PosetrackerChangedListener += OnHMDPoseTrackerDofChanged;

            }
            else if (eventID == RESUMENATIVERENDEREVENT)
            {
                Debug.Log("=============Unity Log===============   StereoRenderer -- RunOnRenderThread RESUMENATIVERENDEREVENT");
#if EZXRCS
            if (StereoRenderer.currentInstance != null)
            {
                Debug.Log("=============Unity Log===============   StereoRenderer -- RunOnRenderThread RESUMENATIVERENDEREVENT  current renderer state: " + StereoRenderer.currentInstance.m_CurrentState);

                if (StereoRenderer.currentInstance.m_CurrentState == RendererState.Paused)
                {
                    Debug.Log("=============Unity Log===============   StereoRenderer -- RunOnRenderThread RESUMENATIVERENDEREVENT   resumeMtp called");
                    NativeTracking.resumeMtp();
                    StereoRenderer.currentInstance.m_CurrentState = RendererState.Initialized;
                }
                else
                {
                    Debug.Log("=============Unity Log===============   StereoRenderer -- RunOnRenderThread RESUMENATIVERENDEREVENT   resumeMtp not called, reason is the renderer state");
                }
            }
            else
            {
                Debug.Log("=============Unity Log===============   StereoRenderer -- RunOnRenderThread RESUMENATIVERENDEREVENT unknown error");
            }
#endif
            }
            else if (eventID == PAUSENATIVERENDEREVENT)
            {
                Debug.Log("=============Unity Log===============   StereoRenderer -- RunOnRenderThread PAUSENATIVERENDEREVENT");
#if EZXRCS
            if (StereoRenderer.currentInstance != null)
            {
                Debug.Log("=============Unity Log===============   StereoRenderer -- RunOnRenderThread PAUSENATIVERENDEREVENT  current renderer state: " + StereoRenderer.currentInstance.m_CurrentState);

                if (StereoRenderer.currentInstance.m_CurrentState == RendererState.Initialized || StereoRenderer.currentInstance.m_CurrentState == RendererState.Running)
                {
                    Debug.Log("=============Unity Log===============   StereoRenderer -- RunOnRenderThread PAUSENATIVERENDEREVENT   pauseMTP called");
                    NativeTracking.pauseMtp();
                    StereoRenderer.currentInstance.m_CurrentState = RendererState.Paused;
                }
                else
                {
                    Debug.Log("=============Unity Log===============   StereoRenderer -- RunOnRenderThread PAUSENATIVERENDEREVENT   pauseMTP not called, reason is the renderer state");
                }
            }
            else
            {
                Debug.Log("=============Unity Log===============   StereoRenderer -- RunOnRenderThread PAUSENATIVERENDEREVENT unknown error");
            }
#endif
            }
            else if (eventID == STOPNATIVERENDEREVENT)
            {
                //NativeRenderring?.Stop();
                //NativeRenderring = null;
            }
            else if (eventID == SETRENDERTEXTUREEVENT)
            {
                NativeRenderring?.DoExtendedRenderring();
            }
            else if (eventID == RECHANGENATIVERENDEREVENT)
            {
                Debug.Log("=============Unity Log===============   StereoRenderer -- RunOnRenderThread RECHANGENATIVERENDEREVENT HotUpdateRenderer ");
                NativeRenderring?.HotUpdateRenderer();
            }
        }

        private void OnApplicationPause(bool pause)
        {
            if (!Application.isEditor)
            {
                if (pause) // pause
                {
                    Pause();
                }
                else // resume
                {
                    // delay resume
                    //Invoke("Resume", 0.3f);

                    // resume immediately
                    Resume();
                }
            }
        }

        /// <summary> Pause render. </summary>
        public void Pause()
        {
            if (renderMode == RenderMode.Native)
            {
                GL.IssuePluginEvent(RenderThreadHandlePtr, PAUSENATIVERENDEREVENT);
            }
        }

        /// <summary> Resume render. </summary>
        public void Resume()
        {
            if (renderMode == RenderMode.Native)
            {
                GL.IssuePluginEvent(RenderThreadHandlePtr, RESUMENATIVERENDEREVENT);
            }
        }

        public void OnClickReCenterButton()
        {
            Debug.Log("=============Unity Log===============   StereoRenderer -- OnClickReCenterButton");
            if (ARFrame.SessionStatus != EZVIOState.EZVIOCameraState_Tracking)
                return;

            Debug.Log("=============Unity Log===============   StereoRenderer -- Start ReCenter");
            ARFrame.ReCenter();
        }
    }
}

