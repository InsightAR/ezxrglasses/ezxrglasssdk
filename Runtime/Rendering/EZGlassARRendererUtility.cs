﻿using UnityEngine;

namespace EZXR.Glass.Rendering
{

    public class EZGlassARRendererUtility
    {
        public static RenderTexture CreateRenderTexture(int width, int height, int depth = 24, RenderTextureFormat format = RenderTextureFormat.DefaultHDR, bool usequaAnti = true)
        {
            var rt = new RenderTexture(width, height, depth, format);
            /*
            if (SystemInfo.IsFormatSupported(UnityEngine.Experimental.Rendering.GraphicsFormat.R32G32B32A32_SFloat, UnityEngine.Experimental.Rendering.FormatUsage.Render))
            {
                rt.graphicsFormat = UnityEngine.Experimental.Rendering.GraphicsFormat.R32G32B32A32_SFloat;
            }
            if (SystemInfo.IsFormatSupported(UnityEngine.Experimental.Rendering.GraphicsFormat.D32_SFloat_S8_UInt, UnityEngine.Experimental.Rendering.FormatUsage.Render))
            {
                rt.depthStencilFormat = UnityEngine.Experimental.Rendering.GraphicsFormat.D32_SFloat_S8_UInt;
            }
            */
            rt.wrapMode = TextureWrapMode.Clamp;
            if (QualitySettings.antiAliasing > 0 && usequaAnti)
            {
                rt.antiAliasing = QualitySettings.antiAliasing;
            }
            rt.Create();
            return rt;
        }
    }
}