﻿using EZXR.Glass.Tracking2D;
using UnityEditor;
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    public partial class ARAbilities : MonoBehaviour
    {
        #region Tracking2D
        [MenuItem("GameObject/XR Abilities/Tracking2D", false, 20)]
        public static void EnableTracking2D()
        {
            if (FindObjectOfType<Tracking2DManager>() == null)
            {
                string filePath = "Assets/EZXRGlassSDK/Core/Tracking2D/Prefabs/Tracking2DManager.prefab";
                if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                {
                    filePath = AssetDatabase.GUIDToAssetPath("c1ccc76a41cf54cdfb14a3580ad173f2");
                }
                Runtime.PrefabUtility.InstantiatePrefabWithUndoAndSelection(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
            }
        }
        #endregion
    }
}