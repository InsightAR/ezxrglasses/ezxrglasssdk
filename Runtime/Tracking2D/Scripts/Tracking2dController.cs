using System;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.Tracking2D
{
    public class Tracking2dController
    {
        private string m_asset_path = "";
        private IntPtr m_tracking2d;

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="model_path">模型路径</param>
        /// <returns></returns>
        public int CreateTracking2d(string model_path)
        {
            if (m_tracking2d != System.IntPtr.Zero)
            {
                Debug.LogError("tracking2d is created");
                return 0;
            }
            m_asset_path = model_path;
            Debug.Log("model_path:" + model_path);
            m_tracking2d = ExternApi.create2dDetector(model_path);
            if (m_tracking2d == System.IntPtr.Zero)
            {
                Debug.LogError("tracking2d ctrate failed");
                return -1;
            }
            return 1;
        }
        
        /// <summary>
        /// 释放
        /// </summary>
        public void DestroyTracking2d()
        {
            if (m_tracking2d != System.IntPtr.Zero)
            {
                ExternApi.destroy2dDetector(m_tracking2d);
                m_tracking2d = System.IntPtr.Zero;
            }
        }

        /// <summary>
        /// 开始追踪
        /// </summary>
        /// <returns></returns>
        public int SetImage(IntPtr imageptr, float[] extrisic, float[] intrinsic, double timestamp, int width, int height, int format)
        {
            if (m_tracking2d == System.IntPtr.Zero)
            {
                Debug.LogError("tracking2d is not created");
                return 0;
            }
            
            if (imageptr == System.IntPtr.Zero)
            {
                Debug.LogError("image is null");
                return 0;
            }
   
            EZXR_InputImage inputImage = new EZXR_InputImage();
            inputImage.imagePtr = imageptr;
            inputImage.extrisic = extrisic;
            inputImage.intrisic = intrinsic;
            inputImage.timestamp = timestamp;
            inputImage.width = width;
            inputImage.height = height;
            inputImage.format = format;
            int ret = ExternApi.setImage(m_tracking2d, inputImage);
            return ret;
        } 

        public EZXR_Detect2DResult GetDetectedMarker()
        {
            EZXR_Detect2DResult result = new EZXR_Detect2DResult();
            result.state = (int)EZXR_Detect2DState.EZXR_Detect2DState_UnInit;
            if (m_tracking2d == System.IntPtr.Zero)
            {
                Debug.LogError("tracking2d is not created");
                return result;
            }

            result = ExternApi.getDetectedMarker(m_tracking2d);
            return result;
        }
    }
}