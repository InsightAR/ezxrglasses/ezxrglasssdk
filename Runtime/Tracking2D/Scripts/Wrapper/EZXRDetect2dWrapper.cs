using System;
using System.Runtime.InteropServices;

namespace EZXR.Glass.Tracking2D
{
     public static partial class ExternApi
    {
#if (UNITY_IOS || UNITY_TVOS || UNITY_WEBGL) && !UNITY_EDITOR
        public const string LIBRARY_NAME = "__Internal";
#else
        public const string LIBRARY_NAME = "ezglass2dtrack";
#endif

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr create2dDetector(string assetpath);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern void destroy2dDetector(IntPtr detector);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern int setImage(IntPtr detector, EZXR_InputImage inputImage);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        public static extern EZXR_Detect2DResult getDetectedMarker(IntPtr detector);
    }
}