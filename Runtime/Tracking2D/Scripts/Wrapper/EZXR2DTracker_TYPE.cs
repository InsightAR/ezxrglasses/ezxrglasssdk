using System;
using System.Runtime.InteropServices;

namespace EZXR.Glass.Tracking2D
{
    public enum EZVIOImageFormat
    {
        EZVIOImageFormat_YUV420v = 0, //be used in iOS
        EZVIOImageFormat_YUV420f = 1, //be used in iOS
        EZVIOImageFormat_BGRA = 2, //be used in iOS
        EZVIOImageFormat_YUV420A = 3, //YUV_420_888_Android (YYYYYYYY UUVV),java:ImageFormat.YUV_420_888
        EZVIOImageFormat_RGB = 4,
        EZVIOImageFormat_RGBA = 5,
        EZVIOImageFormat_GREY = 6,
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct EZVIOImageRes
    {
        public float width;
        public float height;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct EZVIOCamIntrinsic
    {
        public float fx;
        public float fy;
        public float cx;
        public float cy;
        public float k1;
        public float k2;
        public float k3;
        public float k4;
    }

    public enum EZVIOImagePointerType
    {
        EZVIOImagePointerType_RawData, //please use this one
        EZVIOImagePointerType_iOS_CVPixelRef,
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct EZVIOInputImage
    {
        public IntPtr fullImg; //full image pointer (only needed in stereo mode, e.g. 1280x400)
        public EZVIOImagePointerType imgPtrType;
        public EZVIOImageFormat imgFormat;
        public EZVIOImageRes imgRes;
        public EZVIOCamIntrinsic camIntrinsic;
        public double timestamp;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct EZXR_Pose
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public float[] transform;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public float[] quaternion;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public float[] center;
        public double timestamp;
    }
    [StructLayout(LayoutKind.Sequential)]
    public struct EZXR_MarkerAnchor
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
        public string name;
        public EZXR_Pose pose;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public float[] physicalSize;
        public bool isValid;
    }
    
    public enum EZXR_Detect2DState
    {
        EZXR_Detect2DState_UnInit = 0,
        EZXR_Detect2DState_InitFailed = 1,
        EZXR_Detect2DState_UnInitSuccess = 2,
        EZXR_Detect2DState_Detecting = 3,
        EZXR_Detect2DState_Tracking = 4,
        EZXR_Detect2DState_Lost = 5
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct EZXR_Detect2DResult
    {
        public EZXR_MarkerAnchor markerAnchor;
        public EZXR_Pose imagePose;
        public int state;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct  EZXR_InputImage {
        public IntPtr imagePtr;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public float[] extrisic;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public float[] intrisic;
        public double timestamp;
        public int width;
        public int height;
        public int format;
    };
}
