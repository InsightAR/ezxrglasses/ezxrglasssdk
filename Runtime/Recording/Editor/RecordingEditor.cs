﻿using EZXR.Glass.Recording;
using UnityEditor;
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    public class RecordingEditor : MonoBehaviour
    {
        //[MenuItem("GameObject/XR Abilities/Additional.../RecordingManager", false, 100)]
        public static void EnableRecordingManager()
        {
            if (FindObjectOfType<RecordingManager>() == null)
            {
                string filePath = "Assets/EZXRGlassSDK/Runtime/Recording/Prefabs/RecordingManager.prefab";
                if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                {
                    filePath = AssetDatabase.GUIDToAssetPath("032a8da581f9c4244b251db68247618e");
                }
                PrefabUtility.InstantiatePrefabWithUndoAndSelection(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
            }
        }
    }
}