﻿using System.Collections;
using UnityEngine;
using System.IO;
using NatSuite.Recorders;
using NatSuite.Recorders.Clocks;
using NatSuite.Recorders.Inputs;
using System;
using EZXR.Glass.Runtime;
using EZXR.Glass.Device;

namespace EZXR.Glass.Recording
{
    public class RecordingManager : MonoBehaviour
    {
        private static RecordingManager instance;
        public static RecordingManager Instance
        {
            get
            {
                return instance;
            }
        }

        /// <summary>
        /// 保存状态，0是默认状态，1是正在保存，2是保存完毕
        /// </summary>
        public int savingStatus;

        public bool isRecording;

        private int width = 1280;
        private int height = 960;
        private IMediaRecorder recorder;
        private CameraInput cameraInput;
        private AudioInput audioInput;

        bool beSave = false;

        private Coroutine captureCoroutine;
        private const string gallery_savedDir = "/storage/emulated/0/Pictures/Screenshots";


        private void Awake()
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            //ARRenderRGB.SetRGBResolution(width, height);
        }

        // Start is called before the first frame update
        void Start()
        {
            NormalRGBCameraDevice rgbCameraDevice = new NormalRGBCameraDevice();
            int[] sizeRgbCamera = rgbCameraDevice.getCameraSize();
            width = sizeRgbCamera[0];
            height = sizeRgbCamera[1];
        }

        private void Update()
        {
            if (savingStatus == 2)
            {
                savingStatus = 0;
                instance = null;
                Debug.Log("RecordingManager Saved");
                Destroy(gameObject);
            }
        }

        private void OnApplicationPause(bool pause)
        {
            if (pause)
            {
                HandleRecordingClose();
            }
        }

        private async void GetPathAsync()
        {
            savingStatus = 1;

            var path = await recorder.FinishWriting();
            Debug.Log("Saved Recording to : " + path);
            Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "RecordingResult", "录屏已保存");

            SaveToGallery(path);
        }

        public void HandleRecordingOpen(AudioListener audioListener = null)
        {
            ARRenderRGB.Instance.HandleARRenderOpen();

            var frameRate = 30;
            var sampleRate = 0;
            var channelCount = 0;
            if (audioListener != null) {
                sampleRate = AudioSettings.outputSampleRate;
                channelCount = (int)AudioSettings.speakerMode;
            }
            var clock = new RealtimeClock();
            var videoName = "VID_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".mp4";
            var videoPath = Path.Combine(Application.persistentDataPath, videoName);
            recorder = new MP4Recorder(videoPath, width, height, frameRate, sampleRate, channelCount);
            Camera renderCam = ARRenderRGB.Instance.gameObject.GetComponent<Camera>();
            cameraInput = new CameraInput(recorder, clock, renderCam);
            if (audioListener != null)
            {
                audioInput = new AudioInput(recorder, clock, audioListener);
            }
            Debug.Log("UNITY LOG ========= Start Record");

            isRecording = true;
        } 

        public void HandleRecordingClose()
        {
            if (ARRenderRGB.Instance != null)
            {
                ARRenderRGB.Instance.HandleARRenderClose();
            }

            if (cameraInput != null)
            {
                cameraInput.Dispose();
            }
            

            if (audioInput != null)
            {
                audioInput.Dispose();
            }

            Debug.Log("UNITY LOG ========= Stop Record");

            GetPathAsync();
            isRecording = false;
        }

        public void HandleScreenCapture()
        {
            Debug.Log("UNITY LOG ========= ScreenCapture, size: " + width + ", " + height);

            if (captureCoroutine != null) return;
            isRecording = true;

            ARRenderRGB.Instance.HandleARRenderOpen();

            captureCoroutine = StartCoroutine(WaitForScreenCapture());
        }

        private IEnumerator WaitForScreenCapture()
        {
            yield return new WaitUntil(() => ARRenderRGB.Instance.isReady);
            //RGB 相机打开时曝光适配需要时间，0.41s是尝试值，如果实际测试有不够亮，再进行调整
            yield return new WaitForSeconds(0.41f);
            bool status = ARRenderRGB.Instance.HandleARRenderShot();
            if (!status)
            {
                Debug.Log("UNITY LOG ========= HandleARRenderShot failed! ");
                Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "RecordingResult", "截屏失败");
                captureCoroutine = null;
                yield break;
            }
            else
            {
                Debug.Log("UNITY LOG ========= HandleARRenderShot success! ");
                Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "RecordingResult", "截屏已保存");
            }

            //fix: RT depth为0会引起渲染层级和blend错乱
            RenderTexture rt = new RenderTexture(width, height, 24, RenderTextureFormat.DefaultHDR);
            Camera renderCam = ARRenderRGB.Instance.gameObject.GetComponent<Camera>();
            renderCam.targetTexture = rt;
            renderCam.Render();

            RenderTexture.active = rt;
            Texture2D screenShot = new Texture2D(width, height, TextureFormat.RGB24, false);
            screenShot.ReadPixels(new Rect(0, 0, width, height), 0, 0);// 注：这个时候，它是从RenderTexture.active中读取像素  
            screenShot.Apply();

            //testRenderer.sharedMaterial.SetTexture("_MainTex", screenShot);

            // 重置相关参数，以使用camera继续在屏幕上显示  
            renderCam.targetTexture = null;
            RenderTexture.active = null; // added to avoid errors
            GameObject.Destroy(rt);

            var imageName = "IMG_"+DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".jpg";
            var imagePath = Path.Combine(Application.persistentDataPath, imageName);
            var bytes = screenShot.EncodeToJPG();
            File.WriteAllBytes(imagePath, bytes);

            Debug.Log("UNITY LOG ========= ScreenCapture, save jpg at path: " + imagePath);
            SaveToGallery(imagePath);

            captureCoroutine = null;
            ARRenderRGB.Instance.HandleARRenderClose();
            //if (toastCoroutine != null) StopCoroutine(toastCoroutine);
            //toastCoroutine = StartCoroutine(WaitForToastTips());

            isRecording = false;
        }

        private void SaveToGallery(string src_path)
        {
            Debug.Log("UNITY LOG =========, save to gallery from: " + src_path);

            var dst_dir = NativeLib.GetScreenshotsPath();
            if (string.IsNullOrEmpty(dst_dir) || !Directory.Exists(dst_dir))
            {
                dst_dir = Path.GetDirectoryName(gallery_savedDir);
            }

            var dst_path = Path.Combine(dst_dir, Path.GetFileName(src_path));
            File.Move(src_path, dst_path);

            Debug.Log("UNITY LOG =========, saved to gallery at: " + dst_path);

            NativeLib.AddToAlbum(dst_path);
            Debug.Log("UNITY LOG =========, AddToAlbum at: " + dst_path);

            //var src_name = Path.GetFileNameWithoutExtension(src_path);
            //AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            //AndroidJavaObject currentActivity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
            //AndroidJavaObject contentResolver = currentActivity.Call<AndroidJavaObject>("getContentResolver");

            //// 调用insertImage保存到相册
            //AndroidJavaClass galleryClass = new AndroidJavaClass("android.provider.MediaStore$Images$Media");
            //AndroidJavaObject galleryObject = new AndroidJavaObject("java.io.File", src_path);
            //AndroidJavaObject galleryUri = galleryClass.CallStatic<AndroidJavaObject>("insertImage",
            //    contentResolver, galleryObject.Call<string>("getAbsolutePath"), src_name, "");

            //Debug.Log("UNITY LOG =========, saved to gallery!");
            //File.Delete(src_path);

            savingStatus = 2;
        }
    }
}