using EZXR.Glass.Runtime;
using EZXR.Glass.QRScanner;
using EZXR.Glass.SixDof;
using EZXR.Glass.UI;
using System;
using System.Collections;
using System.Collections.Generic;
#if TMP_PRESENT
using TMPro;
#endif
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace EZXR.Glass.Inputs
{
    //[ExecuteInEditMode]
    public class KeyBoardHandlerNew : MonoBehaviour
    {
        public enum KeyboardType
        {
            /// <summary>
            /// 小写字母
            /// </summary>
            LowerCase,
            /// <summary>
            /// 大写字母
            /// </summary>
            Uppercase,
            /// <summary>
            /// 数字
            /// </summary>
            Numbers,
            /// <summary>
            /// 符号
            /// </summary>
            Symbols
        }
        /// <summary>
        /// 当前键盘类型：
        /// </summary>
        public KeyboardType keyboardType;
        public GameObject canvas;
        Collider collier;
        /// <summary>
        /// 所有按键的父物体
        /// </summary>
        public Transform keysParent;

        /// <summary>
        /// 确认键被按下，用于全局需要通过确认键触发的情况
        /// </summary>
        public UnityAction OnEnterClicked;
        #if TMP_PRESENT
        private TMP_InputField curTMPInputField;
#endif
        private InputField curInputField;
        #if TMP_PRESENT
        public TextMeshProUGUI m_TextMeshPro;
#endif
        public Button[] letters;

        public Image capslock;
        /// <summary>
        /// 大写的时候要亮的一个绿灯
        /// </summary>
        public GameObject capslockDot;
        /// <summary>
        /// 数字按钮
        /// </summary>
        public GameObject num;
        public GameObject symbol;
        public GameObject abc;
        /// <summary>
        /// 存放letters中默认的键值
        /// </summary>
        string[] letters4Change;
        /// <summary>
        /// 用于数字键盘，存放与字母键盘一一对应的数字和符号
        /// </summary>
        public string[] num4Change;
        /// <summary>
        /// 用于符号键盘，存放与字母键盘一一对应的符号
        /// </summary>
        public string[] symbols4Change;

        /// <summary>
        /// 0是非按下，1是按下
        /// </summary>
        public Sprite[] capslockStatus;
        #if TMP_PRESENT
        public TextMeshProUGUI inputfield;
#endif

        Ray ray = new Ray();
        RaycastHit hitInfo;
        /// <summary>
        /// 焦点
        /// </summary>
        public Transform focus;
        /// <summary>
        /// 开始显示焦点的距离
        /// </summary>
        float showFocusDistance = 0.05f;
        /// <summary>
        /// 焦点在此距离的时候不再变小，即localScale为1
        /// </summary>
        float focusNearDistance = 0.01f;

        /// <summary>
        /// 拖动键盘的把手
        /// </summary>
        public Image handler;
        /// <summary>
        /// 0是Normal，1是Enter，2是Press
        /// </summary>
        public Sprite[] handlerStatus;
        public GameObject keyboardHover;
        public SpatialAnchor spatialAnchor;

        private void Awake()
        {
            collier = GetComponent<Collider>();

            Button[] buttons = keysParent.GetComponentsInChildren<Button>();
            foreach (Button button in buttons)
            {
                button.onClick.AddListener(() => OnKeyClicked(button));
            }

            abc.SetActive(false);

            letters4Change = new string[letters.Length];
            for (int i = 0; i < letters.Length; i++)
            {
                letters4Change[i] = letters[i].name;
            }


            HideImmediately();
        }

        private void KeyboardRecenterNear(bool enabled = false)
        {
            transform.Recenter(new Vector3(-0.015f, -0.15f, 0.4f));
            transform.localScale = new Vector3(0.0001806142f, 0.0001806142f, 1);
        }

        private void KeyboardRecenterFar(bool enabled = false)
        {
            transform.Recenter(new Vector3(-0.015f, -0.15f, 0.6f));
            transform.localScale = new Vector3(0.0001806142f, 0.0001806142f, 1);
        }

        void OnEnable()
        {
            //扫描按钮事件注册
            QRScannerManager.RegisterCompleteListener(OnScanComplete);
            //SystemManager.OnPowerPressed += KeyboardRecenter;

            //#if UNITY_EDITOR
            //        foreach (SpatialButton button in letters)
            //        {
            //            button.Text = button.name;
            //        }
            //        foreach (SpatialButton button in symbols) 
            //        {
            //            button.Text = button.name;
            //        }
            //#endif
        }

        private void OnDisable()
        {
            XRMan.Hands.Left.RI_Internal = true;
            XRMan.Hands.Right.RI_Internal = true;
            XRMan.Controllers.Left.RI_Internal = true;
            XRMan.Controllers.Right.RI_Internal = true;

            //WristPanel.Left.RemoveListener(KeyboardRecenter);
            //SystemManager.OnPowerPressed -= KeyboardRecenter;

            //扫描按钮事件注册
            QRScannerManager.UnregisterCompleteListener(OnScanComplete);
        }

        private void Start()
        {
            if (Application.isPlaying)
            {
                ChangeLettersToLower();

                //ChangeSymbols("#+=");
            }
        }

        InputType lastType;
        // Update is called once per frame
        void Update()
        {
            if (HandleControllerManager.Instance.GetButtonDown(HandleKeyCode.Back))
            {
                Hide();
            }

            //Debug.Log(m_TextMeshPro.GetRenderedValues().ToString("##.###") + ", " + m_TextMeshPro.GetPreferredValues().ToString("##.###"));
#if TMP_PRESENT
            if (m_TextMeshPro.GetPreferredValues().x >= 1148)
            {
                m_TextMeshPro.alignment = TextAlignmentOptions.Right;
            }
            else
            {
                m_TextMeshPro.alignment = TextAlignmentOptions.Left;
            }

            if (!((curInputField != null && curInputField.gameObject.activeInHierarchy) || (curTMPInputField != null && curTMPInputField.gameObject.activeInHierarchy)))
            {
                Hide();
            }
#endif
            //if (Input.GetKeyDown(KeyCode.T))
            //{
            //    KeyboardRecenter();
            //}

            //transform.rotation = XRMan.Head.rotation * Quaternion.Euler(0, 180, 0);

            if (canvas.activeSelf)
            {
                if (XRMan.Input.Type == InputType.Hand)
                {
                    if (lastType != XRMan.Input.Type)
                    {
                        KeyboardRecenterNear();
                    }
                    else
                    {
                        if (Vector3.Distance(transform.position, XRMan.Head.position) > 0.5f)
                        {
                            XRMan.Input.Left.RI_Internal = true;
                            XRMan.Input.Right.RI_Internal = true;
                        }
                        else
                        {
                            XRMan.Input.Left.RI_Internal = false;
                            XRMan.Input.Right.RI_Internal = false;
                        }
                    }
                }
                else if (XRMan.Input.Type == InputType.Controller)
                {
                    if (lastType != XRMan.Input.Type)
                    {
                        KeyboardRecenterFar();
                    }
                }
                lastType = XRMan.Input.Type;
            }

            if (XRMan.Hands.Right != null && XRMan.Hands.Right.Exist && XRMan.Hands.Right.InView(HandJointType.Index_4))
            {
                ray.origin = XRMan.Hands.Right.GetJointData(HandJointType.Index_4).position;
                ray.direction = -transform.forward;
            }
            else if (XRMan.Hands.Left != null && XRMan.Hands.Left.Exist && XRMan.Hands.Left.InView(HandJointType.Index_4))
            {
                ray.origin = XRMan.Hands.Left.GetJointData(HandJointType.Index_4).position;
                ray.direction = -transform.forward;
            }

            if (Physics.Raycast(ray, out hitInfo))
            {
                if (hitInfo.distance < showFocusDistance)
                {
                    focus.gameObject.SetActive(true);
                    focus.position = hitInfo.point;
                    focus.localScale = Vector3.one * (1f * (Math.Clamp(hitInfo.distance - focusNearDistance, 0, 100)) / (showFocusDistance - focusNearDistance) + 1.0f);
                }
                else
                {
                    focus.gameObject.SetActive(false);
                }
            }
        }

        public void Show(InputField inputField)
        {
            if (XRMan.Input.Type == InputType.Hand)
            {
                XRMan.Input.Left.RI_Internal = false;
                XRMan.Input.Right.RI_Internal = false;

                KeyboardRecenterNear();
            }
            else
            {
                KeyboardRecenterFar();
            }

            curInputField = inputField;
            #if TMP_PRESENT
            m_TextMeshPro.text = inputField.text;
#endif
            gameObject.SetActive(true);
        }

        #if TMP_PRESENT
        public void Show(TMP_InputField inputField)
        {
            if (XRMan.Input.Type == InputType.Hand)
            {
                XRMan.Input.Left.RI_Internal = false;
                XRMan.Input.Right.RI_Internal = false;

                KeyboardRecenterNear();
            }
            else
            {
                KeyboardRecenterFar();
            }

            curTMPInputField = inputField;
            m_TextMeshPro.text = inputField.text;
            gameObject.SetActive(true);
        }
#endif

        public void Hide()
        {
            if (gameObject.activeInHierarchy && enabled)
            {
                StartCoroutine(WaitThenHide());
            }
        }

        IEnumerator WaitThenHide()
        {
            yield return new WaitForSeconds(0.5f);

            HideImmediately();
        }

        public void HideImmediately()
        {
            curInputField = null;
            #if TMP_PRESENT
            curTMPInputField = null;
#endif
            gameObject.SetActive(false);
        }

        void ExchangeLetters()
        {
            if (keyboardType != KeyboardType.Uppercase)
            {
                ChangeLettersToUpper();
            }
            else
            {
                ChangeLettersToLower();
            }
        }

        void ChangeLettersToUpper()
        {
            keyboardType = KeyboardType.Uppercase;
            for (int i = 0; i < letters.Length; i++)
            {
                letters[i].name = letters4Change[i].ToUpper();
                #if TMP_PRESENT
                letters[i].GetComponentInChildren<TextMeshProUGUI>().text = letters[i].name;
#endif
            }
            //CapsLock按下状态
            capslock.sprite = capslockStatus[1];
            capslockDot.SetActive(true);
        }

        void ChangeLettersToLower()
        {
            keyboardType = KeyboardType.LowerCase;
            for (int i = 0; i < letters.Length; i++)
            {
                letters[i].name = letters4Change[i].ToLower();
                #if TMP_PRESENT
                letters[i].GetComponentInChildren<TextMeshProUGUI>().text = letters[i].name;
#endif
            }
            //CapsLock非按下状态
            capslock.sprite = capslockStatus[0];
            capslockDot.SetActive(false);
        }

        void ToggleNum()
        {
            //keyboardType = KeyboardType.Numbers;
            //num.sprite = capslockStatus[1];
            for (int i = 0; i < letters.Length; i++)
            {
                letters[i].name = num4Change[i];
                #if TMP_PRESENT
                letters[i].GetComponentInChildren<TextMeshProUGUI>().text = num4Change[i];
#endif
            }
        }

        void ToggleSymbol()
        {
            //keyboardType = KeyboardType.Symbols;
            //symbol.sprite = capslockStatus[1];
            for (int i = 0; i < letters.Length; i++)
            {
                letters[i].name = symbols4Change[i];
#if TMP_PRESENT
                letters[i].GetComponentInChildren<TextMeshProUGUI>().text = symbols4Change[i];
#endif
            }
        }

        //void ChangeSymbols(string target = "")
        //{
        //    if (symbol.name == "123" || target == "#+=")
        //    {
        //        symbol.name = "#+=";
        //        symbol.GetComponentInChildren<TextMeshProUGUI>().text = symbol.name;
        //        for (int i = 0; i < symbols.Length; i++)
        //        {
        //            symbols[i].name = num4Change[i];
        //            symbols[i].GetComponentInChildren<TextMeshProUGUI>().text = num4Change[i];
        //        }
        //    }
        //    else if (symbol.name == "#+=" || target == "123")
        //    {
        //        symbol.name = "123";
        //        symbol.GetComponentInChildren<TextMeshProUGUI>().text = symbol.name;
        //        for (int i = 0; i < symbols.Length; i++)
        //        {
        //            symbols[i].name = symbols4Change[i];
        //            symbols[i].GetComponentInChildren<TextMeshProUGUI>().text = symbols4Change[i];
        //        }
        //    }
        //}

        public void OnKeyHoverEnter(string key)
        {
            Debug.Log("OnKeyHoverEnter: " + key);
        }

        public void OnStartPress(string key)
        {
            Debug.Log("OnStartPress: " + key);
        }

        public void OnPressing(string key)
        {
            Debug.Log("OnPressing: " + key);
        }

        //OnKeyDown
        public void OnKeyDown(string key)
        {
            Debug.Log("OnKeyDown: " + key);
        }

        //OnKeyUp
        public void OnKeyUp(string key)
        {
            Debug.Log("OnKeyUp: " + key);
        }
        public void OnEndPress(string key)
        {
            Debug.Log("OnEndPress: " + key);
        }

        public void OnKeyClicked(Button sender)
        {
            switch (sender.name)
            {
                case "Del":
#if TMP_PRESENT
                    if (m_TextMeshPro.text.Length > 0)
                    {
                        m_TextMeshPro.text = m_TextMeshPro.text.Substring(0, m_TextMeshPro.text.Length - 1);
                    }
#endif
                    break;
                case "Clear":
#if TMP_PRESENT
                    m_TextMeshPro.text = "";
#endif
                    break;
                case "CapsLock":
                    //num.sprite = capslockStatus[0];
                    //symbol.sprite = capslockStatus[0];
                    ExchangeLetters();
                    break;
                case "123":
                    //if (keyboardType == KeyboardType.Numbers)
                    //{
                    //    //num.sprite = capslockStatus[0];
                    //    ChangeLettersToLower();
                    //}
                    //else
                    capslock.transform.parent.gameObject.SetActive(false);
                    num.SetActive(false);
                    symbol.SetActive(false);
                    abc.SetActive(true);
                    {
                        //capslock.sprite = capslockStatus[0];
                        //capslockDot.SetActive(false);
                        //symbol.sprite = capslockStatus[0];
                        ToggleNum();
                    }
                    break;
                case "#+=":
                    //if (keyboardType == KeyboardType.Symbols)
                    //{
                    //    //symbol.sprite = capslockStatus[0];
                    //    ChangeLettersToLower();
                    //}
                    //else
                    capslock.transform.parent.gameObject.SetActive(false);
                    num.SetActive(false);
                    symbol.SetActive(false);
                    abc.SetActive(true);
                    {
                        //capslock.sprite = capslockStatus[0];
                        //capslockDot.SetActive(false);
                        //num.sprite = capslockStatus[0];
                        ToggleSymbol();
                    }
                    break;
                case "ABC":
                    capslock.transform.parent.gameObject.SetActive(true);
                    num.SetActive(true);
                    symbol.SetActive(true);
                    abc.SetActive(false);
                    if (keyboardType == KeyboardType.Uppercase)
                    {
                        ChangeLettersToUpper();
                    }
                    else
                    {
                        ChangeLettersToLower();
                    }
                    break;
                case "QR":
                    StartScan();
                    break;
                case "Return":
                    if (OnEnterClicked != null)
                    {
                        OnEnterClicked.Invoke();
                    }
                    else
                    {
                        //m_TextMeshPro.text += "\r\n";
                    }
                    break;
                case "Keyboard":
                    Debug.Log("KeyBoardHandlerNew => Keyboard");
                    Hide();
                    break;
                default:
#if TMP_PRESENT
                    m_TextMeshPro.text += sender.name;
#endif
                    break;
            }
#if TMP_PRESENT
            SetInputField(m_TextMeshPro.text);
#endif
        }

        void SetInputField(string text)
        {
#if TMP_PRESENT
            if (m_TextMeshPro != null)
                m_TextMeshPro.text = text;
#endif
            if (curInputField != null)
            {
                curInputField.text = text;
            }
#if TMP_PRESENT
            else if (curTMPInputField != null)
            {
                curTMPInputField.text = text;
            }
#endif
            else
            {
                Hide();
            }
        }

        public void OnKeyHoverExit(string key)
        {
            Debug.Log("OnKeyHoverExit: " + key);
        }

        /// <summary>
        /// 0是normal，1是hover，2是press
        /// </summary>
        /// <param name="eventType"></param>
        public void OnHandlerEvent(int eventType)
        {
            Debug.Log("KeyBoardHandler => OnHandlerEvent: " + eventType);
            handler.sprite = handlerStatus[eventType];
            keyboardHover.SetActive(eventType == 2 ? true : false);
            spatialAnchor.lookToHead = keyboardHover.activeSelf;
        }

        /// <summary>
        /// 点击扫描按钮调用此处
        /// </summary>
        public void StartScan()
        {
            canvas.SetActive(false);
            collier.enabled = false;
            QRScannerManager.RequestOpen();

            Debug.Log("KeyBoardHandler => StartScan: ");
            XRMan.Input.Left.RI_Internal = true;
            XRMan.Input.Right.RI_Internal = true;
        }

        /// <summary>
        /// 扫描完毕回调此处
        /// </summary>
        /// <param name="status"></param>
        /// <param name="content"></param>
        private void OnScanComplete(bool status, string content)
        {
            if (gameObject.activeInHierarchy && enabled)
            {
                Debug.Log("KeyBoardHandlerNew, Scanning Complete: " + content);

                if (status)
                {
                    if (XRMan.Input.Type == InputType.Hand)
                    {
                        XRMan.Input.Left.RI_Internal = false;
                        XRMan.Input.Right.RI_Internal = false;
                    }

                    canvas.SetActive(true);
                    collier.enabled = true;
                    SetInputField(content);
                }
                else
                {
                    canvas.SetActive(true);
                    collier.enabled = true;
                    //Toast.Show("未识别到二维码");
                }
            }
        }
    }
}