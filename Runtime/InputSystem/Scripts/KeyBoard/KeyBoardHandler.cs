using EZXR.Glass.Runtime;
using EZXR.Glass.UI;
using System;
using System.Collections;
using System.Collections.Generic;
#if TMP_PRESENT
using TMPro;
#endif
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace EZXR.Glass.Inputs
{
    //[ExecuteInEditMode]
    public class KeyBoardHandler : MonoBehaviour
    {
        /// <summary>
        /// 确认键被按下，用于全局需要通过确认键触发的情况
        /// </summary>
        public UnityAction OnEnterClicked;
        #if TMP_PRESENT
        private TMP_InputField curTMPInputField;
#endif
        private InputField curInputField;
        #if TMP_PRESENT
        public TextMeshPro m_TextMeshPro;
#endif
        public SpatialButton[] letters;

        public SpatialButton_Thick symbol;
        /// <summary>
        /// 左侧小键盘
        /// </summary>
        public SpatialButton[] symbols;
        /// <summary>
        /// 与数字键盘一一对应的符号，用于切换符号和数字
        /// </summary>
        public string[] symbols4Change;
        public string[] num4Change;

        public SpatialButton_Thick capslock;
        /// <summary>
        /// 0是非按下，1是按下
        /// </summary>
        public Texture2D[] capslockStatus;

        private void Awake()
        {
            SpatialButton_Thick[] buttons = transform.GetComponentsInChildren<SpatialButton_Thick>();
            foreach (SpatialButton_Thick button in buttons)
            {
                button.AddEvent(SpatialButton.SpatialButtonEventType.OnPointerClicked, () => OnKeyClicked(button));
            }
        }

        private void KeyboardRecenter(bool enabled = false)
        {
            transform.Recenter(new Vector3(0, -0.3f, 0.5f));
        }

        void OnEnable()
        {
            //SystemManager.OnPowerPressed += KeyboardRecenter;

            //#if UNITY_EDITOR
            //        foreach (SpatialButton button in letters)
            //        {
            //            button.Text = button.name;
            //        }
            //        foreach (SpatialButton button in symbols) 
            //        {
            //            button.Text = button.name;
            //        }
            //#endif
        }

        private void OnDisable()
        {
            //WristPanel.Left.RemoveListener(KeyboardRecenter);
            //SystemManager.OnPowerPressed -= KeyboardRecenter;
        }

        private void Start()
        {
            if (Application.isPlaying)
            {
                ChangeLettersToLower();

                ChangeSymbols("#+&");
            }
        }

        // Update is called once per frame
        void Update()
        {
            //if (Input.GetKeyDown(KeyCode.T))
            //{
            //    KeyboardRecenter();
            //}
        }

        public void Show(InputField inputField)
        {
            KeyboardRecenter();

            curInputField = inputField;
            #if TMP_PRESENT
            m_TextMeshPro.text = inputField.text;
#endif
            gameObject.SetActive(true);
        }
        #if TMP_PRESENT
        public void Show(TMP_InputField inputField)
        {
            KeyboardRecenter();

            curTMPInputField = inputField;
            m_TextMeshPro.text = inputField.text;
            gameObject.SetActive(true);
        }
#endif

        public void Hide()
        {
            curInputField = null;
            #if TMP_PRESENT
            curTMPInputField = null;
#endif
            gameObject.SetActive(false);
        }

        void ExchangeLetters()
        {
            if (char.IsLower(letters[0].Text[0]))
            {
                ChangeLettersToUpper();
            }
            else if (char.IsUpper(letters[0].Text[0]))
            {
                ChangeLettersToLower();
            }
        }

        void ChangeLettersToUpper()
        {
            foreach (SpatialButton button in letters)
            {
                button.Text = button.Text.ToUpper();
            }
            //CapsLock非按下状态
            capslock.Icon = capslockStatus[1];
        }

        void ChangeLettersToLower()
        {
            foreach (SpatialButton button in letters)
            {
                button.Text = button.Text.ToLower();
            }
            //CapsLock非按下状态
            capslock.Icon = capslockStatus[0];
        }

        void ChangeSymbols(string target = "")
        {
            if (symbol.Text == "123" || target == "#+&")
            {
                symbol.Text = "#+&";
                for (int i = 0; i < symbols.Length; i++)
                {
                    symbols[i].Text = num4Change[i];
                }
            }
            else if (symbol.Text == "#+&" || target == "123")
            {
                symbol.Text = "123";
                for (int i = 0; i < symbols.Length; i++)
                {
                    symbols[i].Text = symbols4Change[i];
                }
            }
        }

        public void OnKeyHoverEnter(string key)
        {
            Debug.Log("OnKeyHoverEnter: " + key);
        }

        public void OnStartPress(string key)
        {
            Debug.Log("OnStartPress: " + key);
        }

        public void OnPressing(string key)
        {
            Debug.Log("OnPressing: " + key);
        }

        //OnKeyDown
        public void OnKeyDown(string key)
        {
            Debug.Log("OnKeyDown: " + key);
        }

        //OnKeyUp
        public void OnKeyUp(string key)
        {
            Debug.Log("OnKeyUp: " + key);
        }
        public void OnEndPress(string key)
        {
            Debug.Log("OnEndPress: " + key);
        }

        public void OnKeyClicked(SpatialButton sender)
        {
            switch (sender.Text)
            {
                case "Del":
                    #if TMP_PRESENT
                    if (m_TextMeshPro.text.Length > 0)
                    {
                        m_TextMeshPro.text = m_TextMeshPro.text.Substring(0, m_TextMeshPro.text.Length - 1);
                    }
#endif
                    break;
                case "#+&":
                    ChangeSymbols("123");
                    break;
                case "123":
                    ChangeSymbols("#+&");
                    break;
                case "CapsLock":
                    ExchangeLetters();
                    break;
                case "Language":

                    break;
                case "Return":
                    //m_TextMeshPro.text += "\r\n";
                    if (OnEnterClicked != null)
                    {
                        OnEnterClicked.Invoke();
                    }
                    break;
                case "Keyboard":
                    Hide();
                    break;
                default:
                    #if TMP_PRESENT
                    m_TextMeshPro.text += sender.Text;
#endif
                    break;
            }

                    #if TMP_PRESENT
            if (curInputField != null)
            {
                curInputField.text = m_TextMeshPro.text;
            }

            if (curTMPInputField != null)
            {
                curTMPInputField.text = m_TextMeshPro.text;
            }
#endif
        }

        public void OnKeyHoverExit(string key)
        {
            Debug.Log("OnKeyHoverExit: " + key);
        }
    }
}