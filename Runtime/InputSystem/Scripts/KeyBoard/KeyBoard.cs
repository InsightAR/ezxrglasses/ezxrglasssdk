﻿using System;
using System.Collections;
using System.Collections.Generic;
#if TMP_PRESENT
using TMPro;
#endif
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace EZXR.Glass.Inputs
{
    public class KeyBoard : MonoBehaviour
    {
        private static KeyBoard instance;
        public static KeyBoard Instance
        {
            get
            {
                return instance;
            }
        }

        public KeyBoardHandlerNew keyBoardHandler;

        /// <summary>
        /// 确认键被按下，用于全局需要通过确认键触发的情况
        /// </summary>
        public static UnityAction OnEnterClicked
        {
            get
            {
                return Instance.keyBoardHandler.OnEnterClicked;
            }
            set
            {
                Instance.keyBoardHandler.OnEnterClicked = value;
            }
        }

        public static Action Callback_OnPowerPressed;
        public static Action Callback_OnPowerLongPressed;
        public static Action Callback_OnVolumeIncreasePressed;
        public static Action Callback_OnVolumeDecreasePressed;
        public enum KeyEventType
        {
            Power,
            PowerLong,
            VolumeIncrease,
            VolumeDecrease,
        }
        public Dictionary<KeyEventType, bool> keyEventHandler = new Dictionary<KeyEventType, bool>();


        private void Awake()
        {
            instance = this;

            //keyBoardHandler.OnEnterClicked = OnEnterClicked;

            foreach (KeyEventType item in Enum.GetValues(typeof(KeyEventType)))
            {
                keyEventHandler.Add(item, false);
            }
        }
        // Start is called before the first frame update
        void Start()
        {
            keyBoardHandler.Hide();
        }

        // Update is called once per frame
        void Update()
        {
            if (EventSystem.current != null && EventSystem.current.currentSelectedGameObject != null && ((HandInputModule.eventData_Left != null && HandInputModule.eventData_Left.pointerPress != null) || (HandInputModule.eventData_Right != null && HandInputModule.eventData_Right.pointerPress != null)))
            {
                InputField inputField = EventSystem.current.currentSelectedGameObject.GetComponent<InputField>();

                    #if TMP_PRESENT
                TMP_InputField tmpInputField = EventSystem.current.currentSelectedGameObject.GetComponent<TMP_InputField>();
#endif
                if (inputField != null)
                {
                    keyBoardHandler.Show(inputField);
                }
                    #if TMP_PRESENT
                else if (tmpInputField != null)
                {
                    keyBoardHandler.Show(tmpInputField);
                }
#endif
                //else
                //{
                //    keyBoardHandler.Hide();
                //}
            }
            //else
            //{
            //    keyBoard.Hide();
            //}
        }

        //public bool GetKey(KeyEventType keyEventType)
        //{
        //    return keyEventHandler[keyEventType];
        //}

        //public void GetKeyDown(KeyEventType keyEventType)
        //{

        //}

        //public void GetKeyUp(KeyEventType keyEventType)
        //{

        //}
    }
}