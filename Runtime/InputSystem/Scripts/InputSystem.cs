using System;
using System.Collections;
using EZXR.Glass.Runtime;
using EZXR.Glass.SixDof;
using UnityEngine;
using UnityEngine.EventSystems;

namespace EZXR.Glass.Inputs
{
    [ScriptExecutionOrder(-46)]
    public class InputSystem : MonoBehaviour
    {
        #region singleton
        private static InputSystem instance;
        public static InputSystem Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        [SerializeField]
        private InputType m_InputType;
        [Obsolete("Please use 'XRMan.Input.Type' instead!")]
        public InputType inputType
        {
            get
            {
                return m_InputType;
            }
        }

        /// <summary>
        /// 当前激活的控制器类型
        /// </summary>
        [Obsolete("Please use 'XRMan.Input.Type' instead!")]
        public static InputType CurrentActiveControllerType
        {
            get
            {
                return Instance.m_InputType;
            }
        }

        /// <summary>
        /// 开启动态模式时，支持热切换
        /// </summary>
        [SerializeField]
        private bool dynamicMode = true;

        /// <summary>
        /// InputSystem当前是否被启用
        /// </summary>
        private bool isActive = true;

        public HandleControllerManager controllerManager;
        public ARHandManager handManager;

        private static InputInfoBase left;
        /// <summary>
        /// 当前正在使用的左手输入信息：左手裸手信息或左手柄信息
        /// </summary>
        [Obsolete("Use \"XRMan.Input.Left\" instead!")]
        public static InputInfoBase Left
        {
            get
            {
                return left;
            }
        }

        private static InputInfoBase right;
        /// <summary>
        /// 当前正在使用的右手输入信息：右手裸手信息或右手柄信息
        /// </summary>
        [Obsolete("Use \"XRMan.Input.Right\" instead!")]
        public static InputInfoBase Right
        {
            get
            {
                return right;
            }
        }

        /// <summary>
        /// 左手裸手信息
        /// </summary>
        [Obsolete("Use \"XRMan.Hands.Left\" instead!")]
        public static HandInfo LeftHand
        {
            get
            {
                return ARHandManager.leftHand;
            }
        }

        /// <summary>
        /// 右手裸手信息
        /// </summary>
        [Obsolete("Use \"XRMan.Hands.Right\" instead!")]
        public static HandInfo RightHand
        {
            get
            {
                return ARHandManager.rightHand;
            }
        }

        /// <summary>
        /// 左手柄信息
        /// </summary>
        [Obsolete("Use \"XRMan.Controllers.Left\" instead!")]
        public static ControllerInfo LeftController
        {
            get
            {
                return HandleControllerManager.leftHand;
            }
        }

        /// <summary>
        /// 右手柄信息
        /// </summary>
        [Obsolete("Use \"XRMan.Controllers.Right\" instead!")]
        public static ControllerInfo RightController
        {
            get
            {
                return HandleControllerManager.rightHand;
            }
        }

        /// <summary>
        /// 按键按住或握持住时，手柄保持激活
        /// </summary>
        private bool keepHandlesActive = false;

        private bool isInited = false;
        private bool isPaused = false;
        private bool isGlassStandby = false;
        private bool isHandlesStandby = false;
        private bool isHandlesOutOfBound = false;

        public void SetActive(bool value)
        {
            isActive = value;

            switch (CurrentActiveControllerType)
            {
                case InputType.Hand:
                    ARHandManager.Instance.gameObject.SetActive(value);
                    break;
                case InputType.Controller:
                    HandleControllerManager.Instance.gameObject.SetActive(value);
                    break;
            }
        }

        private void Awake()
        {
            instance = this;

            //TODO 初始化交互：头控<手势<手柄
            if (dynamicMode) m_InputType = InputType.Helmet;

            Debug.Log($"ControllerManager, init dynamicMode: {dynamicMode}, controllerType: {CurrentActiveControllerType}");

            if (dynamicMode)
            {
                m_InputType = InputType.Hand;
                left = XRMan.Hands.Left;
                right = XRMan.Hands.Right;
            }

        }

        private IEnumerator Start()
        {
            Debug.Log("InputSystem--> KeyBoard");
            if (KeyBoard.Instance == null)
            {
                Instantiate(ResourcesManager.Load<GameObject>("KeyBoard"));
            }

            HandleControllerManager.Instance.InitRegistration(null, OnHandleConnectChanged, null, OnHandleButtonChanged, OnHandleHoldChanged, OnHandleSilenceChanged);
            HandleControllerSession.Instance.InitServerReconnectRegistration(OnServerReconnected);
#if SYSTEMUI
            SDKEventSystem.OnSystemEnterStandbyMode += OnServerStandby;
#else
            OSEventSystem.OnSystemEnterStandbyMode += OnServerStandby;
#endif

            if (dynamicMode)
            {
                //StartCoroutine(ChangeToGestureController());
                //StartCoroutine(CheckStatus());
                m_InputType = InputType.Hand;
                left = XRMan.Hands.Left;
                right = XRMan.Hands.Right;

                HandleControllerManager.Instance.SetActive(false);
                ARHandManager.Instance.SetActive(true & isActive);
                Debug.Log($"ControllerManager, init Change to GestureController ({Time.frameCount})");

                Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "ControllerType", Application.identifier + "," + "Dynamic");
            }
            else
            {
                if (CurrentActiveControllerType == InputType.Hand)
                {
                    left = XRMan.Hands.Left;
                    right = XRMan.Hands.Right;
                    HandleControllerManager.Instance.SetActive(false);

                    if (!isPaused)
                    {
                        Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "ControllerType", Application.identifier + "," + "Hand");
                        Debug.Log($"ControllerManager, init SendIntent ControllerType HandTracking ({Time.frameCount})");
                    }
                }
                else
                {
                    left = XRMan.Controllers.Left;
                    right = XRMan.Controllers.Right;
                    ARHandManager.Instance.SetActive(false);

                    if (!isPaused)
                    {
                        Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "ControllerType", Application.identifier + "," + "Controllers");
                        Debug.Log($"ControllerManager, init SendIntent ControllerType Controllers ({Time.frameCount})");
                    }
                }
            }

            yield return new WaitUntil(() => SessionManager.Instance != null && SessionManager.Instance.IsInited);

            HandleControllerSession.Instance.ChangeControllerType((int)CurrentActiveControllerType);

            isInited = true;

        }

        private void Update()
        {
            // 不受HandleControllerSession未激活影响
            HandleControllerSession.Instance?.UpdateHandleCallback();

            if (isGlassStandby)
            {
                isGlassStandby = false;
                if (dynamicMode || (!dynamicMode && CurrentActiveControllerType == InputType.Hand))
                {
                    StartCoroutine(ChangeToGestureController());
                }
                Debug.Log($"ControllerManager, Awake after standby, isGlassStandby = {isGlassStandby} ({Time.frameCount})");
            }

            if (isHandlesStandby)
            {
                isHandlesStandby = false;
                if (dynamicMode) StartCoroutine(ChangeToGestureController());
                Debug.Log($"ControllerManager, Awake after Handles Standby, controllerType: {CurrentActiveControllerType}, dynamicMode: {dynamicMode} ({Time.frameCount})");
            }

            //if (!isPaused && Input.GetKeyDown(KeyCode.I))
            //    InitDynamicMode(ControllerType.Controllers);
            //if (!isPaused && Input.GetKeyDown(KeyCode.E))
            //    EnableDynamicMode();
            //if (!isPaused && Input.GetKeyDown(KeyCode.H))
            //    DisableDynamicMode(ControllerType.HandTracking);
            //if (!isPaused && Input.GetKeyDown(KeyCode.C))
            //    DisableDynamicMode(ControllerType.Controllers);
        }

        private void OnApplicationPause(bool pause)
        {
            if (!pause)
            {
                if (isInited)
                {
                    if (dynamicMode)
                    {
                        Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "ControllerType", Application.identifier + "," + "Dynamic");

                        //手柄未连接状态下(回调失效)，Resume时强制切到手势模式
                        if (!HandleControllerManager.Instance.GetConnectState(HandType.Left) &&
                            !HandleControllerManager.Instance.GetConnectState(HandType.Right))
                        {
                            isHandlesStandby = true;
                            Debug.Log($"ControllerManager, OnApplicationResume(Handles Standby) ChangeControllerType: {CurrentActiveControllerType}, dynamicMode: {dynamicMode} ({Time.frameCount})");
                        }
                    }
                    else
                    {
                        switch (m_InputType)
                        {
                            case InputType.Helmet:
                                Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "ControllerType", Application.identifier + "," + "Head");
                                break;
                            case InputType.Hand:
                                Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "ControllerType", Application.identifier + "," + "Hand");
                                break;
                            case InputType.Controller:
                                Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "ControllerType", Application.identifier + "," + "Controllers");
                                break;
                        }
                    }

                    HandleControllerSession.Instance.ChangeControllerType((int)CurrentActiveControllerType);

                    Debug.Log($"ControllerManager, OnApplicationResume(Inited) ChangeControllerType: {CurrentActiveControllerType}, dynamicMode: {dynamicMode} ({Time.frameCount})");

                }
                else
                {
                    //在上一应用时进入了待机模式(手柄算法会重置)，
                    //再Resume该应用时应通知Server同步到当前ControllerType
                    StartCoroutine(CheckStatusWhenResume());
                }
            }

            isPaused = pause;

            Debug.Log($"ControllerManager, OnApplicationPause isPaused: {isPaused}, dynamicMode: {dynamicMode} ({Time.frameCount})");
        }


        // for fix
        private IEnumerator CheckStatus()
        {
            yield return new WaitUntil(() => SessionManager.Instance != null && SessionManager.Instance.IsInited);

            Debug.Log($"ControllerManager, rightHand check ConnectState: {HandleControllerManager.Instance.GetConnectState(HandType.Right)}");

            if (HandleControllerManager.Instance.GetConnectState(HandType.Left) ||
                HandleControllerManager.Instance.GetConnectState(HandType.Right))
            {
                //StartCoroutineChangeToHandleController());
            }
        }

        private IEnumerator CheckStatusWhenResume()
        {
            yield return new WaitUntil(() => SessionManager.Instance != null && SessionManager.Instance.IsInited);

            HandleControllerSession.Instance.ChangeControllerType((int)CurrentActiveControllerType);

            Debug.Log($"ControllerManager, OnApplicationResume(UnInit) ChangeControllerType: {CurrentActiveControllerType}, dynamicMode: {dynamicMode} ({Time.frameCount})");

        }

        /// <summary>
        /// 动态模式初始化时，指定控制器类型
        /// note: 用于应用切回前台时，由systemui广播调用
        /// </summary>
        public void InitDynamicMode(InputType type)
        {
            StartCoroutine(InitDynamicMode_Coroutine(type));
        }

        /// <summary>
        /// 开启动态模式，自动热切换
        /// note: 仅用于systemui使用，在前台回到dynamic应用时调用
        /// </summary>
        public void EnableDynamicMode()
        {
            StartCoroutine(EnableDynamicMode_Coroutine());
        }

        /// <summary>
        /// 禁用动态模式时，指定控制器类型
        /// note: 仅用于systemui使用，在前台回到非dynamic应用时调用
        /// </summary>
        /// <param name="type"></param>
        public void DisableDynamicMode(InputType type)
        {
            StartCoroutine(DisableDynamicMode_Coroutine(type));
        }

        private IEnumerator InitDynamicMode_Coroutine(InputType type)
        {
            yield return new WaitUntil(() => isInited);

            if (!dynamicMode) yield break;

            if (type == InputType.Hand)
            {
                if (isPaused) yield break;
                if (m_InputType == InputType.Hand) yield break;

                m_InputType = InputType.Hand;
                left = XRMan.Hands.Left;
                right = XRMan.Hands.Right;
                HandleControllerSession.Instance.ChangeControllerType((int)m_InputType);

                HandleControllerManager.Instance.SetActive(false);
                ARHandManager.Instance.SetActive(true & isActive);
                Debug.Log($"ControllerManager, InitDynamicMode Change to GestureController, leftHand = {Left.GetType().Name}, rightHand = {Right.GetType().Name} ({Time.frameCount})");
            }
            else if (type == InputType.Controller)
            {
                //手柄未连接时从纯手柄应用切回launcher，强制置为手势
                if (!HandleControllerManager.Instance.GetConnectState(HandType.Left) && !HandleControllerManager.Instance.GetConnectState(HandType.Right))
                {
                    if (m_InputType == InputType.Hand) yield break;

                    m_InputType = InputType.Hand;
                    left = XRMan.Hands.Left;
                    right = XRMan.Hands.Right;
                    HandleControllerSession.Instance.ChangeControllerType((int)m_InputType);

                    HandleControllerManager.Instance.SetActive(false);
                    ARHandManager.Instance.SetActive(true & isActive);
                    Debug.Log($"ControllerManager, OnApplicationResume Change to GestureController ({Time.frameCount})");

                    //Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "ControllerType", Application.identifier + "," + "Dynamic");
                }
                else
                {
                    if (isPaused) yield break;
                    if (m_InputType == InputType.Controller) yield break;

                    m_InputType = InputType.Controller;
                    left = XRMan.Controllers.Left;
                    right = XRMan.Controllers.Right;
                    HandleControllerSession.Instance.ChangeControllerType((int)m_InputType);

                    ARHandManager.Instance.SetActive(false);
                    HandleControllerManager.Instance.SetActive(true & isActive);
                    Debug.Log($"ControllerManager, InitDynamicMode Change to HandleController, leftHand = {Left.GetType().Name}, rightHand = {Right.GetType().Name} ({Time.frameCount})");
                }
            }
        }

        private IEnumerator EnableDynamicMode_Coroutine()
        {
            yield return new WaitUntil(() => isInited);

            dynamicMode = true;

            //Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "ControllerType", Application.identifier + "," + "Dynamic");
        }

        private IEnumerator DisableDynamicMode_Coroutine(InputType type)
        {
            yield return new WaitUntil(() => isInited);

            dynamicMode = false;

            switch (type)
            {
                case InputType.Helmet:
                    StartCoroutine(ChangeToHelmetController());
                    break;
                case InputType.Hand:
                    StartCoroutine(ChangeToGestureController());
                    break;
                case InputType.Controller:
                    StartCoroutine(ChangeToHandleController());
                    break;
            }
        }

        /// <summary>
        /// 手柄连接状态改变
        /// </summary>
        /// <param name="handType"></param>
        /// <param name="connected"></param>
        private void OnHandleConnectChanged(HandType handType, bool connected)
        {
            if (!dynamicMode) return;
            if (isPaused) return;

            //Debug.Log($"ControllerManager, {handType} OnHandleConnectChanged, connected = {connected} ({Time.frameCount})");

            if (connected)
            {
                if (!isHandlesOutOfBound) StartCoroutine(ChangeToHandleController());
            }
            else
            {
                if (!HandleControllerManager.Instance.GetConnectState(HandType.Left) &&
                    !HandleControllerManager.Instance.GetConnectState(HandType.Right))
                {
                    keepHandlesActive = false;
                    StartCoroutine(ChangeToGestureController());
                }
                isHandlesOutOfBound = true;
            }

            Debug.Log($"ControllerManager, {handType} OnHandleConnectChanged, connected = {connected}, isHandlesOutOfBound = {isHandlesOutOfBound} ({Time.frameCount})");
        }

        /// <summary>
        /// 手柄按键状态改变
        /// </summary>
        /// <param name="handType"></param>
        /// <param name="pressed"></param>
        private void OnHandleButtonChanged(HandType handType, bool pressed)
        {
            if (!dynamicMode) return;
            if (isPaused) return;

            //Debug.Log($"ControllerManager, {handType} OnHandleButtonChanged, pressed = {pressed} ({Time.frameCount})");

            StartCoroutine(ChangeToHandleController());

            //按键按住过程中，保持手柄功能激活
            keepHandlesActive = pressed;

            //Debug.Log($"ControllerManager, {handType} OnHandleButtonChanged, isHandlesOutOfBound = {isHandlesOutOfBound} pressed = {pressed} ({Time.frameCount})");

            if (pressed) isHandlesOutOfBound = false;
        }

        /// <summary>
        /// 手柄握持状态改变
        /// </summary>
        /// <param name="handType"></param>
        /// <param name="isHeld"></param>
        private void OnHandleHoldChanged(HandType handType, bool isHeld)
        {
            if (!dynamicMode) return;
            if (isPaused) return;

            //Debug.Log($"ControllerManager, {handType} OnHandleHoldChanged, isHeld = {isHeld} ({Time.frameCount})");

            StartCoroutine(ChangeToHandleController());

            //手柄握持过程中，保持手柄功能激活
            keepHandlesActive = isHeld;

            //Debug.Log($"ControllerManager, {handType} OnHandleHoldChanged, isHandlesOutOfBound = {isHandlesOutOfBound} isHeld = {isHeld} ({Time.frameCount})");

            if (isHeld) isHandlesOutOfBound = false;
        }

        /// <summary>
        /// 手柄静置状态改变
        /// </summary>
        /// <param name="handType"></param>
        /// <param name="silent"></param>
        private void OnHandleSilenceChanged(HandType handType, bool silent)
        {
            if (!dynamicMode) return;
            if (isPaused) return;

            //Debug.Log($"ControllerManager, {handType} OnHandleSilenceChanged, silent = {silent} ({Time.frameCount})");
            //Debug.Log($"ControllerManager, {handType} OnHandleSilenceChanged, isHandlesOutOfBound = {isHandlesOutOfBound} !silent = {!silent} ({Time.frameCount})");

            if (!silent) isHandlesOutOfBound = false;

            if (silent && !keepHandlesActive)
            {
                StartCoroutine(ChangeToGestureController());
            }
        }

        /// <summary>
        /// (手柄算法)Server重连
        /// </summary>
        /// <param name="connected"></param>
        private void OnServerReconnected(bool connected)
        {
            Debug.Log($"ControllerManager, OnServerReconnected, connected = {connected} ({Time.frameCount})");
            if (connected && isInited)
            {
                //在Server重启后应通知Server同步到当前ControllerType
                StartCoroutine(CheckStatusWhenResume());
            }
        }

        /// <summary>
        /// (眼镜系统)进入待机状态
        /// </summary>
        private void OnServerStandby()
        {
            isGlassStandby = true;
            Debug.Log($"ControllerManager, OnServerSleep, isGlassStandby = {isGlassStandby} ({Time.frameCount})");
        }

        private IEnumerator ChangeToHelmetController()
        {
            if (isPaused) yield break;
            if (m_InputType == InputType.Helmet) yield break;

            //TODO

            if (dynamicMode)
                Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "ControllerType", Application.identifier + "," + "Dynamic");
            else
                Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "ControllerType", Application.identifier + "," + "Head");

            yield return new WaitUntil(() => SessionManager.Instance != null && SessionManager.Instance.IsInited);
            HandleControllerSession.Instance.ChangeControllerType((int)m_InputType);

        }

        private IEnumerator ChangeToGestureController()
        {
            if (isPaused) yield break;
            if (m_InputType == InputType.Hand) yield break;

            m_InputType = InputType.Hand;
            left = XRMan.Hands.Left;
            right = XRMan.Hands.Right;
            //HandleControllerSession.Instance.ChangeControllerType((int)m_ControllerType);

            HandleControllerManager.Instance.SetActive(false);
            ARHandManager.Instance.SetActive(true & isActive);
            Debug.Log($"ControllerManager, Change to GestureController, leftHand = {Left.GetType().Name}, rightHand = {Right.GetType().Name} ({Time.frameCount})");

            //Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "ControllerType", Application.identifier + "," + "Hand");
            if (dynamicMode)
                Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "ControllerType", Application.identifier + "," + "Dynamic");
            else
                Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "ControllerType", Application.identifier + "," + "Hand");

            yield return new WaitUntil(() => SessionManager.Instance != null && SessionManager.Instance.IsInited);
            HandleControllerSession.Instance.ChangeControllerType((int)m_InputType);
        }

        private IEnumerator ChangeToHandleController()
        {
            if (isPaused) yield break;
            if (m_InputType == InputType.Controller) yield break;

            m_InputType = InputType.Controller;
            left = XRMan.Controllers.Left;
            right = XRMan.Controllers.Right;
            //HandleControllerSession.Instance.ChangeControllerType((int)m_ControllerType);

            ARHandManager.Instance.SetActive(false);
            HandleControllerManager.Instance.SetActive(true & isActive);
            Debug.Log($"ControllerManager, Change to HandleController, leftHand = {Left.GetType().Name}, rightHand = {Right.GetType().Name} ({Time.frameCount})");

            //Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "ControllerType", Application.identifier + "," + "Controllers");
            if (dynamicMode)
                Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "ControllerType", Application.identifier + "," + "Dynamic");
            else
                Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "ControllerType", Application.identifier + "," + "Controllers");

            yield return new WaitUntil(() => SessionManager.Instance != null && SessionManager.Instance.IsInited);
            HandleControllerSession.Instance.ChangeControllerType((int)m_InputType);
        }
    }
}
