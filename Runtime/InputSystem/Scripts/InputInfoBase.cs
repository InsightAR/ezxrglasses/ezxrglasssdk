using System;
using EZXR.Glass.Inputs;
using EZXR.Glass.Runtime;
using UnityEngine;

namespace EZXR.Glass.Inputs
{
    public enum HandType
    {
        Left = 0,
        Right = 1
    }

    public abstract class InputInfoBase : MonoBehaviour
    {
        #region 单手信息
        /// <summary>
        /// 控制器类型：头控、手势、手柄
        /// </summary>
        [Obsolete("Use \"InputType\" instead!")]
        public InputType inputType;
        /// <summary>
        /// 控制器类型：头控、手势、手柄
        /// </summary>
        public InputType InputType { get { return inputType; } }
        /// <summary>
        /// 属于左手还是右手，0为左手，1为右手
        /// </summary>
        [Obsolete("Use \"HandType\" instead!")]
        [HideInInspector]
        public HandType handType;
        /// <summary>
        /// 属于左手还是右手，0为左手，1为右手
        /// </summary>
        public HandType HandType { get { return handType; } }

        public abstract bool Exist { get; }

        /// <summary>
        /// 手掌正在朝向头部，如果手掌朝向和手掌与头的连线的夹角小于angle则认为手掌是朝向了头部
        /// </summary>
        /// <param name="angle">默认90</param>
        /// <returns></returns>
        [Obsolete("Use \"IsPalmFacingHead\" instead!")]
        public abstract bool isPalmFacingHead(float angle = 90);
        public abstract bool IsPalmFacingHead(float angle = 90);


        #region 捏合（不论是否捏住了物体）
        /// <summary>
        /// 当前是否在做捏合动作
        /// </summary>
        [HideInInspector]
        [Obsolete("Use \"IsPinching\" instead!")]
        public bool isPinching;
        /// <summary>
        /// 当前手是否在做捏合动作
        /// </summary>
        public bool IsPinching { get { return isPinching; } }
        /// <summary>
        /// 手开始做捏合动作（仅仅开始捏合的帧为true）
        /// </summary>
        [Obsolete("Use \"StartPinch\" instead!")]
        [HideInInspector]
        public bool startPinch;
        /// <summary>
        /// 手开始做捏合动作（仅仅开始捏合的帧为true）
        /// </summary>
        public bool StartPinch { get { return startPinch; } }
        /// <summary>
        /// 手结束做捏合动作（仅仅结束捏合的帧为true）
        /// </summary>
        [Obsolete("Use \"EndPinch\" instead!")]
        [HideInInspector]
        public bool endPinch;
        /// <summary>
        /// 手结束做捏合动作（仅仅结束捏合的帧为true）
        /// </summary>
        public bool EndPinch { get { return endPinch; } }
        #endregion

        #region 抓取（抓住了物体）
        /// <summary>
        /// 指示手正在通过射线拖拽物体
        /// </summary>
        protected bool isRayGrabbing;
        /// <summary>
        /// 指示手正在通过射线拖拽物体
        /// </summary>
        public bool IsRayGrabbing
        {
            get
            {
                return isRayGrabbing;
            }
            private set
            {
                isRayGrabbing = value;

                if (isRayGrabbing)
                {
                    if (XRMan.Input.Left != null && XRMan.Input.Right != null && ((handType == HandType.Left && XRMan.Input.Right.IsRayGrabbing) || (handType == HandType.Right && XRMan.Input.Left.IsRayGrabbing)) && XRMan.Input.Left.CurRayGrabbingTarget == XRMan.Input.Right.CurRayGrabbingTarget)
                    {
                        isTwoHandRayGrabbing = true;
                    }
                }
                else
                {
                    isTwoHandRayGrabbing = false;
                }
            }
        }

        protected Transform curRayGrabbingTarget;
        /// <summary>
        /// 当前射线正在抓取的物体（非旋转手柄或者缩放角）
        /// </summary>
        public Transform RayGrabbingTarget
        {
            get
            {
                return curRayGrabbingTarget;
            }
            set
            {
                if (curRayGrabbingTarget == null && value == null)
                {
                    if (IsRayGrabbing)
                    {
                        IsRayGrabbing = false;
                        CurRayGrabbingSpatialObject = null;
                    }
                    return;
                }

                if (curRayGrabbingTarget == null && value != null)
                {
                    SpatialObject.PerformOnHandRayGrab(value.GetComponent<Collider>());
                }
                else if (curRayGrabbingTarget != null && value == null)
                {
                    SpatialObject.PerformOnHandRayRelease(curRayGrabbingTarget.GetComponent<Collider>());
                }

                curRayGrabbingTarget = value;

                if (value != null)
                {
                    IsRayGrabbing = true;
                    CurRayGrabbingSpatialObject = value.GetComponent<SpatialObject>();
                }
                else
                {
                    IsRayGrabbing = false;
                    CurRayGrabbingSpatialObject = null;
                }
            }
        }
        /// <summary>
        /// 当前射线正在抓取的物体（非旋转手柄或者缩放角）
        /// </summary>
        [Obsolete("Use \"RayGrabbingTarget\" instead!")]
        public Transform CurRayGrabbingTarget
        {
            get
            {
                return curRayGrabbingTarget;
            }
            set
            {
                if (curRayGrabbingTarget == null && value == null)
                {
                    if (IsRayGrabbing)
                    {
                        IsRayGrabbing = false;
                        CurRayGrabbingSpatialObject = null;
                    }
                    return;
                }

                if (curRayGrabbingTarget == null && value != null)
                {
                    SpatialObject.PerformOnHandRayGrab(value.GetComponent<Collider>());
                }
                else if (curRayGrabbingTarget != null && value == null)
                {
                    SpatialObject.PerformOnHandRayRelease(curRayGrabbingTarget.GetComponent<Collider>());
                }

                curRayGrabbingTarget = value;

                if (value != null)
                {
                    IsRayGrabbing = true;
                    CurRayGrabbingSpatialObject = value.GetComponent<SpatialObject>();
                }
                else
                {
                    IsRayGrabbing = false;
                    CurRayGrabbingSpatialObject = null;
                }
            }
        }

        protected SpatialObject curRayGrabbingSpatialObject;
        public SpatialObject RayGrabbingSpatialObject
        {
            get
            {
                return curRayGrabbingSpatialObject;
            }
            set
            {
                curRayGrabbingSpatialObject = value;
            }
        }
        [Obsolete("Use \"RayGrabbingSpatialObject\" instead!")]
        public SpatialObject CurRayGrabbingSpatialObject
        {
            get
            {
                return curRayGrabbingSpatialObject;
            }
            set
            {
                curRayGrabbingSpatialObject = value;
            }
        }

        /// <summary>
        /// 指示手正在通过近距离抓取的方式拖拽物体
        /// </summary>
        private bool isCloseGrabbing;
        /// <summary>
        /// 指示手正在通过近距离抓取的方式拖拽物体
        /// </summary>
        public bool IsCloseGrabbing
        {
            get
            {
                return isCloseGrabbing;
            }
            private set
            {
                isCloseGrabbing = value;
                if (isCloseGrabbing)
                {
                    //Debug.Log("IsCloseGrabbing: " + handType.ToString() + ", " + XRMan.Hands.Left.IsCloseGrabbing + ", " + XRMan.Hands.Right.IsCloseGrabbing + ", " + (InputSystem.leftHand.CurCloseGrabbingTarget == InputSystem.rightHand.CurCloseGrabbingTarget));
                    if (((handType == HandType.Left && XRMan.Hands.Right.IsCloseGrabbing) || (handType == HandType.Right && XRMan.Hands.Left.IsCloseGrabbing)) && XRMan.Input.Left.CurCloseGrabbingTarget == XRMan.Input.Right.CurCloseGrabbingTarget)
                    {
                        isTwoHandCloseGrabbing = true;
                    }
                }
                else
                {
                    isTwoHandCloseGrabbing = false;
                }
            }
        }

        protected Transform curCloseGrabbingTarget;
        /// <summary>
        /// 当前正在近距离抓取的Transform（非旋转手柄或者缩放角）
        /// </summary>
        /// <param name="other"></param>
        public Transform CloseGrabbingTarget
        {
            get
            {
                return curCloseGrabbingTarget;
            }
            set
            {
                if (curCloseGrabbingTarget == null && value == null)
                {
                    if (IsCloseGrabbing)
                    {
                        IsCloseGrabbing = false;
                        CurCloseGrabbingSpatialObject = null;
                    }
                    return;
                }

                if (curCloseGrabbingTarget == null && value != null)
                {
                    SpatialObject.PerformOnHandTriggerGrab(value.GetComponent<Collider>());
                }
                else if (curCloseGrabbingTarget != null && value == null)
                {
                    SpatialObject.PerformOnHandTriggerRelease(curCloseGrabbingTarget.GetComponent<Collider>());
                }

                curCloseGrabbingTarget = value;

                if (value != null)
                {
                    IsCloseGrabbing = true;
                    CurCloseGrabbingSpatialObject = value.GetComponent<SpatialObject>();
                }
                else
                {
                    IsCloseGrabbing = false;
                    CurCloseGrabbingSpatialObject = null;
                }
            }
        }
        /// <summary>
        /// 当前正在近距离抓取的Transform（非旋转手柄或者缩放角）
        /// </summary>
        /// <param name="other"></param>
        [Obsolete("Use \"CloseGrabbingTarget\" instead!")]
        public Transform CurCloseGrabbingTarget
        {
            get
            {
                return curCloseGrabbingTarget;
            }
            set
            {
                if (curCloseGrabbingTarget == null && value == null)
                {
                    if (IsCloseGrabbing)
                    {
                        IsCloseGrabbing = false;
                        CurCloseGrabbingSpatialObject = null;
                    }
                    return;
                }

                if (curCloseGrabbingTarget == null && value != null)
                {
                    SpatialObject.PerformOnHandTriggerGrab(value.GetComponent<Collider>());
                }
                else if (curCloseGrabbingTarget != null && value == null)
                {
                    SpatialObject.PerformOnHandTriggerRelease(curCloseGrabbingTarget.GetComponent<Collider>());
                }

                curCloseGrabbingTarget = value;

                if (value != null)
                {
                    IsCloseGrabbing = true;
                    CurCloseGrabbingSpatialObject = value.GetComponent<SpatialObject>();
                }
                else
                {
                    IsCloseGrabbing = false;
                    CurCloseGrabbingSpatialObject = null;
                }
            }
        }

        protected SpatialObject curCloseGrabbingSpatialObject;
        public SpatialObject CloseGrabbingSpatialObject
        {
            get
            {
                return curCloseGrabbingSpatialObject;
            }
            set
            {
                curCloseGrabbingSpatialObject = value;
            }
        }
        [Obsolete("Use \"CloseGrabbingSpatialObject\" instead!")]
        public SpatialObject CurCloseGrabbingSpatialObject
        {
            get
            {
                return curCloseGrabbingSpatialObject;
            }
            set
            {
                curCloseGrabbingSpatialObject = value;
            }
        }

        /// <summary>
        /// 手当前是否正在抓取物体（无论射线还是近距离）
        /// </summary>
        public bool IsGrabbing
        {
            get
            {
                if (CurCloseGrabbingTarget == null && CurRayGrabbingTarget == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        private Vector3 grabPosition;
        /// <summary>
        /// 捏合时刻抓住的点的世界坐标
        /// </summary>
        public Vector3 GrabPosition { get { return grabPosition; } set { grabPosition = value; } }

        /// <summary>
        /// 捏合时刻抓住的点在被抓住的物体的局部坐标系下的坐标
        /// </summary>
        [Obsolete("Use \"GrabPositionLocal\" instead!")]
        [HideInInspector]
        public Vector3 grabLocalPoint;
        /// <summary>
        /// 捏合时刻抓住的点在被抓住的物体的局部坐标系下的坐标
        /// </summary>
        public Vector3 GrabPositionLocal { get { return grabLocalPoint; } set { grabLocalPoint = value; } }

        /// <summary>
        /// 得到当前手正在抓取的物体（无论射线还是近距离）
        /// </summary>
        public Transform GrabbingTarget
        {
            get
            {
                if (CurCloseGrabbingTarget == null)
                {
                    if (CurRayGrabbingTarget != null)
                    {
                        return CurRayGrabbingTarget;
                    }
                }
                else
                {
                    return CurCloseGrabbingTarget;
                }
                return null;
            }
        }

        /// <summary>
        /// 得到当前手正在抓取的物体（无论射线还是近距离）
        /// </summary>
        [Obsolete("Use \"GrabbingTarget\" instead!")]
        public Transform CurGrabbingTarget
        {
            get
            {
                if (CurCloseGrabbingTarget == null)
                {
                    if (CurRayGrabbingTarget != null)
                    {
                        return CurRayGrabbingTarget;
                    }
                }
                else
                {
                    return CurCloseGrabbingTarget;
                }
                return null;
            }
        }

        public SpatialObject GrabbingSpatialObject
        {
            get
            {
                if (CloseGrabbingSpatialObject == null)
                {
                    if (RayGrabbingSpatialObject != null)
                    {
                        return RayGrabbingSpatialObject;
                    }
                }
                else
                {
                    return CloseGrabbingSpatialObject;
                }
                return null;
            }
        }
        #endregion

        #region 拖拽
        /// <summary>
        /// 正在拖拽
        /// </summary>
        [Obsolete("Use \"IsDragging\" instead!")]
        [HideInInspector]
        public bool isDragging;
        /// <summary>
        /// 正在拖拽
        /// </summary>
        public bool IsDragging { get { return isDragging; } }
        #endregion

        #region 射线
        /// <summary>
        /// 射线交互模块的启用状态
        /// </summary>
        public virtual bool RaycastInteraction { get { return false; } set { } }

        public virtual bool RI_Internal
        {
            get
            {
                return false;
            }
            set
            {

            }
        }

        private InputRaycast inputRaycast;
        /// <summary>
        /// 获得当前输入设备的射线交互模块
        /// </summary>
        public InputRaycast InputRaycast
        {
            get
            {
                if (inputRaycast == null)
                {
                    inputRaycast = GetComponent<InputRaycast>();
                }
                return inputRaycast;
            }
        }

        /// <summary>
        /// 设置射线显示还是隐藏
        /// </summary>
        /// <param name="state">true为显示</param>
        [Obsolete("Use \"InputRaycast.SetRayLineState\" instead!")]
        public virtual void SetRayLineState(bool state)
        {
            InputRaycast.SetRayLineState(state);
        }

        /// <summary>
        /// 获得射线远端圆圈的显示隐藏状态
        /// </summary>
        /// <returns></returns>
        [Obsolete("Use \"InputRaycast.GetRayRingState\" instead!")]
        public bool GetRayRingState()
        {
            return InputRaycast.GetRayRingState();
        }

        /// <summary>
        /// 设置射线远端圆圈的显示隐藏
        /// </summary>
        /// <param name="state"></param>
        [Obsolete("Use \"InputRaycast.SetRayRingState\" instead!")]
        public void SetRayRingState(bool state)
        {
            InputRaycast.SetRayRingState(state);
        }

        /// <summary>
        /// 射线起始生效距离，决定了射线起始点的位置（从肩膀点到食指根节点为发射方向，食指根节点Position + 发射方向 * 生效距离）
        /// </summary>
        [Obsolete("Use \"RayStartDistance\" instead!")]
        [HideInInspector]
        public float rayStartDistance = 0.15f;
        /// <summary>
        /// 射线起始生效距离，决定了射线起始点的位置（从肩膀点到食指根节点为发射方向，食指根节点Position + 发射方向 * 生效距离）
        /// </summary>
        public float RayStartDistance { get { return rayStartDistance; } set { rayStartDistance = value; } }
        /// <summary>
        /// 手的射线起始点
        /// </summary>
        [Obsolete("Use \"RayStartPoint\" instead!")]
        [HideInInspector]
        public Vector3 rayPoint_Start;
        /// <summary>
        /// 手的射线起始点
        /// </summary>
        public Vector3 RayStartPoint { get { return rayPoint_Start; } }
        /// <summary>
        /// 手的射线终止点
        /// </summary>
        [Obsolete("Use \"RayEndPoint\" instead!")]
        [HideInInspector]
        public Pose rayPoint_End;
        /// <summary>
        /// 手的射线终止点
        /// </summary>
        public Pose RayEndPoint { get { return rayPoint_End; } }
        /// <summary>
        /// 从射线起始点到终止点的射线方向（单位向量）
        /// </summary>
        [Obsolete("Use \"RayDirection\" instead!")]
        [HideInInspector]
        public Vector3 rayDirection;
        /// <summary>
        /// 从射线起始点到终止点的射线方向（单位向量）
        /// </summary>
        public Vector3 RayDirection { get { return rayDirection; } }
        #endregion

        #region 触碰信息（包括射线打到以及近距离食指指尖触碰）
        protected bool isRayContacting;
        /// <summary>
        /// 指示当前射线是否正射在某个物体上
        /// </summary>
        public bool IsRayContacting
        {
            get
            {
                return isRayContacting;
            }
        }
        /// <summary>
        /// 当前射线碰到的Collider（仅仅是射线碰到，并不是射线抓取）
        /// </summary>
        protected Collider curRayContactingTarget;
        public Collider CurRayContactingTarget
        {
            get
            {
                return curRayContactingTarget;
            }
            set
            {
                if (value == null)
                {
                    isRayContacting = false;
                }
                else
                {
                    isRayContacting = true;
                }

                //if (curRayContactingTarget != value)
                {
                    //记录上一帧被射线打到的物体的相关信息
                    LastRayContactingTarget = curRayContactingTarget;

                    curRayContactingTarget = value;

                    if (curRayContactingTarget != null)
                    {
                        curRayContactingTarget_Renderer = (curRayContactingTarget == null ? null : curRayContactingTarget.GetComponent<Renderer>());
                        if (curRayContactingTarget_Renderer != null)
                        {
                            if (curRayContactingTarget_PropertyBlock == null)
                            {
                                curRayContactingTarget_PropertyBlock = new MaterialPropertyBlock();
                            }
                            curRayContactingTarget_Renderer.GetPropertyBlock(curRayContactingTarget_PropertyBlock);
                        }

                        if (curRayContactingTarget_Renderer != null && curRayContactingTarget_PropertyBlock != null)
                        {
                            curRayContactingTarget_PropertyBlock.SetVector("_HitWorldPos", Vector3.one * 1000);
                            curRayContactingTarget_Renderer.SetPropertyBlock(curRayContactingTarget_PropertyBlock);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 射线碰到Collider的时候触发此事件
        /// </summary>
        public event Action<Collider, bool> Event_GetRayCastResult;

        /// <summary>
        /// 由HandRaycast调用此处来设置当前射线射击到的目标
        /// </summary>
        /// <param name="other">射线打到的Collider</param>
        /// <param name="isUI">射线打到的是否是UI</param>
        public void SetCurRayContactingTarget(Collider other, bool isUI = false)
        {
            if (CurRayContactingTarget != null && other != null)
            {
                if (CurRayContactingTarget != other)
                {
                    SpatialObject.PerformOnHandRayExit(CurRayContactingTarget);
                    SpatialObject.PerformOnHandRayEnter(other);
                }
                else
                {
                    SpatialObject.PerformOnHandRayStay(other);
                }
            }
            else if (CurRayContactingTarget == null && other != null)
            {
                SpatialObject.PerformOnHandRayEnter(other);
            }
            else if (CurRayContactingTarget != null && other == null)
            {
                SpatialObject.PerformOnHandRayExit(CurRayContactingTarget);
            }

            CurRayContactingTarget = other;

            if (Event_GetRayCastResult != null)
            {
                //将射线检测结果分发出
                Event_GetRayCastResult(other, isUI);
            }
        }
        [HideInInspector]
        public Renderer curRayContactingTarget_Renderer;
        [HideInInspector]
        public MaterialPropertyBlock curRayContactingTarget_PropertyBlock;

        /// <summary>
        /// 射线上一次碰到的Collider
        /// </summary>
        protected Collider lastRayContactingTarget;
        public Collider LastRayContactingTarget
        {
            get
            {
                return lastRayContactingTarget;
            }
            set
            {
                if (lastRayContactingTarget != value)
                {
                    lastRayContactingTarget = value;
                    if (lastRayContactingTarget != null)
                    {
                        lastRayContactingTarget_Renderer = (lastRayContactingTarget == null ? null : lastRayContactingTarget.GetComponent<Renderer>());
                        if (lastRayContactingTarget_Renderer != null)
                        {
                            if (lastRayContactingTarget_PropertyBlock == null)
                            {
                                lastRayContactingTarget_PropertyBlock = new MaterialPropertyBlock();
                            }
                            lastRayContactingTarget_Renderer.GetPropertyBlock(lastRayContactingTarget_PropertyBlock);
                        }
                    }
                }
            }
        }
        [HideInInspector]
        public Renderer lastRayContactingTarget_Renderer;
        [HideInInspector]
        public MaterialPropertyBlock lastRayContactingTarget_PropertyBlock;
        #endregion

        #region 与非SpatialObject的物体交互
        [HideInInspector]
        public Transform lastPointingObject;
        private bool isRayPressing;
        /// <summary>
        /// 仅用于非SpatialObject物体，指示手正在通过射线点击非SpatialObject，会向这种物体发送pointerDownHandler事件，通常用于UGUI这类的点击
        /// </summary>
        [HideInInspector]
        public bool IsRayPressing
        {
            get
            {
                return isRayPressing;
            }
            set
            {
                if (isRayPressing != value)
                {
                    if (value)
                    {
                        IsRayPressDown = true;
                    }
                    else
                    {
                        IsRayPressUp = true;
                    }
                }
                else
                {
                    IsRayPressDown = false;
                    IsRayPressUp = false;
                }
                isRayPressing = value;

            }
        }
        [HideInInspector]
        public bool IsRayPressDown;
        [HideInInspector]
        public bool IsRayPressUp;
        [HideInInspector]
        [Obsolete("Use \"CurPressingObject\" instead!")]
        public Transform curPressingObject;
        public Transform CurPressingObject { get { return curPressingObject; } set { curPressingObject = value; } }

        #endregion

        #region 交互能力开关
        /// <summary>
        /// 设置或获得当前手的显示状态（仅仅影响显示，并不影响交互）
        /// </summary>
        public virtual bool Visibility { get { return false; } set { } }

        /// <summary>
        /// 设置或获得近距离交互状态
        /// </summary>
        public virtual bool TouchInteraction
        {
            get
            {
                return false;
            }
            set
            {

            }
        }

        /// <summary>
        /// 设置或获得手部的物理碰撞交互状态
        /// </summary>
        public virtual bool PhysicsInteraction
        {
            get
            {
                return false;
            }
            set
            {

            }
        }
        #endregion

        #region 手指近距离触摸UGUI信息
        /// <summary>
        /// 当前是否切换到近距离触摸UGUI模式
        /// </summary>
        [HideInInspector]
        public bool isCloseContactingUGUI;
        #endregion
        #endregion

        #region 双手信息
        /// <summary>
        /// 正在执行双手射线抓取操作
        /// </summary>
        [Obsolete("Use \"IsTwoHandRayGrabbing\" instead!")]
        public static bool isTwoHandRayGrabbing;
        /// <summary>
        /// 正在执行双手射线抓取操作
        /// </summary>
        public static bool IsTwoHandRayGrabbing { get { return isTwoHandRayGrabbing; } }
        /// <summary>
        /// 正在执行双手近距离抓取操作
        /// </summary>
        [Obsolete("Use \"IsTwoHandCloseGrabbing\" instead!")]
        public static bool isTwoHandCloseGrabbing;
        /// <summary>
        /// 正在执行双手近距离抓取操作
        /// </summary>
        public static bool IsTwoHandCloseGrabbing { get { return isTwoHandCloseGrabbing; } }
        /// <summary>
        /// 双手捏合时，左手当前捏住的点的世界坐标
        /// </summary>
        [Obsolete("Use \"LeftGrabPointPosition\" instead!")]
        public static Vector3 targetObjectPosition_Left;
        /// <summary>
        /// 双手捏合时，左手当前捏住的点的世界坐标
        /// </summary>
        public static Vector3 LeftGrabPointPosition { get { return targetObjectPosition_Left; } }
        /// <summary>
        /// 双手捏合时，右手当前捏住的点的世界坐标
        /// </summary>
        [Obsolete("Use \"RightGrabPointPosition\" instead!")]
        public static Vector3 targetObjectPosition_Right;
        /// <summary>
        /// 双手捏合时，右手当前捏住的点的世界坐标
        /// </summary>
        public static Vector3 RightGrabPointPosition { get { return targetObjectPosition_Right; } }
        #endregion

        /// <summary>
        /// 初始化，设置当前输入设备的类型以及所属的手是左手还是右手
        /// </summary>
        /// <param name="inputType">输入设备类型：头控、手势、手柄</param>
        /// <param name="handType">0为左手，1为右手</param>
        public virtual void Init(InputType inputType, HandType handType)
        {
            this.inputType = inputType;
            this.handType = handType;

            //if (handType == HandType.Left) leftHand = this;
            //if (handType == HandType.Right) rightHand = this;
        }

        protected virtual void Awake()
        {

        }

        protected virtual void Start()
        {

        }

        protected virtual void FixedUpdate()
        {
            
        }

        protected virtual void Update()
        {
            UpdatePinching();
        }

        protected virtual void UpdatePinching()
        {

        }
    }
}
