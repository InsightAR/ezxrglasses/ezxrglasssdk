﻿using System;
using UnityEngine;

namespace EZXR.Glass.Inputs
{
    public enum InputType 
    {
        [InspectorName(null)]
        Helmet = 0,     //头控
        Hand = 1,   //手势
        Controller = 2,     //手柄
        [Obsolete("Use \"Controller\" instead!", true)]
        Controllers = -1,     //手柄
        [Obsolete("Use \"Hand\" instead!", true)]
        HandTracking = -2,   //手势
    }
}
