
using UnityEngine;

namespace EZXR.Glass.Inputs
{
    public enum InteractionType
    {
        All = 0,
        NearInteraction,
        RemoteInteraction
    }
}
