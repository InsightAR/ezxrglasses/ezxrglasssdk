﻿using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

namespace EZXR.Glass.Runtime
{
    public partial class ARAbilities : MonoBehaviour
    {
        #region InputSystem
        [MenuItem("GameObject/XR Abilities/InputSystem", false, 20)]
        public static void EnableInputSystem()
        {
            if (FindObjectOfType<EZXR.Glass.Inputs.InputSystem>() == null)
            {
                string filePath = "Assets/EZXRGlassSDK/Runtime/InputSystem/Prefabs/InputSystem.prefab";
                if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                {
                    filePath = AssetDatabase.GUIDToAssetPath("4c1509f78a7904c9980227d2731ebe61");
                }
                Runtime.PrefabUtility.InstantiatePrefabWithUndoAndSelection(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
            }
        }
        #endregion

        #region EventSystem
        [MenuItem("GameObject/XR Abilities/Additional.../EventSystem", false, 20)]
        public static void EnableEventSystem()
        {
            if (FindObjectOfType<HandInputModule>() == null)
            {
                string filePath = "Assets/EZXRGlassSDK/CoreRuntimeHandTracking/Resources/EventSystem.prefab";
                if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                {
                    filePath = AssetDatabase.GUIDToAssetPath("6e33b2edc7ac4784ebe0163210ea60f2");
                }
                Runtime.PrefabUtility.InstantiatePrefabWithUndoAndSelection(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
            }
        }
        #endregion
    }
}