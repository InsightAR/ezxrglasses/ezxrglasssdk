using EZXR.Glass.Inputs;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(EZXR.Glass.Inputs.InputSystem))]
[CanEditMultipleObjects]
public class InputSystemInspector : Editor
{
    private SerializedProperty dynamicMode;
    private SerializedProperty inputType;
    public InputSystem inputSystem;

    void OnEnable()
    {
        inputSystem = (InputSystem)target;

        dynamicMode = serializedObject.FindProperty("dynamicMode");
        inputType = serializedObject.FindProperty("m_InputType");

        //Debug.Log($"dynamicMode: {dynamicMode.boolValue}");
        //Debug.Log($"controllerType: {controllerType.enumValueIndex}");
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.PropertyField(dynamicMode);
        if (dynamicMode.boolValue)
        {
            if (inputSystem.handManager != null)
            {
                inputSystem.handManager.m_LeftHand.SetActive(true);
                inputSystem.handManager.m_RightHand.SetActive(true);
            }
            if (inputSystem.controllerManager != null)
            {
                inputSystem.controllerManager.leftController.gameObject.SetActive(true);
                inputSystem.controllerManager.rightController.gameObject.SetActive(true);
            }
        }
        else
        {
            EditorGUILayout.PropertyField(inputType);
            if (inputSystem.inputType == InputType.Hand)
            {
                if (inputSystem.handManager != null)
                {
                    inputSystem.handManager.m_LeftHand.SetActive(true);
                    inputSystem.handManager.m_RightHand.SetActive(true);
                }
                if (inputSystem.controllerManager != null)
                {
                    inputSystem.controllerManager.leftController.gameObject.SetActive(false);
                    inputSystem.controllerManager.rightController.gameObject.SetActive(false);
                }
            }
            else if (inputSystem.inputType == InputType.Controller)
            {
                if (inputSystem.handManager != null)
                {
                    inputSystem.handManager.m_LeftHand.SetActive(false);
                    inputSystem.handManager.m_RightHand.SetActive(false);
                }
                if (inputSystem.controllerManager != null)
                {
                    inputSystem.controllerManager.leftController.gameObject.SetActive(true);
                    inputSystem.controllerManager.rightController.gameObject.SetActive(true);
                }
            }
        }

        serializedObject.ApplyModifiedProperties();
    }
}
