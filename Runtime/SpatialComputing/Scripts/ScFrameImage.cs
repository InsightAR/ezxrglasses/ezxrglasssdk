using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZXR.Glass.SixDof;
using EZXR.Glass.Runtime;

namespace EZXR.Glass.SpatialComputing 
{

    public struct ImageFrameData 
    {
        // YUV420, 图像format先留空
        public IntPtr fullImage;       
        
        // image height
        public int    width;

        // image width
        public int    height;

        // pinhole: k1, k2, p1, p2;
        // fisheye: k1, k2, k3, k4;
        public float[] distortionCoefficient; 

        // 0: pinhole,  1: fisheye;
        public int distortionType;      

        // intrinsics,  fx,fy,cx,cy;
        public float[] intrinsics;        

        // image time stamp
        public double imageTime;

        // head pose accoridng to the image timestamp;
        public Pose headPoseImgTs;       

        // head pose on image-camera
        public float[] imageToHeadPoseOffset;

        // 拍摄时角速度，若负数则为无效值
        public float captureAngularVelocity;

        public static ImageFrameData Create() {
            return new ImageFrameData(){
                fullImage = IntPtr.Zero,
                width = 0,
                height = 0,
                distortionCoefficient = new float[4] {0,0,0,0},
                distortionType = 0,
                intrinsics = new float[4] {0,0,0,0},
                imageTime = 0,
                headPoseImgTs = Pose.identity,
                imageToHeadPoseOffset = new float[16],
                captureAngularVelocity = -1.0f
            };
        }

        public override string ToString()
        {
            return string.Format($"width:{width}, height:{height}, " + 
                    $"distortionCoefficient:{distortionCoefficient[0]}, {distortionCoefficient[1]}, {distortionCoefficient[2]}, {distortionCoefficient[3]}, " +
                    $"distortionType:{distortionType}, " +
                    $"imageTime:{imageTime}, " +
                    $"headPoseImgTs pos:{headPoseImgTs.position.x}, {headPoseImgTs.position.y}, {headPoseImgTs.position.z}, " + 
                    $"headPoseImgTs rot:{headPoseImgTs.rotation.x}, {headPoseImgTs.rotation.y}, {headPoseImgTs.rotation.z}, {headPoseImgTs.rotation.w}, " + 
                    $"imageToHeadPoseOffset: row0 {imageToHeadPoseOffset[0]}, {imageToHeadPoseOffset[1]}, {imageToHeadPoseOffset[2]}, {imageToHeadPoseOffset[3]}, " + 
                    $"row1 {imageToHeadPoseOffset[4]}, {imageToHeadPoseOffset[5]}, {imageToHeadPoseOffset[6]}, {imageToHeadPoseOffset[7]}, " +
                    $"row2 {imageToHeadPoseOffset[8]}, {imageToHeadPoseOffset[9]}, {imageToHeadPoseOffset[10]}, {imageToHeadPoseOffset[11]}, " +
                    $"row3 {imageToHeadPoseOffset[12]}, {imageToHeadPoseOffset[13]}, {imageToHeadPoseOffset[14]}, {imageToHeadPoseOffset[15]}, " + 
                    $"angularvelocity {captureAngularVelocity} "
                    );
        }
    }
    [Serializable]
    public class RequestData
    {
        [Serializable]
        public class Alg
        {
            public string imageEncodingData;
            public string protobufEncodingData;
        }

        public Alg alg;
        public long timestamp;
        public string sign;

        public RequestData()
        {
            alg = new Alg();
        }
    }
    [Serializable]
    public class ResponeData
    {
        [Serializable]
        public class Result
        {
            public string protobufEncodingData;
            public int algCode;
        }
        public string code;
        public string msg;
        public Result result;
    }



}

