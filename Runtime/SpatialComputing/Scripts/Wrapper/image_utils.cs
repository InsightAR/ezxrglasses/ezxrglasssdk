
using System;
using System.Runtime.InteropServices;

namespace EZXR.Glass.SpatialComputing
{
    public enum SC_ImageFormat
    {
        SC_Image_INVALID       = -1,

        SC_Image_GRAY          = 0,     // 按照 @xuninghao 上面的注释，可以直接进行压缩
        
        SC_Image_RGB_888       = 1,     // https://developer.apple.com/documentation/accelerate/1533062-vimageconvert_rgba8888torgb888
        SC_Image_BGRA_8888     = 2,     // RGB_888、BGRA_8888代表每个通道都是8bit，使用opencv可以直接转换
        
        SC_Image_YUV420_YU12   = 11,    // YUV420P，opencv的C++头文件里，没有YU12字符，上面的csdn链接写的是YU12和YV12都用YUV420P即可
        SC_Image_YUV420_YV12   = 12,    // YUV420P，opencv的C++头文件里有，就是YUV420P
        SC_Image_YUV420_NV12   = 13,    // https://blog.csdn.net/u012633319/article/details/95669597，12和21的UV顺序不同 
        SC_Image_YUV420_NV21   = 14     // 还是区分，不容易引入隐藏bug
    }

    public enum SC_CameraType
    {
        SC_PINHOLE = 0,
        SC_FISHEYE = 1,
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SC_InputImage
    {
        public SC_ImageFormat format;
        public IntPtr ptr0;
        public int len_ptr0;
        public IntPtr ptr1;
        public int len_ptr1;
        public IntPtr ptr2;
        public int len_ptr2;
        public double timestamp_s;
        public int width;
        public int height;
        public float fx;
        public float fy;
        public float cx;
        public float cy;
        public float k1;
        public float k2;
        public float k3_p1;
        public float k4_p2;
        public SC_CameraType camera_type;
    }

    public static partial class ExternApi
    {
        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool SC_mergeRawImageTwoPlanes(SC_InputImage input_image, out IntPtr ptr_buffer);

        [DllImport(LIBRARY_NAME, CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool SC_ConvertRawImageToJpeg(SC_InputImage input_image, out IntPtr jpg_buffer, out UIntPtr jpg_buffer_size);
    }
}