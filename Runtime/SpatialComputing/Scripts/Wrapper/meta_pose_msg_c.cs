
using System.Runtime.InteropServices;

namespace EZXR.Glass.SpatialComputing
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Pose2dMsgC
    {
        public PoseType pose_type;
        public double t_s;
        public int map_id;
        public double lon;
        public double lat;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public float[] xy;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public float[] xy_uncertainty;
        public float compass_clockwise;
        public float compass_uncertainty;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Pose3dMsgC
    {
        public PoseType pose_type;
        public double t_s;
        public int map_id;
        public double state_t_s;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public float[] pose;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public float[] xyz_uncertainty;
        public float traj_length;
    }
    [StructLayout(LayoutKind.Sequential)]
    public struct FusePose3dMsgC
    {
        public Pose3dMsgC track_pose_msg;
        public Pose3dMsgC loc_pose_msg;
        public Pose3dMsgC fuse_pose_msg;
        public double fuse_t_s;
    }
}