using System;
using UnityEngine;
using AOT;
using System.Collections.Generic;
using UnityEngine.UI;
using EZXR.Glass.Runtime;
using EZXR.Glass.Runtime.Threading;
using EZXR.Glass.Network.WebRequest;
using System.Runtime.InteropServices;

namespace EZXR.Glass.SpatialComputing
{
    public class SpatialComputingController : MonoBehaviour
    {
        //public static SpatialComputingController CurrentInstance {
        //    get {
        //        return _CurrentInstance;
        //    }
        //}
        private static SpatialComputingController _CurrentInstance;
        /// <summary>
        /// 定位资源的URL
        /// 初始值为钱江世纪公园C6中庭地图
        /// </summary>
        public static string LocURL = "https://yx-reloc.easexr.com/managed/mng-reloc-1026/api/alg/cloud/aw/reloc?map=4460530123071";
        
        private static int _InputImageWidth;
        private static int _InputImageHeight;


        public bool m_imgUndistOnDevice = false; 
        public SC_VPSResultState currentVpsResultState = new SC_VPSResultState();

        private DeviceType m_deviceType = DeviceType.EzxrGlass6dof;
        private string m_sdk_version = "1.2.0";
        
        private IntPtr m_spatialComputingHandle;
        private bool m_vpsRequestManualOnce = false;
        private bool m_vpsRequestAuto = true;
        private double m_timestampNow = 0;
        private Matrix4x4 m_cameraTransformNow;

        private Matrix4x4 m_trackFrameTransformNow;
        private Matrix4x4 m_trackFrameToCameraOffset = Matrix4x4.identity;
  
        private bool m_fisrtFrameArrived = true;


        void Awake()
        {
            _CurrentInstance = this;
        }

        void Start()
        {
            m_fisrtFrameArrived = true;
       
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                m_deviceType = DeviceType.ARKit;
            }
            Loom.Initialize();
            
        }

        void OnDestroy()
        {
        }

        public void useImageUndistortion(bool useUndist) {
            m_imgUndistOnDevice = useUndist;
            if (m_spatialComputingHandle == IntPtr.Zero)
            {
                Debug.LogWarning("useImageUndistortion failed ：scSpatialComputing not valid");
                return;
            }
            SpatialComputing.ExternApi.scSetBoolDoUndistortSC(m_spatialComputingHandle, m_imgUndistOnDevice);
        }
        public void destroySpatialComputingVPS()
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
                SpatialComputing.ExternApi.scSpatialComputingDestroy(m_spatialComputingHandle);
        }
        public void createSpatialComputingVPS(string assetPath) {

            IntPtr t_spatialComputingHandle = SpatialComputing.ExternApi.scSpatialComputingCreate();
            if (t_spatialComputingHandle == IntPtr.Zero)
            {
                Debug.LogError("scSpatialComputingCreate failed");
                return;
            }
            SpatialComputing.ExternApi.scSetRequestFrameCallingSC(t_spatialComputingHandle, OnCloudLocRequestCallback);
            SpatialComputing.ExternApi.scSetBoolDoUndistortSC(t_spatialComputingHandle, m_imgUndistOnDevice);

            string configPath = assetPath + "/localizer_fusion_config.json";
            /*var result = */
            SpatialComputing.ExternApi.scSetSmoothConfig(t_spatialComputingHandle, configPath);
            //Debug.Log($"xxxxxx, scSetSmoothConfig result: {result}");
            string vps_configPath = assetPath + "/mock_vpsc_config.json";
            SpatialComputing.ExternApi.scSetVPSCConfig(t_spatialComputingHandle, vps_configPath);
            //Debug.Log($"xxxxxx, scSetVPSCConfig result: {result}");
            string vps_image_state_path = assetPath + "/vps_image_state.json";
            SpatialComputing.ExternApi.scSetVPSImageStateConfig(t_spatialComputingHandle, vps_image_state_path);
            //Debug.Log($"xxxxxx, scSetVPSImageStateConfig result: {result}");

            //set VPSDeviceInfoC
            VPSDeviceInfoC vpsDeviceInfoC = new VPSDeviceInfoC();
            vpsDeviceInfoC.device_brand = SystemInfoHelper.GetDeivceName();
            vpsDeviceInfoC.product_name = SystemInfoHelper.GetDeviceModel();
            string device_serial_number = NativeLib.GetDeviceId();
            if (device_serial_number == null)
            {
                device_serial_number = SystemInfoHelper.GetDeviceUniqueIdentifier();
            }
            vpsDeviceInfoC.SN_code = device_serial_number;
            vpsDeviceInfoC.VIO_type = m_deviceType.ToString();
            vpsDeviceInfoC.SDK_version = m_sdk_version;
            SpatialComputing.ExternApi.scSetVPSDeviceInfoSC(t_spatialComputingHandle, vpsDeviceInfoC);
            m_spatialComputingHandle = t_spatialComputingHandle;
            // printf VPSDeviceInfoC
            Debug.Log("device_brand: " + vpsDeviceInfoC.device_brand + "\n" +
                        "product_name: " + vpsDeviceInfoC.product_name + "\n" +
                        "SN_code: " + vpsDeviceInfoC.SN_code + "\n" +
                        "VIO_type: " + vpsDeviceInfoC.VIO_type + "\n" +
                        "SDK_version: " + vpsDeviceInfoC.SDK_version + "\n");
        }

        public void updateImageFrameData(ImageFrameData frameData)
        {
            // Debug.Log(" xnh CpuImageController:OnImageFrameDataRecieved " + frameData);

            if (m_fisrtFrameArrived)
            {
                Matrix4x4 imageToHead = new Matrix4x4();
                imageToHead.SetRow(0, new Vector4(frameData.imageToHeadPoseOffset[0], frameData.imageToHeadPoseOffset[1], frameData.imageToHeadPoseOffset[2], frameData.imageToHeadPoseOffset[3]));
                imageToHead.SetRow(1, new Vector4(frameData.imageToHeadPoseOffset[4], frameData.imageToHeadPoseOffset[5], frameData.imageToHeadPoseOffset[6], frameData.imageToHeadPoseOffset[7]));
                imageToHead.SetRow(2, new Vector4(frameData.imageToHeadPoseOffset[8], frameData.imageToHeadPoseOffset[9], frameData.imageToHeadPoseOffset[10], frameData.imageToHeadPoseOffset[11]));
                imageToHead.SetRow(3, new Vector4(frameData.imageToHeadPoseOffset[12], frameData.imageToHeadPoseOffset[13], frameData.imageToHeadPoseOffset[14], frameData.imageToHeadPoseOffset[15]));

                m_trackFrameToCameraOffset = imageToHead.inverse;

                //OnTrackToImagePoseOffset(headToImage);
                if (m_spatialComputingHandle != IntPtr.Zero)
                {
                    float[] trackToImagePoseOffset = SpatialComputing.Helper.GetTrackToImagePoseOffset(m_trackFrameToCameraOffset);
                    SendVPSTrackToImagePoseOffset(trackToImagePoseOffset);
                }
                m_fisrtFrameArrived = false;
            }

            // image camera pose 
            // but what we need is head pose,  added by @xuninghao.
            // Matrix4x4 imageCamTransform;
            // {
            Matrix4x4 headTransform = Matrix4x4.TRS(
                new Vector3(frameData.headPoseImgTs.position.x, frameData.headPoseImgTs.position.y, frameData.headPoseImgTs.position.z),
                new Quaternion(frameData.headPoseImgTs.rotation.x, frameData.headPoseImgTs.rotation.y, frameData.headPoseImgTs.rotation.z, frameData.headPoseImgTs.rotation.w),
                new Vector3(1, 1, 1));
            //     imageCamTransform = headTransform * headToImage;   
            // }

            // timestamp
            double timestamp = frameData.imageTime;

            // assemble SC_InputImage
            SC_InputImage inputImage = new SC_InputImage();
            {
                inputImage.format = SC_ImageFormat.SC_Image_YUV420_NV21;
                inputImage.ptr0 = frameData.fullImage;
                inputImage.len_ptr0 = (int)(1.5 * frameData.width * frameData.height);
                inputImage.ptr1 = IntPtr.Zero;
                inputImage.len_ptr1 = 0;

                inputImage.width = (int)(frameData.width);
                inputImage.height = (int)(frameData.height);

                inputImage.fx = frameData.intrinsics[0];
                inputImage.fy = frameData.intrinsics[1];
                inputImage.cx = frameData.intrinsics[2];
                inputImage.cy = frameData.intrinsics[3];
                inputImage.timestamp_s = timestamp;

                inputImage.k1 = frameData.distortionCoefficient[0];
                inputImage.k2 = frameData.distortionCoefficient[1];
                inputImage.k3_p1 = frameData.distortionCoefficient[2];
                inputImage.k4_p2 = frameData.distortionCoefficient[3];

                inputImage.camera_type = SC_CameraType.SC_PINHOLE;
            }
            _InputImageWidth = frameData.width;
            _InputImageHeight = frameData.height;
            // trackingstate
            TrackState trackState = TrackState.tracking;

            m_timestampNow = timestamp;

            Pose3dMsgC pose3dMsgC = SpatialComputing.Helper.GetPose3DMsgC(headTransform, timestamp, ScreenOrientation.LandscapeLeft);
            //Debug.Log(SpatialComputing.Helper.Pose3dMsgCToString(pose3dMsgC));

            bool vpsRequestAuto = false;

            if (m_vpsRequestAuto == true)
            {
                SC_InputFrame sc_input_frame = SpatialComputing.Helper.GetSC_InputFrame(pose3dMsgC.pose, inputImage, timestamp, frameData.captureAngularVelocity);
                Debug.Log("SC_InputImage: " + SpatialComputing.Helper.SC_InputImageToString(sc_input_frame.input_image) + "\nSC_InputFrame: angularVelocity: " + sc_input_frame.angular_velocity);
                SendVPSRequestFrame(sc_input_frame);
                vpsRequestAuto = true;
            }

            if (m_vpsRequestManualOnce == true && vpsRequestAuto == false)
            {
                SC_InputFrame sc_input_frame = SpatialComputing.Helper.GetSC_InputFrame(pose3dMsgC.pose, inputImage, timestamp, frameData.captureAngularVelocity);
                Debug.Log("SC_InputImage: " + SpatialComputing.Helper.SC_InputImageToString(sc_input_frame.input_image) + "\nSC_InputFrame: angularVelocity: " + sc_input_frame.angular_velocity);
                SendVPSRequestFrame(sc_input_frame);
                m_vpsRequestManualOnce = false;
                Debug.Log("m_vpsRequestManualOnce == true");
            }
        }
        public void SetVPSRequestManual(bool vpsRequestManualOnce)
        {
            m_vpsRequestManualOnce = vpsRequestManualOnce;
        }
        public void SetVPSRequestAuto(bool vpsRequestAuto)
        {
            m_vpsRequestAuto = vpsRequestAuto;
        }

        public void GetTimestampAndCameraMatrix(out double timestamp, out Matrix4x4 cameraToWorldMatrix)
        {
            timestamp = m_timestampNow;
            cameraToWorldMatrix = m_cameraTransformNow;
        }

        public void TrigerApplyLocResultImmediately()
        {
             if (m_spatialComputingHandle != IntPtr.Zero)
             {
                  SpatialComputing.ExternApi.scTrigerApplyLocResultImmediately(m_spatialComputingHandle);
             }
        }

        public void SetRecordStoragePathAndJpgQualitySC(string path, int quality)
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                SpatialComputing.ExternApi.scSetRecordStoragePathAndJpgQualitySC(m_spatialComputingHandle, path, quality);
            }
        }

        public bool StartRecordSC(string current_scence_cid)
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                return SpatialComputing.ExternApi.scStartRecordSC(m_spatialComputingHandle, current_scence_cid);
            }
            return false;
        }

        public void StopRecordSC()
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                SpatialComputing.ExternApi.scStopRecordSC(m_spatialComputingHandle);
            }
        }

        public Pose getLocOffsetPose()
        {
            Pose offsetPose = Pose.identity;
            if (ARFrame.SessionStatus != EZVIOState.EZVIOCameraState_Tracking)
                return offsetPose;
            double timestamp = ARFrame.HeadPoseTimestamp;
            ScreenOrientation orientation = ScreenOrientation.LandscapeLeft;

            Matrix4x4 trackFrameTransform = Matrix4x4.TRS(
                new Vector3(ARFrame.HeadPose.position.x, ARFrame.HeadPose.position.y, ARFrame.HeadPose.position.z),
                new Quaternion(ARFrame.HeadPose.rotation.x, ARFrame.HeadPose.rotation.y, ARFrame.HeadPose.rotation.z, ARFrame.HeadPose.rotation.w),
                new Vector3(1, 1, 1)
            );

            TrackState trackState = TrackState.detecting;
            if (ARFrame.SessionStatus <= EZVIOState.EZVIOCameraState_Detecting || ARFrame.SessionStatus >= EZVIOState.EZVIOCameraState_Track_Fail)
            {
                trackState = TrackState.detecting;
            }
            else if (ARFrame.SessionStatus == EZVIOState.EZVIOCameraState_Tracking)
            {
                trackState = TrackState.tracking;
            }
            else if (ARFrame.SessionStatus == EZVIOState.EZVIOCameraState_Track_Limited)
            {
                trackState = TrackState.track_limited;
            }
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                Pose3dMsgC pose3dMsgC = SpatialComputing.Helper.GetPose3DMsgC(trackFrameTransform, timestamp, orientation);
                //Debug.Log(SpatialComputing.Helper.Pose3dMsgCToString(pose3dMsgC));

                FusePose3dMsgC fusePose3dMsgC = new FusePose3dMsgC();
                bool success = SpatialComputing.ExternApi.scInputTrackGetFuse(m_spatialComputingHandle, pose3dMsgC, trackState, out fusePose3dMsgC);
                
                if (success)
                {
                    Matrix4x4 offsetMatrix = SpatialComputing.Helper.GetOffsetMatrix(fusePose3dMsgC, orientation);
                    offsetPose.position = offsetMatrix.GetColumn(3);
                    offsetPose.rotation = offsetMatrix.rotation;
                }
            }

            m_cameraTransformNow = trackFrameTransform * m_trackFrameToCameraOffset;
            m_trackFrameTransformNow = trackFrameTransform;

            return offsetPose;
        }


        private void SendVPSTrackToImagePoseOffset(float[] trackToImagePoseOffset)
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                SpatialComputing.ExternApi.scSetExtrinsicImageToHead(m_spatialComputingHandle, trackToImagePoseOffset);
            }
        }
        //for local Image test
        private void SendVPSRequestFrame(SC_InputFrame sc_input_frame)
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                SpatialComputing.ExternApi.scInputVPSRequestFrameSC(m_spatialComputingHandle, sc_input_frame);
            }
        }


        [MonoPInvokeCallback(typeof(SC_REQUEST_CLOUDLOC_CALLBACK))]
        public static void OnCloudLocRequestCallback(SpatialComputing.SC_CloudLocRequest request_data)
        {
            Debug.Log("OnCloudLocRequestCallback " + request_data.meta.timestamp_s.ToString() + " " + request_data.byte_length.ToString() + " " + request_data.request_info_length.ToString());
            EzxrCloudLocRequestImpl request = new EzxrCloudLocRequestImpl(request_data, _InputImageWidth, _InputImageHeight);
            Debug.Log("OnCloudLocRequestCallback QueueOnMainThread");
            Loom.QueueOnMainThread(o =>
            {
                Debug.Log("OnCloudLocRequestCallback RequestCloudLocDirectly");
                if(_CurrentInstance != null)
                    _CurrentInstance.RequestCloudLocDirectly(request);
            }, null);
            Debug.Log("OnCloudLocRequestCallback end");
        }

        private void RequestCloudLocDirectly(SpatialComputing.EzxrCloudLocRequestImpl requestData)
        {
            DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long time = (long)(DateTime.UtcNow - epochStart).TotalMilliseconds;
            long requestT = time;
            string nonce = Guid.NewGuid().ToString().Substring(0, 8);

            var tk = "";

            string signature = "cloud.reloc|" + nonce + "|" + requestT + "|" + tk;
            string hashSha256 = Runtime.EncodeUtility.Sha256(signature).ToLower();

            RequestData data = new RequestData();
            data.alg.imageEncodingData = requestData.jpgStr;
            data.alg.protobufEncodingData = requestData.requestInfoStr;
            data.timestamp = requestT;
            data.sign = hashSha256;

            string data4Send = JsonUtility.ToJson(data);
            //Debug.Log("RequestCloudLocDirectly - imageEncodingData.Length: " + data.alg.imageEncodingData.Length + ", protobufEncodingData: " + data.alg.protobufEncodingData);
            UnityWebRequest.Instance.SetRequestHeader("Content-Type", "application/json");
            UnityWebRequest.Instance.Create(LocURL, data4Send, RequestMode.POST, OnCloudLocCompleted, "RequestCloudLocDirectly");
        }

        private void OnCloudLocCompleted(string data, string identifier, long statusCode = 200)
        {
            Debug.Log("RequestCloudLocDirectly - OnCloudLocCompleted: " + data);
            if (data == null) {
                Debug.LogError("OnCloudLocCompleted - data is null ");
                return;
            }              
            ResponeData responeResult = JsonUtility.FromJson<ResponeData>(data);
            //HandleSingleCloudLocResult(responeResult.result.protobufEncodingData, responeResult.result.algCode.ToString());
            if (responeResult.result == null)
            {
                Debug.LogError("OnCloudLocCompleted - result data is null ");
                return;
            }
            string protoData = responeResult.result.protobufEncodingData;
            string algCode = responeResult.result.algCode.ToString();
            if (protoData == null)
            {
                Debug.LogError("OnCloudLocCompleted - protoData is null ");
                return;
            }
            int algCodeNum = int.Parse(algCode);
            if (algCodeNum >= 9000)
            {
                Debug.LogError("algCode >= 9000, error code: " + algCode);
                return;
            }

            SC_CloudLocResult ezxrCloudLocResult = new SC_CloudLocResult();
            SC_CloudLocResultMeta ezxrCloudLocResultMeta = new SC_CloudLocResultMeta();
            ezxrCloudLocResult.meta = ezxrCloudLocResultMeta;
            try
            {
                //base64 解码
                byte[] buffer = Convert.FromBase64String(protoData);
                int length = buffer.Length;
                IntPtr resultPtr = Marshal.AllocHGlobal(length);
                Marshal.Copy(buffer, 0, resultPtr, length);
                ezxrCloudLocResult.result_info_ptr = resultPtr;
                ezxrCloudLocResult.result_length = length;

                Debug.Log("OnCloudLocCompleted " + ezxrCloudLocResult.result_length.ToString());
                SC_VPSResultState vpsResultState = SpatialComputing.ExternApi.scSetVPSResultCalledSC(m_spatialComputingHandle, ezxrCloudLocResult);
                currentVpsResultState = vpsResultState;
                printfVPSResultState(vpsResultState);

                //供底层算法调用之后，释放指针
                Marshal.FreeHGlobal(resultPtr);
            }
            catch (FormatException exp)
            {
                Debug.LogError("OnCloudLocCompleted - FormatException: " + exp.Message);
            }
        }


        void printfVPSResultState(SC_VPSResultState vpsResultState)
        {
            // EzxrCore.Log.ECLog.AddDebugLog($"ts: {vpsResultState.t_s}", 1);
            string status = "";
            switch (vpsResultState.vps_result_status)
            {
                case LOCSTATUS.SUCCESS:
                    status = "定位成功，返回pose等信息";
                    break;
                case LOCSTATUS.FAIL_UNKNOWN:
                    status = "定位失败，走完了定位算法流程，但是图像无法定位到给定地图中";
                    break;
                case LOCSTATUS.FAIL_MATCH:
                    status = "定位失败，具体原因1 hy: not used";
                    break;
                case LOCSTATUS.FAIL_INLIER:
                    status = "定位失败，具体原因2 hy: not used";
                    break;
                case LOCSTATUS.INVALID_DEVICEINFO:
                    status = "数据不合法，传入的protobuf.deviceInfo不符合规范";
                    break;
                case LOCSTATUS.INVALID_LOCALIZER:
                    status = "数据不合法，部署阶段的localizer未成功初始化";
                    break;
                case LOCSTATUS.INVALID_IMAGE:
                    status = "数据不合法，传入的图像或protobuf.deviceInfo中出现不被接受的图像格式（仅接收通道数为1或3,且类型为CV_8U或CV_8UC3的图像）";
                    break;
                case LOCSTATUS.INVALID_IMAGE_PROTO_MATCH:
                    status = "数据不合法，传入的图像文件长度，与protobuf.deviceInfo中记录的图像字节数不匹配";
                    break;
                case LOCSTATUS.INVALID_MESSAGE:
                    status = "传入的message不合法";
                    break;
                case LOCSTATUS.FAIL_SUMMARY_UNKNOWN:
                    status = "hy: not used";
                    break;
                case LOCSTATUS.FAIL_SUMMARY_NOMAP:
                    status = "未加载完成可用的summary map hy: not used";
                    break;
            }
            // EzxrCore.Log.ECLog.AddDebugLog($"reason: {status}", 2);

        }
    }
}