using System;
using System.IO;
using System.IO.Compression;
using UnityEngine;
using UnityEngine.UI;
namespace EZXR.Glass.SpatialComputing
{
    public class EZXRSpatialComputingRecordDebugData : MonoBehaviour
    {

        public Text recordStatus;

        public EZXRSpatialComputingManager locmanager;
        private SpatialComputingController locController;

        // Start is called before the first frame update
        void Start()
        {
            if (recordStatus) recordStatus.text = isRecording ? "Recording Debug Data" : "Not Recording Debug Data";
            locController = locmanager.CurrentSCController;
        }

        // Update is called once per frame
        void Update()
        {

        }

        private string lastSaveRecordPath = "";
        private bool optLocking = false;
        private bool isRecording = false;
        public void OnSCRecordBtnClick()
        {
            if (optLocking) return;
            optLocking = true;

            if (!isRecording)
            {
                if (locController != null)
                {
                    //获取当前时间，转成string
                    string time = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
                    string path = Application.persistentDataPath + "/EZXR_SC_" + time;
                    //判断文件夹是否存在，不存在则创建
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    locController.SetRecordStoragePathAndJpgQualitySC(path, 75);
                    //获取app名称和当前scene名称
                    string appname = Application.productName;
                    string scenename = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
                    string current_scence_cid = appname + "_" + scenename;
                    locController.StartRecordSC(current_scence_cid);

                    lastSaveRecordPath = path;

                    isRecording = true;
                    if (recordStatus) recordStatus.text = "Recording Debug Data";
                }
            }
            else
            {
                if (locController != null)
                {
                    // Stop Record
                    locController.StopRecordSC();

                    // Compress
                    bool isCompSucc = false;
                    string sourceDirectory;
                    string zipFilePath;
                    if (recordStatus) recordStatus.text = "Record Debug Data Compressing ";

                    if (Directory.Exists(lastSaveRecordPath + "/"))
                    {
                        DirectoryInfo direction = new DirectoryInfo(lastSaveRecordPath + "/");
                        DirectoryInfo[] infos = direction.GetDirectories("*", SearchOption.TopDirectoryOnly);

                        if (infos.Length > 1 || infos.Length == 0)
                        {
                            isCompSucc = false;
                        }
                        else
                        {
                            sourceDirectory = lastSaveRecordPath + "/" + infos[0].Name;

                            zipFilePath = lastSaveRecordPath + "/" + infos[0].Name + ".zip";

                            ZipFile.CreateFromDirectory(sourceDirectory, zipFilePath, System.IO.Compression.CompressionLevel.Optimal, false);

                            isCompSucc = true;
                        }
                    }
                    else
                    {
                        isCompSucc = false;
                    }

                    // Compress ok or not
                    if (!isCompSucc)
                    {
                        if (recordStatus) recordStatus.text = "Data Compressed Failed, try again";
                        optLocking = false;
                        return;
                    }
                    else
                    {
                        if (recordStatus) recordStatus.text = "Data Compressed OK.";
                    }

                    // Upload


                    isRecording = false;
                }
            }

            optLocking = false;
        }

    }
}