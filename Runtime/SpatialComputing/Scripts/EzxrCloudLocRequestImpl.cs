using EZXR.Glass.Runtime;
using System;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace EZXR.Glass.SpatialComputing
{
    public struct EzxrCloudLocRequestImpl
    {
        public SC_CloudLocRequestMeta meta;  // query struct
        public string jpgStr;         // jpg string
        public string requestInfoStr;

        public EzxrCloudLocRequestImpl(SC_CloudLocRequest requestData, int width, int height)
        {
            if (requestData.byte_length == 0 || requestData.jpg_ptr == IntPtr.Zero)
            {
                this.jpgStr = "";
            }
            else
            {
                // byte[] imageBytes = new byte[requestData.byte_length];
                // Marshal.Copy(requestData.jpg_ptr, imageBytes, 0, requestData.byte_length);
                // byte[] jpgBytes = ImageConversion.EncodeArrayToJPG(imageBytes, GraphicsFormat.B8G8R8_UNorm, width, height);
                byte[] jpgBytes = GetJPEGArray(requestData, width, height);
                this.jpgStr = Convert.ToBase64String(jpgBytes);
                //  Marshal.FreeHGlobal(requestData.jpgPtr); native自己会释放
            }

            if (requestData.request_info_length == 0 || requestData.request_info_ptr == IntPtr.Zero)
            {
                this.requestInfoStr = "";
            }
            else
            {
                byte[] protoBytes = new byte[requestData.request_info_length];
                Marshal.Copy(requestData.request_info_ptr, protoBytes, 0, requestData.request_info_length);
                this.requestInfoStr = Convert.ToBase64String(protoBytes);
                //   Marshal.FreeHGlobal(requestData.reqestInfoPtr);// native自己会释放
            }
            this.meta = new SC_CloudLocRequestMeta();
            this.meta.timestamp_s = requestData.meta.timestamp_s;
        }

        private static byte[] GetJPEGArray(SC_CloudLocRequest requestData, int width, int height)
        {
            //System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            //stopwatch.Start();
            byte[] byteArray = new byte[requestData.byte_length];
            Marshal.Copy(requestData.jpg_ptr, byteArray, 0, requestData.byte_length);
            byte[] nativeByteMirrorYArray = Texture2DHelper.FilpY(byteArray, width, height, 3);
            //Debug.Log("requestData.byte_length: " + requestData.byte_length + " width " + m_width + " height " + m_height);
            byte[] jpgBytes = ImageConversion.EncodeArrayToJPG(nativeByteMirrorYArray, GraphicsFormat.B8G8R8_UNorm, (uint)width, (uint)height, (uint)width * 3);
            //stopwatch.Stop();
            //TimeSpan elapsedTime = stopwatch.Elapsed;
            //Debug.Log("GetJPEGArray - elapsedTime: " + elapsedTime.TotalMilliseconds);
            // EzxrCore.Log.ECLog.SaveFile(byteArray, "test.bin");
            // EzxrCore.Log.ECLog.SaveFile(jpgBytes, "test.jpg");
            return jpgBytes;
        }
    }
}
