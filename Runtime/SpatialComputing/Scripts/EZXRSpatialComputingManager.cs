using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.SceneManagement;
using EZXR.Glass;
using EZXR.Glass.Runtime;
using EZXR.Glass.SixDof;
using EZXR.Glass.Device;

namespace EZXR.Glass.SpatialComputing
{
    public enum LocCam
    {
            LocCam_RGB,
            // LocCam_FisheyeGray
    }
    public enum LocRequestMode
    {
        LocRequestAuto,
        LocRequestManual
    }

    public class EZXRSpatialComputingManager : MonoBehaviour
    {
        #region singleton
        private static EZXRSpatialComputingManager _instance;
        public static EZXRSpatialComputingManager Instance
        {
            get
            {
                return _instance;
            }
        }
        #endregion

        //public delegate void ImageFrameDataDelegate(ImageFrameData imageFrameData);

        //public event ImageFrameDataDelegate OnImageFrameData;
        public SC_VPSResultState currentVpsResultState {
            get {
                if (scController == null)
                    return new SC_VPSResultState();
                else
                    return scController.currentVpsResultState;
                  
            }
        }

        public SpatialComputingController CurrentSCController {
            get {
                return scController;
            }
        }
        [SerializeField]
        LocCam locCamType = LocCam.LocCam_RGB;
        [SerializeField]
        bool useCameraAE_forMovingMode = false;
        //如果使用XRMan，不需要再指定CameraOffsetTransform
        [SerializeField]
        Transform m_CameraOffsetTransform;
        [SerializeField]
        string locRequestURL;
        [SerializeField]
        LocRequestMode requestLocMode;

        [SerializeField]
        private bool _UseImgUndistortion;

        public bool UseImgUndistortion {
            set {
                if (scController != null) {
                    scController.useImageUndistortion(value);
                    _UseImgUndistortion = value;
                }
            }
            get {
                return _UseImgUndistortion;
            }
        }

        private NormalRGBCameraDevice rgbCameraDevice;
        private EZVIOInputImage locCamImageBuffer;
        private SpatialComputingController scController = null;

        private bool hasExtractOST = false;
        private OSTMatrices ostMatrices = new OSTMatrices();
        private float[] imageIntrinsics = new float[8];
        private float currentAngularVelocity = -1.0f;

        private Pose imageTsHeadPose;

        private ImageFrameData imageFrameData = ImageFrameData.Create();
        

        private void Awake()
        {
            _instance = this;
            scController = this.gameObject.AddComponent<SpatialComputingController>();

        }

        private void OnEnable()
        {
            if (locCamType == LocCam.LocCam_RGB)
            {
                StartCoroutine(startRGBCamera());
                StartCoroutine(refreshCameraAEMode());
            }
        }

        private void OnDisable()
        {
            if (locCamType == LocCam.LocCam_RGB)
            {
                stopRGBCamera();
            }
        }
        private void OnDestroy()
        {
            stopSCLocSession();
        }

        private void LateUpdate()
        {
            if (scController != null)
            {
                Pose poseOffset = scController.getLocOffsetPose();
                if (poseOffset != Pose.identity)
                {
                    if (!XRMan.Exist)
                    {
                        if (m_CameraOffsetTransform != null)
                        {
                            m_CameraOffsetTransform.position = poseOffset.position;
                            m_CameraOffsetTransform.rotation = poseOffset.rotation;
                        }
                    }
                    else {
                        XRMan.offset_SpatialComputing.SetTRS(poseOffset.position, poseOffset.rotation, Vector3.one);
                    }
                    
                }
            }
        }

        public void startSCLocSession(string assetPath)
        {
            if (assetPath == null)
            {
                Debug.LogError("configuration resources assetPath is error");
            }
            Debug.Log("OnCopyCompleted : " + assetPath);

            SpatialComputingController.LocURL = locRequestURL;

            if (scController == null) {
                scController = this.gameObject.AddComponent<SpatialComputingController>();
            }
            scController.useImageUndistortion(UseImgUndistortion);
            scController.createSpatialComputingVPS(assetPath);
            if (requestLocMode == LocRequestMode.LocRequestAuto)
            {
                scController.SetVPSRequestAuto(true);
            }
            else
            { //LocRequestMode.LocRequestManual
                scController.SetVPSRequestAuto(false);
                scController.SetVPSRequestManual(true);
            }
            

            InvokeRepeating("UpdateCameraImage", 0.5f, 1.0f);
        }

        public void stopSCLocSession()
        {
            if (scController == null)
            {
                return;
            }
            scController.destroySpatialComputingVPS();
        }

        public void TrigerApplyLocResultImmediately() {
            if (scController == null)
            {
                return;
            }
            scController.TrigerApplyLocResultImmediately();
        }

        public void EnableCameraAE_ForMovingMode(bool _enable) {
            useCameraAE_forMovingMode = _enable;
            StartCoroutine(refreshCameraAEMode());
        }
        private IEnumerator startRGBCamera()
        {
            yield return new WaitUntil(() => SessionManager.Instance != null && SessionManager.Instance.IsInited);

            rgbCameraDevice = new NormalRGBCameraDevice();
            rgbCameraDevice.Open();
        }

        private void stopRGBCamera()
        {
            if (rgbCameraDevice != null)
            {
                if (useCameraAE_forMovingMode)
                {
                    rgbCameraDevice.setCameraAEMode(CameraAE_Mode.AE_Auto);
                }
                rgbCameraDevice.Close();
            }
            rgbCameraDevice = null;
            if (locCamImageBuffer.fullImg != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(locCamImageBuffer.fullImg);
                locCamImageBuffer.fullImg = IntPtr.Zero;
            }
        }

        private IEnumerator refreshCameraAEMode() {
            yield return new WaitUntil(() => rgbCameraDevice!=null && rgbCameraDevice.IsStarted());
            if (useCameraAE_forMovingMode)
            {
                rgbCameraDevice.setCameraAEMode(CameraAE_Mode.AE_ForMovingMode);
            }
        }
        // test save images
        /*
        Texture2D texture0 = null;
        Texture2D texture1 = null;
        Texture2D texture2 = null;
        byte[] dataU;
        byte[] dataV;
        int count = 0;
        byte[] byteArray = null;
        */

        private void UpdateCameraImage()
        {
            if (ARFrame.SessionStatus != EZVIOState.EZVIOCameraState_Tracking)
                return;

            if (!hasExtractOST)
            {
                NativeTracking.GetOSTParams(ref ostMatrices);
                hasExtractOST = true;
            }
            if (locCamType == LocCam.LocCam_RGB)
            {
                if (rgbCameraDevice == null)
                {
                    return;
                }
                currentAngularVelocity = rgbCameraDevice.getCameraMotionAngle();

                bool res = rgbCameraDevice.getCurrentImage(ref locCamImageBuffer, imageIntrinsics);
                if (res)
                {
                    double timestamp_sec = locCamImageBuffer.timestamp;
                    imageTsHeadPose = ARFrame.GetHistoricalHeadPose(timestamp_sec);

                    // assemble image frame data
                    AssembleImageFrameData();

                    // distribute image frame data
                    if (scController != null)
                    {
                        scController.updateImageFrameData(imageFrameData);
                    }
                }
            }
        }

        private void AssembleImageFrameData()
        {
            imageFrameData.fullImage = locCamImageBuffer.fullImg;
            imageFrameData.width = (int)locCamImageBuffer.imgRes.width;
            imageFrameData.height = (int)locCamImageBuffer.imgRes.height;
            imageFrameData.distortionCoefficient[0] = imageIntrinsics[4];
            imageFrameData.distortionCoefficient[1] = imageIntrinsics[5];
            imageFrameData.distortionCoefficient[2] = imageIntrinsics[6];
            imageFrameData.distortionCoefficient[3] = imageIntrinsics[7];
            imageFrameData.distortionType = 0; // pinhole;
            imageFrameData.intrinsics[0] = imageIntrinsics[0];
            imageFrameData.intrinsics[1] = imageIntrinsics[1];
            imageFrameData.intrinsics[2] = imageIntrinsics[2];
            imageFrameData.intrinsics[3] = imageIntrinsics[3];
            imageFrameData.imageTime = locCamImageBuffer.timestamp;
            imageFrameData.headPoseImgTs.position = imageTsHeadPose.position;
            imageFrameData.headPoseImgTs.rotation = imageTsHeadPose.rotation;
            Array.Copy(ostMatrices.T_RGB_Head, imageFrameData.imageToHeadPoseOffset, ostMatrices.T_RGB_Head.Length);
            imageFrameData.captureAngularVelocity = currentAngularVelocity;
        }



        /*
        private void TestSave() {

            // test images
            {
                //写入文件测试，写入10张
                if (count < 10)
                {
                    if(byteArray == null) {
                        byteArray = new byte[(int)(1.5 * imageFrameData.width * imageFrameData.height)];
                    }
                    Marshal.Copy(imageFrameData.fullImage, byteArray, 0, (int)(1.5 * imageFrameData.width * imageFrameData.height));

                    if (texture0 == null || texture0.width != (int)(imageFrameData.width) || texture0.height != (int)(imageFrameData.height))
                    {
                        texture0 = new Texture2D((int)imageFrameData.width, (int)imageFrameData.height, TextureFormat.R8, false, false);
                    }
                    texture0.LoadRawTextureData(byteArray);
                    var encodedJpg = texture0.EncodeToJPG();
                    string filePath = System.IO.Path.Combine($"/storage/emulated/0/Pictures", $"frame0713_{count}_0.jpg");
                    Debug.Log("save jpg to " + filePath);
                    System.IO.File.WriteAllBytes(filePath, encodedJpg);

                    if (texture1 == null || texture1.width != (int)(imageFrameData.width)/2 || texture1.height != (int)(imageFrameData.height)/2)
                    {
                        texture1 = new Texture2D((int)imageFrameData.width / 2, (int)imageFrameData.height / 2, TextureFormat.R8, false, false);
                    }
                    if(dataU == null)
                    {
                        dataU = new byte[(int)imageFrameData.width / 2 * (int)imageFrameData.height / 2];
                    }

                    for(int i=0;i< (int)imageFrameData.width / 2; i++)
                    {
                        for(int j=0;j< (int)imageFrameData.height / 2; j++)
                        {
                            dataU[j * (int)imageFrameData.width / 2 + i] = byteArray[(int)imageFrameData.width * (int)imageFrameData.height + 2 * (j * (int)imageFrameData.width / 2 + i)];
                        }
                    }
                    texture1.LoadRawTextureData(dataU);
                    encodedJpg = texture1.EncodeToJPG();
                    filePath = System.IO.Path.Combine($"/storage/emulated/0/Pictures", $"frame0713_{count}_1.jpg");
                    Debug.Log("save jpg to " + filePath);
                    System.IO.File.WriteAllBytes(filePath, encodedJpg);

                    if (texture2 == null || texture2.width != (int)(imageFrameData.width)/2 || texture2.height != (int)(imageFrameData.height)/2)
                    {
                        texture2 = new Texture2D((int)imageFrameData.width/2, (int)imageFrameData.height/2, TextureFormat.R8, false, false);
                    }
                    if (dataV == null)
                    {
                        dataV = new byte[(int)imageFrameData.width / 2 * (int)imageFrameData.height / 2];
                    }
                        for (int i = 0; i < (int)imageFrameData.width / 2; i++)
                    {
                        for (int j = 0; j < (int)imageFrameData.height / 2; j++)
                        {
                            dataV[j * (int)imageFrameData.width / 2 + i] = byteArray[(int)imageFrameData.width * (int)imageFrameData.height + 2 * (j * (int)imageFrameData.width / 2 + i)+1];
                        }
                    }
                    texture2.LoadRawTextureData(dataV);
                    encodedJpg = texture2.EncodeToJPG();
                    filePath = System.IO.Path.Combine($"/storage/emulated/0/Pictures", $"frame0713_{count}_2.jpg");
                    Debug.Log("save jpg to " + filePath);
                    System.IO.File.WriteAllBytes(filePath, encodedJpg);

                    count++;
                }
            }
        }
        */
    }
}
