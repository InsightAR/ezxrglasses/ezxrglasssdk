using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections.LowLevel.Unsafe;
using EZXR.Glass.Runtime;

namespace EZXR.Glass.SpatialComputing
{
    public static partial class Helper
    {
        public static Pose3dMsgC GetPose3DMsgC(Matrix4x4 pose, double timestamp, ScreenOrientation orientation)
        {
            Pose3dMsgC pose3dMsgC = new Pose3dMsgC();
            pose3dMsgC.pose_type = PoseType.track;
            pose3dMsgC.t_s = timestamp;
            pose3dMsgC.map_id = 0;
            pose3dMsgC.state_t_s = -1.0;
            Matrix4x4 pose_left = CoordinateTransformation.GetMatrixToLandscapeLeft(orientation, pose);
            //Debug.Log("GetPose3DMsgC pose: " + pose + " pose_left: " + pose_left);
            CoordinateTransformation.ConvertUnityPoseToCvPose_c2w(pose_left, out pose3dMsgC.pose);
            pose3dMsgC.xyz_uncertainty = new float[3] { 0, 0, 0 };
            pose3dMsgC.traj_length = 0.0f;
            return pose3dMsgC;
        }
        
        public static string Pose3dMsgCToString(Pose3dMsgC pose3dMsgC)
        {
            string str = "";
            str += "pose_type: " + pose3dMsgC.pose_type + "\n";
            str += "t_s: " + pose3dMsgC.t_s + "\n";
            str += "map_id: " + pose3dMsgC.map_id + "\n";
            str += "state_t_s: " + pose3dMsgC.state_t_s + "\n";
            str += "pose: " + "\n";
            str += "    : " + pose3dMsgC.pose[0] + ", " + pose3dMsgC.pose[1] + ", " + pose3dMsgC.pose[2] + ", " + pose3dMsgC.pose[3] + "\n";
            str += "    : " + pose3dMsgC.pose[4] + ", " + pose3dMsgC.pose[5] + ", " + pose3dMsgC.pose[6] + ", " + pose3dMsgC.pose[7] + "\n";
            str += "    : " + pose3dMsgC.pose[8] + ", " + pose3dMsgC.pose[9] + ", " + pose3dMsgC.pose[10] + ", " + pose3dMsgC.pose[11] + "\n";
            str += "    : " + pose3dMsgC.pose[12] + ", " + pose3dMsgC.pose[13] + ", " + pose3dMsgC.pose[14] + ", " + pose3dMsgC.pose[15] + "\n";
            str += "xyz_uncertainty: " + "\n";
            str += "    x: " + pose3dMsgC.xyz_uncertainty[0] + "\n";
            str += "    y: " + pose3dMsgC.xyz_uncertainty[1] + "\n";
            str += "    z: " + pose3dMsgC.xyz_uncertainty[2] + "\n";
            str += "traj_length: " + pose3dMsgC.traj_length + "\n";
            return str;
        }

        public static string FusePose3dMsgCToString(FusePose3dMsgC fusePose3dMsgC)
        {
            string str = "";
            str += "track_pose_msg: " + "\n";
            str += Pose3dMsgCToString(fusePose3dMsgC.track_pose_msg);
            str += "loc_pose_msg: " + "\n";
            str += Pose3dMsgCToString(fusePose3dMsgC.loc_pose_msg);
            str += "fuse_pose_msg: " + "\n";
            str += Pose3dMsgCToString(fusePose3dMsgC.fuse_pose_msg);
            str += "fuse_t_s: " +  fusePose3dMsgC.fuse_t_s + "\n";
            return str;
        }

        public static string SC_InputImageToString(SC_InputImage inputImage)
        {
            string str = "";
            str += "width: " + inputImage.width + "\n";
            str += "height: " + inputImage.height + "\n";
            str += "format: " + inputImage.format + "\n";
            str += "timestamp: " + inputImage.timestamp_s + "\n";
            str += "image len_ptr0: " + inputImage.len_ptr0 + " len_ptr1: " + inputImage.len_ptr1 +  "\n";
            str += "fx: " + inputImage.fx + "\n";
            str += "fy: " + inputImage.fy + "\n";
            str += "cx: " + inputImage.cx + "\n";
            str += "cy: " + inputImage.cy + "\n";
            str += "k1: " + inputImage.k1 + "\n";
            str += "k2: " + inputImage.k2 + "\n";
            str += "k3_p1: " + inputImage.k3_p1 + "\n";
            str += "k4_p2: " + inputImage.k4_p2 + "\n";
            str += "camera_type: " + inputImage.camera_type + "\n";
            return str;
        }

        public static Matrix4x4 GetUnityPose(FusePose3dMsgC fusePose3dMsgC)
        {

            Pose3dMsgC pose3dMsgC = fusePose3dMsgC.fuse_pose_msg;
            Matrix4x4 pose = CoordinateTransformation.ConvertCVPoseToUnityPose_c2w(pose3dMsgC.pose);
            return pose;
        }

        public static Matrix4x4 GetOffsetMatrix(FusePose3dMsgC fusePose3dMsgC, ScreenOrientation orientation)
        {
            Pose3dMsgC fuse_pose_msg = fusePose3dMsgC.fuse_pose_msg;
            Matrix4x4 fuse_pose = CoordinateTransformation.ConvertCVPoseToUnityPose_c2w(fuse_pose_msg.pose); 
            //Matrix4x4 fuse_pose_orientation = CoordinateTransformation.GetMatrixFromLandscapeLeft(orientation, fuse_pose);
            //Debug.Log("fuse_pose: " + fuse_pose.ToString());
            Pose3dMsgC track_pose_msg = fusePose3dMsgC.track_pose_msg;
            Matrix4x4 track_pose = CoordinateTransformation.ConvertCVPoseToUnityPose_c2w(track_pose_msg.pose);
            //Matrix4x4 track_pose_orientation = CoordinateTransformation.GetMatrixFromLandscapeLeft(orientation, track_pose);
            //Debug.Log("track_pose: " + track_pose.ToString());
            //Matrix4x4 offestMatrix = fuse_pose_orientation * track_pose_orientation.inverse;
            Matrix4x4 offestMatrix = fuse_pose * track_pose.inverse;
            //Debug.Log(" offestMatrix orientation : " + offestMatrix + " offestMatrix : " + fuse_pose * track_pose.inverse);
            return offestMatrix;
        }


        public static SC_InputFrame GetSC_InputFrame(float[] cameraToWorldMatrix, SC_InputImage input_image, double timestamp, float angularVelocity)
        {
            SC_InputFrame sc_input_frame = new SC_InputFrame();
            sc_input_frame.input_image = input_image;
            sc_input_frame.valid_T_cv_head_to_world = true; // 必须设置成true，不要走融合定位的查询逻辑
            sc_input_frame.T_cv_head_to_world = cameraToWorldMatrix;
            sc_input_frame.angular_velocity = angularVelocity;
            return sc_input_frame;
        }

        public static float[] GetTrackToImagePoseOffset(Matrix4x4 trackToImagePose)
        {
            Matrix4x4 trackToImagePose_cv = CoordinateTransformation.ConvertUnityPoseToCVPose_c2c(trackToImagePose);
            float[] ImagePoseOffset = new float[16] {
                trackToImagePose_cv.m00, trackToImagePose_cv.m01, trackToImagePose_cv.m02, trackToImagePose_cv.m03,
                trackToImagePose_cv.m10, trackToImagePose_cv.m11, trackToImagePose_cv.m12, trackToImagePose_cv.m13,
                trackToImagePose_cv.m20, trackToImagePose_cv.m21, trackToImagePose_cv.m22, trackToImagePose_cv.m23,
                trackToImagePose_cv.m30, trackToImagePose_cv.m31, trackToImagePose_cv.m32, trackToImagePose_cv.m33
            };

            return ImagePoseOffset;
        }
    }
}