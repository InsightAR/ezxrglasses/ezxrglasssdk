﻿using EZXR.Glass.SpatialComputing;
using UnityEditor;
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    public partial class ARAbilities : MonoBehaviour
    {
        #region SpatialComputing
        [MenuItem("GameObject/XR Abilities/SpatialComputing", false, 20)]
        public static void EnableSpatialComputing()
        {
            if (FindObjectOfType<EZXRSpatialComputingManager>() == null)
            {
                string filePath = "Assets/EZXRGlassSDK/Runtime/SpatialComputing/Prefabs/SpatialComputingManager.prefab";
                if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                {
                    filePath = AssetDatabase.GUIDToAssetPath("2ff96ba100da74917a4b639f15c46cae");
                }
                Runtime.PrefabUtility.InstantiatePrefabWithUndoAndSelection(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
            }
        }
        #endregion
    }
}