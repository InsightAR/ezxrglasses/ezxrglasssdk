using EZXR.Glass.Runtime;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
namespace EZXR.Glass.SpatialMesh
{
    public class SpatialMeshDetectorWrapper
    {

        /// <summary>
        /// 保存以及获得的Smoothed Meshes
        /// 需要运行在高于AROS 0.6.0版本上
        /// </summary>
        /// <returns>运行在低于AROS 0.6.0版本上,永远返回false，获取成功返回true</returns>
        public static bool getBackendSmoothedMeshData(ref EZVIOBackendMesh meshData) {
            return NativeAPI.getBackendSmoothedMeshData(ref meshData);
        }
        public static bool getBackendIncrementalMeshData(ref EZVIOBackendIncrementalMesh meshData) { 
            return NativeAPI.getBackendIncrementalMeshData(ref meshData); 
        }

        /// <summary>
        /// 保存以及获得的Smoothed Meshes
        /// 需要运行在高于AROS 0.6.0版本上
        /// </summary>
        /// <returns>运行在低于AROS 0.6.0版本上,永远返回false，保存成功返回true</returns>
        public static bool SaveBackendSmoothedMeshData() {
            return NativeAPI.SaveBackendSmoothedMeshData();
        }

        private partial struct NativeAPI
        {
#if !UNITY_EDITOR
        [DllImport(NativeConsts.NativeLibrary)]
        public static extern bool getBackendSmoothedMeshData(ref EZVIOBackendMesh meshData);
            
        [DllImport(NativeConsts.NativeLibrary)]
        public static extern bool getBackendIncrementalMeshData(ref  EZVIOBackendIncrementalMesh meshData);
        
        //[DllImport(NativeConsts.NativeLibrary)]
        //public static extern bool getBackendMeshData(ref EZVIOBackendMesh meshData);

        //[DllImport(NativeConsts.NativeLibrary)]
        //public static extern bool runPauseMeshAndDFS();

        //[DllImport(NativeConsts.NativeLibrary)]
        //public static extern bool runResumeMeshAndDFS();

        //[DllImport(NativeConsts.NativeLibrary)]
        //public static extern bool runSaveMesh();

        [DllImport(NativeConsts.NativeLibrary)]
        public static extern bool SaveBackendSmoothedMeshData();
#else
            public static bool getBackendSmoothedMeshData(ref EZVIOBackendMesh meshData) { return false; }

            public static bool getBackendIncrementalMeshData(ref EZVIOBackendIncrementalMesh meshData) { return false; }

            public static bool SaveBackendSmoothedMeshData() { return false; }

            //public static bool getBackendMeshData(ref EZVIOBackendMesh meshData) { return false; }

            //public static bool runPauseMeshAndDFS() { return true; }

            //public static bool runResumeMeshAndDFS() { return true; }

            //public static bool runSaveMesh() { return true; }
#endif
        }
    }
}