﻿using EZXR.Glass.SpatialMesh;
using UnityEditor;
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    public partial class ARAbilities : MonoBehaviour
    {
        #region SpatialMesh
        [MenuItem("GameObject/XR Abilities/Additional.../SpatialMesh", false, 100)]
        public static void EnableSpatialMesh()
        {
            if (FindObjectOfType<SpatialMeshManager>() == null)
            {
                string filePath = "Assets/EZXRGlassSDK/Core/SpatialMesh/Prefabs/SpatialMeshManager.prefab";
                if (AssetDatabase.LoadAssetAtPath<GameObject>(filePath) == null)
                {
                    filePath = AssetDatabase.GUIDToAssetPath("9cb4ff0104470844eafdb0f39509e0b2");
                }
                Runtime.PrefabUtility.InstantiatePrefabWithUndoAndSelection(AssetDatabase.LoadAssetAtPath<GameObject>(filePath));
            }
        }
        #endregion
    }
}