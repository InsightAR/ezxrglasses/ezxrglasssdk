using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.Inputs
{
    public class HandleControllerEffect : MonoBehaviour
    {
        public HandType handType;
        public Texture defaultTexture;
        public Texture[] activeTextures;
        public UnityEngine.Renderer[] activeRenderers;

        private HandleKeyCode[] allKeyCode = new HandleKeyCode[]
        {
            HandleKeyCode.Primary,
            HandleKeyCode.Secondary,
            HandleKeyCode.Back,
            HandleKeyCode.Grid,
            HandleKeyCode.Home,
            HandleKeyCode.Rocker,
            HandleKeyCode.Trigger
        };

        private void Update()
        {
            for (int i = 0; i < allKeyCode.Length; i++)
            {
                //按键Down/Up可能会受应用切后台影响而丢失状态监听，不能闭环
                //if (HandleControllerManager.Instance.GetButtonDown(handType, allKeyCode[i]))
                //{
                //    if (i < activeTextures.Length && i < activeRenderers.Length)
                //        activeRenderers[i].material.SetTexture("_MainTex", activeTextures[i]);
                //}
                //else if (HandleControllerManager.Instance.GetButtonUp(handType, allKeyCode[i]))
                //{
                //    if (i < activeRenderers.Length)
                //        activeRenderers[i].material.SetTexture("_MainTex", defaultTexture);
                //}

                if (HandleControllerManager.Instance.GetButton(handType, allKeyCode[i]))
                {
                    if (i < activeTextures.Length && i < activeRenderers.Length)
                        activeRenderers[i].material.SetTexture("_MainTex", activeTextures[i]);
                }
                else
                {
                    if (i < activeRenderers.Length)
                        activeRenderers[i].material.SetTexture("_MainTex", defaultTexture);
                }
            }
        }
    }
}