using System;
using System.Collections;
using System.Collections.Generic;
using EZXR.Glass.Runtime;
using EZXR.Glass.Inputs;
using EZXR.Glass.SixDof;
using UnityEngine;

namespace EZXR.Glass.Inputs
{
    [ScriptExecutionOrder(-48)]
    public class ControllerInfo : InputInfoBase
    {
        public Transform handleOrigin;
        public Transform tipPoint;

        GameObject dummy;

        /// <summary>
        /// 手柄当前是否已配对
        /// </summary>
        protected bool isBinding = false;
        public bool IsBinding
        {
            get
            {
                return isBinding;
            }
        }
        /// <summary>
        /// 手柄当前是否已连接
        /// </summary>
        protected bool isConnected = false;
        public bool IsConnected
        {
            get
            {
                return isConnected;
            }
        }
        /// <summary>
        /// 手柄当前电量
        /// </summary>
        protected float powerStats = 0.0f;
        public float PowerStats
        {
            get
            {
                return powerStats;
            }
        }

        /// <summary>
        /// 手柄当前是否被握持
        /// </summary>
        protected bool isHeld;
        public bool IsHeld
        {
            get
            {
                return isHeld;
            }
        }

        /// <summary>
        /// 手柄当前是否被静置
        /// </summary>
        protected bool isSilent;
        public bool IsSilent
        {
            get
            {
                return isSilent;
            }
        }

        protected Vector2 axis2DCoord;

        /// <summary>
        /// 监听手柄的生命状态改变(立即发生)
        /// </summary>
        private bool bindingChanged = false;
        private bool connectedChanged = false;
        private bool powerChanged = false;
        private bool holdChanged = false;
        private bool silenceChanged = false;
        /// <summary>
        /// 监听到生命状态改变的回调(在Update中发生)
        /// 可能立即发生也可能滞后一帧
        /// </summary>
        private Action<bool> bindingAction;
        private Action<bool> connectedAction;
        private Action<float> powerChangedAction;
        private Action<bool> holdChangedAction;
        private Action<bool> silenceChangedAction;
        /// 保存按键状态(在Update中发生，可能立即发生也可能滞后一帧)
        private Dictionary<HandleKeyCode, HandleKeyEvent> buttonState;
        /// 监听按键状态的改变(立即发生)
        private Dictionary<HandleKeyCode, bool> buttonStateChanged;
        //private Dictionary<HandleKeyCode, float> buttonDownTimeSpan;

        private bool isPaused = false;

        Func<InputInfoBase, bool> _EnableRaycastByExternal;
        public Func<InputInfoBase, bool> EnableRaycastByExternal
        {
            get
            {
                return _EnableRaycastByExternal;
            }
            set
            {
                _EnableRaycastByExternal = value;
                RefreshRaycastExternal();
            }
        }

        /// <summary>
        /// 手柄当前是否已连接
        /// </summary>
        public override bool Exist
        {
            get
            {
                if (Application.isEditor)
                {
                    return true;
                }
                return isConnected;
            }
        }
        /// <summary>
        /// 手掌正在朝向头部
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public override bool isPalmFacingHead(float angle = 90) { return false; }
        public override bool IsPalmFacingHead(float angle = 90)
        {
            return isPalmFacingHead(angle);
        }

        #region 交互能力开关
        /// <summary>
        /// 全局手柄开关，与Enabled互斥，由外部递交的逻辑函数控制，如果置为false的话，Visibility和RaycastInteraction将会失效，且被置为false
        /// </summary>
        public static event Func<bool> EnableByExternal;
        private bool m_Enabled = true;
        /// <summary>
        /// 全局手柄开关，与EnableByExternal互斥，由外部直接修改，如果置为false的话，Visibility和RaycastInteraction将会失效，且被置为false
        /// </summary>
        public bool Enabled
        {
            get
            {
                return m_Enabled;
            }
            set
            {
                m_Enabled = value;
                Visibility &= value;
                RaycastInteraction &= value;
                TouchInteraction &= value;
                PhysicsInteraction &= value;
            }
        }

        /// <summary>
        /// 设置或获得当前手柄的显示状态（仅仅影响显示，并不影响交互），如果Enabled为false则Visibility也将为false
        /// </summary>
        private bool visibility = true;
        public override bool Visibility
        {
            get
            {
                /*if (handType == HandType.Left) return false;
                else */
                return visibility;
            }
            set
            {
                //if (handType == HandType.Right)
                {
                    // 若手柄已断连，应过滤掉visible为true的情况
                    var visible = m_Enabled && (EnableByExternal == null ? true : EnableByExternal.Invoke()) && value && HandleControllerManager.Instance.GetConnectState(handType);
                    Debug.Log("HandleControllerInfo, Visibility setter: " + visible);

                    // 判断是否开发者自定义了手柄模型
                    var dummyType = handType == HandType.Left ? "LeftDummy" : "RightDummy";
                    // 若在handleOrigin下添加自定义的手柄模型，则隐藏默认手柄
                    var hasAlter = handleOrigin.GetComponentsInChildren<Renderer>().Length > 0;
                    if (!hasAlter)
                    {
                        dummy = transform.Find(dummyType).gameObject;
                    }
                    else
                    {
                        dummy = handleOrigin.gameObject;
                    }
                    dummy.SetActive(visible);

                    visibility = visible;
                }
            }
        }

        private bool raycastInteractionByUser = true;
        /// <summary>
        /// 设置或获得手柄射线交互状态（true：启用射线交互模块；false：禁用射线交互模块），如果Enabled为false则RaycastInteraction也将为false
        /// </summary>
        public override bool RaycastInteraction
        {
            get
            {
                return InputRaycast ? InputRaycast.enabled : false;
            }
            set
            {
                raycastInteractionByUser = value;
                UpdateRaycastInteractionState();
            }
        }
        private bool raycastInteractionByInternal = true;
        public override bool RI_Internal
        {
            get
            {
                return InputRaycast ? InputRaycast.enabled : false;
            }
            set
            {
                raycastInteractionByInternal = value;
                UpdateRaycastInteractionState();
            }
        }

        private void UpdateRaycastInteractionState()
        {
            if (InputRaycast != null)
            {
                InputRaycast.enabled = m_Enabled && (EnableByExternal == null ? true : EnableByExternal.Invoke()) && raycastInteractionByInternal && raycastInteractionByUser;
                Debug.Log("HandInfo => UpdateRaycastInteractionState: " + InputRaycast.enabled);
            }
        }

        /// <summary>
        /// 设置或获得近距离交互状态（手柄暂不支持）
        /// </summary>
        public override bool TouchInteraction
        {
            get
            {
                return false;
            }
            set
            {

            }
        }

        /// <summary>
        /// 设置或获得手部的物理碰撞交互状态
        /// </summary>
        public override bool PhysicsInteraction
        {
            get
            {
                return false;
            }
            set
            {

            }
        }
        #endregion

        protected override void Awake()
        {
            base.Awake();

            _EnableRaycastByExternal = Internal_EnableRaycastByExternal;

            buttonState = new Dictionary<HandleKeyCode, HandleKeyEvent>();
            //buttonDownTimeSpan = new Dictionary<HandleKeyCode, float>();
            buttonStateChanged = new Dictionary<HandleKeyCode, bool>();
        }

        bool Internal_EnableRaycastByExternal(InputInfoBase inputInfo)
        {
            //Debug.Log("EnableRaycastByExternal ControllerInfo");
            return true;
        }

        protected override void Update()
        {
            base.Update();

            UpdateStatus();

#if UNITY_EDITOR
            InputRaycast.enabled = dummy.activeSelf;
#endif

            if (handleOrigin != null)
            {
                //射线方向
                if (!(HandleControllerManager.SetRayDirByExternal != null && HandleControllerManager.SetRayDirByExternal(this, ref rayDirection)))
                {
                    rayDirection = handleOrigin.forward;
                }

                //射线起点
                if (!(HandleControllerManager.SetRayStartPointByExternal != null && HandleControllerManager.SetRayStartPointByExternal(this, ref rayPoint_Start)))
                {
                    rayPoint_Start = handleOrigin.position + rayDirection * rayStartDistance;
                }
            }
        }

        private void OnApplicationPause(bool pause)
        {
            //Debug.Log($"HandleControllerInfo, OnApplicationPause: pause = {pause} handType = {this.handType} isBinding = {isBinding}, isConnected = {isConnected}, powerStats = {powerStats} ({Time.frameCount})");

            if (!pause)
            {
                isBinding = HandleControllerManager.Instance.GetBindState(handType);
                isConnected = HandleControllerManager.Instance.GetConnectState(handType);
                powerStats = HandleControllerManager.Instance.GetPowerStats(handType);
                Debug.Log($"HandleControllerInfo, OnApplicationPause: pause = {pause} handType = {this.handType} isBinding = {isBinding}, isConnected = {isConnected}, powerStats = {powerStats}");

                Visibility = isConnected;
                RI_Internal = isConnected;
                TouchInteraction = isConnected;
                PhysicsInteraction = isConnected;
            }

            isPaused = pause;
            //UpdatePinching();

            buttonState.Clear(); buttonStateChanged.Clear();
            startPinch = false; endPinch = false; isPinching = false;
        }

        private void OnEnable()
        {
            //在OnEnable中执行，是因为需要在切换到对应的控制器的时候将射线逻辑替换为当前控制器的射线逻辑
            RefreshRaycastExternal();

            //Debug.Log($"HandleControllerInfo, OnEnable: handType = {this.handType} isBinding = {isBinding}, isConnected = {isConnected}, powerStats = {powerStats} ({Time.frameCount})");

            isBinding = HandleControllerManager.Instance.GetBindState(handType);
            isConnected = HandleControllerManager.Instance.GetConnectState(handType);
            powerStats = HandleControllerManager.Instance.GetPowerStats(handType);
            Debug.Log($"HandleControllerInfo, OnEnable: handType = {this.handType} isBinding = {isBinding}, isConnected = {isConnected}, powerStats = {powerStats}");

            //UpdatePinching();

            buttonState.Clear(); buttonStateChanged.Clear();
            startPinch = false; endPinch = false; isPinching = false;
        }

        /// <summary>
        /// 更新了_EnableRaycastByExternal之后需要Refresh以生效
        /// </summary>
        void RefreshRaycastExternal()
        {
            InputRaycast.EnableRaycastByExternal = _EnableRaycastByExternal;
        }

        private void OnDisable()
        {
            //UpdatePinching();

            buttonState.Clear(); buttonStateChanged.Clear();
            startPinch = false; endPinch = false; isPinching = false;

            Debug.Log($"HandleControllerInfo, OnDisable: handType = {this.handType} isBinding = {isBinding}, isConnected = {isConnected}, powerStats = {powerStats} ({Time.frameCount})");
        }

        /// <summary>
        /// 初始化，设置当前输入设备的类型以及所属的手是左手还是右手
        /// </summary>
        /// <param name="inputType">输入设备类型：头控、手势、手柄</param>
        /// <param name="handType">0为左手，1为右手</param>
        public override void Init(InputType inputType, HandType handType)
        {
            base.Init(inputType, handType);

            //if (handType == HandType.Right)
            {
                // 若在handleOrigin下添加自定义的手柄模型，则隐藏默认手柄
                var hasAlter = handleOrigin.GetComponentsInChildren<Renderer>().Length > 0;
                // fix: 控制器切换时不能任意设置手柄显示
                if (hasAlter) transform.Find(handType == HandType.Left ? "LeftDummy" : "RightDummy").gameObject.SetActive(!hasAlter);
            }

            isBinding = HandleControllerManager.Instance.GetBindState(handType);
            isConnected = HandleControllerManager.Instance.GetConnectState(handType);
            powerStats = HandleControllerManager.Instance.GetPowerStats(handType);
            Debug.Log($"HandleControllerInfo, Init: handType = {this.handType} isBinding = {isBinding}, isConnected = {isConnected}, powerStats = {powerStats}");

            StartCoroutine(CheckStatus());
        }

        public void UpdateBindingState(bool status, Action<bool> callback)
        {
            if (isBinding == status) return;

            isBinding = status;
            bindingChanged = true;
            bindingAction = callback;
            Debug.Log($"HandleControllerInfo, UpdateBinding: handType = {this.handType} isBinding = {status}");

            if (isPaused || !gameObject.activeInHierarchy) UpdateStatus();
        }

        public void UpdateConnectedState(bool connected, Action<bool> callback)
        {
            if (isConnected == connected) return;

            isConnected = connected;
            connectedChanged = true;
            connectedAction = callback;
            Debug.Log($"HandleControllerInfo, UpdateConnectedState: handType = {this.handType} isConnected = {connected}");

            if (!connected) { buttonState.Clear(); buttonStateChanged.Clear(); }
            if (!connected) { startPinch = false; endPinch = false; isPinching = false; }
            if (isPaused || !gameObject.activeInHierarchy) UpdateStatus();
        }

        public void UpdatePowerStats(float power, Action<float> callback)
        {
            Debug.Log($"HandleControllerInfo, UpdatePowerStats: handType = {this.handType} power = {power}");
            if (powerStats == power) return;

            powerStats = power;
            powerChanged = true;
            powerChangedAction = callback;

            if (isPaused || !gameObject.activeInHierarchy) UpdateStatus();
        }

        public void UpdateButtonState(HandleKeyCode keyCode, bool pressed)
        {
            if (isPaused || !gameObject.activeInHierarchy) return;

            //Debug.Log($"HandleControllerInfo, {handType} {keyCode} StateChanged to {(pressed ? "pressed" : "released")} ({Time.frameCount})");

            if (!buttonState.ContainsKey(keyCode))
            {
                buttonState[keyCode] = pressed ? HandleKeyEvent.Idle : HandleKeyEvent.Pressing;
                buttonStateChanged[keyCode] = true;
            }
            else if ((pressed && buttonState[keyCode] == HandleKeyEvent.Idle) || (!pressed && buttonState[keyCode] == HandleKeyEvent.Pressing))
            {
                buttonStateChanged[keyCode] = true;
            }
        }

        public void UpdateAxis2D(Vector2 coord)
        {
            if (isPaused || !gameObject.activeInHierarchy) return;

            axis2DCoord = coord;
        }

        public void UpdateHoldState(bool isHeld, Action<bool> callback)
        {
            if (this.isHeld == isHeld) return;

            this.isHeld = isHeld;
            holdChanged = true;
            holdChangedAction = callback;
            //Debug.Log($"HandleControllerInfo, UpdateHoldState: handType = {this.handType} isHeld = {isHeld} ({Time.frameCount})");

            if (isPaused || !gameObject.activeInHierarchy) UpdateStatus();
        }

        public void UpdateSilenceState(bool isSilent, Action<bool> callback)
        {
            if (this.isSilent == isSilent) return;

            this.isSilent = isSilent;
            silenceChanged = true;
            silenceChangedAction = callback;
            //Debug.Log($"HandleControllerInfo, UpdateSilenceState: handType = {this.handType} isSilent = {isSilent} ({Time.frameCount})");

            if (isPaused || !gameObject.activeInHierarchy) UpdateStatus();
        }

        public bool GetButtonDown(HandleKeyCode keyCode)
        {
            if (!isPaused && isConnected && buttonState.ContainsKey(keyCode))
                return buttonState[keyCode] == HandleKeyEvent.Down;
            return false;
        }
        public bool GetButtonUp(HandleKeyCode keyCode)
        {
            if (!isPaused && isConnected && buttonState.ContainsKey(keyCode))
                return buttonState[keyCode] == HandleKeyEvent.Up;
            return false;
        }
        public bool GetButton(HandleKeyCode keyCode)
        {
            if (!isPaused && isConnected && buttonState.ContainsKey(keyCode))
                return buttonState[keyCode] == HandleKeyEvent.Pressing;
            return false;
        }
        public Vector2 GetAxis2D()
        {
            return axis2DCoord;
        }

        protected override void UpdatePinching()
        {
            var keys = new List<HandleKeyCode>(buttonStateChanged.Keys);
            for (int j = 0; j < keys.Count; ++j)
            {
                var keyCode = keys[j];
                if (!buttonStateChanged[keyCode]) continue;
                if (buttonState[keyCode] == HandleKeyEvent.Idle)
                {
                    buttonState[keyCode] = HandleKeyEvent.Down;
                    //Debug.Log($"HandleControllerInfo, {handType} {keyCode} change to Down from Idle ({Time.frameCount})");
                    //按下扳机键触发开始捏合动作
                    if (keyCode == HandleKeyCode.Trigger) startPinch = true;
                }
                else if (buttonState[keyCode] == HandleKeyEvent.Pressing)
                {
                    buttonState[keyCode] = HandleKeyEvent.Up;
                    //Debug.Log($"HandleControllerInfo, {handType} {keyCode} change to Up from Pressing ({Time.frameCount})");
                    //松开扳机键触发结束捏合动作
                    if (keyCode == HandleKeyCode.Trigger) endPinch = true;
                }
                //buttonStateChanged.Remove(keyCode);
            }

            keys = new List<HandleKeyCode>(buttonState.Keys);
            for (int i = 0; i < keys.Count; ++i)
            {
                var keyCode = keys[i];
                if (buttonStateChanged.ContainsKey(keyCode))
                {
                    buttonStateChanged.Remove(keyCode);
                    continue;
                }
                if (buttonState[keyCode] == HandleKeyEvent.Down)
                {
                    buttonState[keyCode] = HandleKeyEvent.Pressing;
                    //Debug.Log($"HandleControllerInfo, {handType} {keyCode} change to Pressing from Down ({Time.frameCount})");
                    // startPinch结束，触发持续捏合
                    if (keyCode == HandleKeyCode.Trigger) { startPinch = false; isPinching = true; }
                }
                else if (buttonState[keyCode] == HandleKeyEvent.Up)
                {
                    buttonState[keyCode] = HandleKeyEvent.Idle;
                    //Debug.Log($"HandleControllerInfo, {handType} {keyCode} change to Idle from Up ({Time.frameCount})");
                    // endPinch结束，触发结束捏合
                    if (keyCode == HandleKeyCode.Trigger) { endPinch = false; isPinching = false; }
                }
            }
        }

        private void UpdateStatus()
        {
            if (bindingChanged)
            {
                if (bindingAction != null) bindingAction(isBinding);
                bindingChanged = false;
            }

            if (connectedChanged)
            {
                if (connectedAction != null) connectedAction(isConnected);
                connectedChanged = false;

                Visibility = isConnected;
                RI_Internal = isConnected;
                TouchInteraction = isConnected;
                PhysicsInteraction = isConnected;
            }

            if (powerChanged)
            {
                if (powerChangedAction != null) powerChangedAction(powerStats);
                powerChanged = false;
            }

            if (holdChanged)
            {
                if (holdChangedAction != null) holdChangedAction(isHeld);
                holdChanged = false;
            }

            if (silenceChanged)
            {
                if (silenceChangedAction != null) silenceChangedAction(isSilent);
                silenceChanged = false;
            }
        }

        // for fix
        public IEnumerator CheckStatus()
        {
            yield return new WaitUntil(() => SessionManager.Instance != null && SessionManager.Instance.IsInited);

            isBinding = HandleControllerManager.Instance.GetBindState(handType);
            isConnected = HandleControllerManager.Instance.GetConnectState(handType);
            powerStats = HandleControllerManager.Instance.GetPowerStats(handType);
            Debug.Log($"HandleControllerInfo, Check: handType = {this.handType} isBinding = {isBinding}, isConnected = {isConnected}, powerStats = {powerStats} ({Time.frameCount})");

            Visibility = isConnected;
            RI_Internal = isConnected;
            TouchInteraction = isConnected;
            PhysicsInteraction = isConnected;
        }
    }
}