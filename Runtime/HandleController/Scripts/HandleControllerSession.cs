using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using AOT;
using EZXR.Glass.Runtime;
using EZXR.Glass.SixDof;
using UnityEngine;

namespace EZXR.Glass.Inputs
{
    public enum HandleKeyCode
    {
        Home = 301,
        [Obsolete("Use \"Back\" instead!")]
        Return = 300,
        Back = 300,
        Primary = 303,
        Secondary = 302,
        Rocker = 304,
        Trigger = 305,
        Grid = 306
    }

    [ScriptExecutionOrder(-50)]
    public class HandleControllerSession : MonoBehaviour
    {
        //[StructLayout(LayoutKind.Sequential, Pack = 1)]
        [StructLayout(LayoutKind.Sequential)]
        private struct HandControllerResult
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
            public float[] pose;    //xyz, wxyz
            public float is_3dof;   //0: 6dof，1: 3dof
            public double timestamp;
            public int controllerId; //0左手柄，1右手柄
        }

        private static HandleControllerSession instance;
        public static HandleControllerSession Instance
        {
            get
            {
                return instance;
            }
        }

        public static Pose controllerPose_left = new Pose();
        public static Pose controllerPose_right = new Pose();

        private HandControllerResult controllerLeftResult = new HandControllerResult();
        private HandControllerResult controllerRightResult = new HandControllerResult();

        private Action<HandType, bool> bindingEventCallback;
        private Action<HandType, bool> connectEventCallback;
        private Action<HandType, HandleKeyCode, bool> buttonEventCallback;
        private Action<HandType, Vector2> axis2DEventCallback;
        private Action<HandType, HandleKeyCode, bool> holdEventCallback;
        private Action<HandType, bool> silenceEventCallback;
        private Action<HandType, int> trackingStateEventCallback;
        private Action<HandType, bool> upgradeOTAEventCallback;
        private Action<bool> serverReconnectEventCallback;

        //public Action<bool> dynamicModeChangedCallBack;

        private delegate void ControllerBindEventCallback(int value);
        private delegate void ControllerConnectEventCallback(bool connected, int controllerId);
        private delegate void ControllerButtonEventCallback(int keycode, int state, int controllerId);
        private delegate void ControllerAxis2DEventCallback(int x, int y, int controllerId);
        private delegate void ControllerHoldEventCallback(int keycode, bool isHeld, int controllerId);
        private delegate void ControllerSilenceEventCallback(bool sleepless, int controllerId);

        private delegate void ControllerHxrctUpdateCallback(int status, int controllerId);
        //private delegate void ControllerDynamicModeChangedCallback(int status);

        private delegate void ControllerServerReconnectedCallback(bool connected);

        private struct ControllerBindEventUserData { public int status; public bool changed; }
        private struct ControllerConnectEventUserData { public bool connected; public int controllerId; public bool changed; }
        private struct ControllerButtonEventUserData { public int keycode; public int state; public int controllerId; public bool changed; }
        private struct ControllerAxis2DEventUserData { public int x; public int y; public int controllerId; public bool changed; }
        private struct ControllerHoldEventUserData { public int keycode; public bool isHeld; public int controllerId; public bool changed; }
        private struct ControllerSilenceEventUserData { public bool sleepless; public int controllerId; public bool changed; }
        private struct ControllerHxrctUpdateUserData { public int status; public int controllerId; public bool changed; }
        private struct ControllerServerReconnectedUserData { public bool connected; public bool changed; }

        private static ControllerBindEventUserData bindEventUserData;
        private static ControllerConnectEventUserData connectEventUserData;
        private static ControllerButtonEventUserData buttonEventUserData;
        private static ControllerAxis2DEventUserData axis2DEventUserData;
        private static ControllerHoldEventUserData holdEventUserData;
        private static ControllerSilenceEventUserData silenceEventUserData;
        private static ControllerHxrctUpdateUserData hxrctUpdateUserData;
        private static ControllerServerReconnectedUserData serverReconnectedUserData;

        //private static bool isPaused;
        private static bool isRegisterActive;

        private void Awake()
        {
            instance = this;

            controllerLeftResult.controllerId = -1;
            controllerRightResult.controllerId = -1;
            controllerLeftResult.pose = new float[7];
            controllerRightResult.pose = new float[7];
            controllerLeftResult.is_3dof = -1;
            controllerRightResult.is_3dof = -1;

            RegisterNativeAPI(); //当应用内SceneChanged时不会触发OnApplicationPause
                                 //需要在Awake时就注册NativeAPI
            Debug.Log("HandleControllerSession Awake");
        }

        private void OnApplicationPause(bool pause)
        {
            //isPaused = pause;
            //Debug.Log($"xxxxxx, OnApplicationPause call: pause = {pause}");

            if (!pause)
            {
                RegisterNativeAPI();
                Debug.Log("HandleControllerSession OnApplicationResume, successfully register Events(8)");
            }
            else
            {
                UnregisterNativeAPI();
                Debug.Log("HandleControllerSession OnApplicationPause, successfully unregister Events(8)");
            }
        }

        private void OnDestroy()
        {
            UnregisterNativeAPI();
            Debug.Log("HandleControllerSession, destroy register Events(8)");
        }

        private void RegisterNativeAPI()
        {
            if (isRegisterActive) return;

            isRegisterActive = true;

            NativeAPI.registerControllerBondChangeCallback(OnControllerBindEventCallback);
            NativeAPI.registerControllerConnectEventCallback(OnControllerConnectEventCallback);
            NativeAPI.registerControllerTouchEvent(OnControllerButtonEventCallback);
            NativeAPI.registerControllerRockerTouchEvent(OnControllerAxis2DEventCallback);
            NativeAPI.registerControllerTouchChangeEventCallback(OnControllerHoldEventCallback);
            NativeAPI.registerControllerSleepChangeCallback(OnControllerSilenceEventCallback);
            NativeAPI.registerControllerHxrctUpdateCallback(OnControllerHxrctUpdateCallback);
            //NativeAPI.registerControllerDynamicModeChangedCallback(OnControllerDynamicModeChangedCallback);
            //NativeAPI.registerConnectChangedCallback(OnControllerServerReconnectedCallback);
            NativeTracking.RegisterConnectChangedCallback(OnControllerServerReconnectedCallback);

            Debug.Log("HandleControllerSession RegisterNativeAPI, successfully register Events(8)");
        }

        private void UnregisterNativeAPI()
        {
            isRegisterActive = false;

            NativeAPI.registerControllerBondChangeCallback(null);
            NativeAPI.registerControllerConnectEventCallback(null);
            NativeAPI.registerControllerTouchEvent(null);
            NativeAPI.registerControllerRockerTouchEvent(null);
            NativeAPI.registerControllerTouchChangeEventCallback(null);
            NativeAPI.registerControllerSleepChangeCallback(null);
            NativeAPI.registerControllerHxrctUpdateCallback(null);
            //NativeAPI.registerControllerDynamicModeChangedCallback(null);
            //NativeAPI.registerConnectChangedCallback(null);
            NativeTracking.UnregisterConnectChangedCallback(OnControllerServerReconnectedCallback);

            Debug.Log("HandleControllerSession UnregisterNativeAPI, successfully unregister Events(8)");
        }

        // Update is called once per frame
        private void Update()
        {
            /*
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //Debug.Log("HandleControllerSession, Space KeyDown");
                Debug.Log($"HandleControllerSession, get Battery: L{NativeAPI.GetControllerBattery(0)}, R{NativeAPI.GetControllerBattery(1)}");
                Debug.Log($"HandleControllerSession, get ConnectState: L{NativeAPI.GetControllerConnectState(0)}, R{NativeAPI.GetControllerConnectState(1)}");
            }
            if (Input.GetKeyDown(KeyCode.C))
            {
                //Debug.Log("HandleControllerSession, C KeyDown");
                Debug.Log($"HandleControllerSession, cancel Binding: L{NativeAPI.ClearBond(0)}, R{NativeAPI.ClearBond(1)}");
            }
            if (Input.GetKeyDown(KeyCode.L))
            {
                //Debug.Log("HandleControllerSession, L KeyDown");
                Debug.Log($"HandleControllerSession, Left Binding: {NativeAPI.bondController(0)}");
            }
            if (Input.GetKeyDown(KeyCode.R))
            {
                //Debug.Log("HandleControllerSession, R KeyDown");
                Debug.Log($"HandleControllerSession, Right Binding: {NativeAPI.bondController(1)}");
            }
            if (Input.GetKeyDown(KeyCode.V))
            {
                //Debug.Log("HandleControllerSession, V KeyDown");
                Debug.Log($"HandleControllerSession, vibrate: {NativeAPI.vibrateController(1, 2, 200)}");
            }
            if (Input.GetKeyDown(KeyCode.B))
            {
                //Debug.Log("HandleControllerSession, B KeyDown");
                Debug.Log($"HandleControllerSession, bind status: {NativeAPI.GetControllerBondState() & 0xF0}");
            }

            if (Input.GetKeyDown(KeyCode.C))
            {
                //Debug.Log("HandleControllerSession, C KeyDown");
                Debug.Log($"HandleControllerSession, cancel Binding: L{NativeAPI.ClearBond(0)}, R{NativeAPI.ClearBond(1)}");
            }
            if (Input.GetKeyDown(KeyCode.R))
            {
                //Debug.Log("HandleControllerSession, R KeyDown");
                Debug.Log($"HandleControllerSession, Right Binding: {NativeAPI.bondController(1)}");
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                Debug.Log($"HandleControllerSession, HostVersion: {NativeAPI.ControllerClient_GetVersion()}, " +
                    $"ClientVersion: (L){NativeAPI.ControllerClient_GetCtrlVersion(0)}(R){NativeAPI.ControllerClient_GetCtrlVersion(1)}");
            }
            if (Input.GetKeyDown(KeyCode.U))
            {
                Debug.Log($"HandleControllerSession, HostUpdate: {NativeAPI.ControllerHxrUpgradeHost("/sdcard/Download/SA2101_52833_7.zip")}");
            }
            if (Input.GetKeyDown(KeyCode.W))
            {
                Debug.Log($"HandleControllerSession, ClientUpdate: (R){NativeAPI.updateHxrct("/sdcard/Download/hxrct_E7.zip", 1)}");
            }
            
            if (Input.GetKeyDown(KeyCode.G))
            {
                Debug.Log($"HandleControllerSession, GetCurrentControllerType: (R){NativeAPI.getCurrentHandType()}");
            }
            if (Input.GetKeyDown(KeyCode.M))
            {
                Debug.Log($"HandleControllerSession, GetControllerDeviceSN: (L){GetControllerDeviceSN(0)}(R){GetControllerDeviceSN(1)}");
            }
            */
        }

        /// <summary>
        /// 初始化注册回调
        /// </summary>
        public void InitRegistration(Action<HandType, bool> bindingEventCallback,
                                     Action<HandType, bool> connectEventCallback,
                                     Action<HandType, HandleKeyCode, bool> buttonEventCallback,
                                     Action<HandType, Vector2> axis2DEventCallback,
                                     Action<HandType, HandleKeyCode, bool> holdEventCallback,
                                     Action<HandType, bool> silenceEventCallback,
                                     Action<HandType, int> trackingStateEventCallback)
        {
            this.bindingEventCallback = bindingEventCallback;
            this.connectEventCallback = connectEventCallback;
            this.buttonEventCallback = buttonEventCallback;
            this.axis2DEventCallback = axis2DEventCallback;
            this.holdEventCallback = holdEventCallback;
            this.silenceEventCallback = silenceEventCallback;
            this.trackingStateEventCallback = trackingStateEventCallback;
        }

        /// <summary>
        /// 初始化注册OTA回调
        /// </summary>
        /// <param name="upgradeOTAEventCallback"></param>
        public void InitOTARegistration(Action<HandType, bool> upgradeOTAEventCallback)
        {
            this.upgradeOTAEventCallback = upgradeOTAEventCallback;
        }

        /// <summary>
        /// 初始化注册Server重连回调
        /// </summary>
        /// <param name="serverReconnectEventCallback"></param>
        public void InitServerReconnectRegistration(Action<bool> serverReconnectEventCallback)
        {
            this.serverReconnectEventCallback = serverReconnectEventCallback;
        }

        /// <summary>
        /// 进行手柄配对
        /// </summary>
        /// <param name="controllerId">0左手柄，1右手柄</param>
        /// <returns></returns>
        public bool BindHandle(int controllerId)
        {
            // 先解绑，再配对
            NativeAPI.ClearBond(controllerId);
            // 返回0是执行正常，成功与否要从回调中获得
            return NativeAPI.bondController(controllerId) >= 0;
        }

        /// <summary>
        /// 断开手柄配对
        /// </summary>
        /// <param name="controllerId">0左手柄，1右手柄</param>
        /// <returns></returns>
        public bool UnbindHandle(int controllerId)
        {
            // 返回0是执行正常，成功与否要从回调中获得
            return NativeAPI.ClearBond(controllerId) >= 0;
        }

        /// <summary>
        /// 取消手柄配对
        /// </summary>
        public void CancelBindHandle()
        {
            NativeAPI.CancelBond();
        }

        /// <summary>
        /// 获取绑定状态
        /// </summary>
        /// <param name="handType">指定手柄</param>
        /// <returns></returns>
        public bool GetBindState(HandType handType)
        {
            int value = NativeAPI.GetControllerBondState();
            if (handType == HandType.Left)
            {
                if ((value & 0x0F) == 0x00) return false;   // unbind
                if ((value & 0x0F) == 0x04) return true;    // bound
            }
            else
            {
                if ((value & 0xF0) == 0x00) return false;   // unbind
                if ((value & 0xF0) == 0x40) return true;    // bound
            }
            Debug.Log($"HandleControllerSession, GetBindState Exception: {value}");
            return false;
        }

        /// <summary>
        /// 获取连接状态
        /// </summary>
        /// <param name="controllerId">0左手柄，1右手柄</param>
        /// <returns></returns>
        public bool GetConnectState(int controllerId)
        {
            // 返回-1或0是未连接，返回1是已连接
            return NativeAPI.GetControllerConnectState(controllerId) > 0;
        }

        /// <summary>
        /// 获取手柄电量
        /// </summary>
        /// <param name="controllerId">0左手柄，1右手柄</param>
        /// <returns></returns>
        public float GetPowerStats(int controllerId)
        {
            // 电量0到6，转换成百分比
            return NativeAPI.GetControllerBattery(controllerId) / 6.0f;    // 满电量6
        }

        /// <summary>
        /// 进行手柄震动
        /// </summary>
        /// <param name="controllerId">0左手柄，1右手柄</param>
        /// <param name="level">震动等级 1-8</param>
        /// <param name="time">震动时长 0-65535 ms</param>
        public bool VibrateHandle(int controllerId, int level, int time)
        {
            // 返回0是执行正常，没有回调
            return NativeAPI.vibrateController(controllerId, level, time) >= 0;
        }

        /// <summary>
        /// 切换控制器类型
        /// </summary>
        /// <param name="type">0头控，1手势，2手柄</param>
        public bool ChangeControllerType(int type)
        {
            if (SessionManager.Instance && SessionManager.Instance.IsInited)
            {
                NativeAPI.changeControllerType(type);
                return true;
            }
            return false;
        }

        /// <summary>
        /// 设置动态切换模式
        /// </summary>
        /// <param name="type">0关闭，1开启</param>
        //public void ChangeControllerDynamicMode(bool status)
        //{
        //    Debug.Log($"HandleControllerSession, changeControllerDynamicMode: {status}");
        //    NativeAPI.changeControllerDynamicMode(status ? 1 : 0);
        //}

        /// <summary>
        /// 获取动态切换模式(用于跨应用)
        /// </summary>
        /// <returns>0关闭，1开启，-1未定义</returns>
        //public int GetCurrentDynamicMode()
        //{
        //    return NativeAPI.getCurrentDynamicMode();
        //}

        /// <summary>
        /// 获取当前控制器类型(用于跨应用)
        /// </summary>
        /// <returns></returns>
        //public int GetCurrentControllerType()
        //{
        //    return NativeAPI.getCurrentHandType();
        //}

        /// <summary>
        /// 获取手柄设备SN号
        /// </summary>
        /// <param name="controllerId">0左手柄，1右手柄</param>
        /// <returns></returns>
        public string GetControllerDeviceSN(int controllerId)
        {
            string sn = Marshal.PtrToStringAnsi(NativeAPI.getControllerMac(controllerId));
            Debug.Log($"HandleControllerSession, GetControllerDeviceSN: {sn}");
            return sn;
        }

        /// <summary>
        /// 获取手柄固件版本号
        /// </summary>
        /// <param name="controllerId">0左手柄，1右手柄</param>
        /// <returns></returns>
        public int GetOTAVersion(int controllerId)
        {
            return NativeAPI.ControllerClient_GetCtrlVersion(controllerId);
        }

        /// <summary>
        /// 获取手柄Host固件版本号
        /// </summary>
        /// <returns></returns>
        public int GetHostOTAVersion()
        {
            return NativeAPI.ControllerClient_GetVersion();
        }

        /// <summary>
        /// 升级手柄固件
        /// </summary>
        /// <param name="controllerId">0左手柄，1右手柄</param>
        /// <param name="package">固件路径</param>
        /// <returns></returns>
        public bool UpgradeOTA(int controllerId, string package)
        {
            //return NativeAPI.updateHxrct(package, controllerId) == 1;
            Debug.Log($"HandleControllerSession, UpgradeOTA: controllerId = {controllerId}, package = {package}," +
                $"result = {NativeAPI.updateHxrct(package, controllerId)}");
            return true;
        }

        /// <summary>
        /// 升级手柄Host固件
        /// </summary>
        /// <param name="package">固件路径</param>
        /// <returns></returns>
        public bool UpgradeHostOTA(string package)
        {
            //return NativeAPI.ControllerHxrUpgradeHost(package) == 1;
            Debug.Log($"HandleControllerSession, UpgradeHostOTA: package = {package}," +
                $"result = {NativeAPI.ControllerHxrUpgradeHost(package)}");
            return true;
        }

        /// <summary>
        /// 更新手柄位姿状态
        /// </summary>
        public void UpdateHandlePose()
        {
            if (ARFrame.SessionStatus == EZVIOState.EZVIOCameraState_Tracking)
            {
                // 适配0dof、3dof、6dof
                var translation = HMDPoseTracker.Instance.transform.position;
                var quaternion = HMDPoseTracker.Instance.transform.rotation;
                // 适配空间计算
                if (HMDPoseTracker.Instance.UseLocalPose)
                {
                    translation = HMDPoseTracker.Instance.transform.localPosition;
                    quaternion = HMDPoseTracker.Instance.transform.localRotation;
                }
                var originHeadPose = new float[]
                {
                    translation.x, translation.y,translation.z,
                    quaternion.x, quaternion.y, quaternion.z, quaternion.w
                };

                //var originHeadPose = new float[7];
                //Array.Copy(ARFrame.OriginVIOResult.translation, 0, originHeadPose, 0, 3);
                //Array.Copy(ARFrame.OriginVIOResult.quaternion, 0, originHeadPose, 3, 4);
                ////Debug.Log($"HandControllerSession, feed originHeadPose: {originHeadPose[0]},{originHeadPose[1]},{originHeadPose[2]},{originHeadPose[3]},{originHeadPose[4]},{originHeadPose[5]},{originHeadPose[6]}");

                float left_is_3dof = controllerLeftResult.is_3dof, right_is_3dof = controllerRightResult.is_3dof;
                if (NativeAPI.getControllerResultWithHead(ref controllerLeftResult, ref controllerRightResult, originHeadPose, originHeadPose.Length))
                {
#if UNITY_EDITOR
                    var left_position = Vector3.zero;
                    var left_rotation = Quaternion.identity;
                    var right_position = Vector3.zero;
                    var right_rotation = Quaternion.identity;
#else
                    var left_position = new Vector3(controllerLeftResult.pose[0], controllerLeftResult.pose[1], controllerLeftResult.pose[2]);
                    var left_rotation = new Quaternion(controllerLeftResult.pose[4], controllerLeftResult.pose[5], controllerLeftResult.pose[6], controllerLeftResult.pose[3]);
                    var right_position = new Vector3(controllerRightResult.pose[0], controllerRightResult.pose[1], controllerRightResult.pose[2]);
                    var right_rotation = new Quaternion(controllerRightResult.pose[4], controllerRightResult.pose[5], controllerRightResult.pose[6], controllerRightResult.pose[3]);
#endif
                    controllerPose_left.position = left_position;
                    controllerPose_left.rotation = left_rotation;
                    //ConversionUtility.RecenterByOffset(controllerPose_left, ARFrame.accumulatedRecenterOffset4x4, ref controllerPose_left);
                    //leftController.SetPositionAndRotation(controllerPose_left.position, controllerPose_left.rotation);

                    controllerPose_right.position = right_position;
                    controllerPose_right.rotation = right_rotation;
                    //ConversionUtility.RecenterByOffset(controllerPose_right, ARFrame.accumulatedRecenterOffset4x4, ref controllerPose_right);
                    //rightController.SetPositionAndRotation(controllerPose_right.position, controllerPose_right.rotation);

                    if (left_is_3dof != controllerLeftResult.is_3dof)
                    {
                        Debug.Log($"HandleControllerSession, controllerLeftResult is_3dof changed to: {controllerLeftResult.is_3dof}");
                        trackingStateEventCallback?.Invoke(HandType.Left, (int)controllerLeftResult.is_3dof);
                    }
                    if (right_is_3dof != controllerRightResult.is_3dof)
                    {
                        Debug.Log($"HandleControllerSession, controllerRightResult is_3dof changed to: {controllerRightResult.is_3dof}");
                        trackingStateEventCallback?.Invoke(HandType.Right, (int)controllerRightResult.is_3dof);
                    }
                }
            }
        }

        /// <summary>
        /// 通过Update将多线程或异步回调转化到主线程中处理
        /// </summary>
        public void UpdateHandleCallback()
        {
            if (bindEventUserData.changed)
            {
                bindEventUserData.changed = false;

                bool status = false;
                bool valid_left = false;
                int value = bindEventUserData.status;
                if ((value & 0x0F) == 0x00) { valid_left = true; status = false; }
                if ((value & 0x0F) == 0x04) { valid_left = true; status = true; }
                Debug.Log($"HandleControllerSession, UpdateHandleCallback bindingEventCallback: handType = Left, valid = {valid_left}, status = {status}");
                if (valid_left) HandleControllerSession.Instance.bindingEventCallback(HandType.Left, status);
                bool valid_right = false;
                if ((value & 0xF0) == 0x00) { valid_right = true; status = false; }
                if ((value & 0xF0) == 0x40) { valid_right = true; status = true; }
                Debug.Log($"HandleControllerSession, UpdateHandleCallback bindingEventCallback: handType = Right, valid = {valid_right}, status = {status}");
                if (valid_right) HandleControllerSession.Instance.bindingEventCallback(HandType.Right, status);
            }

            if (connectEventUserData.changed)
            {
                connectEventUserData.changed = false;

                var connected = connectEventUserData.connected;
                var controllerId = connectEventUserData.controllerId;
                Debug.Log($"HandleControllerSession, UpdateHandleCallback connectEventCallback: connected = {connected}, controllerId = {controllerId}");
                HandleControllerSession.Instance.connectEventCallback((HandType)controllerId, connected);
            }

            if (buttonEventUserData.changed)
            {
                buttonEventUserData.changed = false;

                var controllerId = buttonEventUserData.controllerId;
                var keycode = buttonEventUserData.keycode;
                var state = buttonEventUserData.state;
                //Debug.Log($"HandleControllerSession, UpdateHandleCallback buttonEventCallback: keycode = {keycode}, state = {state}, controllerId = {controllerId}");
                HandleControllerSession.Instance.buttonEventCallback((HandType)controllerId, (HandleKeyCode)keycode, state > 0);
            }

            if (axis2DEventUserData.changed)
            {
                axis2DEventUserData.changed = false;

                var x = axis2DEventUserData.x;
                var y = axis2DEventUserData.y;
                var controllerId = axis2DEventUserData.controllerId;
                //Debug.Log($"HandleControllerSession, UpdateHandleCallback axis2DEventCallback: x = {x}, y = {y}, controllerId = {controllerId}");
                float axis_x = x < 8 ? Mathf.Lerp(-1, 0, x / 8.0f) : Mathf.Lerp(0, 1, (x - 8) / (15.0f - 8.0f));
                float axis_y = y < 8 ? Mathf.Lerp(-1, 0, y / 8.0f) : Mathf.Lerp(0, 1, (y - 8) / (15.0f - 8.0f));
                HandleControllerSession.Instance.axis2DEventCallback((HandType)controllerId, new Vector2(axis_x, axis_y));
            }

            if (holdEventUserData.changed)
            {
                holdEventUserData.changed = false;

                var keycode = holdEventUserData.keycode;
                var isHeld = holdEventUserData.isHeld;
                var controllerId = holdEventUserData.controllerId;
                //Debug.Log($"HandleControllerSession, UpdateHandleCallback holdEventCallback: keycode = {keycode}, hold = {isHeld}, controllerId = {controllerId}");
                HandleControllerSession.Instance.holdEventCallback((HandType)controllerId, (HandleKeyCode)keycode, isHeld);
            }

            if (silenceEventUserData.changed)
            {
                silenceEventUserData.changed = false;

                var sleepless = silenceEventUserData.sleepless;
                var controllerId = silenceEventUserData.controllerId;
                //Debug.Log($"HandleControllerSession, UpdateHandleCallback silenceEventCallback: silent = {!sleepless}, controllerId = {controllerId}");
                HandleControllerSession.Instance.silenceEventCallback((HandType)controllerId, !sleepless);
            }

            if (hxrctUpdateUserData.changed)
            {
                hxrctUpdateUserData.changed = false;

                var status = hxrctUpdateUserData.status;
                var controllerId = hxrctUpdateUserData.controllerId;
                Debug.Log($"HandleControllerSession, UpdateHandleCallback upgradeOTAEventCallback: status = {status}, controllerId = {controllerId}");
                HandleControllerSession.Instance.upgradeOTAEventCallback((HandType)controllerId, status == 0);
            }

            if (serverReconnectedUserData.changed)
            {
                serverReconnectedUserData.changed = false;

                Debug.Log($"HandleControllerSession, UpdateHandleCallback serverReconnectEventCallback: connected = {serverReconnectedUserData.connected}");
                HandleControllerSession.Instance.serverReconnectEventCallback(serverReconnectedUserData.connected);
            }
        }

        //手柄绑定状态回调
        [MonoPInvokeCallback(typeof(ControllerBindEventCallback))]
        private static void OnControllerBindEventCallback(int value)
        {
            //if (isPaused) return;
            bindEventUserData.changed = true;
            bindEventUserData.status = value;

            //Debug.Log($"HandleControllerSession, OnControllerBindEventCallback bindEventUserData: changed = {bindEventUserData.changed}, status = {bindEventUserData.status}");
        }

        //手柄连接状态回调 左手0 右手1
        [MonoPInvokeCallback(typeof(ControllerConnectEventCallback))]
        private static void OnControllerConnectEventCallback(bool connected, int controllerId)
        {
            //if (isPaused) return;
            connectEventUserData.changed = true;
            connectEventUserData.connected = connected;
            connectEventUserData.controllerId = controllerId;

            //Debug.Log($"HandleControllerSession, OnControllerConnectEventCallback connectEventUserData: changed = {connectEventUserData.changed}, connected = {connectEventUserData.connected}, controllerId = {connectEventUserData.controllerId}");
        }

        //手柄按键事件回调
        //controllerId 0左手柄，1右手柄
        //state 1按下，0弹起
        //keycode 300返回，301主页，302BY键，303AX键，304摇杆键，305扳机键，306Grid键
        [MonoPInvokeCallback(typeof(ControllerButtonEventCallback))]
        private static void OnControllerButtonEventCallback(int keycode, int state, int controllerId)
        {
            //if (isPaused) return;
            buttonEventUserData.changed = true;
            buttonEventUserData.keycode = keycode;
            buttonEventUserData.state = state;
            buttonEventUserData.controllerId = controllerId;

            //Debug.Log($"HandleControllerSession, OnControllerButtonEventCallback buttonEventUserData: changed = {buttonEventUserData.changed}, keycode = {buttonEventUserData.keycode}, state = {buttonEventUserData.state}, controllerId = {buttonEventUserData.controllerId}");
        }

        //手柄摇杆事件回调
        //controllerId 0左手柄，1右手柄
        //xy整型，左下角[0,0]，中间[8,8]，右上角[15,15]
        [MonoPInvokeCallback(typeof(ControllerAxis2DEventCallback))]
        private static void OnControllerAxis2DEventCallback(int x, int y, int controllerId)
        {
            //if (isPaused) return;
            axis2DEventUserData.changed = true;
            axis2DEventUserData.x = x;
            axis2DEventUserData.y = y;
            axis2DEventUserData.controllerId = controllerId;

            //Debug.Log($"HandleControllerSession, OnControllerAxis2DEventCallback axis2DEventUserData: changed = {axis2DEventUserData.changed}, x = {axis2DEventUserData.x}, y = {axis2DEventUserData.y}, controllerId = {axis2DEventUserData.controllerId}");
        }

        //手柄握持事件回调
        //原理是按键的触摸反馈
        //controllerId 0左手柄，1右手柄
        //keycode 302BY键，303AX键，304摇杆键，305扳机键
        [MonoPInvokeCallback(typeof(ControllerAxis2DEventCallback))]
        private static void OnControllerHoldEventCallback(int keycode, bool isHeld, int controllerId)
        {
            //if (isPaused) return;
            holdEventUserData.changed = true;
            holdEventUserData.keycode = keycode;
            holdEventUserData.isHeld = isHeld;
            holdEventUserData.controllerId = controllerId;

            //Debug.Log($"HandleControllerSession, OnControllerHoldEventCallback holdEventUserData: changed = {holdEventUserData.changed}, keycode = {holdEventUserData.keycode}, isHeld = {holdEventUserData.isHeld}, controllerId = {holdEventUserData.controllerId}");
        }

        //手柄静置事件回调
        //原理是监测IMU是否静止
        //controllerId 0左手柄，1右手柄
        [MonoPInvokeCallback(typeof(ControllerSilenceEventCallback))]
        private static void OnControllerSilenceEventCallback(bool sleepless, int controllerId)
        {
            //if (isPaused) return;
            silenceEventUserData.changed = true;
            silenceEventUserData.sleepless = sleepless;
            silenceEventUserData.controllerId = controllerId;

            //Debug.Log($"HandleControllerSession, OnControllerSilenceEventCallback silenceEventUserData: changed = {silenceEventUserData.changed}, sleepless = {silenceEventUserData.sleepless}, controllerId = {silenceEventUserData.controllerId}");
        }

        //手柄Client固件更新回调
        //stauts 0成功
        //controllerId 0左手柄，1右手柄
        [MonoPInvokeCallback(typeof(ControllerHxrctUpdateCallback))]
        private static void OnControllerHxrctUpdateCallback(int status, int controllerId)
        {
            //if (isPaused) return;
            hxrctUpdateUserData.changed = true;
            hxrctUpdateUserData.status = status;
            hxrctUpdateUserData.controllerId = controllerId;

            Debug.Log($"HandleControllerSession, OnControllerHxrctUpdateCallback hxrctUpdateUserData: changed = {hxrctUpdateUserData.changed}, status = {hxrctUpdateUserData.status}, controllerId = {hxrctUpdateUserData.controllerId}");
        }

        //控制器dynamicMode状态更新回调
        //status 0关闭，1开启，-1未定义
        //[MonoPInvokeCallback(typeof(ControllerDynamicModeChangedCallback))]
        //private static void OnControllerDynamicModeChangedCallback(int status)
        //{
        //    Debug.Log($"HandleControllerSession, OnControllerDynamicModeChangedCallback: status = {status}");
        //    if (status >= 0 && HandleControllerSession.Instance.dynamicModeChangedCallBack != null)
        //    {
        //        HandleControllerSession.Instance.dynamicModeChangedCallBack(status > 0);
        //    }
        //}

        //手柄Server重连状态回调
        //connected true已重连
        [MonoPInvokeCallback(typeof(Action<bool>))]
        private static void OnControllerServerReconnectedCallback(bool connected)
        {
            //if (isPaused) return;
            serverReconnectedUserData.changed = true;
            serverReconnectedUserData.connected = connected;

            Debug.Log($"HandleControllerSession, OnControllerServerReconnectedCallback serverReconnectedUserData: changed = {serverReconnectedUserData.changed}, connected = {serverReconnectedUserData.connected}");
        }

        private partial struct NativeAPI
        {
#if !UNITY_EDITOR
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern bool getControllerResultWithHead(ref HandControllerResult controllerLeftResult, ref HandControllerResult controllerRightResult, float[] headpose, int headposeSize);

            //注册手柄按键事件回调
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void registerControllerTouchEvent(ControllerButtonEventCallback callback);
            //注册手柄摇杆事件回调
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void registerControllerRockerTouchEvent(ControllerAxis2DEventCallback callback);
            //注册手柄握持事件回调
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void registerControllerTouchChangeEventCallback(ControllerHoldEventCallback callback);
            //注册手柄绑定状态回调
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void registerControllerBondChangeCallback(ControllerBindEventCallback callback);
            //注册手柄连接状态回调
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void registerControllerConnectEventCallback(ControllerConnectEventCallback callback);
            //注册手柄静置状态回调
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void registerControllerSleepChangeCallback(ControllerSilenceEventCallback callback);

            //注册手柄(手柄Client)固件更新状态回调
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void registerControllerHxrctUpdateCallback(ControllerHxrctUpdateCallback callback);

            ////注册手柄Server重连状态回调
            //[DllImport(NativeConsts.NativeLibrary)]
            //public static extern void registerConnectChangedCallback(ControllerServerReconnectedCallback callback);

            //进行手柄配对：左手0 右手1 (手柄同时按住扳机键和返回键)
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern int bondController(int controllerId);
            //断开手柄配对：左手0 右手1
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern int ClearBond(int controllerId);
            //取消手柄配对
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void CancelBond();
            //获取手柄绑定状态
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern int GetControllerBondState();
            //获取手柄连接状态：1连接,0未连接
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern int GetControllerConnectState(int controllerId);
            //获取手柄电量，[0, 6]
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern int GetControllerBattery(int controllerId);
            //进行手柄震动
            //level 震动等级 1-8
            //time 震动时长 0-65535 ms
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern int vibrateController(int controllerId, int level, int time);

            //控制器类型切换时发送通知
            //type 0-头控，1-手势，2-手柄
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void changeControllerType(int type);

            //获取当前控制器类型切换到的类型
            //type 0-头控，1-手势，2-手柄 (default为-1)
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern int getCurrentHandType();

            // 获取手柄序列号
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern IntPtr getControllerMac(int controllerId);
            //获取眼镜(手柄Host)Nordic固件版本
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern int ControllerClient_GetVersion();
            //获取手柄(手柄Client)固件版本
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern int ControllerClient_GetCtrlVersion(int controllerId);
            //眼镜(手柄Host)Nordic固件更新
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern int ControllerHxrUpgradeHost(string path);
            //手柄(手柄Client)固件更新
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern int updateHxrct(string path, int controllerId);

            ////注册控制器dynamicMode状态更新回调
            //[DllImport(NativeConsts.NativeLibrary)]
            //public static extern void registerControllerDynamicModeChangedCallback(ControllerDynamicModeChangedCallback callback);
            ////设置控制器dynamicMode状态
            //[DllImport(NativeConsts.NativeLibrary)]
            //public static extern void changeControllerDynamicMode(int mode);
            ////获取控制器dynamicMode状态
            //[DllImport(NativeConsts.NativeLibrary)]
            //public static extern int getCurrentDynamicMode();
#else
            public static bool getControllerResultWithHead(ref HandControllerResult controllerLeftResult, ref HandControllerResult controllerRightResult, float[] headpose, int headposeSize)
            {
                return false;
            }

            //注册手柄按键事件回调
            public static void registerControllerTouchEvent(ControllerButtonEventCallback callback) { }
            //注册手柄摇杆事件回调
            public static void registerControllerRockerTouchEvent(ControllerAxis2DEventCallback callback) { }
            //注册手柄握持事件回调
            public static void registerControllerTouchChangeEventCallback(ControllerHoldEventCallback callback) { }
            //注册手柄绑定状态回调
            public static void registerControllerBondChangeCallback(ControllerBindEventCallback callback) { }
            //注册手柄连接状态回调
            public static void registerControllerConnectEventCallback(ControllerConnectEventCallback callback) { }
            //注册手柄静置状态回调
            public static void registerControllerSleepChangeCallback(ControllerSilenceEventCallback callback) { }

            ////注册手柄Server重连状态回调
            //public static void registerConnectChangedCallback(ControllerServerReconnectedCallback callback) { }

            //进行手柄配对：左手0 右手1 (手柄同时按住扳机键和返回键)
            public static int bondController(int controllerId)
            {
                return -1;
            }
            //断开手柄配对：左手0 右手1
            public static int ClearBond(int controllerId)
            {
                return -1;
            }
            //取消手柄配对
            public static void CancelBond()
            {
                return;
            }
            //获取手柄绑定状态
            public static int GetControllerBondState()
            {
                return -1;
            }
            //获取手柄连接状态：1连接,0未连接
            public static int GetControllerConnectState(int controllerId)
            {
                return -1;
            }
            //获取手柄电量，[0, 6]
            public static int GetControllerBattery(int controllerId)
            {
                return -1;
            }
            //进行手柄震动
            //level 震动等级 1-8
            //time 震动时长 0-65535 ms
            public static int vibrateController(int controllerId, int level, int time)
            {
                return -1;
            }

            //控制器类型切换时发送通知
            //type 0-头控，1-手势，2-手柄
            public static void changeControllerType(int type)
            {
            }

            //获取当前控制器类型切换到的类型
            //type 0-头控，1-手势，2-手柄 (default为-1)
            public static int getCurrentHandType()
            {
                return -1;
            }

            // 获取手柄序列号
            public static IntPtr getControllerMac(int controllerId)
            {
                return IntPtr.Zero;
            }

            //获取眼镜(手柄Host)Nordic固件版本
            public static int ControllerClient_GetVersion()
            {
                return -1;
            }
            //获取手柄(手柄Client)固件版本
            public static int ControllerClient_GetCtrlVersion(int controllerId)
            {
                return -1;
            }
            //眼镜(手柄Host)Nordic固件更新
            public static int ControllerHxrUpgradeHost(string path)
            {
                return -1;
            }
            //手柄(手柄Client)固件更新
            public static int updateHxrct(string path, int controllerId)
            {
                return -1;
            }
            //注册手柄(手柄Client)固件更新状态回调
            public static void registerControllerHxrctUpdateCallback(ControllerHxrctUpdateCallback callback) { }

            ////注册控制器dynamicMode状态更新回调
            //public static void registerControllerDynamicModeChangedCallback(ControllerDynamicModeChangedCallback callback) { }

            ////设置控制器dynamicMode状态
            //public static void changeControllerDynamicMode(int mode)
            //{
            //}

            ////获取控制器dynamicMode状态
            //public static int getCurrentDynamicMode()
            //{
            //    return -1;
            //}
#endif
        }

    }
}