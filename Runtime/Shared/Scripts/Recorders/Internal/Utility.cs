/* 
*   NatCorder
*   Copyright (c) 2020 Yusuf Olokoba.
*/

namespace NatSuite.Recorders.Internal {

    using UnityEngine;
    using System;
    using System.IO;

    public static class Utility {

        private static string directory;

        public static string GetPath (string extension) {
            if (directory == null) {
                var editor = Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor;
                directory = editor ? Path.Combine(Directory.GetCurrentDirectory() , "EditorDebug/Video") :Path.Combine( Application.persistentDataPath,"Video");
            }

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            var timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            var name = $"{timestamp}{extension}";
            var path = Path.Combine(directory, name);
            return path;
        }
    }
}