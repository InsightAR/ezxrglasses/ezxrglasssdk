﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    [ScriptExecutionOrder(-30)]
    public partial class Utilities : MonoBehaviour
    {
        private static Utilities_Android instance_Android;
        public static Utilities_Android Android
        {
            get
            {
                if (instance_Android == null)
                {
                    instance_Android = FindObjectOfType<Utilities_Android>();
                    if (instance_Android == null)
                    {
                        GameObject go = new GameObject("Utilities_Android");
                        instance_Android = go.AddComponent<Utilities_Android>();
                        if (!Application.isEditor)
                        {
                            instance_Android.unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                            instance_Android.unityActivity = instance_Android.unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
                            instance_Android.intentObject = new AndroidJavaObject("android.content.Intent");
                            instance_Android.packageManagerObject = instance_Android.unityActivity.Call<AndroidJavaObject>("getPackageManager");
                            instance_Android.applicationContext = instance_Android.unityActivity.Call<AndroidJavaObject>("getApplicationContext");
                        }
                    }
                }
                return instance_Android;
            }
        }

        public class Utilities_Android : MonoBehaviour
        {
            const string packageName_Launcher = "com.ezxr.glass.arlauncher";
            public AndroidJavaClass unityPlayerClass;
            public AndroidJavaObject unityActivity;
            public AndroidJavaObject intentObject;
            public AndroidJavaObject packageManagerObject;
            public AndroidJavaObject applicationContext;

            /// <summary>
            /// 如果要打开的是当前Activity，则直接通过此回调传递参数
            /// </summary>
            public Action<string[]> SameActivityDirectCallback;
            /// <summary>
            /// 跳转到外部应用
            /// </summary>
            public Action<string> GotoExternalApp;

            /// <summary>
            /// 获得应用启动时OnCreate中的Intent传参（>>>>>>>>>>>>>>>>>>>>>>注意：需要EZXRUnityPlayerActivity.java<<<<<<<<<<<<<<<<<<<<<<<<<<）
            /// </summary>
            /// <param name="paramName"></param>
            /// <returns></returns>
            public string GetActivityCreateParams(string paramName)
            {
                // 获取启动当前Activity时传递的Intent对象
                AndroidJavaClass EZXRUnityPlayerActivity = new AndroidJavaClass("com.unity3d.player.EZXRUnityPlayerActivity");
                AndroidJavaObject intent = EZXRUnityPlayerActivity.GetStatic<AndroidJavaObject>("oncreateIntent");
                bool hasExtra = intent.Call<bool>("hasExtra", paramName);
                Debug.Log("AndroidUtils--> GetActivityStartParams: hasExtra: " + hasExtra);
                Debug.Log("AndroidUtils--> getStringExtra: " + intent.Call<string>("getStringExtra", paramName));
                if (hasExtra)
                {
                    //AndroidJavaObject extras = intent.Call<AndroidJavaObject>("getExtras");
                    return intent.Call<string>("getStringExtra", paramName);
                }
                return "";
            }

            /// <summary>
            /// 得到此Android应用被外部打开时传入的参数
            /// </summary>
            /// <param name="paramName"></param>
            /// <returns></returns>
            public string GetActivityStartParams(string paramName)
            {
                AndroidJavaObject intent;
                intent = /*intent ??*/ unityActivity.Call<AndroidJavaObject>("getIntent");
                bool hasExtra = intent.Call<bool>("hasExtra", paramName);
                Debug.Log("AndroidUtils--> GetActivityStartParams: hasExtra: " + hasExtra);
                Debug.Log("AndroidUtils--> getStringExtra: " + intent.Call<string>("getStringExtra", paramName));
                string value = "";
                if (hasExtra)
                {
                    //AndroidJavaObject extras = intent.Call<AndroidJavaObject>("getExtras");
                    value = intent.Call<string>("getStringExtra", paramName);
                }
                //置空，避免下次调用的时候依然获得上次的结果
                unityActivity.Call("setIntent", new AndroidJavaObject("android.content.Intent"));
                return value;
            }

            public void StartActivityDelayed(string pkgName, float delay = 1, params string[] extraData)
            {
                if (!isProcessingStartActivity)
                {
                    isProcessingStartActivity = true;
                    StartCoroutine(Coroutine_StartActivityDelayed(pkgName, delay, extraData));
                }
            }

            bool isProcessingStartActivity = false;
            IEnumerator Coroutine_StartActivityDelayed(string pkgName, float delay = 1, params string[] extraData)
            {
                yield return new WaitForSeconds(delay);

                StartActivity(pkgName, extraData);
                isProcessingStartActivity = false;
            }

            public void StartLauncher(params string[] extraData)
            {
                string topmost = GetTopMostApp();

                if (topmost != packageName_Launcher)
                {
                    StartActivity(packageName_Launcher, extraData);
                }
                else
                {
                    SendIntent("Instructions", packageName_Launcher, extraData);
                }
            }

            /// <summary>
            /// 打开指定包名的Activity
            /// </summary>
            /// <param name="pkgName">包名</param>
            /// <param name="extraData">要传递的参数，需要成对传入，按一个key后跟一个value的形式</param>
            public void StartActivity(string pkgName, params string[] extraData)
            {
                if (Application.identifier == pkgName)
                {
                    if (SameActivityDirectCallback != null)
                    {
                        SameActivityDirectCallback(extraData);
                    }
                }
                else
                {
                    if (Application.platform == RuntimePlatform.Android)
                    {
                        AndroidJavaObject joIntent = packageManagerObject.Call<AndroidJavaObject>("getLaunchIntentForPackage", pkgName);
                        if (null != joIntent)
                        {
                            AndroidJavaObject joNIntent = joIntent.Call<AndroidJavaObject>("addFlags", joIntent.GetStatic<int>("FLAG_ACTIVITY_REORDER_TO_FRONT"));
                            if (extraData.Length > 0 && extraData.Length % 2 == 0)
                            {
                                for (int i = 0; i < extraData.Length; i += 2)
                                {
                                    joIntent.Call<AndroidJavaObject>("putExtra", extraData[i], extraData[i + 1]);
                                }
                            }
                            unityActivity.Call("startActivity", joNIntent);
                            joIntent.Dispose();

                            if (GotoExternalApp != null)
                            {
                                Debug.Log("GotoExternalApp: " + pkgName);
                                GotoExternalApp.Invoke(pkgName);
                            }
                        }
                        else
                        {
                            string msg = "Package <" + pkgName + "> not exist on device.";
                            Debug.Log(msg);

                            //ShowToast(msg);
                        }
                    }
                    else
                    {
                        Debug.Log("StartActivity Package: " + pkgName);
                    }
                }
            }

            /// <summary>
            /// 判断当前package是否在运行
            /// </summary>
            /// <param name="packageName"></param>
            /// <returns></returns>
            public bool PackageIsRunning(string packageName)
            {
                AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject activity = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaObject context = activity.Call<AndroidJavaObject>("getApplicationContext");
                AndroidJavaObject activityManager = context.Call<AndroidJavaObject>("getSystemService", "activity");

                AndroidJavaObject runningProcesses = activityManager.Call<AndroidJavaObject>("getRunningAppProcesses");
                int numOfProcesses = runningProcesses.Call<int>("size");

                for (int i = 0; i < numOfProcesses; i++)
                {
                    AndroidJavaObject processInfo = runningProcesses.Call<AndroidJavaObject>("get", i);
                    string processName = processInfo.Get<string>("processName");

                    if (processName.Contains(packageName))
                    {
                        Debug.Log("AppStatus: " + packageName + " is running.");
                        return true;
                    }
                }

                Debug.Log("AppStatus: " + packageName + " is not running.");
                return false;
            }

            /// <summary>
            /// 显示Toast通知
            /// </summary>
            /// <param name="content"></param>
            public void ShowToast(string content)
            {
                Debug.Log("Toast: " + content);

                AndroidJavaClass jT = new AndroidJavaClass("android.widget.Toast");
                AndroidJavaObject jMsg = new AndroidJavaObject("java.lang.String", content);
                AndroidJavaObject jC = unityActivity.Call<AndroidJavaObject>("getApplicationContext");
                int length = jT.GetStatic<int>("LENGTH_SHORT");
                AndroidJavaObject toast = jT.CallStatic<AndroidJavaObject>("makeText", jC, jMsg, length);
                toast.Call("show");
            }

            /// <summary>
            /// 发送Intent到目标应用，如果packageName为空则发给当前最上层应用，extraData为键值对，必须成对输入，且key和value都必须不为空
            /// </summary>
            /// <param name="action"></param>
            /// <param name="packageName">指定要发送给哪个应用。如果为空的话表示发给当前在前台的应用（需要系统权限以及ActivityUtils.java）</param>
            /// <param name="extraData">键值对，必须成对输入，且key和value都必须不为空</param>
            public void SendIntent(string action, string packageName, params object[] extraData)
            {
                if (Application.isEditor)
                {
                    return;
                }

                intentObject = new AndroidJavaObject("android.content.Intent");
                intentObject.Call<AndroidJavaObject>("setAction", action);
                if (extraData.Length > 0 && extraData.Length % 2 == 0)
                {
                    for (int i = 0; i < extraData.Length; i += 2)
                    {
                        //string key = extraData[i];
                        //string value = string.IsNullOrEmpty(extraData[i + 1]) ? "null" : extraData[i + 1];
                        //Debug.Log("SendIntent: " + key + ", " + value);
                        intentObject.Call<AndroidJavaObject>("putExtra", extraData[i], extraData[i + 1]);
                    }
                }

                if (string.IsNullOrEmpty(packageName))
                {
                    AndroidJavaClass activityUtils = new AndroidJavaClass("com.unity3d.player.ActivityUtils");
                    string[] getForegroundActivity = activityUtils.CallStatic<string[]>("getForegroundActivity", unityActivity);

                    // 输出字符串数组
                    Debug.Log("getForegroundActivity: " + string.Join(", ", getForegroundActivity));

                    packageName = getForegroundActivity[0];
                }

                if (!string.IsNullOrEmpty(packageName))
                {
                    // 设置要发送广播的包名
                    intentObject.Call<AndroidJavaObject>("setPackage", packageName);

                    string permissionName = "com.ezxr.glass.permission.SYSTEM_MENU_NOTIFICATION";
                    // 检查是否已经声明了自定义权限
                    int permissionStatus = packageManagerObject.Call<int>("checkPermission", permissionName, unityActivity.Call<string>("getPackageName"));
                    if (permissionStatus == 0)
                    {
                        //Debug.Log("MyBroadcastReceiver, permission get: " + packageName + ", " + extraData[0]);
                        applicationContext.Call("sendBroadcast", intentObject, permissionName);
                    }
                    else
                    {
                        // 没有声明权限，不发送广播
                        Debug.LogError("MyBroadcastReceiver, No permission to send broadcast");
                    }
                }
            }

            /// <summary>
            /// 得到最顶层且正在交互的应用的包名
            /// </summary>
            /// <returns></returns>
            public string GetTopMostInteractingApp()
            {
                AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
                AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject unityActivity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");

                AndroidJavaClass activityUtils = new AndroidJavaClass("com.unity3d.player.ActivityUtils");
                Debug.Log("getForegroundActivity: 开始");
                string[] getForegroundActivity = activityUtils.CallStatic<string[]>("getForegroundActivity", unityActivity);

                // 输出字符串数组
                Debug.Log("getForegroundActivity: " + string.Join(", ", getForegroundActivity));

                string packageName = getForegroundActivity[0];

                return packageName;
            }

            /// <summary>
            /// 得到最顶层应用的包名（不考虑是否当前获得焦点与否，可以获得类似SystemUI在内的顶层应用的包名，得到的结果类似：adb shell dumpsys activity activities获取的mFocusedApp）
            /// </summary>
            /// <returns></returns>
            public string GetTopMostApp()
            {
                // 创建一个 Intent 对象
                AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

                // 获取 UnityPlayer 类
                AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

                // 获取当前 Activity
                AndroidJavaObject unityActivity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");

                // 获取 ActivityManager 对象
                AndroidJavaClass activityManagerClass = new AndroidJavaClass("android.app.ActivityManager");

                // 获取 Context.ACTIVITY_SERVICE 常量值
                string activityService = unityActivity.GetStatic<string>("ACTIVITY_SERVICE");

                // 获取系统服务
                AndroidJavaObject activityManager = unityActivity.Call<AndroidJavaObject>("getSystemService", activityService);

                // 获取正在运行的任务列表
                AndroidJavaObject taskList = activityManager.Call<AndroidJavaObject>("getRunningTasks", 1);

                // 获取任务列表中的第一个任务
                AndroidJavaObject taskInfo = taskList.Call<AndroidJavaObject>("get", 0);

                // 获取任务信息中的顶部 Activity
                AndroidJavaObject topActivity = taskInfo.Get<AndroidJavaObject>("topActivity");

                // 获取顶部 Activity 的包名
                string packageName = topActivity.Call<string>("getPackageName");

                return packageName;
            }

            /// <summary>
            /// 强制杀掉最顶层应用（需要系统权限）
            /// </summary>
            public void KillTopMostApp()
            {
                string packageName = GetTopMostApp();

                KillApp(packageName);
            }

            /// <summary>
            /// 强制杀掉指定包名的应用
            /// </summary>
            /// <param name="packageName"></param>
            public void KillApp(string packageName)
            {
                if (packageName != "com.ezxr.glass.arlauncher")
                {
                    Debug.Log("getForegroundActivity: KillApp: " + packageName);
                    AndroidJavaClass activityManagerClass = new AndroidJavaClass("android.app.ActivityManager");
                    AndroidJavaObject activityManager = unityActivity.Call<AndroidJavaObject>("getSystemService", "activity");
                    activityManager.Call("forceStopPackage", packageName);
                }
            }

            /// <summary>
            /// 获取另外一个应用中的files目录下的文件（需要和此应用签名一致）
            /// </summary>
            /// <param name="packageName">应用包名</param>
            /// <param name="filePath">文件相对路径（相对于files目录的路径）</param>
            /// <returns></returns>
            public string GetFilePath(string packageName, string filePath)
            {
                // 获取当前应用的 Context
                AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject currentActivityObject = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaObject applicationContextObject = currentActivityObject.Call<AndroidJavaObject>("getApplicationContext");

                string fileFullPath = "";

                try
                {
                    // 获取另一个应用的 Context
                    AndroidJavaObject otherContextObject = applicationContextObject.Call<AndroidJavaObject>("createPackageContext", packageName, 0);

                    // 访问另一个应用的私有目录中的文件
                    AndroidJavaObject filesDirObject = otherContextObject.Call<AndroidJavaObject>("getExternalFilesDir", "");//getFilesDir
                    fileFullPath = Path.Combine(filesDirObject.Call<string>("getAbsolutePath"), filePath);
                }
                catch (AndroidJavaException e)
                {
                    // 处理异常
                    Debug.LogError("GetFilePath Failed: " + e.ToString());
                }
                return fileFullPath;
            }

            /// <summary>
            /// 得到指定包名的VersionCode
            /// </summary>
            /// <param name="packageName"></param>
            /// <returns></returns>
            public long GetVersionCode(string packageName)
            {
                long versionCode = -1;

                if (!Application.isEditor)
                {
                    try
                    {
                        AndroidJavaObject packageInfo = packageManagerObject.Call<AndroidJavaObject>("getPackageInfo", packageName, 0);

                        if (packageInfo != null)
                        {
                            versionCode = packageInfo.Call<long>("getLongVersionCode");
                            Debug.Log(packageName + ", Versioncode is: " + versionCode);
                        }
                        else
                        {
                            Debug.Log(packageName + ", Versioncode is: not exists");
                        }
                    }
                    catch (AndroidJavaException e)
                    {
                        Debug.LogError("Failed to get versioncode for package: " + packageName);
                        Debug.LogError(e.Message);
                    }
                }

                return versionCode;
            }
        }
    }
}