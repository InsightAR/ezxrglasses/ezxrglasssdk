package com.unity3d.player;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.content.pm.PackageManager;

import java.util.HashMap;

interface IBroadcastReceiver {
    void onReceive(Bundle message);
}

public class UnityBroadcastReceiver extends BroadcastReceiver {

    public HashMap<String, IBroadcastReceiver> broadcastReceiverMap = new HashMap<>();
    private HashMap<String, Bundle> dataMap = new HashMap<>();

    public void SetBroadcastReceiver(String action, IBroadcastReceiver broadcastReceiver) {
        broadcastReceiverMap.put(action, broadcastReceiver);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        //Log.d("UnityBroadcastReceiver", "Received message: before");

        // 检查是否已经声明了自定义权限
        String permissionName = "com.ezxr.glass.permission.SYSTEM_MENU_NOTIFICATION";
        int permissionStatus = context.checkCallingOrSelfPermission(permissionName);
        if (permissionStatus == PackageManager.PERMISSION_GRANTED) {
            // 已经声明了权限，可以继续执行操作
            //Log.d("UnityBroadcastReceiver", "permissionStatus PERMISSION_GRANTED");

            String action = intent.getAction();
            if (action != null) {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    IBroadcastReceiver broadcastReceiver = broadcastReceiverMap.get(action);
                    if (broadcastReceiver != null) {
                        broadcastReceiver.onReceive(extras);
                    }
                }
            }

            // // 获取广播消息的内容
            // String message = intent.getStringExtra("WristPanel_Touched");

            // // 使用Log类输出收到的广播内容
            // Log.d("UnityBroadcastReceiver", "Received message: " + message);

            // broadcastReceiver.onReceive(message);

            // Log.d("UnityBroadcastReceiver", "After Call to unity: " + message);
        } else {
            // 没有声明权限，不执行操作
            Log.e("UnityBroadcastReceiver", "No permission to send broadcast");
        }

    }
}