using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidBroadcastInterface : AndroidJavaProxy
{
    public Action<AndroidJavaObject> OnReceiveCallback;

    public AndroidBroadcastInterface() : base("com.unity3d.player.IBroadcastReceiver")
    {

    }

    public void onReceive(AndroidJavaObject bundle)
    {
        if (OnReceiveCallback != null)
        {
            OnReceiveCallback(bundle);
        }
    }
}
