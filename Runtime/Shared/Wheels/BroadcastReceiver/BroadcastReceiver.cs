﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    public class BroadcastReceiver : MonoBehaviour
    {
        private static BroadcastReceiver instance;
        public static BroadcastReceiver Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GameObject("BroadcastReceiver").AddComponent<BroadcastReceiver>();
                }
                return instance;
            }
        }

        public enum DataType
        {
            String,
            Int
        }

        Dictionary<string, Action<string>> onReceiveDic = new Dictionary<string, Action<string>>();
        Dictionary<string, DataType> onReceiveTypeDic = new Dictionary<string, DataType>();
        Dictionary<string, bool> onReceiveThreadsafeDic = new Dictionary<string, bool>();
        Queue<KeyValuePair<string, string>> onReceiveQueue = new Queue<KeyValuePair<string, string>>();

        private static AndroidJavaObject m_UnityActivity;
        private static AndroidJavaObject m_Receiver;

        bool isPause;

        private void OnApplicationPause(bool pause)
        {
            //为了避免进入待机时收到的广播在Resume的时候被消费，需要在Pause和Resume的时候清理收到的广播的队列
            isPause = pause;
            Debug.Log("clear queue");
            onReceiveQueue.Clear();
        }

        private void Update()
        {
            if (onReceiveQueue.Count > 0)
            {
                KeyValuePair<string, string> keyValuePair = onReceiveQueue.Dequeue();
                if (onReceiveDic.ContainsKey(keyValuePair.Key))
                {
                    onReceiveDic[keyValuePair.Key]?.Invoke(keyValuePair.Value);
                }
            }
        }

        /// <summary>
        /// 创建一个UnityBroadcastReceiver实例，用于从特定广播action获得数据
        /// </summary>
        /// <param name="actions">要检索的广播action，多个action之间使用 "," 分隔</param>
        public void Register(params string[] actions)
        {
            if (!Application.isEditor)
            {
                m_UnityActivity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
                m_Receiver = new AndroidJavaObject("com.unity3d.player.UnityBroadcastReceiver");

                AndroidBroadcastInterface androidBroadcastInterface = new AndroidBroadcastInterface();
                androidBroadcastInterface.OnReceiveCallback += OnReceive;

                Debug.Log("UnityBroadcastReceiver unity Start action: " + actions);

                AndroidJavaObject intentFilter = new AndroidJavaObject("android.content.IntentFilter");

                foreach (string action in actions)
                {
                    intentFilter.Call("addAction", action);
                    m_Receiver.Call("SetBroadcastReceiver", action, androidBroadcastInterface);
                }

                string permissionName = "com.ezxr.glass.permission.SYSTEM_MENU_NOTIFICATION";//这个权限专门用于获得系统菜单悬浮窗APP发来的广播，定义在系统菜单悬浮窗APP的manifest中
                AndroidJavaObject registerReceiver = m_UnityActivity.Call<AndroidJavaObject>("registerReceiver", m_Receiver, intentFilter, permissionName, null);
            }
        }

        /// <summary>
        /// 创建一个BroadcastReceiver实例，用于从特定广播action获得数据
        /// </summary>
        /// <param name="actions">要检索的广播action，多个action之间使用 "," 分隔</param>
        /// <param name="key">用于在action数据包中取数据用的key</param>
        /// <param name="onReceiveCallback">如果bundle中存在key，则将Value从此处回调</param>
        public void Register(string[] actions, string key, Action<string> onReceiveCallback, DataType dataType)
        {
            Register(actions);
            RegisterCallback(key, onReceiveCallback, dataType);
        }

        public void Register(string[] actions, Dictionary<string, Action<string>> dic, DataType dataType)
        {
            Register(actions);
            foreach (KeyValuePair<string, Action<string>> item in dic)
            {
                RegisterCallback(item.Key, item.Value, dataType);
            }
        }

        void OnReceive(AndroidJavaObject bundle)
        {
            if (!isPause)
            {
                //Debug.Log("UnityBroadcastReceiver unity onReceive Received message from Android before");
                foreach (KeyValuePair<string, Action<string>> pair in onReceiveDic)
                {
                    if (bundle.Call<bool>("containsKey", pair.Key))
                    {
                        if (onReceiveTypeDic[pair.Key] == DataType.String)
                        {
                            string value = bundle.Call<string>("getString", pair.Key);
                            if (pair.Value != null && !string.IsNullOrEmpty(value))
                            {
                                //Debug.Log("UnityBroadcastReceiver unity onReceive Received message from Android: " + pair.Key + ", " + value);

                                if (onReceiveThreadsafeDic[pair.Key])
                                {
                                    onReceiveQueue.Enqueue(new KeyValuePair<string, string>(pair.Key, value));
                                }
                                else
                                {
                                    pair.Value?.Invoke(value);
                                }
                            }
                        }
                        else if (onReceiveTypeDic[pair.Key] == DataType.Int)
                        {
                            string value = bundle.Call<int>("getInt", pair.Key).ToString();
                            //Debug.Log("UnityBroadcastReceiver unity onReceive Received message from Android Int: " + pair.Key + ", " + value);

                            if (onReceiveThreadsafeDic[pair.Key])
                            {
                                onReceiveQueue.Enqueue(new KeyValuePair<string, string>(pair.Key, value));
                            }
                            else
                            {
                                pair.Value?.Invoke(value);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 每个Action收到的数据是一个Bundle，通过此函数可设置检索的key以及回调，当bundle中存在对应key的时候将回调（回调为主线程安全）
        /// </summary>
        /// <param name="key"></param>
        /// <param name="onReceiveCallback"></param>
        /// <param name="dataType"></param>
        /// <param name="threadSafe">是否转为线程安全之后回调onReceiveCallback，默认为true。如果要接收进入待机或者熄屏时的广播请将此置为false</param>
        public void RegisterCallback(string key, Action<string> onReceiveCallback, DataType dataType, bool threadSafe = true)
        {
            if (onReceiveDic.ContainsKey(key))
            {
                onReceiveDic[key] = onReceiveCallback;
                onReceiveTypeDic[key] = dataType;
                onReceiveThreadsafeDic[key] = threadSafe;
            }
            else
            {
                onReceiveDic.Add(key, onReceiveCallback);
                onReceiveTypeDic.Add(key, dataType);
                onReceiveThreadsafeDic.Add(key, threadSafe);
            }
        }

        public void UnregisterCallback(string key)
        {
            if (onReceiveDic.ContainsKey(key))
            {
                onReceiveDic.Remove(key);
            }
        }

        public void UnregisterAllCallback()
        {
            foreach (KeyValuePair<string, Action<string>> pair in onReceiveDic)
            {
                onReceiveDic.Remove(pair.Key);
            }
        }
    }
}