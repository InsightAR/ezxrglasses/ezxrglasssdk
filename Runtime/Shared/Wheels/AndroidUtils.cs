﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    public class AndroidUtils : MonoBehaviour
    {
        static AndroidJavaObject intent;
        /// <summary>
        /// 如果要打开的是当前Activity，则直接通过此回调传递参数
        /// </summary>
        public static Action<string[]> SameActivityDirectCallback;

        /// <summary>
        /// 得到此Android应用被外部打开时传入的参数
        /// </summary>
        /// <param name="paramName"></param>
        /// <returns></returns>
        public static string GetActivityStartParams(string paramName)
        {
            AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            intent = /*intent ??*/ currentActivity.Call<AndroidJavaObject>("getIntent");
            bool hasExtra = intent.Call<bool>("hasExtra", paramName);
            Debug.Log("AndroidUtils--> GetActivityStartParams: hasExtra: " + hasExtra);
            Debug.Log("AndroidUtils--> getStringExtra: " + intent.Call<string>("getStringExtra", paramName));
            if (hasExtra)
            {
                //AndroidJavaObject extras = intent.Call<AndroidJavaObject>("getExtras");
                return intent.Call<string>("getStringExtra", paramName);
            }
            return "";
        }

        /// <summary>
        /// 打开指定包名的Activity
        /// </summary>
        /// <param name="pkgName">包名</param>
        /// <param name="extraData">要传递的参数，需要成对传入，按一个key后跟一个value的形式</param>
        public static void StartActivity(string pkgName, params string[] extraData)
        {
            if(Application.platform != RuntimePlatform.Android)
            {
                return;
            }

            if (Application.identifier == pkgName)
            {
                if (SameActivityDirectCallback != null)
                {
                    SameActivityDirectCallback(extraData);
                }
            }
            else
            {
                AndroidJavaClass jcPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject joActivity = jcPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaObject joPackageManager = joActivity.Call<AndroidJavaObject>("getPackageManager");
                AndroidJavaObject joIntent = joPackageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", pkgName);
                if (null != joIntent)
                {
                    AndroidJavaObject joNIntent = joIntent.Call<AndroidJavaObject>("addFlags", joIntent.GetStatic<int>("FLAG_ACTIVITY_REORDER_TO_FRONT"));
                    if (extraData.Length > 0 && extraData.Length % 2 == 0)
                    {
                        for (int i = 0; i < extraData.Length; i += 2)
                        {
                            joIntent.Call<AndroidJavaObject>("putExtra", extraData[i], extraData[i + 1]);
                        }
                    }
                    joActivity.Call("startActivity", joNIntent);
                    joIntent.Dispose();
                }
                else
                {
                    string msg = "Package <" + pkgName + "> not exist on device.";
                    Debug.Log(msg);

                    ShowToast(msg);
                }
            }
        }

        /// <summary>
        /// 显示Toast通知
        /// </summary>
        /// <param name="content"></param>
        public static void ShowToast(string content)
        {
            Debug.Log(content);

            AndroidJavaClass jcPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject joActivity = jcPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaClass jT = new AndroidJavaClass("android.widget.Toast");
            AndroidJavaObject jMsg = new AndroidJavaObject("java.lang.String", content);
            AndroidJavaObject jC = joActivity.Call<AndroidJavaObject>("getApplicationContext");
            int length = jT.GetStatic<int>("LENGTH_SHORT");
            AndroidJavaObject toast = jT.CallStatic<AndroidJavaObject>("makeText", jC, jMsg, length);
            toast.Call("show");
        }
    }
}