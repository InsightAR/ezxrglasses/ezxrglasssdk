using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tab : MonoBehaviour
{
    public event Action<int> OnTabClicked;
    RectTransform[] options;
    RectTransform optionBG;
    /// <summary>
    /// 由于options = transform.Find("Options").GetComponentsInChildren<RectTransform>();得到的数组始终包括Options本身，所以此id从1开始计算
    /// </summary>
    int curOptionID = 1;

    bool interaction = true;

    // Start is called before the first frame update
    void Start()
    {
        options = transform.Find("Options").GetComponentsInChildren<RectTransform>();
        optionBG = transform.Find("OptionBG").GetComponent<RectTransform>();
        GetComponent<Button>().onClick.AddListener(() => Internal_OnTabClicked());
    }

    // Update is called once per frame
    void Update()
    {

    }

    public bool Interaction
    {
        get { return interaction; }
        set { interaction = value; }
    }

    public void Internal_OnTabClicked()
    {
        if (interaction)
        {
            curOptionID++;
            if (curOptionID >= options.Length)
            {
                curOptionID = 1;
            }
            optionBG.anchoredPosition = options[curOptionID].anchoredPosition;

            if (OnTabClicked != null)
            {
                OnTabClicked(curOptionID - 1);
            }
        }
    }
}
