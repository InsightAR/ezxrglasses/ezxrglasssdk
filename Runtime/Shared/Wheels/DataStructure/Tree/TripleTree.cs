using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TripleTreeNode<T1, T2, T3>
{
    public T1 Value0;
    public T2 Value1;
    public T3 Value2;

    public TripleTreeNode(T1 value0, T2 value1, T3 value2)
    {
        Value0 = value0;
        Value1 = value1;
        Value2 = value2;
    }
}

[Serializable]
public class TripleTree<T0, T1, T2, T3>
{
    public List<T0> keys = new List<T0>();
    public List<TripleTreeNode<T1, T2, T3>> values = new List<TripleTreeNode<T1, T2, T3>>();

    public TripleTreeNode<T1, T2, T3> this[T0 key]
    {
        get
        {
            int index = keys.IndexOf(key);
            if (index == -1)
            {
                throw new KeyNotFoundException("The key was not found in the dictionary.");
            }
            return values[index];
        }
    }

    public TripleTreeNode<T1, T2, T3> this[int index]
    {
        get
        {
            if (index < 0 || index >= keys.Count)
            {
                throw new IndexOutOfRangeException("Index is out of range.");
            }
            return values[index];
        }
    }

    public bool ContainsKey(T0 key)
    {
        return keys.Contains(key);
    }

    public void Add(T0 key, T1 value0, T2 value1, T3 value2)
    {
        if (!ContainsKey(key))
        {
            keys.Add(key);
            values.Add(new TripleTreeNode<T1, T2, T3>(value0, value1, value2));
        }
        else
        {
            throw new ArgumentException("An item with the same key has already been added.");
        }
    }

    public void Insert(int index, T0 key, T1 value0, T2 value1, T3 value2)
    {
        keys.Insert(index, key);
        values.Insert(index, new TripleTreeNode<T1, T2, T3>(value0, value1, value2));
    }

    public void Remove(T0 key)
    {
        int index = keys.IndexOf(key);
        if (index != -1)
        {
            keys.RemoveAt(index);
            values.RemoveAt(index);
        }
    }

    public void RemoveAt(int index)
    {
        if (index < 0 || index >= keys.Count)
        {
            throw new ArgumentOutOfRangeException(nameof(index), "Index is out of range.");
        }
        keys.RemoveAt(index);
        values.RemoveAt(index);
    }

    public void Clear()
    {
        keys.Clear();
        values.Clear();
    }

    public int Count
    {
        get { return keys.Count; }
    }

    public List<T0> Keys
    {
        get { return keys; }
    }

    public List<TripleTreeNode<T1, T2, T3>> Values
    {
        get { return values; }
    }
}

