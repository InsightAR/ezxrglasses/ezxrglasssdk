using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SerializedDictionary<T0, T1>
{
    public List<T0> keys = new List<T0>();
    public List<T1> values = new List<T1>();

    public T1 this[T0 key]
    {
        get
        {
            int index = keys.IndexOf(key);
            if (index == -1)
            {
                throw new KeyNotFoundException();
            }
            return values[index];
        }
        set
        {
            int index = keys.IndexOf(key);
            if (index == -1)
            {
                keys.Add(key);
                values.Add(value);
            }
            else
            {
                values[index] = value;
            }
        }
    }

    public T1 this[int index]
    {
        get
        {
            if (index < 0 || index >= keys.Count)
            {
                throw new IndexOutOfRangeException("Index is out of range.");
            }
            return values[index];
        }
        set
        {
            if (index < 0 || index >= keys.Count)
            {
                throw new IndexOutOfRangeException("Index is out of range.");
            }
            values[index] = value;
        }
    }

    public bool ContainsKey(T0 key)
    {
        return keys.Contains(key);
    }

    public void Add(T0 key, T1 value)
    {
        if (!ContainsKey(key))
        {
            keys.Add(key);
            values.Add(value);
        }
        else
        {
            throw new ArgumentException("An item with the same key has already been added.");
        }
    }

    public void Insert(int index, T0 key, T1 value)
    {
        keys.Insert(index, key);
        values.Insert(index, value);
    }

    public void Remove(T0 key)
    {
        int index = keys.IndexOf(key);
        if (index != -1)
        {
            keys.RemoveAt(index);
            values.RemoveAt(index);
        }
    }

    public void RemoveAt(int index)
    {
        if (index < 0 || index >= keys.Count)
        {
            throw new ArgumentOutOfRangeException(nameof(index), "Index is out of range.");
        }
        keys.RemoveAt(index);
        values.RemoveAt(index);
    }

    public void Clear()
    {
        keys.Clear();
        values.Clear();
    }

    public int Count
    {
        get { return keys.Count; }
    }

    public List<T0> Keys
    {
        get { return keys; }
        set { keys = value; }
    }

    public List<T1> Values
    {
        get { return values; }
    }
}
