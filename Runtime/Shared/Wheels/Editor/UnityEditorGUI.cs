﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class UnityEditorGUI : MonoBehaviour
{
    public static void DrawLine(Color color = default, int thickness = 1, int padding = 10, int margin = 0)
    {
        color = color != default ? color : Color.grey;
        Rect r = EditorGUILayout.GetControlRect(false, GUILayout.Height(padding + thickness));
        r.height = thickness;
        r.y += padding * 0.5f;

        if (margin < 0)
        {
            r.x = 0;
            r.width = EditorGUIUtility.currentViewWidth;
        }
        else if (margin > 0)
        {
            // shrink line width
            r.x += margin;
            r.width -= margin * 2;
        }

        EditorGUI.DrawRect(r, color);
    }
}
