﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wheels.Unity
{
    public class TransformExtensionsHandler_RotateTo : MonoBehaviour
    {
        public Quaternion targetRotation;
        public float duration;
        public Action callBack;
        public Space space;
        /// <summary>
        /// 相机在做Lerp前的旋转信息
        /// </summary>
        Quaternion startRotation;
        /// <summary>
        /// Lerp用的计时器，用于将相机Lerp移动到指定观察点位，同时Lerp旋转与指定观察点位一致
        /// </summary>
        float timer;
        /// <summary>
        /// 是否开始Lerp
        /// </summary>
        bool start;

        public void Init(Quaternion targetRotation, float duration, Space space = Space.Self, Action callBack = null)
        {
            if (space == Space.Self)
            {
                startRotation = transform.localRotation;
            }
            else
            {
                startRotation = transform.rotation;
            }

            this.targetRotation = targetRotation;
            this.duration = duration;
            this.callBack = callBack;
            this.space = space;
            start = true;
            timer = 0;
        }

        // Update is called once per frame
        void LateUpdate()
        {
            if (start)
            {
                timer += (1.0f / duration * Time.deltaTime);
                if (space == Space.Self)
                {
                    transform.localRotation = Quaternion.Lerp(startRotation, targetRotation, timer);
                }
                else
                {
                    transform.rotation = Quaternion.Lerp(startRotation, targetRotation, timer);
                }
                if (timer >= 1.0f)
                {
                    if (space == Space.Self)
                    {
                        transform.localRotation = Quaternion.Lerp(startRotation, targetRotation, timer);
                    }
                    else
                    {
                        transform.rotation = Quaternion.Lerp(startRotation, targetRotation, timer);
                    }
                    start = false;
                    timer = 0;
                    if (callBack != null)
                    {
                        callBack();
                    }
                }
            }
        }


    }
}