#if TMP_PRESENT
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using EZXR.Glass.Runtime;

[AddComponentMenu("UI/LocalizedTextMeshPro - Text (UI)", 11)]
public class LocalizedTextMeshProUGUI : TextMeshProUGUI
{
    /// <summary>
    /// 对应翻译资源中的keys，默认请保持和text一致
    /// </summary>
    public string key;

    protected override void Awake()
    {
        base.Awake();
        if (Application.isPlaying)
        {
            key = base.text;
            //Debug.Log("KeyBefore: " + key + ", name: " + transform.parent.name);

            LocalizedManager.OnSystemLanguageChanged += SystemManager_OnSystemLanguageChanged;
#if UNITY_EDITOR
            LocalizedManager.AddToDic(key);
#endif
        }
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        if (Application.isPlaying)
        {
            GetLocalizedText();
        }
    }

    protected override void Start()
    {
        base.Start();
        if (Application.isPlaying)
        {
            if (LocalizedManager.Current == null || !LocalizedManager.Current.LocalizeExist(key))
            {
                key = "";
            }
        }
    }

    private void SystemManager_OnSystemLanguageChanged(string language)
    {
        Debug.Log("SystemManager_OnSystemLanguageChanged: " + language);
        GetLocalizedText();
    }

    void GetLocalizedText()
    {
        if (LocalizedManager.Current == null)
        {
            return;
        }
        string localizedText = LocalizedManager.Current.GetLocalizedText(key);
        if (!string.IsNullOrEmpty(localizedText))
        {
            base.text = localizedText;
        }
        //Debug.Log("font Changed: " + LocalizedManager.Current.Font.name);
        //无论是否有对应的语言都要切换字体，像从网络拉下来的文字或者用户手动输入的文字都无法从字体配置文件中找到对应的翻译
        font = LocalizedManager.Current.Font;
    }

    public void SetTextAndKey(string value)
    {
        base.text = value;
        key = value;
        GetLocalizedText();
    }

    public override string text
    {
        get
        {
            return base.text;
        }
        set
        {
            base.text = value;
            key = value;
            GetLocalizedText();
        }
    }
}
#endif