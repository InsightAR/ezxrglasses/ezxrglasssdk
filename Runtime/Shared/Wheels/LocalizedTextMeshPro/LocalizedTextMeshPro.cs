#if TMP_PRESENT
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using EZXR.Glass.Runtime;

[AddComponentMenu("Mesh/LocalizedTextMeshPro - Text")]
public class LocalizedTextMeshPro : TextMeshPro
{
    /// <summary>
    /// 对应翻译资源中的keys，默认请保持和text一致
    /// </summary>
    public string key;

    protected override void Awake()
    {
        base.Awake();
        if (Application.isPlaying)
        {
            key = text;
            //Debug.Log("KeyBefore: " + key + ", name: " + transform.parent.name);

            LocalizedManager.OnSystemLanguageChanged += SystemManager_OnSystemLanguageChanged;
#if UNITY_EDITOR
            LocalizedManager.AddToDic(key);
#endif
        }
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        if (Application.isPlaying)
        {
            GetLocalizedText();
        }
    }

    protected override void Start()
    {
        base.Start();
        if (Application.isPlaying)
        {
            if (!LocalizedManager.Current.LocalizeExist(key))
            {
                key = "";
            }
        }
    }

    private void SystemManager_OnSystemLanguageChanged(string language)
    {
        GetLocalizedText();
    }

    void GetLocalizedText()
    {
        string localizedText = LocalizedManager.Current.GetLocalizedText(key);
        if (!string.IsNullOrEmpty(localizedText))
        {
            text = localizedText;
        }
        //无论是否有对应的语言都要切换字体，像从网络拉下来的文字或者用户手动输入的文字都无法从字体配置文件中找到对应的翻译
        font = LocalizedManager.Current.Font;
    }

    public void SetTextAndKey(string value)
    {
        text = value;
        key = value;
        GetLocalizedText();
    }
}
#endif