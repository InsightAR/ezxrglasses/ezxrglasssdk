#if TMP_PRESENT
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEditor;
using TMPro.EditorUtilities;

[CustomEditor(typeof(LocalizedTextMeshPro), true), CanEditMultipleObjects]
public class CustomTMPInspector : TMP_EditorPanel
{
    LocalizedTextMeshPro localizedTextMeshPro;
    SerializedProperty text;
    protected override void OnEnable()
    {
        base.OnEnable();

        localizedTextMeshPro = target as LocalizedTextMeshPro;

        text = serializedObject.FindProperty("key");
        //if (string.IsNullOrEmpty(localizedTextMeshPro.key))
        //{
        //    localizedTextMeshPro.key = m_TextProp.stringValue;
        //}
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        //EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(text);
        //if (EditorGUI.EndChangeCheck())
        //{
        //    m_TextProp.stringValue = text.stringValue;
        //}
        //if (string.IsNullOrEmpty(localizedTextMeshPro.key) && !string.IsNullOrEmpty(m_TextProp.stringValue))
        //{
        //    localizedTextMeshPro.key = m_TextProp.stringValue;
        //    EditorUtility.SetDirty(localizedTextMeshPro.gameObject);
        //}
        serializedObject.ApplyModifiedProperties();

        base.OnInspectorGUI();
    }
}
#endif