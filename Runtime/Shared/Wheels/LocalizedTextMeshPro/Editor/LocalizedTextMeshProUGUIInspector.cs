#if TMP_PRESENT
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEditor;
using TMPro.EditorUtilities;

[CustomEditor(typeof(LocalizedTextMeshProUGUI), true), CanEditMultipleObjects]
public class CustomTMPUIInspector : TMP_EditorPanelUI
{
    LocalizedTextMeshProUGUI localizedTextMeshProUGUI;
    SerializedProperty text;
    protected override void OnEnable()
    {
        base.OnEnable();

        localizedTextMeshProUGUI = target as LocalizedTextMeshProUGUI;

        text = serializedObject.FindProperty("key");
        //if (string.IsNullOrEmpty(localizedTextMeshProUGUI.key))
        //{
        //    localizedTextMeshProUGUI.key = m_TextProp.stringValue;
        //}
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        //EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(text);
        //if (EditorGUI.EndChangeCheck())
        //{
        //    m_TextProp.stringValue = text.stringValue;
        //}
        //if (string.IsNullOrEmpty(localizedTextMeshProUGUI.key) && !string.IsNullOrEmpty(m_TextProp.stringValue))
        //{
        //    localizedTextMeshProUGUI.key = m_TextProp.stringValue;
        //    EditorUtility.SetDirty(localizedTextMeshProUGUI.gameObject);
        //}
        serializedObject.ApplyModifiedProperties();

        base.OnInspectorGUI();
    }
}
#endif