﻿#if TMP_PRESENT
using EZXR.Glass.Inputs;
using EZXR.Glass.Runtime;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.Events;

[ScriptExecutionOrder(-100)]
public class LocalizedManager : MonoBehaviour
{
#region singleton
    private static LocalizedManager instance;
    public static LocalizedManager Current
    {
        get
        {
            return instance;
        }
    }
#endregion

    bool isInited;

    /// <summary>
    /// 支持的系统语言（key：语言；value0：语言配置文件完整路径；value1：字体文件
    /// </summary>
    public BinaryTree<string, TextAsset, TMP_FontAsset> fontAssets;
    /// <summary>
    /// 语言初始化完毕才可以获得curLanguage
    /// </summary>
    public static string curLanguage = "cn";
    /// <summary>
    /// 语言初始化完毕才可以去获得curLanguage
    /// </summary>
    public static bool languageInitialized = false;
    public TMP_FontAsset Font
    {
        get { return fontAssets[curLanguage].Value1; }
    }

    /// <summary>
    /// 系统语言发生变化
    /// </summary>
    public static event UnityAction<string> OnSystemLanguageChanged;
    static Dictionary<string, SerializedDictionary<string, string>> languageDic = new Dictionary<string, SerializedDictionary<string, string>>();

    static SerializedDictionary<string, string> baseDicForCollect = new SerializedDictionary<string, string>();

    public static void AddToDic(string key)
    {
        //Debug.Log("Key: " + key);
        baseDicForCollect[key] = key;
    }

#if UNITY_EDITOR
    [InitializeOnLoadMethod]
    public static void Initialize()
    {
        Debug.Log("Initialize language");
    }
#endif

    private void Awake()
    {
        instance = this;

        languageInitialized = false;

        curLanguage = instance.fontAssets.Keys[0];

        OSEventSystem.OnInstructionsReceived_LanguageReceived += LanguageRefreshed;

        if (!languageDic.ContainsKey(curLanguage))
        {
            SerializedDictionary<string, string> serializedDictionary = JsonUtility.FromJson<SerializedDictionary<string, string>>(fontAssets[curLanguage].Value0.text);
            languageDic.Add(curLanguage, serializedDictionary);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (!isInited)
        {
            isInited = true;
            Debug.Log("want language 0");
            Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "GetLanguage", "GetLanguage");
        }
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.G))
        {
            SerializedDictionary<string, string> dic0 = new SerializedDictionary<string, string>();
            dic0.Add("测试", "测试1");
            dic0.Add("设备信息", "设备信息1");
            dic0.Add("未连接", "未连接1");
            File.WriteAllText(Path.Combine(Application.dataPath, "Languages/cn.json"), JsonUtility.ToJson(dic0));

            dic0 = new SerializedDictionary<string, string>();
            dic0.Add("测试", "test");
            dic0.Add("设备信息", "DeviceInfo");
            dic0.Add("未连接", "NotConnected");
            File.WriteAllText(Path.Combine(Application.dataPath, "Languages/en.json"), JsonUtility.ToJson(dic0));
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            LanguageRefreshed("tc");
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            LanguageRefreshed("en");
        }

        ////用于从程序中保存所有文本
        //if (Input.GetKeyDown(KeyCode.S))
        //{
        //    File.WriteAllText(Path.Combine(Application.dataPath, "Languages/cn.json"), JsonUtility.ToJson(baseDicForCollect));
        //}
#endif
    }

    private void OnApplicationPause(bool pause)
    {
        if (!pause)
        {
            if (isInited)
            {
                Debug.Log("want language 1");
                Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "GetLanguage", "GetLanguage");
            }
        }
        else
        {
            languageInitialized = false;
        }
    }

    public void LanguageRefreshed(string language)
    {
        Debug.Log("LocalizedManager=> LanguageRefreshed: " + language);
        languageInitialized = true;
        if (curLanguage != language)
        {
            if (!languageDic.ContainsKey(language))
            {
                SerializedDictionary<string, string> serializedDictionary = JsonUtility.FromJson<SerializedDictionary<string, string>>(fontAssets[language].Value0.text);
                languageDic.Add(language, serializedDictionary);
            }

            if (languageDic.ContainsKey(language))
            {
                Debug.Log("LocalizedManager=> OnSystemLanguageChanged: " + language);
                curLanguage = language;
                if (OnSystemLanguageChanged != null)
                {
                    OnSystemLanguageChanged.Invoke(curLanguage);
                }
            }
            else
            {
                Debug.LogError("LocalizedManager=> LanguageRefreshed" + language + " not exists");
            }
        }
    }

    public bool LocalizeExist(string key)
    {
        if (!string.IsNullOrEmpty(key))
        {
            if (!languageDic.ContainsKey(curLanguage))
            {
                SerializedDictionary<string, string> serializedDictionary = JsonUtility.FromJson<SerializedDictionary<string, string>>(fontAssets[curLanguage].Value0.text);
                languageDic.Add(curLanguage, serializedDictionary);
            }

            if (languageDic.ContainsKey(curLanguage))
            {
                if (languageDic[curLanguage] != null && languageDic[curLanguage].ContainsKey(key))
                {
                    return true;
                }
            }
            else
            {
                Debug.LogWarning("LocalizedManager=> GetLocalizedText " + curLanguage + " not exists");
            }
        }

        return false;
    }

    public string GetLocalizedText(string key)
    {
        if (!string.IsNullOrEmpty(key))
        {
            if (!languageDic.ContainsKey(curLanguage))
            {
                SerializedDictionary<string, string> serializedDictionary = JsonUtility.FromJson<SerializedDictionary<string, string>>(fontAssets[curLanguage].Value0.text);
                languageDic.Add(curLanguage, serializedDictionary);
            }

            if (languageDic.ContainsKey(curLanguage))
            {
                if (languageDic[curLanguage] != null && languageDic[curLanguage].ContainsKey(key))
                {
                    return languageDic[curLanguage][key];
                }
            }
            else
            {
                Debug.LogWarning("LocalizedManager=> GetLocalizedText " + curLanguage + " not exists");
            }
        }

        return key;
    }
}
#endif