﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wheels.Unity
{
    public static class Vector3Extensions
    {
        public static Vector3 TransformPosition(this Pose p, Vector3 localPosition)
        {
            Matrix4x4 rotationMatrix = Matrix4x4.TRS(Vector3.zero, p.rotation, Vector3.one);
            return rotationMatrix.MultiplyPoint3x4(localPosition) + p.position;
        }

        public static Vector3 TransformDirection(this Pose p, Vector3 localDirection)
        {
            Matrix4x4 rotationMatrix = Matrix4x4.TRS(Vector3.zero, p.rotation, Vector3.one);
            return rotationMatrix.MultiplyPoint3x4(localDirection);
        }

        #region 坐标点在相机视野内
        public static bool InView(this Vector3 worldPoint, Camera camera)
        {
            if (camera == null)
            {
                Debug.LogWarning("Camera reference is null.");
                return false;
            }

            Vector3 viewportPos = camera.WorldToViewportPoint(worldPoint);

            return (viewportPos.x > 0 && viewportPos.x < 1 && viewportPos.y > 0 && viewportPos.y < 1 && viewportPos.z > 0);
        }

        public static bool InStereoView(this Vector3 worldPoint, Camera leftCamera, Camera rightCamera)
        {
            if (leftCamera == null)
            {
                Debug.LogWarning("LeftCamera reference is null.");
                return false;
            }

            if (rightCamera == null)
            {
                Debug.LogWarning("RightCamera reference is null.");
                return false;
            }

            Vector3 viewportPos_L = leftCamera.WorldToViewportPoint(worldPoint);
            Vector3 viewportPos_R = rightCamera.WorldToViewportPoint(worldPoint);

            return (viewportPos_L.x > 0 && viewportPos_L.x < 1 && viewportPos_L.y > 0 && viewportPos_L.y < 1 && viewportPos_L.z > 0) || (viewportPos_R.x > 0 && viewportPos_R.x < 1 && viewportPos_R.y > 0 && viewportPos_R.y < 1 && viewportPos_R.z > 0);
        }
        #endregion

        #region 计算两个向量的水平夹角和垂直夹角
        public static float HorizontalAngle(this Vector3 a, Vector3 b)
        {
            Vector3 aProject = new Vector3(a.x, 0, a.z).normalized;
            Vector3 bProject = new Vector3(b.x, 0, b.z).normalized;
            float dotProduct = Vector3.Dot(aProject, bProject);
            float thetaH = Mathf.Acos(dotProduct);
            return Mathf.Rad2Deg * thetaH;
        }

        public static float VerticalAngle(this Vector3 a, Vector3 b)
        {
            float dotA = Vector3.Dot(a.normalized, Vector3.up);
            float dotB = Vector3.Dot(b.normalized, Vector3.up);

            float acosA = Mathf.Acos(dotA);
            float acosB = Mathf.Acos(dotB);

            float angleA0 = Mathf.Rad2Deg * acosA;
            float angleB0 = Mathf.Rad2Deg * acosB;

            float angleA = 90 - angleA0;
            float angleB = 90 - angleB0;

            //Debug.Log("angleA: " + angleA);
            //Debug.Log("angleB: " + angleB);

            return Mathf.Abs(angleA - angleB);
        }
        #endregion
    }
}