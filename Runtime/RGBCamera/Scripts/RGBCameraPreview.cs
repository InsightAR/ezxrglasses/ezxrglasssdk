using EZXR.Glass.Runtime;
using EZXR.Glass.SixDof;
using System.Collections;
using System.IO;
using UnityEngine;

namespace EZXR.Glass.Device
{
    /// <summary>
    /// 用于绘制RGB背景，需绑定在Plane,Quad或其他具有MeshRenderer的GameObject上
    /// 采用YUVShader绘制背景，因此留意该shader是否添加到工程设置中（"Project Settings/Graphics"）
    /// RGB相机需要由外部管理者传入，也可参考RGBPreviewDemo自行创新新对象
    /// 启用反畸变可能会带来边缘显示重影
    /// </summary>
    public class RGBCameraPreview : MonoBehaviour
    {  
        private NormalRGBCameraDevice rgbCameraDevice;

        public bool enablePreview {
            set {
                enablePreviewing(value);
                _enablePreview = value;
            }
            get {
                return _enablePreview;
            }
        }
        private bool _enablePreview = true;
        public bool enableUndistortion
        {
            set
            {
                setUndistortion(value);
            }
        }

        private MeshRenderer quad;

        private Texture2D m_VideoTexture_Y;
        private Texture2D m_VideoTexture_UV;
        private Material _videoBackgroundMat;
        public bool isUndistortionSetted = false;

        private int[] previewSize = new int[2];

        private EZVIOInputImage m_CamImageBuffer;
        private float[] m_cameraIntrinsics = new float[8];

        private bool isSavingPreview = false;

        private bool isPrepared = false;

        // Start is called before the first frame update
        void Start()
        {
            quad = this.gameObject.GetComponent<MeshRenderer>();
            if (quad == null)
            {
                quad = this.gameObject.AddComponent<MeshRenderer>();
            }

            //InvokeRepeating("savePreviewPics", 0.5f, 0.5f);
        }
        private void OnEnable()
        {
            StartCoroutine(openRGBCamera());
        }
        private void OnDisable()
        {
            closeRGBCamera();
        }
        // Update is called once per frame
        void Update()
        {
            drawRgbCameraPreview();
        }

        public void savePreviewPics() {
            StartCoroutine(saveOnePic());
        }

        public void enablePreviewing(bool enable) {
            bool isCameraOpened = rgbCameraDevice == null ? false : rgbCameraDevice.IsStarted();
            if (enable)
            {
                if (!isCameraOpened) {
                    if (rgbCameraDevice == null) {
                        rgbCameraDevice = new NormalRGBCameraDevice();
                    }
                    Debug.Log("-20002- enablePreviewing true openRGB");
                    rgbCameraDevice.Open();
                }
            }
            else {
                if (isCameraOpened) {
                    Debug.Log("-20002- enablePreviewing false closeRGB");
                    rgbCameraDevice.Close();
                    rgbCameraDevice = null;
                }
            }
            quad.enabled = enable;
        }
        private IEnumerator saveOnePic() {
            if (m_VideoTexture_Y == null || rgbCameraDevice == null)
                yield return new WaitForEndOfFrame();
            if (isSavingPreview)
                yield return null;
            isSavingPreview = true;
            byte[] bufferData = m_VideoTexture_Y.EncodeToJPG();
            if (bufferData != null) {
                File.WriteAllBytes("/storage/emulated/0/download/previewImage.jpg", bufferData);
            }
            isSavingPreview = false;
        }

        private void drawRgbCameraPreview() {
            if (!_enablePreview)
                return;
            if (rgbCameraDevice == null)
                return;
            bool res = rgbCameraDevice.getCurrentImage(ref m_CamImageBuffer, m_cameraIntrinsics);
            if (!res)
            {
                return;
            }
            m_VideoTexture_Y.LoadRawTextureData(m_CamImageBuffer.fullImg, previewSize[0] * previewSize[1]);
            m_VideoTexture_Y.Apply();
            m_VideoTexture_UV.LoadRawTextureData(m_CamImageBuffer.fullImg + previewSize[0] * previewSize[1], previewSize[0] * previewSize[1] / 2);
            m_VideoTexture_UV.Apply();
        }

        private bool preparePreview()
        {
            if (isPrepared)
                return true;
            if (rgbCameraDevice == null)
                return false;

            quad = this.gameObject.GetComponent<MeshRenderer>();
            if (quad == null)
            {
                quad = this.gameObject.AddComponent<MeshRenderer>();
            }
            _videoBackgroundMat = new Material(Shader.Find("EZXR/YUV2RGB"));
            int[] imageSize = rgbCameraDevice.getCameraSize();
            m_VideoTexture_Y = new Texture2D(imageSize[0], imageSize[1], TextureFormat.R8, false);
            m_VideoTexture_UV = new Texture2D(imageSize[0] / 2, imageSize[1] / 2, TextureFormat.RG16, false);
            previewSize[0] = imageSize[0];
            previewSize[1] = imageSize[1];
            _videoBackgroundMat.SetTexture("_YTex", m_VideoTexture_Y);
            _videoBackgroundMat.SetTexture("_UVTex", m_VideoTexture_UV);
            _videoBackgroundMat.SetFloat("_TexWidth", previewSize[0]);
            _videoBackgroundMat.SetFloat("_TexHeight", previewSize[1]);
            m_CamImageBuffer = new EZVIOInputImage();
            quad.material = _videoBackgroundMat;
            isPrepared = true;
            return true;
        }

        private void setUndistortion(bool enable)
        {
            if (_videoBackgroundMat == null)
                return;
            if (enable)
            {
                _videoBackgroundMat.SetInt("_bDistortion", 1);
                _videoBackgroundMat.SetFloat("_Fx", m_cameraIntrinsics[0]);
                _videoBackgroundMat.SetFloat("_Fy", m_cameraIntrinsics[1]);
                _videoBackgroundMat.SetFloat("_Cx", m_cameraIntrinsics[2]);
                _videoBackgroundMat.SetFloat("_Cy", m_cameraIntrinsics[3]);
                _videoBackgroundMat.SetFloat("_K0", m_cameraIntrinsics[4]);
                _videoBackgroundMat.SetFloat("_K1", m_cameraIntrinsics[5]);
                _videoBackgroundMat.SetFloat("_K2", 0.0f);
                _videoBackgroundMat.SetFloat("_P0", m_cameraIntrinsics[6]);
                _videoBackgroundMat.SetFloat("_P1", m_cameraIntrinsics[7]);
            }
            else
            {
                _videoBackgroundMat.SetInt("_bDistortion", 0);
            }
        }

        private IEnumerator openRGBCamera() {
            rgbCameraDevice = new NormalRGBCameraDevice();
            yield return new WaitUntil(() => SessionManager.Instance != null && SessionManager.Instance.IsInited);
            rgbCameraDevice.Open();
            preparePreview();
        }
        private void closeRGBCamera()
        {
            if (rgbCameraDevice != null)
                rgbCameraDevice.Close();
            rgbCameraDevice = null;
        }
    }
}