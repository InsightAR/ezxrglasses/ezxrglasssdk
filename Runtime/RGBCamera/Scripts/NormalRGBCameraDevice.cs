﻿using EZXR.Glass.Runtime;
using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace EZXR.Glass.Device
{
    public enum CameraAE_Mode
    {
        AE_Auto,
        AE_ForMovingMode
    }
    public class NormalRGBCameraDevice
    {
      
        private bool hasCameraOpened = false;
        private int mCameraID = 0;//0:rgb,1:left grey FishEye,2:ToF
        public void Open()
        {
            if (hasCameraOpened)
                return;
            if (!Application.isEditor)
            {
                NativeApi.nativeOpenCamera(GetHashCode(), mCameraID);
            }
            hasCameraOpened = true;
        }

        public void Close()
        {
            if (!hasCameraOpened)
                return;
            hasCameraOpened = false;
            if (!Application.isEditor)
            {
                NativeApi.nativeCloseCamera(GetHashCode(), mCameraID);
            }
        }

        public bool IsStarted()
        {
            if (!Application.isEditor)
            {
                return NativeApi.nativeIsCameraStarted(mCameraID);
            }
            return false;
        }
        public bool getCurrentImage(ref EZVIOInputImage image, float[] intrinsic, EZVIOImageFormat specifiedFormat)
        {
            if (hasCameraOpened)
            {
                return NativeApi.nativeGetCurrentImageWithFormat(mCameraID, ref image, intrinsic, (int)specifiedFormat);
            }
            return false;
        }

        public bool getCurrentImage(ref EZVIOInputImage image, float[] intrinsic)
        {
            if (hasCameraOpened)
            {
                return NativeApi.nativeGetCurrentImage(mCameraID, ref image, intrinsic);
            }
            return false;
        }

        /// <summary>
        /// 获取RGB相机是否在剧烈运动
        /// </summary>
        /// <returns></returns>
        public bool isCameraMotionViolently()
        {
            if (hasCameraOpened)
            {
                return NativeApi.nativeIsCameraMotionViolently(mCameraID);
            }
            return false;
        }

        /// <summary>
        /// 获取RGB相机运动角速度
        /// </summary>
        /// <returns></returns>
        public float getCameraMotionAngle()
        {
            if (hasCameraOpened)
            {
                return NativeApi.nativegetCameraMotionAngle(mCameraID);
            }
            return 0.0f;
        }
        /// <summary>
        /// 绘制RGB背景，传入纹理上更新RGBA数据
        /// 但因为当前方法会有较多的数据处理计算
        /// 建议使用RGBCameraPreview中的方式绘制纹理
        /// 
        /// </summary>
        /// <param name="texturePtr"></param>
        public void DrawRGBVideoFrame(IntPtr texturePtr)
        {
            if (!Application.isEditor)
            {
                NativeApi.DrawRGBVideoFrame(texturePtr);
            }
        }

        public void setCameraAEMode(CameraAE_Mode ae_Mode) {
            if (!Application.isEditor)
            {
                NativeApi.nativeSetCameraAEMode(mCameraID,(int)ae_Mode);
            }
        }


        public int[] getCameraSize() {
            CameraResolution cameraResolution = new CameraResolution();
            NativeApi.getRGBCameraResolution(ref cameraResolution);
            return new int[] { cameraResolution.width, cameraResolution.height };
        }
        public void getRGBCameraIntrics(float[] intrinsic)
        {
            NativeApi.nativeGetRGBCameraIntrics(intrinsic);
        }

        private partial struct NativeApi
        {
#if !UNITY_EDITOR
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void nativeOpenCamera(int holderHandle, int CameraID);
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void nativeCloseCamera(int holderHandle, int CameraID);
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern bool nativeIsCameraStarted(int CameraID);

            [DllImport(NativeConsts.NativeLibrary)]
            public static extern bool nativeGetCurrentImage(int cameraId, ref EZVIOInputImage image, float[] intrinsic);

            [DllImport(NativeConsts.NativeLibrary)]
            public static extern bool nativeGetCurrentImageWithFormat(int cameraId, ref EZVIOInputImage image, float[] intrinsic, int outputFormat);

            [DllImport(NativeConsts.NativeLibrary)]
            public static extern bool nativeIsCameraMotionViolently(int cameraId);
            
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern float nativegetCameraMotionAngle(int cameraId);

            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void DrawRGBVideoFrame(IntPtr texturePtr);

            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void nativeSetCameraAEMode(int cameraId, int aeMode);

            [DllImport(NativeConsts.NativeLibrary)]
            public static extern bool getRGBCameraResolution(ref CameraResolution camera_res);
            
            [DllImport(NativeConsts.NativeLibrary)]
            public static extern void nativeGetRGBCameraIntrics(float[] intrinsic);
#else
            public static void nativeOpenCamera(int holderHandle, int CameraID) { }
            public static void nativeCloseCamera(int holderHandle, int CameraID) { }
            public static bool nativeIsCameraStarted(int CameraID) { return false; }


            public static bool nativeGetCurrentImage(int cameraId, ref EZVIOInputImage image, float[] intrinsic) { return false; }


            public static bool nativeGetCurrentImageWithFormat(int cameraId, ref EZVIOInputImage image, float[] intrinsic, int outputFormat) {
                return false;
            }


            public static bool nativeIsCameraMotionViolently(int cameraId) {
                return false;
            }

            public static float nativegetCameraMotionAngle(int cameraId) {
                return 0.0f;
            }

            public static void DrawRGBVideoFrame(IntPtr texturePtr) { }

            public static void nativeSetCameraAEMode(int cameraId, int aeMode) { }

            public static bool getRGBCameraResolution(ref CameraResolution camera_res) { return false; }

            public static void nativeGetRGBCameraIntrics(float[] intrinsic) { }
#endif
        }
    }
}
