Shader "EZXR/YUV2RGB"
{
Properties 
{
    _YTex ("Y channel texture", 2D) = "white" {}
    _UVTex ("UV channel texture", 2D) = "white" {}
    _bDistortion("bDistortion", INT) = 0
    _TexWidth ("texture width", Float) = 1280.0
    _TexHeight ("texture height", Float) = 720.0
    _Fx      ("Fx", Float) = 1043.130005
    _Fy      ("Fy", Float) = 1038.619995
    _Cx      ("Cx", Float) = 641.926025
    _Cy      ("Cy", Float) = 359.115997
    _K0      ("K0", Float) = 0.246231
    _K1      ("K1", Float) = -0.727204
    _K2      ("K2", Float) = 0.726065
    _P0      ("P0", Float) = 0.0
    _P1      ("P1", Float) = 0.0
}
SubShader 
{
    // Setting the z write off to make sure our video overlay is always rendered at back.
    ZWrite Off
    ZTest Off
    Tags { "Queue" = "Background" }
    Pass 
    {
        CGPROGRAM       
        #pragma vertex vert
        #pragma fragment frag
        #include "UnityCG.cginc"

        struct v2f
        {
            float4 vertex : SV_POSITION;
            float2 uv : TEXCOORD0;
        };
        
        v2f vert (appdata_img v)
        {
            v2f o;
            o.vertex = UnityObjectToClipPos(v.vertex);
            o.uv = MultiplyUV(UNITY_MATRIX_TEXTURE0, v.texcoord.xy);
            o.uv = float2(o.uv.x, 1 - o.uv.y);
            return o;
        }

        sampler2D _YTex;
        sampler2D _UVTex;
        
        float _bDistortion;
        float _TexWidth;
        float _TexHeight;
        float _Fx;
        float _Fy;
        float _Cx;
        float _Cy;
        float _K0;
        float _K1;
        float _K2;
        float _P0;
        float _P1;
        

        fixed4 frag (v2f i) : SV_Target
        {
            float undistored_x = i.uv.x;
            float undistored_y = i.uv.y;
            

            if(_bDistortion == 1)
            {
                float x = i.uv.x - 0.5;
                float y = i.uv.y - 0.5;
                float r2 = x * x + y * y;
                float icdist = 1.0 + r2 * (_K0 + r2 * (_K1 + r2 * _K2));
                undistored_x = x * icdist + 2 * _P0 * x * y + _P1 * (r2 + 2 * x * x) + 0.5;
                undistored_y = y * icdist + _P0 * (r2 + 2 * y * y) + 2 * _P1 * x * y + 0.5;
            }

            float y_value, u_value, v_value;
            y_value = tex2D(_YTex, float2(undistored_x, undistored_y)).r;
            u_value = tex2D(_UVTex, float2(undistored_x, undistored_y)).g;
            v_value = tex2D(_UVTex, float2(undistored_x, undistored_y)).r;

            // The YUV to RGBA conversion, please refer to: http://en.wikipedia.org/wiki/YUV
            // Y'UV420sp (NV21) to RGB conversion (Android) section.
            float r = y_value + 1.370705 * (v_value - 0.5);
            float g = y_value - 0.698001 * (v_value - 0.5) - (0.337633 * (u_value - 0.5));
            float b = y_value + 1.732446 * (u_value - 0.5);

            return float4(r, g, b, 1.0);
        }
        ENDCG
    }
}
}