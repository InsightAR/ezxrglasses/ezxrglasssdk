﻿using EZXR.Glass.Network.SocketIOClient.Arguments;

namespace EZXR.Glass.Network.SocketIOClient
{
    public delegate void EventHandler(ResponseArgs args);
}
