﻿namespace EZXR.Glass.Network.SocketIOClient
{
    public enum EngineIOProtocol
    {
        Open,
        Close,
        Ping,
        Pong,
        Message,
        Upgrade,
        Noop
    }
}
