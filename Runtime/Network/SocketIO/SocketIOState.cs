﻿namespace EZXR.Glass.Network.SocketIOClient
{
    public enum SocketIOState
    {
        None,
        Connected,
        Closed
    }
}
