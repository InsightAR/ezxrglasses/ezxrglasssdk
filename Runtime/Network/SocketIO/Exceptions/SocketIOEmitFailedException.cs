﻿using System;

namespace EZXR.Glass.Network.SocketIOClient.Exceptions
{
    [Obsolete]
    public class SocketIOEmitFailedException : Exception
    {
    }
}
