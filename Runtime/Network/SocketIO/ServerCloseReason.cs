﻿namespace EZXR.Glass.Network.SocketIOClient
{
    public enum ServerCloseReason
    {
        ClosedByClient,
        ClosedByServer,
        Aborted
    }
}
