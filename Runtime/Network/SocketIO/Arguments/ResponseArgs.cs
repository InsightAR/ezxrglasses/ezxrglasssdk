﻿namespace EZXR.Glass.Network.SocketIOClient.Arguments
{
    public class ResponseArgs
    {
        public string RawText { get; set; }
        public string Text { get; set; }
    }
}
