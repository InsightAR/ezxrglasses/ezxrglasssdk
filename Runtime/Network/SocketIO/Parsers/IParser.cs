﻿using System.Threading.Tasks;

namespace EZXR.Glass.Network.SocketIOClient.Parsers
{
    public interface IParser
    {
        Task ParseAsync(ResponseTextParser rtp);
    }
}
