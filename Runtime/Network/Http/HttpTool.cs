﻿using EZXR.Glass.Runtime;

namespace EZXR.Glass.Network.Http
{
    /// @cond UN_DOC
    public class HttpTool : SingletonManager<HttpTool>
    {
        // 无法在外界使用构造函数，确保Singleton
        protected HttpTool() { }
    }
    /// @endcond
}
