﻿using System;
using System.Collections.Generic;
using UnityEngine.Networking;

namespace EZXR.Glass.Network.Http
{
	/**
     * \~english
     * @enum Http Method type
     * 
     * \~chinese
     * @enum Http 方法类型
     */
	public enum HttpMethod {
		POST,/**< 
		        * \~english POST method
		        * \~chinese POST方法
		        */
		GET/**< 
            * \~english GET method
            * \~chinese GET方法
            */
	}
	
	/**
     * \~english
     * Http request class
     * 
     * \~chinese
     * Http请求类
     */
    public class HttpRequest
    {
	    /**
	     * \~english
	     * Request address
	     * 
	     * \~chinese
	     * 请求地址
	     */
        public string url;
	    /**
	     * \~english
	     * Request address
	     * 
	     * \~chinese
	     * 请求地址
	     */
		public HttpMethod method;
	    /**
	     * \~english
	     * Request header
	     * 
	     * \~chinese
	     * 请求头
	     */
		private Dictionary<string, string> headers;
	    /**
	     * \~english
	     * Request body
	     * 
	     * \~chinese
	     * 请求体
	     */
		public RequestBody requestBody;
	    /**
	     * \~english
	     * The timeout period of the request
	     * 
	     * \~chinese
	     * 该请求的超时时间
	     */
		public int timeout;

		public HttpRequest(HttpMethod method) {
			this.method = method;
			this.url = "";
			this.requestBody = null;
			this.timeout = 0;
			this.headers = new Dictionary<string, string> ();
		}


        // public HttpRequest Url(string url) {
		// 	this.url = url;
		// 	return this;
		// }

		// public HttpRequest Method(HttpMethod method, RequestBody body) {
		// 	// if (method == null) throw new NullReferenceException ("method cannot be null");
		// 	// if (method.Length == 0)	throw new InvalidOperationException ("method cannot be empty");

		// 	this.method = method;
		// 	this.body = body;
		// 	return this;
		// }

		/**
         * \~english
         * @brief Add request header
         *
         * @param name Request header name
		 * 
         * @param value Request header's value
         * 
         * \~chinese
         * @brief 添加请求头
         *
         * @param name 请求头名字
		 * 
         * @param value 请求头的值
         */
		public void AddHeader(string name, string value) {
			this.headers.Add (name, value);
		}

		/**
         * \~english
         * @brief Remove request header
         *
         * @param name Request header name
		 * 
         * \~chinese
         * @brief 移除请求头
         *
         * @param name 请求头名字
		 * 
         */
		public void RemoveHeader(string name) {
			this.headers.Remove (name);
		}

		// public HttpRequest Timeout(int timeout) {
		// 	this.timeout = timeout;
		// 	return this;
		// }

		// public int Timeout() {
		// 	return timeout;
		// }

		// public HttpRequest Get() {
		// 	Method (UnityWebRequest.kHttpVerbGET, null);
		// 	return this;
		// }

		// public HttpRequest Post(RequestBody body) {
		// 	Method (UnityWebRequest.kHttpVerbPOST, body);
		// 	return this;
		// }

		// public HttpRequest Put(RequestBody body) {
		// 	Method (UnityWebRequest.kHttpVerbPUT, body);
		// 	return this;
		// }

		// public HttpRequest Delete() {
		// 	Method (UnityWebRequest.kHttpVerbDELETE, null);
		// 	return this;
		// }

		// public string Url() {
		// 	return url;
		// }

		// public HttpMethod Method() {
		// 	return method;
		// }

		// public RequestBody Body() {
		// 	return body;
		// }

		/**
         * \~english
         * @brief Get all request headers
         *
         * \~chinese
         * @brief 获取所有请求头
         *
         */
		public Dictionary<string, string> Headers() {
			return headers;
		}


    }
}
