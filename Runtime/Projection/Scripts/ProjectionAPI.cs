﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace EZXR.Glass.Projection
{
    public class ProjectionAPI
    {
        static AndroidJavaObject instance;
        static AndroidJavaObject Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AndroidJavaObject("com.ezxr.glass.projectionlib.API");
                }
                return instance;
            }
        }

        /// <summary>
        /// 启动应用时调用，先创建API对象，再调用，非单例模式
        /// </summary>
        /// <param name="callBack_OnStart"></param>
        /// <param name="callBack_OnStop"></param>
        public static void Init(Action<int> callBack_OnStart, Action callBack_OnStop)
        {
#if UNITY_EDITOR

#else
            ProjectionCallback projectionCallback = new ProjectionCallback(callBack_OnStart, callBack_OnStop);
            AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject unityActivity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
            Instance.Call("init", unityActivity, projectionCallback);
#endif
        }

        // 退出应用时调用
        public static void DeInit()
        {
#if UNITY_EDITOR

#else
            Instance.Call("deInit");
#endif
        }

        // 收到投屏/录屏回调时调用，需要在render线程里面调用该接口
        // textureId：需要共享的纹理Id
        // width：纹理宽度（目前在投录屏server已写死1280×960,该字段暂无使用）
        // height：纹理高度（目前在投录屏server已写死1280×960,该字段暂无使用）
        public static void Start(int textureId, int width, int height)
        {
#if UNITY_EDITOR

#else
            Instance.Call("start", textureId, width, height);
#endif
        }

        // 普通3D应用切换到后台时调用
        public static void Stop()
        {
#if UNITY_EDITOR

#else
            Instance.Call("stop");
#endif
        }

        /// <summary>
        /// 通知投录屏服务器刷新
        /// </summary>
        /// <param name="timestamp">当前帧时间戳（目前该字段暂无使用，后面可优化）</param>
        public static void NotifyNewFrame(long timestamp)
        {
#if UNITY_EDITOR

#else
            Instance.Call("notifyNewFrame", timestamp);
#endif
        }
    }
}