﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.Projection
{
    public class ProjectionCallback : AndroidJavaProxy
    {
        public event Action<int> callBack_OnStart;
        public event Action callBack_OnStop;

        public ProjectionCallback() : base("com.ezxr.glass.projectionlib.callback.ProjectionCallback")
        {
        }

        public ProjectionCallback(
            Action<int> callBack_OnStart,
            Action callBack_OnStop
            ) : base("com.ezxr.glass.projectionlib.callback.ProjectionCallback")
        {
            this.callBack_OnStart += callBack_OnStart;
            this.callBack_OnStop += callBack_OnStop;
        }

        /// <summary>
        /// 开始投屏或录屏 type: 0投屏 1录屏 2截屏
        /// </summary>
        /// <param name="type"></param>
        void onStart(int type)
        {
            Debug.Log("ProjectionCallback--> onStart: " + type);
            if (callBack_OnStart != null)
            {
                callBack_OnStart(type);
            }
        }
        /// <summary>
        /// 结束投屏或录屏
        /// </summary>
        void onStop()
        {
            Debug.Log("ProjectionCallback--> onStop");
            if (callBack_OnStop != null)
            {
                callBack_OnStop();
            }
        }
    }
}