using System;
using System.Collections;
using UnityEngine;

namespace EZXR.Glass.QRScanner
{
    public class QRScannerManager : MonoBehaviour
    {
        private static ScannerHandler scannerHandler;

        private static Action startListeners;
        private static Action<bool, string> completeListeners;

        public static void RequestOpen()
        {
            if (scannerHandler == null)
            {
                var instance = Instantiate(Resources.Load<GameObject>("ScannerWindow"));
                scannerHandler = instance.GetComponent<ScannerHandler>();
            }

            scannerHandler.gameObject.SetActive(true);
            scannerHandler.SetupListeners(startListeners, completeListeners);
        }

        public static void RequestClose()
        {
            //startListeners = null;
            //completeListeners = null;
            if (scannerHandler != null)
            {
                scannerHandler.gameObject.SetActive(false);
                scannerHandler.SetupListeners(null, null);
            }
        }

        public static void RegisterStartListener(Action onStart)
        {
            startListeners += onStart;
            scannerHandler?.SetupListeners(startListeners, completeListeners);
        }

        public static void UnregisterStartListener(Action onStart)
        {
            startListeners -= onStart;
            scannerHandler?.SetupListeners(startListeners, completeListeners);
        }

        public static void RegisterCompleteListener(Action<bool, string> onComplete)
        {
            completeListeners += onComplete;
            scannerHandler?.SetupListeners(startListeners, completeListeners);
        }

        public static void UnregisterCompleteListener(Action<bool, string> onComplete)
        {
            completeListeners -= onComplete;
            scannerHandler?.SetupListeners(startListeners, completeListeners);
        }
    }
}
