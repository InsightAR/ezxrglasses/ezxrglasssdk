using EZXR.Glass.Runtime;
using EZXR.Glass.Inputs;
using EZXR.Glass.MiraCast;
using EZXR.Glass.Recording;
using EZXR.Glass.SixDof;
using System;
using System.Collections;
using UnityEngine;
#if USE_LOCALIZATION
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
#endif

namespace EZXR.Glass.Runtime
{
    [ScriptExecutionOrder(-3000)]
    public class OSEventSystem : MonoBehaviour
    {
        public static event Action OnInstructionsReceived_EZXRServerResetEnd;
        public static event Action<string> OnInstructionsReceived_StartParamsReceived;
        public static event Action<string> OnInstructionsReceived_LanguageReceived;
        /// <summary>
        /// 系统进入待机模式的时候会触发
        /// </summary>
        public static event Action OnSystemEnterStandbyMode;

        /// <summary>
        /// 系统收到特定游戏数据的时候触发
        /// </summary>
        public static event Action<string> OnGameMSGReceived;

        public bool enableSystemUI = true;

        // Start is called before the first frame update
        void Awake()
        {
            
        }

        private void Start()
        {
            //确保在所有脚本中最优先执行，且在Start中注册，如果在Awake中注册的瞬间收到了广播，但是其他类实例尚未准备完毕，就会出现空引用错误
            Debug.Log("register language");
            BroadcastReceiver.Instance.Register("Instructions", "android.sunny.action.SCREEN", "com.ezxr.glass.GAME_MSG");
            BroadcastReceiver.Instance.RegisterCallback("SetSDKInputState", OnReceive_SetSDKInputState, BroadcastReceiver.DataType.String);
            BroadcastReceiver.Instance.RegisterCallback("SetSDKInputState", OnReceive_SetSDKInputState, BroadcastReceiver.DataType.String);
            BroadcastReceiver.Instance.RegisterCallback("SetSDKRayState", OnReceive_RayState, BroadcastReceiver.DataType.String);
            BroadcastReceiver.Instance.RegisterCallback("Miracast", OnReceive_MiracastInstructions, BroadcastReceiver.DataType.String);
            BroadcastReceiver.Instance.RegisterCallback("Recording", OnReceive_RecordingInstructions, BroadcastReceiver.DataType.String);
            BroadcastReceiver.Instance.RegisterCallback("EZXRServerReset", OnReceiveReset, BroadcastReceiver.DataType.String);
            BroadcastReceiver.Instance.RegisterCallback("SetSDKDynamicControllerType", OnReceive_SetSDKDynamicControllerType, BroadcastReceiver.DataType.String);
            BroadcastReceiver.Instance.RegisterCallback("startParams", OnStartParamsReceived, BroadcastReceiver.DataType.String);
            BroadcastReceiver.Instance.RegisterCallback("SCREEN_STATE", OnReceive_SCREEN_STATE, BroadcastReceiver.DataType.Int);
            BroadcastReceiver.Instance.RegisterCallback("game_data", OnReceive_GAME_MSG, BroadcastReceiver.DataType.String);
            BroadcastReceiver.Instance.RegisterCallback("Language", OnReceive_Language, BroadcastReceiver.DataType.String);
        }

        private void OnApplicationPause(bool pause)
        {
            if (!pause)
            {
                SetSystemInteractionState(enableSystemUI);

                OnReceive_SetSDKInputState("True");
            }
            //else
            //{
            //    //移除Broadcast实例，以避免下次resume的时候莫名会收到待机时候的SCREEN_STATE广播
            //    Destroy(BroadcastReceiver.Instance.gameObject);
            //}
        }

        /// <summary>
        /// Receive from System, set hand visibility
        /// </summary>
        /// <param name="data">true for show</param>
        private void OnReceive_SetSDKInputState(string data)
        {
            Debug.Log("UnityBroadcastReceiver OnReceive_SetSDKInputState: " + data);

            bool enabled;
            if (bool.TryParse(data, out enabled))
            {
                if (InputSystem.Instance != null)
                {
                    InputSystem.Instance.SetActive(enabled);
                }
            }
        }

        /// <summary>
        /// Receive from System, set RaycastInteraction status
        /// </summary>
        /// <param name="data"></param>
        private void OnReceive_RayState(string data)
        {
            bool enable = bool.Parse(data);

            Debug.Log("UnityBroadcastReceiver OnReceive_RayState: " + data);
            if (XRMan.Input.Left != null)
            {
                XRMan.Hands.Left.RI_Internal = enable;
                XRMan.Controllers.Left.RI_Internal = enable;
                //InputSystem.leftHand.RaycastInteraction = enable;
            }
            if (XRMan.Input.Right != null)
            {
                XRMan.Hands.Right.RI_Internal = enable;
                XRMan.Controllers.Right.RI_Internal = enable;
                //InputSystem.rightHand.RaycastInteraction = enable;
            }
            //if (UGUIHandler.Instance != null)
            //{
            //    UGUIHandler.Instance.gameObject.SetActive(enable);
            //}
        }

        /// <summary>
        /// Receive from System, Miracast instructions
        /// </summary>
        /// <param name="data"></param>
        void OnReceive_MiracastInstructions(string data)
        {
            Debug.Log("UnityBroadcastReceiver OnReceive_MiracastInstructions: " + data);

            if (!string.IsNullOrEmpty(data))
            {
                string[] parameters = data.Split('^');
                switch (parameters[0])
                {
                    case "Start":
                        StartCoroutine(TryStartServer(parameters[1]));
                        break;
                    case "Stop":
                        if (MiracastManager.Instance != null && MiracastManager.Instance.isMiracasting)
                        {
                            MiracastManager.Instance.Stop();
                        }
                        break;
                }
            }
        }

        IEnumerator TryStartServer(string serverIP)
        {
            if (MiracastManager.Instance != null)
            {
                MiracastManager.Instance.Stop();
                //Destroy(MiracastManager.Instance.gameObject);
                Debug.Log("TryStartServer: 0");
            }

            yield return new WaitForSeconds(2);

            GameObject miracastManagerObj = Instantiate(ResourcesManager.Load<GameObject>("MiracastManager"));
            Debug.Log("TryStartServer: 1");
            GameObject cameraRigObj = HMDPoseTracker.Instance.gameObject;
            if (XRMan.Exist)
            {
                miracastManagerObj.transform.position = XRMan.offset_SpatialComputing.GetColumn(3);
                miracastManagerObj.transform.rotation = XRMan.offset_SpatialComputing.rotation;
            }
            else
            {
                if (cameraRigObj != null && cameraRigObj.transform.parent != null)
                {
                    miracastManagerObj.transform.parent = cameraRigObj.transform.parent;
                    miracastManagerObj.transform.localPosition = Vector3.zero;
                    miracastManagerObj.transform.localRotation = Quaternion.identity;
                }
            }

            if (MiracastManager.Instance != null)
            {
                Debug.Log("TryStartServer: 2");
                MiracastManager.Instance.Run(serverIP);
                Debug.Log("TryStartServer: 3");
                Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "MiracastStatus", "Started");
                Debug.Log("TryStartServer: 4");
            }
        }

        /// <summary>
        /// Receive from System, Recording instructions
        /// </summary>
        /// <param name="data"></param>
        private void OnReceive_RecordingInstructions(string data)
        {
            Debug.Log("UnityBroadcastReceiver OnReceive_RecordingInstructions: " + data);

            switch (data)
            {
                case "recordingStart":
                    if (RecordingManager.Instance != null)
                    {
                        Destroy(RecordingManager.Instance.gameObject);
                    }
                    StartCoroutine(tryStartRecording());
                    break;
                case "recordingStop":
                    if (RecordingManager.Instance != null && RecordingManager.Instance.isRecording)
                    {
                        RecordingManager.Instance.HandleRecordingClose();
                    }
                    break;
                case "screenshotStart":
                    if (RecordingManager.Instance != null)
                    {
                        Destroy(RecordingManager.Instance.gameObject);
                    }
                    StartCoroutine(tryStartScreenShot());

                    break;
            }
        }

        private IEnumerator tryStartRecording()
        {
            yield return new WaitForSeconds(2);

            GameObject recordingManagerObj = Instantiate(ResourcesManager.Load<GameObject>("RecordingManager"));
            GameObject cameraRigObj = HMDPoseTracker.Instance.gameObject;
            if (XRMan.Exist)
            {
                recordingManagerObj.transform.position = XRMan.offset_SpatialComputing.GetColumn(3);
                recordingManagerObj.transform.rotation = XRMan.offset_SpatialComputing.rotation;
            }
            else
            {
                if (cameraRigObj != null && cameraRigObj.transform.parent != null)
                {
                    recordingManagerObj.transform.parent = cameraRigObj.transform.parent;
                    recordingManagerObj.transform.localPosition = Vector3.zero;
                    recordingManagerObj.transform.localRotation = Quaternion.identity;
                }
            }
            if (RecordingManager.Instance != null)
                RecordingManager.Instance.HandleRecordingOpen();
        }
        private IEnumerator tryStartScreenShot()
        {
            yield return new WaitForSeconds(2);
            GameObject recordingManagerObj = Instantiate(ResourcesManager.Load<GameObject>("RecordingManager"));
            GameObject cameraRigObj = HMDPoseTracker.Instance.gameObject;
            if (XRMan.Exist)
            {
                recordingManagerObj.transform.position = XRMan.offset_SpatialComputing.GetColumn(3);
                recordingManagerObj.transform.rotation = XRMan.offset_SpatialComputing.rotation;
            }
            else
            {
                if (cameraRigObj != null && cameraRigObj.transform.parent != null)
                {
                    recordingManagerObj.transform.parent = cameraRigObj.transform.parent;
                    recordingManagerObj.transform.localPosition = Vector3.zero;
                    recordingManagerObj.transform.localRotation = Quaternion.identity;
                }
            }
            if (RecordingManager.Instance != null)
                RecordingManager.Instance.HandleScreenCapture();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data">Start/End</param>
        private void OnReceiveReset(string data)
        {
            Debug.Log("UnityBroadcastReceiver OnReceiveReset: OnInstructionsReceived_Reset + " + data);
            if (data == "End")
            {
                if (OnInstructionsReceived_EZXRServerResetEnd != null)
                {
                    OnInstructionsReceived_EZXRServerResetEnd();
                }
            }
        }

        /// <summary>
        /// 接收系统广播实现打开SystemUI
        /// </summary>
        /// <param name="obj">1为进入待机，2为退出待机（收不到，不要用）</param>
        private void OnReceive_SCREEN_STATE(string obj)
        {
            Debug.Log("OnReceive_SCREEN_STATE: " + obj);
            if (obj == "1")
            {
                if (OnSystemEnterStandbyMode != null)
                {
                    OnSystemEnterStandbyMode();
                }
            }
        }

        private void OnReceive_GAME_MSG(string obj)
        {
            Debug.Log("OnReceive_GAME_MSG: " + obj);
            if (OnGameMSGReceived != null)
            {
                OnGameMSGReceived(obj);
            }
        }

        /// <summary>
        /// 设置系统级交互的状态，包括：食指指尖Home、手腕菜单、系统级手的显示状态
        /// </summary>
        /// <param name="state">false表示禁用系统级交互</param>
        public static void SetSystemInteractionState(bool state)
        {
            Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "SetSystemHandState", state.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data">0是head，1是hand，2是controller</param>
        private void OnReceive_SetSDKDynamicControllerType(string data)
        {
            Debug.Log("UnityBroadcastReceiver OnReceiveReset: OnReceive_SetSDKDynamicControllerType + " + data);
            InputType controllerType = (InputType)int.Parse(data);

            InputSystem.Instance.InitDynamicMode(controllerType);
        }

        private void OnStartParamsReceived(string data)
        {
            Debug.Log("UnityBroadcastReceiver OnInstructionsReceived_StartParamsReceived: " + data);

            if (OnInstructionsReceived_StartParamsReceived != null)
            {
                OnInstructionsReceived_StartParamsReceived(data);
            }
        }

        private void OnReceive_Language(string data)
        {
#if USE_LOCALIZATION
            if (LocalizationSettings.HasSettings)
            {
                ILocalesProvider il = LocalizationSettings.AvailableLocales;
                if (il != null && il.Locales != null && il.Locales.Count > 0)
                {
                    string localtionCode = new string("");
                    if (data == "cn")
                    {
                        localtionCode = "zh";
                    }
                    else if (data == "tc")
                    {
                        localtionCode = "zh-TW";
                    }
                    else if (data == "en")
                    {
                        localtionCode = "en";
                    }
                    if (localtionCode != "")
                    {
                        for (int indexLc = 0; indexLc < il.Locales.Count; indexLc++)
                        {
                            Locale lc = il.Locales[indexLc];
                            if (localtionCode == lc.Formatter.ToString())
                            {
                                LocalizationSettings.Instance.SetSelectedLocale(lc);
                                break;
                            }
                        }
                    }
                }
            }
#endif
            if (OnInstructionsReceived_LanguageReceived != null)
            {
                OnInstructionsReceived_LanguageReceived(data);
            }
        }
    }
}
