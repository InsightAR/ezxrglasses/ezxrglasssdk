﻿using EZXR.Glass.Projection;
using UnityEngine;
namespace EZXR.Glass.Runtime
{
    [ScriptExecutionOrder(-101)]
    public class SystemManager : MonoBehaviour
    {
        private static SystemManager instance;

        // Start is called before the first frame update
        void Awake()
        {
            instance = this;

            Instantiate(ResourcesManager.Load<GameObject>("OSEventSystem"));
        }

        private void Start()
        {
            if (ProjectionManager.Instance == null)
            {
                GameObject projManager = Instantiate(ResourcesManager.Load<GameObject>("ProjectionManager"));
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
        private void OnApplicationQuit()
        {

        }
    }
}