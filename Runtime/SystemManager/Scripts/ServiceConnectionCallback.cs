﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    public class ServiceConnectionCallback : AndroidJavaProxy
    {
        public ServiceConnectionCallback() : base("com.ezxr.glass.nativelib.callback.ServiceConnectionCallback ")
        {
        }

        void onServiceConnected()
        {
            Debug.Log("ServiceConnectionCallback--> onServiceConnected");

        }
        void onServiceDisconnected()
        {
            Debug.Log("ServiceConnectionCallback--> onServiceDisconnected");

        }
    }
}