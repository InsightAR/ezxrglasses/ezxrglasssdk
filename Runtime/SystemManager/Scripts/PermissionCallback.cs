﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    public class PermissionCallback : AndroidJavaProxy
    {
        public PermissionCallback() : base("com.ezxr.glass.nativelib.callback.PermissionCallback")
        {
        }

        void onPermissionUpdated(string[] granted, string[] rejected)
        {
            Debug.Log("PermissionCallback--> onPermissionUpdated, granted:" + granted.Length + ", rejected:" + rejected.Length);
        }
    }
}