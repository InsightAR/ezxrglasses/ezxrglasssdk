﻿using EZXR.Glass.Runtime;
using EZXR.Glass.Rendering;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;
using UnityEngine;

namespace EZXR.Glass.MiraCast
{
    public class MiraCastEditor : EditorWindow
    {
        static AddRequest addRequest;
        static RemoveRequest removeRequest;
        static Queue<string> packageNameQueue;

        [MenuItem("GameObject/XR Abilities/Additional.../MiraCast/Enable", false, 100)]
        public static void EnableMiraCast()
        {
            var symbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android);
            if (!symbols.Contains("ARMiracast"))
            {
                if (EditorUtility.DisplayDialog("Importing Package", "Start Import Packages: \n1) com.unity.renderstreaming@3.1.0-exp.2\n2) com.unity.inputsystem@1.0.2\n3) com.unity.webrtc@2.4.0-exp.4\n\n\nPlease make sure that this will not make conflicts!", "Yes"))
                {
                    EditorApplication.update += PackageAddProgress;

                    EditorUtility.DisplayProgressBar("Importing Package", "Is importing package, please wait...", 0);

                    addRequest = Client.Add("com.unity.renderstreaming@3.1.0-exp.2");
                }
            }
            else
            {
                //AddMiracastManager();

                //AddMiracastUI();
            }
            UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene());
            AssetDatabase.SaveAssets();
        }
        //[MenuItem("GameObject/XR Abilities/Additional.../MiraCast/Enable", true, 100)]
        //static bool ValidateEnableMiraCast()
        //{
        //    return !PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android).Contains("ARMiracast");
        //}

        #region 移除Miracast
        [MenuItem("GameObject/XR Abilities/Additional.../MiraCast/Disable", false, 100)]
        public static void DisableMiraCast()
        {
            //如果场景中存在MiracastManager，则删除
            MiracastManager miracastManager = FindObjectOfType<MiracastManager>();
            if (miracastManager != null)
            {
                DestroyImmediate(miracastManager.gameObject);
            }

            //如果场景中存在PhoneScreenUI/Canvas/MiracastUI，则删除
            PhoneUIRenderer phoneScreenUI = FindObjectOfType<PhoneUIRenderer>();
            if (phoneScreenUI != null)
            {
                Transform miracastUI = phoneScreenUI.transform.Find("Canvas/MiracastUI");
                if (miracastUI != null)
                {
                    DestroyImmediate(miracastUI.gameObject);
                }
            }

            UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene());
            AssetDatabase.SaveAssets();

            if (EditorUtility.DisplayDialog("MiraCast相关资源移除确认", "是否移除MiraCast导入的PackageManager包？", "OK", "No"))
            {
                RemoveMiracastFromProject();
            }
        }

        [MenuItem("GameObject/XR Abilities/Additional.../MiraCast/Disable", true, 100)]
        static bool ValidateDisableMiraCast()
        {
            return PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android).Contains("ARMiracast");
        }

        /// <summary>
        /// 从工程中彻底移除Miracast带来的改变，包括packagemanager中引入的package和ScriptingDefineSymbols改变
        /// </summary>
        static void RemoveMiracastFromProject()
        {
            //去掉ScriptingDefineSymbols中的ARMiracast，避免移除package的时候会因为引用丢失而报错
            string symbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android);
            if (symbols.Contains("ARMiracast;"))
            {
                symbols = symbols.Replace("ARMiracast;", "");
                PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, symbols);
            }
            else if (symbols.Contains("ARMiracast"))
            {
                symbols = symbols.Replace("ARMiracast", "");
                PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, symbols);
            }
            else
            {
                return;
            }

            // callback for every frame in the editor
            EditorApplication.update += PackageRemovalProgress;
            EditorApplication.LockReloadAssemblies();

            EditorUtility.DisplayProgressBar("Removing Package", "Is removing package, please wait...", 0);

            removeRequest = Client.Remove("com.unity.renderstreaming");
        }

        static void PackageRemovalProgress()
        {
            if (removeRequest.IsCompleted)
            {
                switch (removeRequest.Status)
                {
                    case StatusCode.Failure:    // couldn't remove package
                        EditorUtility.ClearProgressBar();
                        Debug.LogError("Couldn't remove package '" + removeRequest.PackageIdOrName + "': " + removeRequest.Error.message);
                        break;

                    case StatusCode.InProgress:
                        break;

                    case StatusCode.Success:
                        EditorUtility.ClearProgressBar();
                        Debug.Log("Removed package: " + removeRequest.PackageIdOrName);
                        break;
                }

                EditorApplication.update -= PackageRemovalProgress;
                EditorApplication.UnlockReloadAssemblies();
            }

            return;
        }
        #endregion

        static void PackageAddProgress()
        {
            if (addRequest != null && addRequest.IsCompleted)
            {
                switch (addRequest.Status)
                {
                    case StatusCode.Failure:
                        EditorUtility.ClearProgressBar();
                        Debug.LogError("Couldn't add package '" + "': " + addRequest.Error.message);
                        break;

                    case StatusCode.InProgress:
                        break;

                    case StatusCode.Success:
                        EditorUtility.ClearProgressBar();
                        Debug.Log("Added package: " + addRequest.Result.name);

                        //向ScriptingDefineSymbols添加ARMiracast
                        var symbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android);
                        if (!symbols.Contains("ARMiracast"))
                        {
                            symbols += ";ARMiracast";

                            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, symbols);
                        }

                        //AddMiracastManager();

                        //AddMiracastUI();

                        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene());
                        AssetDatabase.SaveAssets();
                        break;
                }

                EditorApplication.update -= PackageAddProgress;
            }
        }

        static void AddMiracastManager()
        {
            //如果场景中不存在MiracastManager的话需要实例化一个
            if (FindObjectOfType<MiracastManager>() == null)
            {
                Runtime.PrefabUtility.InstantiatePrefabWithUndoAndSelection(ResourcesManager.Load<GameObject>("MiracastManager"));
            }
        }

        static void AddMiracastUI()
        {
            //如果场景中不存在PhoneScreenUI的话需要实例化一个，并且向其下的Canvas添加MiracastUI
            PhoneUIRenderer phoneScreenUI = FindObjectOfType<PhoneUIRenderer>();
            if (phoneScreenUI == null)
            {
                phoneScreenUI = (Runtime.PrefabUtility.InstantiatePrefabWithUndoAndSelection(ResourcesManager.Load<GameObject>("PhoneScreenUI")) as GameObject).GetComponent<PhoneUIRenderer>();
            }
            if (phoneScreenUI != null)
            {
                if (phoneScreenUI.transform.Find("Canvas/MiracastUI") == null)
                {
                    Runtime.PrefabUtility.InstantiatePrefabWithUndoAndSelection(ResourcesManager.Load<GameObject>("MiracastUI"), phoneScreenUI.transform.Find("Canvas")).name = "MiracastUI";
                }
            }
        }
    }
}