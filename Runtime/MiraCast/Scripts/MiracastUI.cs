﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
#if ARMiracast
using Unity.RenderStreaming;
using Unity.RenderStreaming.Signaling;
#endif
using UnityEngine;
using UnityEngine.UI;

namespace EZXR.Glass.MiraCast
{
    public class MiracastUI : MonoBehaviour
    {
#if ARMiracast
        InputField inputField;
        Text buttonText;
        string path;

        // Start is called before the first frame update
        void Start()
        {
            inputField = transform.Find("InputField").GetComponent<InputField>();
            buttonText = transform.Find("Button/Text").GetComponent<Text>();

            path = Path.Combine(Application.persistentDataPath, "miracast.cfg");
            if (File.Exists(path))
            {
                inputField.text = File.ReadAllText(path);
                OnButtonClicked();
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnButtonClicked()
        {
            if (buttonText.text == "Connect")
            {
                Run();
                buttonText.text = "DisConnect";
                File.WriteAllText(path, inputField.text);
            }
            else if (buttonText.text == "DisConnect")
            {
                Stop();
                buttonText.text = "Connect";
            }
        }

        void Run()
        {
            MiracastManager.Instance.Run(inputField.text);
        }

        void Stop()
        {
            MiracastManager.Instance.Stop();
        }
#endif
    }
}