﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZXR.Glass.Recording;
using System.Threading;
using System;
using UnityEngine.UI;
#if ARMiracast
using Unity.RenderStreaming;
using Unity.RenderStreaming.Signaling;
#endif

namespace EZXR.Glass.MiraCast
{
    public class MiracastManager : MonoBehaviour
    {
        #region 单例
        private static MiracastManager instance;
        public static MiracastManager Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        public bool isMiracasting;
        public int width = 1280;
        public int height = 960;
#if ARMiracast
        public RenderStreaming renderStreaming;
#endif

        private void Awake()
        {
            if (Application.isPlaying)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
#if ARMiracast
                renderStreaming = GetComponent<RenderStreaming>();
#endif
                ARRenderRGB.SetRGBResolution(width, height);
            }
            else
            {

            }
        }

        private void OnApplicationPause(bool pause)
        {
            if (pause)
            {
                Stop();
            }
        }

        public void Run(string serverIP)
        {
            ARRenderRGB.Instance.HandleARRenderOpen();

#if ARMiracast
            renderStreaming.Run(true, CreateSignaling(typeof(WebSocketSignaling), "ws://" + serverIP, 5, SynchronizationContext.Current));
#endif
            isMiracasting = true;
        }

        public void Stop()
        {
            Debug.Log("MiracastManager Stop 0");
            ARRenderRGB.Instance.HandleARRenderClose();

#if ARMiracast
            Debug.Log("MiracastManager Stop 1");
            //renderStreaming.Stop();
            instance = null;
            Debug.Log("MiracastManager Stop 2");
            Destroy(gameObject);
            Debug.Log("MiracastManager Stop 3");
#endif
        }

#if ARMiracast
        static ISignaling CreateSignaling(Type type, string url, float interval, SynchronizationContext context)
        {
            Debug.Log(type);
            Type _type = type;
            if (_type == null)
            {
                throw new ArgumentException($"Signaling type is undefined. {type}");
            }
            object[] args = { url, interval, context };
            return (ISignaling)Activator.CreateInstance(_type, args);
        }
#endif
    }
}