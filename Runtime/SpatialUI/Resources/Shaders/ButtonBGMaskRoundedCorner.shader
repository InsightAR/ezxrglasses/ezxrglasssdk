﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/ButtonBGMask/RoundedCorner"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Color("Main Color", Color) = (1,1,1,1)
		_Radius("Radius", Range(0.25, 0.5)) = 0.5
	}

	SubShader
	{
		Pass
		{
			Tags {"Queue" = "Transparent"}
			ZWrite Off
			Cull Off
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			fixed4 _Color;
			float4 _MainTex_ST;
			float _Radius;

			struct VertexOutPut
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			VertexOutPut vert(appdata_full v)
			{
				VertexOutPut o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord.xy;
				o.uv.x = 1.0 - o.uv.x;

				return o;
			}

			fixed4 frag(VertexOutPut i) : COLOR
			{
				fixed4 col = tex2D(_MainTex, i.uv) * _Color;
				//圆角
				float2 uv = i.uv.xy - float2(0.5,0.5);//移动UV坐标中心    
				float rx = fmod(uv.x, _Radius);//圆角所在区域，也就是圆角半径为0.1
				float ry = fmod(uv.y, _Radius);//
				float mx = step(_Radius, abs(uv.x));//大于0.4的部分， step(a，x):x<a取0，否则返回1
				float my = step(_Radius, abs(uv.y));// 　　　　　　　　　
				float alpha = 1 - mx * my * step(0.5 - _Radius, length(half2(rx,ry)));//在[0,0.4]范围，mx*my始终为0，最终值始终为1；在（0.4,0.5]范围，所在的圆角区域，mx*my使用为1，大小由圆角半径决定；在剩下的其他区域，mx*my也是为0，最终值为；　　　　　　　　　
				return fixed4(col.rgb,alpha * col.a);// 　　　　　　　
			}
			ENDCG 
		} 
	} 
}