﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.UI
{
    [ExecuteInEditMode]
    public class HorizontalLayoutGroup : LayoutGroup
    {
        List<SpatialUIElement> children = new List<SpatialUIElement>();
        float prevChildCount;
        Vector3 prevSize;
        [Tooltip("单位是m")]
        public float horizontalSpace = 0.001f;
        float prevHorizontalSpace;
        Vector3 anchor;
        public enum Alignment
        {
            Left,
            Middle,
            Right
        }
        public Alignment alignment;
        Alignment prevAlignment;

        public override void OnValidate()
        {
            if (!Application.isPlaying)
            {
                base.OnValidate();

                GetSpatialUIElement();

                if (horizontalSpace != prevHorizontalSpace || prevAlignment != alignment || spatialUIElement.size != prevSize || (children != null && children.Count != prevChildCount))
                {
                    prevHorizontalSpace = horizontalSpace;
                    prevAlignment = alignment;
                    prevSize = spatialUIElement.size;
                    prevChildCount = children.Count;

                    needReLayout = true;
                }
            }
        }

        private void OnDisable()
        {
            children = null;
        }

        // Update is called once per frame
        void Update()
        {
            if (startLayout)
            {
                if (needReLayout)
                {
                    needReLayout = false;

                    if (transform.childCount > 0)
                    {
                        Vector3 totalSize = Vector3.zero;

                        children = new List<SpatialUIElement>();
                        for (int i = 0; i < transform.childCount; i++)
                        {
                            SpatialUIElement temp = transform.GetChild(i).GetComponent<SpatialUIElement>();
                            if (temp != null && temp.gameObject.activeInHierarchy)
                            {
                                totalSize += temp.size;
                                children.Add(temp);
                            }
                        }

                        prevHorizontalSpace = horizontalSpace;
                        prevAlignment = alignment;


                        Vector3 curPos = Vector3.zero;
                        switch (alignment)
                        {
                            case Alignment.Left:
                                anchor = spatialUIElement.size / 2.0f;
                                break;
                            case Alignment.Middle:
                                anchor = new Vector3((totalSize.x + (children.Count - 1) * horizontalSpace) / 2.0f, 0, 0);
                                break;
                            case Alignment.Right:
                                anchor = new Vector3((totalSize.x + (children.Count - 1) * horizontalSpace) - (spatialUIElement.size.x / 2.0f), 0, 0);
                                break;
                        }
                        for (int i = 0; i < children.Count; i++)
                        {
                            if (children[i] != null)
                            {
                                if (curPos.x + children[i].size.x <= spatialUIElement.size.x)
                                {
                                    children[i].transform.localPosition = new Vector3(anchor.x - curPos.x - children[i].size.x / 2.0f, 0, 0);
                                    curPos = new Vector3(curPos.x + children[i].size.x + horizontalSpace, curPos.y, curPos.z);
                                }
                            }
                        }
                    }
                    else
                    {
                        children = null;
                    }
                }
            }
        }
    }
}