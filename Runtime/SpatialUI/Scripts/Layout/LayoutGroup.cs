using UnityEngine;

namespace EZXR.Glass.UI
{
    [ExecuteInEditMode]
    public class LayoutGroup : MonoBehaviour
    {
        protected SpatialUIElement spatialUIElement;
        protected bool needReLayout;

        protected bool startLayout;

        public virtual void OnValidate()
        {

        }

        public virtual void OnEnable()
        {
            Debug.Log("GridLayoutGroup must be attached on Object with SpatialUIElement, or it's parent has SpatialUIElement attached");
            GetSpatialUIElement();

            if (spatialUIElement == null)
            {
                Debug.LogError("GridLayoutGroup must be attached on Object with SpatialUIElement, or it's parent has SpatialUIElement attached");
            }
            else
            {
                startLayout = true;
                needReLayout = true;
            }
        }

        public void ReLayout()
        {
            needReLayout = true;
        }

        protected void GetSpatialUIElement()
        {
            if (spatialUIElement == null)
            {
                spatialUIElement = transform.GetComponent<SpatialUIElement>();
                if (spatialUIElement == null && transform.parent != null)
                {
                    spatialUIElement = transform.GetComponentInParent<SpatialUIElement>(true);
                }
            }
        }
    }
}