﻿using EZXR.Glass.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace EZXR.Glass.UI
{
    [ScriptExecutionOrder(-1)]
    public class WristPanel : MonoBehaviour
    {
        private static WristPanel instance;
        #region 多例
        static WristPanel instance_Left;
        static WristPanel instance_Right;
        public static WristPanel Left
        {
            get
            {
                return instance_Left;
            }
        }
        public static WristPanel Right
        {
            get
            {
                return instance_Right;
            }
        }
        #endregion

        public enum WristType
        {
            Left,
            Right
        }
        public WristType wristType;

        public TextMesh battery;
        public TextMesh time;

        static SpatialTrigger spatialTrigger;

        private void Awake()
        {
            instance = this;

            if (wristType == WristType.Left)
            {
                instance_Left = this;
            }
            else
            {
                instance_Right = this;
            }
        }

        // Update is called once per frame
        void Update()
        {
            battery.text = SystemInfo.batteryLevel * 100 + "%";
            time.text = System.DateTime.Now.ToString("HH:mm");
        }

        /// <summary>
        /// 当手腕面板被触碰的时候回调
        /// </summary>
        /// <param name="action"></param>
        public void AddListener(UnityAction action)
        {
            if (spatialTrigger == null)
            {
                spatialTrigger = transform.Find("SpatialTrigger").GetComponent<SpatialTrigger>();
            }
            spatialTrigger.onTriggerEnter.AddListener(action);
        }

        public void RemoveListener(UnityAction action)
        {
            if (spatialTrigger == null)
            {
                spatialTrigger = transform.Find("SpatialTrigger").GetComponent<SpatialTrigger>();
            }
            spatialTrigger.onTriggerEnter.RemoveListener(action);
        }

        public void ClearListener()
        {
            if (spatialTrigger == null)
            {
                spatialTrigger = transform.Find("SpatialTrigger").GetComponent<SpatialTrigger>();
            }
            spatialTrigger.onTriggerEnter.RemoveAllListeners();
        }
    }
}