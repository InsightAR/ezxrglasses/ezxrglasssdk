﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZXR.Glass.SixDof;
using EZXR.Glass.Runtime;
using System;

namespace EZXR.Glass.UI
{
    [ExecuteInEditMode]
    public class SpatialUIController : MonoBehaviour
    {
        #region Singleton
        private static SpatialUIController instance;
        public static SpatialUIController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<SpatialUIController>();
                    if (instance == null)
                    {
                        GameObject go = new GameObject("SpatialUIController");
                        instance = go.AddComponent<SpatialUIController>();
                    }
                }
                return instance;
            }
        }
        #endregion

        public event Action<float, bool> ReCalculateAllSize;

        /// <summary>
        /// 中间位相机（通常用于Editor）
        /// </summary>
        [HideInInspector]
        public Camera centerCamera;
        /// <summary>
        /// 左眼相机
        /// </summary>
        [HideInInspector]
        public Camera leftCamera;
        /// <summary>
        /// 右眼相机
        /// </summary>
        [HideInInspector]
        public Camera rightCamera;
        /// <summary>
        /// 头的Transform
        /// </summary>
        [HideInInspector]
        public Transform headTransform;
        /// <summary>
        /// 图片的每个像素对应的UnityUnit（Cube的默认边长即为1个UnityUnit）
        /// </summary>
        public float unitsPerPixel;
        ////[HideInInspector]
        public float lastUnitsPerPixel;

        //public ARUIController()
        //{
        //    instance = this;
        //}

        private void Awake()
        {
            if (Application.isPlaying)
            {
                instance = this;
            }
            else
            {
#if UNITY_EDITOR
                //Common.PrefabUtility.UnpackPrefabInstance(gameObject);
                lastUnitsPerPixel = unitsPerPixel;
#endif
            }
        }

        private void Start()
        {
            if (Application.isPlaying)
            {
                if (HMDPoseTracker.Instance != null)
                {
                    leftCamera = XRMan.Eyes.Left != null ? XRMan.Eyes.Left : null;
                    rightCamera = XRMan.Eyes.Right != null ? XRMan.Eyes.Right : null;
                    centerCamera = XRMan.Eyes.Center != null ? XRMan.Eyes.Center : null;
                    headTransform = HMDPoseTracker.Instance.transform;
                }

                if (leftCamera == null)
                {
                    Debug.LogError("leftCamera 不存在!");
                }
                if (rightCamera == null)
                {
                    Debug.LogError("rightCamera 不存在!");
                }
                if (centerCamera == null)
                {
                    Debug.LogError("centerCamera 不存在!");
                }
            }
        }

#if UNITY_EDITOR
        public void PerformReCalculateAllSize(float newUnitsPerPixel)
        {
            if (ReCalculateAllSize != null)
            {
                ReCalculateAllSize(newUnitsPerPixel, true);
            }
        }
#endif
    }
}