﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.UI
{

    public class Spatial
    {
        public enum InteractiveMode
        {
            /// <summary>
            /// 未定义
            /// </summary>
            None,
            /// <summary>
            /// 近距离
            /// </summary>
            Touch,
            /// <summary>
            /// 远距离
            /// </summary>
            Raycast
        }
    }
}