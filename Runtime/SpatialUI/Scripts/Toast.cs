using EZXR.Glass.Runtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace EZXR.Glass.UI
{
    public class Toast : MonoBehaviour
    {
        //static Toast instance;
        //public static Toast Instance
        //{
        //    get
        //    {
        //        if (instance == null)
        //        {
        //            instance = Instantiate(Resources.Load<Toast>("Toast"));
        //        }
        //        return instance;
        //    }
        //}
#if TMP_PRESENT
        public TextMeshProUGUI content;
#endif
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public static void Show(string message)
        {
            Utilities.Android.SendIntent("ToSystem", "com.ezxr.glass.systemui", "Toast", message);

            //Instance.content.text = message;
            //Instance.gameObject.SetActive(true);
            //Instance.StopAllCoroutines();
            //Instance.StartCoroutine(Instance.WaitThenHide(duration));
        }

        //IEnumerator WaitThenHide(float duration)
        //{
        //    yield return new WaitForSeconds(duration);
        //    Instance.gameObject.SetActive(false);
        //}
    }
}