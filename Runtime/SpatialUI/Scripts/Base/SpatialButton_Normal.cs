﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using EZXR.Glass.Inputs;
using UnityEditor;
using EZXR.Glass.Runtime;

namespace EZXR.Glass.UI
{
    /// <summary>
    /// 切换显示按钮背景图或颜色
    /// </summary>
    [ExecuteInEditMode]
    public class SpatialButton_Normal : SpatialButton
    {
        public enum Transition
        {
            None,
            ColorTint,
            TextureSwap,
        }
        public Transition transition;

        //public Texture2D normalTexture;
        public Texture2D hoverTexture;
        public Texture2D pressedTexture;
        public Texture2D disabledTexture;

        //public Color normalColor = Color.white;
        public Color hoverColor = Color.white;
        public Color pressedColor = Color.white;
        public Color disabledColor = Color.white;

        protected void SetMaterialOrColor(Texture2D texture, Color color)
        {
            switch (transition)
            {
                case Transition.ColorTint:
                    SetColor(color);
                    break;
                case Transition.TextureSwap:
                    SetMaterial(texture);
                    break;
            }
        }

        void SetMaterial(Texture2D texture)
        {
            //按钮图换成hover状态
            if (texture != null)
            {
                materialPropertyBlock.SetTexture("_MainTex", texture);
            }
            else
            {
                //重置materialPropertyBlock，因为无法直接SetTexture为null
                materialPropertyBlock.Clear();

            }

            _meshRenderer.SetPropertyBlock(materialPropertyBlock);
        }

        void SetColor(Color color)
        {
            materialPropertyBlock.SetColor("_Color", color);

            _meshRenderer.SetPropertyBlock(materialPropertyBlock);
        }

        /// <inheritdoc />
        public override void OnPointerHoverEnter(Spatial.InteractiveMode interactiveMode = Spatial.InteractiveMode.None)
        {
            base.OnPointerHoverEnter(interactiveMode);
            SetMaterialOrColor(hoverTexture, hoverColor);
        }

        public override void OnPointerHoverExit()
        {
            base.OnPointerHoverExit();
            SetMaterialOrColor(Texture, MainColor);
        }

        /// <inheritdoc />
        public override void OnPointerStartPress()
        {
            base.OnPointerStartPress();
            SetMaterialOrColor(pressedTexture, pressedColor);
        }

        /// <inheritdoc />
        public override void OnPointerUp()
        {
            base.OnPointerUp();
            SetMaterialOrColor(hoverTexture, hoverColor);
        }

    }
}