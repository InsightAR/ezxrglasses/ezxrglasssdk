﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using EZXR.Glass.Inputs;
using UnityEditor;
using EZXR.Glass.Runtime;

namespace EZXR.Glass.UI
{
    /// <summary>
    /// Hover切换显示，按钮背景图放大或缩小
    /// </summary>
    [ExecuteInEditMode]
    public class SpatialButton_NormalZoomBG : SpatialButton_Normal
    {

        Vector3 oriScale;

        // Start is called before the first frame update
        protected override void Start()
        {
            base.Start();

            oriScale = _mesh.localScale;
        }

        protected override void Update()
        {
            base.Update();


        }

        /// <inheritdoc />
        public override void OnPointerHoverEnter(Spatial.InteractiveMode interactiveMode = Spatial.InteractiveMode.None)
        {
            base.OnPointerHoverEnter(interactiveMode);

            _mesh.localScale = 1.2f * oriScale;
        }

        /// <inheritdoc />
        public override void OnPointerHoverExit()
        {
            base.OnPointerHoverExit();

            _mesh.localScale = oriScale;
        }

        /// <inheritdoc />
        public override void OnPointerStartPress()
        {
            base.OnPointerStartPress();

            _mesh.localScale = oriScale;
        }

        /// <inheritdoc />
        public override void OnPointerEndPress()
        {
            base.OnPointerEndPress();

            _mesh.localScale = 1.2f * oriScale;
        }

    }
}