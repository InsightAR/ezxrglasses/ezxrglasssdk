﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZXR.Glass.Inputs;

namespace EZXR.Glass.UI
{
    public abstract class SpatialSelectable : SpatialUIElement
    {
        /// <summary>
        /// 是否启用导航
        /// </summary>
        public bool navigation;
        /// <summary>
        /// 当此元素出现时将作为当前被选中的元素继续进行导航
        /// </summary>
        public bool beFocusedWhenEnabled;
        /// <summary>
        /// 在此元素被设为被选中元素时，记录之前被选中的元素，以便此元素被Disabled的时候将focus权交会
        /// </summary>
        SpatialSelectable lastFocusedElement;
        public SpatialSelectable navigationUp;
        public SpatialSelectable navigationDown;
        public SpatialSelectable navigationLeft;
        public SpatialSelectable navigationRight;

        public abstract void OnRayCastHit(InputInfoBase handInfo);

        protected override void OnEnable()
        {
            base.OnEnable();

            if (Application.isPlaying)
            {
                if (beFocusedWhenEnabled)
                {
                    lastFocusedElement = SpatialUIEventSystem.Instance.CurrentFocusSpatialUIElement;
                    SpatialUIEventSystem.Instance.CurrentFocusSpatialUIElement = this;
                    SpatialUIEventSystem.Instance.CurrentFocusSpatialUIElement.OnPointerHoverEnter();
                }
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            if (SpatialUIEventSystem.Instance != null && SpatialUIEventSystem.Instance.CurrentFocusSpatialUIElement != null && lastFocusedElement != null)
            {
                SpatialUIEventSystem.Instance.CurrentFocusSpatialUIElement.OnPointerHoverExit();
                if (beFocusedWhenEnabled)
                {
                    SpatialUIEventSystem.Instance.CurrentFocusSpatialUIElement = lastFocusedElement;
                    lastFocusedElement = null;
                }
            }
        }

        #region Pointer Event
        /// <summary>
        /// 近距离或射线操作时，近距离操作时用户食指指尖进入了Button上方或者离开了Button上方，射线操作时触碰到了Button上表面
        /// </summary>
        public virtual void OnPointerHoverEnter(Spatial.InteractiveMode interactiveMode = Spatial.InteractiveMode.None)
        {
            Debug.Log("OnPointerHoverEnter");
            if (SpatialUIEventSystem.Instance.CurrentHoverSpatialUIElement != null)
            {
                SpatialUIEventSystem.Instance.CurrentHoverSpatialUIElement.OnPointerHoverExit();
            }
            SpatialUIEventSystem.Instance.CurrentHoverSpatialUIElement = this;
        }
        /// <summary>
        /// 近距离操作时，指尖触碰到了Button上表面时触发；远距离操作时，射线在Button上捏合手指时触发，将会同时顺序触发OnPointerStartPress和OnPointerDown）
        /// </summary>
        public virtual void OnPointerStartPress()
        {
            Debug.Log("OnPointerStartPress");
        }
        /// <summary>
        /// 近距离或射线操作时，近距离操作时Button被按下但是没有到底时触发（每帧），射线操作时Button被按下时触发（每帧）
        /// </summary>
        public virtual void OnPointerPressing()
        {
            //Debug.Log("OnPointerPressing");
        }
        /// <summary>
        /// 近距离或射线操作时，Button被按到底时触发（对于射线，将会同时顺序触发OnPointerStartPress和OnPointerDown）
        /// </summary>
        public virtual void OnPointerDown()
        {
            Debug.Log("OnPointerDown");
        }
        /// <summary>
        /// 近距离或射线操作时，Button被完全松开时触发（对于射线，将会同时顺序触发OnPointerUp和OnPointerEndPress）
        /// </summary>
        public virtual void OnPointerUp()
        {
            Debug.Log("OnPointerUp");
        }
        /// <summary>
        /// 近距离或射线操作时，指尖或射线离开了Button上表面（对于射线，将会同时顺序触发OnPointerUp和OnPointerEndPress）
        /// </summary>
        public virtual void OnPointerEndPress()
        {
            Debug.Log("OnPointerEndPress");
        }
        /// <summary>
        /// 近距离或射线操作时，触发了一次Button点击
        /// </summary>
        public virtual void OnPointerClicked()
        {
            Debug.Log("OnPointerClicked");
        }
        /// <summary>
        /// 近距离或射线操作时，近距离操作时用户食指指尖离开了Button上方或者离开了Button上方，射线操作时离开了Button上表面
        /// </summary>
        public virtual void OnPointerHoverExit()
        {
            Debug.Log("OnPointerHoverExit");
            SpatialUIEventSystem.Instance.CurrentHoverSpatialUIElement = null;
        }
        #endregion
    }
}