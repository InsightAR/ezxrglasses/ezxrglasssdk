﻿using EZXR.Glass.Inputs;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace EZXR.Glass.UI
{
    public class SpatialPanel : SpatialUIElement
    {
        protected override void OnValidate()
        {
            GetComponent<LayoutGroup>()?.OnValidate();
        }

        protected override void Awake()
        {
            base.Awake();
        }

        // Update is called once per frame
        protected override void Update()
        {
            base.Update();

            if (Application.isPlaying)
            {

            }
            else
            {
#if UNITY_EDITOR
                _meshRenderer.gameObject.SetActive(Texture != null);
#endif
            }
        }

    }
}