﻿using EZXR.Glass.Inputs;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace EZXR.Glass.UI
{
    public class SpatialImage : SpatialUIElement
    {
        public enum ImageType
        {
            Simple,
            Filled
        }
        public ImageType imageType;

        protected override void Awake()
        {
            base.Awake();
        }

        protected override void OnDestroy()
        {

        }

        // Update is called once per frame
        protected override void Update()
        {
            base.Update();

        }
    }
}