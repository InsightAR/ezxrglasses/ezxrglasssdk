﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZXR.Glass.Inputs;
using EZXR.Glass.Runtime;

namespace EZXR.Glass.UI
{
    [ExecuteInEditMode]
    public partial class SpatialUIEventSystem : MonoBehaviour
    {
        #region Singleton
        private static SpatialUIEventSystem instance;
        public static SpatialUIEventSystem Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<SpatialUIEventSystem>();
                    if (instance == null)
                    {
                        GameObject go = new GameObject("SpatialUIEventSystem");
                        instance = go.AddComponent<SpatialUIEventSystem>();
                    }
                }
                return instance;
            }
        }
        #endregion

        static Dictionary<Collider, SpatialSelectable> selectablesDic = new Dictionary<Collider, SpatialSelectable>();
        Collider lastOne_Left, lastOne_Right;

        private SpatialSelectable currentFocusSpatialUIElement;
        public SpatialSelectable CurrentFocusSpatialUIElement
        {
            get
            {
                return currentFocusSpatialUIElement;
            }
            set
            {
                currentFocusSpatialUIElement = value;
            }
        }

        private SpatialSelectable currentHoverSpatialUIElement;
        public SpatialSelectable CurrentHoverSpatialUIElement
        {
            get
            {
                return currentHoverSpatialUIElement;
            }
            set
            {
                currentHoverSpatialUIElement = value;
            }
        }

        partial void VolumeNavigateInit();

        private void Awake()
        {
            VolumeNavigateInit();

            if (Application.isPlaying)
            {
                instance = this;
            }
            else
            {
//#if UNITY_EDITOR
//                Common.PrefabUtility.UnpackPrefabInstance(gameObject);
//#endif
            }
        }

        void Start()
        {
            if (Application.isPlaying)
            {
                //用于得到当前手的射线检测的结果
                if (XRMan.Hands.Left != null)
                {
                    XRMan.Hands.Left.Event_GetRayCastResult += OnRayCastHit_Left;
                }
                if (XRMan.Hands.Right != null)
                {
                    XRMan.Hands.Right.Event_GetRayCastResult += OnRayCastHit_Right;
                }
                if (XRMan.Controllers.Left != null)
                {
                    XRMan.Controllers.Left.Event_GetRayCastResult += OnRayCastHit_Left;
                }
                if (XRMan.Controllers.Right != null)
                {
                    XRMan.Controllers.Right.Event_GetRayCastResult += OnRayCastHit_Right;
                }
            }
        }

        private void Update()
        {
            //if (Input.GetKeyDown(UnityEngine.KeyCode.Return))
            //{
            //    currentFocusSpatialUIElement?.OnPointerClicked();
            //}
        }

        public static void RegisterCallBack(Collider collider, SpatialSelectable selectable)
        {
            if (!selectablesDic.ContainsKey(collider))
            {
                selectablesDic.Add(collider, selectable);
            }
        }

        public void OnRayCastHit_Left(Collider other, bool isUI)
        {
            if (lastOne_Left != null && lastOne_Left != other)
            {
                if (selectablesDic.ContainsKey(lastOne_Left))
                {
                    selectablesDic[lastOne_Left].OnRayCastHit(null);
                }
            }

            if (isUI && other != null)
            {
                if (selectablesDic.ContainsKey(other))
                {
                    selectablesDic[other].OnRayCastHit(XRMan.Input.Left);
                }
            }

            lastOne_Left = other;
        }

        public void OnRayCastHit_Right(Collider other, bool isUI)
        {
            if (lastOne_Right != null && lastOne_Right != other)
            {
                if (selectablesDic.ContainsKey(lastOne_Right))
                {
                    selectablesDic[lastOne_Right].OnRayCastHit(null);
                }
            }

            if (isUI && other != null)
            {
                if (selectablesDic.ContainsKey(other))
                {
                    selectablesDic[other].OnRayCastHit(XRMan.Input.Right);
                }
            }

            lastOne_Right = other;
        }
    }
}
