﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using EZXR.Glass.Inputs;
using System;

namespace EZXR.Glass.UI
{
    [CustomEditor(typeof(SpatialButton))]
    [CanEditMultipleObjects]
    public class SpatialButtonInspector : SpatialSelectableInspector
    {
        private SpatialButton _target_SpatialButtonInspector;

        SerializedProperty preTriggerZoneThickness;
        SerializedProperty rearTriggerZoneThickness;
        SerializedProperty useEnterKey;

        SerializedProperty m_DelegatesProperty;

        GUIContent m_IconToolbarMinus;
        GUIContent m_EventIDName;
        GUIContent[] m_EventTypes;
        GUIContent m_AddButonContent;

        protected void Awake()
        {
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            _target_SpatialButtonInspector = target as SpatialButton;

            preTriggerZoneThickness = serializedObject.FindProperty("preTriggerZoneThickness");
            rearTriggerZoneThickness = serializedObject.FindProperty("rearTriggerZoneThickness");
            useEnterKey = serializedObject.FindProperty("useEnterKey");

            m_DelegatesProperty = serializedObject.FindProperty("m_Delegates");
            m_AddButonContent = EditorGUIUtility.TrTextContent("Add New Event Type");
            m_EventIDName = new GUIContent("");
            // Have to create a copy since otherwise the tooltip will be overwritten.
            m_IconToolbarMinus = new GUIContent(EditorGUIUtility.IconContent("Toolbar Minus"));
            m_IconToolbarMinus.tooltip = "Remove all events in this list.";

            string[] eventNames = Enum.GetNames(typeof(SpatialButton.SpatialButtonEventType));
            m_EventTypes = new GUIContent[eventNames.Length];
            for (int i = 0; i < eventNames.Length; ++i)
            {
                m_EventTypes[i] = new GUIContent(eventNames[i]);
            }

            if (m_DelegatesProperty.arraySize == 0)
            {
                OnAddNewSelected(6);
            }
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            serializedObject.Update();

            if (_target_SpatialButtonInspector.size.z <= _target_SpatialButtonInspector.heightMin)
            {
                _target_SpatialButtonInspector.size = new Vector3(_target_SpatialButtonInspector.size.x, _target_SpatialButtonInspector.size.y, _target_SpatialButtonInspector.heightMin * 2);
            }
            EditorGUILayout.PropertyField(preTriggerZoneThickness, new GUIContent("前置触发区厚度（单位：m）"));
            EditorGUILayout.PropertyField(rearTriggerZoneThickness, new GUIContent("后置触发区厚度（单位：m）"));
            EditorGUILayout.PropertyField(useEnterKey, new GUIContent("响应虚拟键盘的Enter键"));

            #region Event
            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            {
                int toBeRemovedEntry = -1;

                Vector2 removeButtonSize = GUIStyle.none.CalcSize(m_IconToolbarMinus);

                for (int i = 0; i < m_DelegatesProperty.arraySize; ++i)
                {
                    //EditorGUILayout.Space();

                    SerializedProperty delegateProperty = m_DelegatesProperty.GetArrayElementAtIndex(i);
                    SerializedProperty eventProperty = delegateProperty.FindPropertyRelative("eventID");
                    SerializedProperty callbacksProperty = delegateProperty.FindPropertyRelative("callback");
                    m_EventIDName.text = eventProperty.enumDisplayNames[eventProperty.enumValueIndex];

                    EditorGUILayout.PropertyField(callbacksProperty, m_EventIDName);
                    Rect callbackRect = GUILayoutUtility.GetLastRect();

                    Rect removeButtonPos = new Rect(callbackRect.xMax - removeButtonSize.x - 8, callbackRect.y + 1, removeButtonSize.x, removeButtonSize.y);
                    if (GUI.Button(removeButtonPos, m_IconToolbarMinus, GUIStyle.none))
                    {
                        toBeRemovedEntry = i;
                    }
                }

                if (toBeRemovedEntry > -1)
                {
                    RemoveEntry(toBeRemovedEntry);
                }

                Rect btPosition = GUILayoutUtility.GetRect(m_AddButonContent, GUI.skin.button);
                const float addButonWidth = 200f;
                btPosition.x = btPosition.x + (btPosition.width - addButonWidth) / 2;
                btPosition.width = addButonWidth;
                if (GUI.Button(btPosition, m_AddButonContent))
                {
                    ShowAddTriggermenu();
                }
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();
            #endregion

            serializedObject.ApplyModifiedProperties();

            SceneView.RepaintAll();
        }

        private void RemoveEntry(int toBeRemovedEntry)
        {
            m_DelegatesProperty.DeleteArrayElementAtIndex(toBeRemovedEntry);
        }

        void ShowAddTriggermenu()
        {
            // Now create the menu, add items and show it
            GenericMenu menu = new GenericMenu();
            for (int i = 0; i < m_EventTypes.Length; ++i)
            {
                bool active = true;

                // Check if we already have a Entry for the current eventType, if so, disable it
                for (int p = 0; p < m_DelegatesProperty.arraySize; ++p)
                {
                    SerializedProperty delegateEntry = m_DelegatesProperty.GetArrayElementAtIndex(p);
                    SerializedProperty eventProperty = delegateEntry.FindPropertyRelative("eventID");
                    if (eventProperty.enumValueIndex == i)
                    {
                        active = false;
                    }
                }
                if (active)
                    menu.AddItem(m_EventTypes[i], false, OnAddNewSelected, i);
                else
                    menu.AddDisabledItem(m_EventTypes[i]);
            }
            menu.ShowAsContext();
            Event.current.Use();
        }

        private void OnAddNewSelected(object index)
        {
            int selected = (int)index;

            m_DelegatesProperty.arraySize += 1;
            SerializedProperty delegateEntry = m_DelegatesProperty.GetArrayElementAtIndex(m_DelegatesProperty.arraySize - 1);
            SerializedProperty eventProperty = delegateEntry.FindPropertyRelative("eventID");
            eventProperty.enumValueIndex = selected;
            serializedObject.ApplyModifiedProperties();
        }

    }
}