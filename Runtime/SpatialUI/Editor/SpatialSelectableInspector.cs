﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using EZXR.Glass.Inputs;
using System;

namespace EZXR.Glass.UI
{
    [CustomEditor(typeof(SpatialSelectable))]
    [CanEditMultipleObjects]
    public class SpatialSelectableInspector : SpatialUIElementInspector
    {
        private SpatialSelectable _target_SpatialSelectableInspector;

        SerializedProperty navigation;
        SerializedProperty beFocusedWhenEnabled;
        SerializedProperty navigationUp;
        SerializedProperty navigationDown;
        SerializedProperty navigationLeft;
        SerializedProperty navigationRight;

        protected void Awake()
        {
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            _target_SpatialSelectableInspector = target as SpatialSelectable;

            navigation = serializedObject.FindProperty("navigation");
            beFocusedWhenEnabled = serializedObject.FindProperty("beFocusedWhenEnabled");
            navigationUp = serializedObject.FindProperty("navigationUp");
            navigationDown = serializedObject.FindProperty("navigationDown");
            navigationLeft = serializedObject.FindProperty("navigationLeft");
            navigationRight = serializedObject.FindProperty("navigationRight");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            serializedObject.Update();

            EditorGUILayout.PropertyField(navigation, new GUIContent("启用按键导航"));
            if (_target_SpatialSelectableInspector.navigation)
            {
                EditorGUILayout.PropertyField(beFocusedWhenEnabled, new GUIContent("BeFocusedWhenEnabled", "当此元素出现时将作为当前被选中的元素继续进行导航"));
                EditorGUILayout.PropertyField(navigationUp, new GUIContent("    Up"));
                EditorGUILayout.PropertyField(navigationDown, new GUIContent("    Down"));
                EditorGUILayout.PropertyField(navigationLeft, new GUIContent("    Left"));
                EditorGUILayout.PropertyField(navigationRight, new GUIContent("    Right"));
            }


            serializedObject.ApplyModifiedProperties();

            SceneView.RepaintAll();
        }


    }
}