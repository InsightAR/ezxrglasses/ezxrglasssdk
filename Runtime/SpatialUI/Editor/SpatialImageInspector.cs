﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace EZXR.Glass.UI
{
    [CustomEditor(typeof(SpatialImage))]
    [CanEditMultipleObjects]
    public class SpatialImageInspector : SpatialUIElementInspector
    {
        SpatialImage _target_SpatialImage;

        protected override void OnEnable()
        {
            base.OnEnable();

            _target_SpatialImage = target as SpatialImage;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            serializedObject.Update();

            serializedObject.ApplyModifiedProperties();

            SceneView.RepaintAll();
        }
    }
}