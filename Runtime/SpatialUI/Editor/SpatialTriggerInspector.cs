﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace EZXR.Glass.UI
{
    [CustomEditor(typeof(SpatialTrigger))]
    [CanEditMultipleObjects]
    public class SpatialTriggerInspector : SpatialSelectableInspector
    {
        private SpatialTrigger _target_SpatialTriggerInspector;

        SerializedProperty onTriggerEnter;

        protected override void OnEnable()
        {
            base.OnEnable();

            _target_SpatialTriggerInspector = target as SpatialTrigger;

            onTriggerEnter = serializedObject.FindProperty("onTriggerEnter");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            serializedObject.Update();

            EditorGUILayout.PropertyField(onTriggerEnter);

            serializedObject.ApplyModifiedProperties();

            SceneView.RepaintAll();
        }
    }
}