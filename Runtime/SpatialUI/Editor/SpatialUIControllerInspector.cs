﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace EZXR.Glass.UI
{
    [CustomEditor(typeof(SpatialUIController))]
    [CanEditMultipleObjects]
    public class SpatialUIControllerInspector : Editor
    {
        private SpatialUIController _target;

        protected void OnEnable()
        {
            _target = target as SpatialUIController;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.HelpBox("图片中的每个像素对应Unity中的多少个Unit（以Cube的默认边长为1个Unit）", MessageType.Info);
            EditorGUILayout.BeginHorizontal();
            {
                //EditorGUI.BeginChangeCheck();
                _target.unitsPerPixel = EditorGUILayout.FloatField("UnitsPerPixel", _target.unitsPerPixel);

                //if (EditorGUI.EndChangeCheck())
                if (GUILayout.Button("Apply"))
                {
                    if (_target.unitsPerPixel <= 0)
                    {
                        EditorUtility.DisplayDialog("提示", "必须是大于0的数字", "OK");
                        _target.unitsPerPixel = _target.lastUnitsPerPixel;
                    }
                    else
                    {
                        _target.lastUnitsPerPixel = _target.unitsPerPixel;
                        _target.PerformReCalculateAllSize(_target.unitsPerPixel);
                        EditorApplication.QueuePlayerLoopUpdate();
                    }
                }
            }
            EditorGUILayout.EndHorizontal();

            serializedObject.ApplyModifiedProperties();

            SceneView.RepaintAll();
        }
    }
}