﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace EZXR.Glass.UI
{
    [CustomEditor(typeof(SpatialButton_Normal))]
    [CanEditMultipleObjects]
    public class SpatialButtonNormalInspector : SpatialButtonInspector
    {
        private SpatialButton_Normal _target_SpatialButtonNormal;

        SerializedProperty transition;

        //SerializedProperty normalTexture;
        SerializedProperty hoverTexture;
        SerializedProperty pressedTexture;
        SerializedProperty disabledTexture;

        //SerializedProperty normalColor;
        SerializedProperty hoverColor;
        SerializedProperty pressedColor;
        SerializedProperty disabledColor;

        //SerializedProperty positionType;
        //SerializedProperty size;
        //SerializedProperty text;
        //SerializedProperty pressed;
        ////SerializedProperty defaultMaterial;
        ////SerializedProperty sharedMaterial;
        //SerializedProperty color;
        //SerializedProperty texture;
        //SerializedProperty preTriggerZoneThickness;
        //SerializedProperty rearTriggerZoneThickness;
        //SerializedProperty states;
        //SerializedProperty clicked;
        //SerializedProperty sortingOrder;

        protected override void OnEnable()
        {
            base.OnEnable();

            _target_SpatialButtonNormal = target as SpatialButton_Normal;

            transition = serializedObject.FindProperty("transition");
            //normalTexture = serializedObject.FindProperty("normalTexture");
            hoverTexture = serializedObject.FindProperty("hoverTexture");
            pressedTexture = serializedObject.FindProperty("pressedTexture");
            disabledTexture = serializedObject.FindProperty("disabledTexture");
            //normalColor = serializedObject.FindProperty("normalColor");
            hoverColor = serializedObject.FindProperty("hoverColor");
            pressedColor = serializedObject.FindProperty("pressedColor");
            disabledColor = serializedObject.FindProperty("disabledColor");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            serializedObject.Update();

            EditorGUILayout.PropertyField(transition);

            switch (_target_SpatialButtonNormal.transition)
            {
                case SpatialButton_Normal.Transition.TextureSwap:
                    EditorGUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.Space(10, false);
                        EditorGUILayout.BeginVertical();
                        {
                            //EditorGUILayout.PropertyField(normalTexture);
                            EditorGUILayout.PropertyField(hoverTexture);
                            EditorGUILayout.PropertyField(pressedTexture);
                            EditorGUILayout.PropertyField(disabledTexture);
                        }
                        EditorGUILayout.EndVertical();
                    }
                    EditorGUILayout.EndHorizontal();
                    break;
                case SpatialButton_Normal.Transition.ColorTint:
                    EditorGUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.Space(10, false);
                        EditorGUILayout.BeginVertical();
                        {
                            //EditorGUILayout.PropertyField(normalColor);
                            EditorGUILayout.PropertyField(hoverColor);
                            EditorGUILayout.PropertyField(pressedColor);
                            EditorGUILayout.PropertyField(disabledColor);
                        }
                        EditorGUILayout.EndVertical();
                    }
                    EditorGUILayout.EndHorizontal();
                    break;
            }

            serializedObject.ApplyModifiedProperties();

            SceneView.RepaintAll();
        }
    }
}