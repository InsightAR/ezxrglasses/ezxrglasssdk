﻿using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace EZXR.Glass.Runtime
{
    public class ScriptExecutionOrder : Attribute
    {
#if SYSTEMUI
        int baseOrder = -1000;
#else
        int baseOrder = 0;
#endif
        public int order;
        public ScriptExecutionOrder(int order) { this.order = baseOrder + order; }
    }

#if UNITY_EDITOR
    [InitializeOnLoad]
    public class ScriptExecutionOrderManager
    {
        static ScriptExecutionOrderManager()
        {
            foreach (MonoScript monoScript in MonoImporter.GetAllRuntimeMonoScripts())
            {
                if (monoScript.GetClass() != null)
                {
                    foreach (var attr in Attribute.GetCustomAttributes(monoScript.GetClass(), typeof(ScriptExecutionOrder)))
                    {
                        var newOrder = ((ScriptExecutionOrder)attr).order;
                        if (MonoImporter.GetExecutionOrder(monoScript) != newOrder)
                            MonoImporter.SetExecutionOrder(monoScript, newOrder);
                    }
                }
            }
        }
    }
#endif

    /*
    // USAGE EXAMPLE
    [ScriptExecutionOrder(100)]
    public class MyScript : MonoBehaviour
    {
            // ...
    }
    */
}