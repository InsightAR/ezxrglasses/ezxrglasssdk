﻿using EZXR.Glass.SixDof;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wheels.Unity;

namespace EZXR.Glass.Runtime
{
    public static class RecenterUtility
    {
        public enum Space
        {
            /// <summary>
            /// 身体坐标系（人站立时胸口朝向的方向为z，重力反方向为y，身体右侧为x）
            /// </summary>
            BodySpace,
            /// <summary>
            /// 头部坐标系（头朝向的方向为z，头顶方向为y，头右侧为x）
            /// </summary>
            HeadSpace,
        }

        /// <summary>
        /// 恢复所有Recenter过的物体的初始相对Head的Pose
        /// </summary>
        public static Dictionary<Transform, Pose> initialPose = new Dictionary<Transform, Pose>();
        /// <summary>
        /// Recenter的历史记录，用于重置坐标系的时候将之前Recenter过的所有物体重新Recenter
        /// </summary>
        public static Dictionary<Transform, KeyValuePair<Vector3, Space>> recenterHistory = new Dictionary<Transform, KeyValuePair<Vector3, Space>>();

        /// <summary>
        /// 重置到视野前
        /// </summary>
        /// <param name="self"></param>
        /// <param name="distance">新位置在Head前向的距离</param>
        /// <param name="space">计算相对于Head的偏移量时用的坐标系</param>
        public static void Recenter(this Transform self, float distance, Space space)
        {
            Recenter(self, distance, 0, space);
        }

        /// <summary>
        /// 重置到视野前
        /// </summary>
        /// <param name="self"></param>
        /// <param name="distance">新位置在Head前向的距离</param>
        /// <param name="angle">Head前向与目标方向的夹角大于此值才会Recenter</param>
        /// <param name="space">计算相对于Head的偏移量时用的坐标系</param>
        public static void Recenter(this Transform self, float distance, float angle = 0, Space space = Space.BodySpace)
        {
            //if (Vector3.Angle(self.position - HMDPoseTracker.Instance.Head.transform.position, HMDPoseTracker.Instance.Head.transform.forward) >= angle)
            //{
            //    SpatialAnchor spatialAnchor = self.GetComponent<SpatialAnchor>();

            //    Vector3 offset;
            //    if (space == Space.BodySpace && (spatialAnchor == null || (spatialAnchor != null && spatialAnchor.mode != SpatialAnchor.Mode.ZeroDof)))
            //    {
            //        offset = Vector3.ProjectOnPlane(HMDPoseTracker.Instance.Head.transform.forward, Vector3.up).normalized * distance;
            //    }
            //    else
            //    {
            //        offset = HMDPoseTracker.Instance.Head.transform.forward * distance;
            //    }

            Recenter(self, Vector3.forward * distance, angle, space);
            //}
        }

        /// <summary>
        /// 重置到视野前相对于Head偏移offset的位置
        /// </summary>
        /// <param name="self"></param>
        /// <param name="offset">相对于Head的偏移量</param>
        /// <param name="angle">Head前向与目标方向的夹角大于此值才会Recenter</param>
        /// <param name="space">计算相对于Head的偏移量时用的坐标系</param>
        public static void Recenter(this Transform self, Vector3 offset, Space space)
        {
            Recenter(self, offset, 0, space);
        }

        /// <summary>
        /// 重置到视野前相对于Head偏移offset的位置
        /// </summary>
        /// <param name="self"></param>
        /// <param name="offset">相对于Head的偏移量</param>
        /// <param name="angle">Head前向与目标方向的夹角大于此值才会Recenter</param>
        /// <param name="space">计算相对于Head的偏移量时用的坐标系</param>
        public static void Recenter(this Transform self, Vector3 offset, float angle = 0, Space space = Space.BodySpace)
        {
            if (Vector3.Angle(self.position - HMDPoseTracker.Instance.Head.transform.position, HMDPoseTracker.Instance.Head.transform.forward) >= angle)
            {
                recenterHistory[self] = new KeyValuePair<Vector3, Space>(offset, space);

                if (!initialPose.ContainsKey(self))
                {
                    initialPose.Add(self, new Pose(HMDPoseTracker.Instance.Head.InverseTransformPoint(self.position), Quaternion.Inverse(HMDPoseTracker.Instance.Head.rotation) * self.rotation));
                }

                SpatialAnchor spatialAnchor = self.GetComponent<SpatialAnchor>();

                Vector3 newPosition;
                if (space == Space.BodySpace && (spatialAnchor == null || (spatialAnchor != null && spatialAnchor.mode != SpatialAnchor.Mode.ZeroDof)))
                {
                    Vector3 forward = Vector3.ProjectOnPlane(HMDPoseTracker.Instance.Head.transform.forward, Vector3.up).normalized;
                    Vector3 up = Vector3.up;
                    Vector3 right = Vector3.Cross(forward, up);
                    newPosition = HMDPoseTracker.Instance.Head.transform.position + forward * offset.z + Vector3.up * offset.y + right * offset.x;
                    Debug.Log("newPosition BodySpace: " + newPosition.ToString("0.###"));
                }
                else
                {
                    newPosition = HMDPoseTracker.Instance.Head.transform.position + HMDPoseTracker.Instance.Head.transform.TransformVector(offset);
                    Debug.Log("newPosition HeadSpace: " + newPosition.ToString("0.###"));
                }

                if (spatialAnchor != null)
                {
                    if (spatialAnchor.mode == SpatialAnchor.Mode.ZeroDof)
                    {
                        spatialAnchor.SetPositionAndRotation(newPosition, HMDPoseTracker.Instance.Head.rotation * Quaternion.Euler(0, 180, 0));
                    }
                    else
                    {
                        spatialAnchor.SetPosition(newPosition, HMDPoseTracker.Instance.Head.transform);
                    }
                }
                else
                {
                    self.position = newPosition;
                    self.LookAt(HMDPoseTracker.Instance.Head.transform);
                }
            }
        }

        public static void RecenterAll()
        {
            Dictionary<Transform, KeyValuePair<Vector3, Space>> recenterHistory_Copy = new Dictionary<Transform, KeyValuePair<Vector3, Space>>(recenterHistory);
            foreach (var item in recenterHistory_Copy)
            {
                item.Key.Recenter(recenterHistory_Copy[item.Key].Key, recenterHistory_Copy[item.Key].Value);
            }
        }

        public static void Reset(this Transform self)
        {
            if (initialPose.ContainsKey(self))
            {
                self.position = HMDPoseTracker.Instance.Head.TransformPoint(initialPose[self].position);
                self.rotation = HMDPoseTracker.Instance.Head.rotation * initialPose[self].rotation;
            }
        }

        /// <summary>
        /// 将所有已经Recenter过的窗口全部Reset（尚未执行过Recenter的窗口不生效，因为没执行过Recenter所以不存在于initialPose中）
        /// </summary>
        public static void ResetAll()
        {
            foreach (var item in initialPose)
            {
                item.Key.position = HMDPoseTracker.Instance.Head.TransformPoint(item.Value.position);
                item.Key.rotation = HMDPoseTracker.Instance.Head.rotation * item.Value.rotation;
            }
        }
    }
}