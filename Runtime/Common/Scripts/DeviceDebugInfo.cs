﻿using UnityEngine;
using System;

namespace EZXR.Glass.Runtime
{
    [Serializable]
    public class DeviceDebugInfo
    {
        public float gyroFPS;
        public float accFPS;
        public float gestureTime;
        public float vioTime;
        public float cameFPS;

        public static DebugInfo FromJson(string str)
        {
            return JsonUtility.FromJson<DebugInfo>(str);
        }

        public static string ToJson(DebugInfo debugInfo)
        {
            return JsonUtility.ToJson(debugInfo);
        }
    }
}

