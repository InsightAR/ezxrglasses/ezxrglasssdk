﻿using System;
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    /// @cond UN_DOC
    public class SingletonManager<T> : MonoBehaviour where T : SingletonManager<T>
    {
        private static T _instance;
        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    GameObject go = new GameObject(typeof(T).Name) as GameObject;
                    _instance = go.AddComponent<T>();
                }
                return _instance;
            }
        }

        public void OnDestroy()
        {
            _instance = null;
        }

        public static bool Exists
        {
            get;
            private set;
        }

        protected void Awake()
        {
            DontDestroyOnLoad(gameObject);
            if (_instance == null)
            {
                _instance = (T)this;
                Exists = true;
            }
            else if (_instance != this)
            {
                throw new InvalidOperationException("Can't have two instances of a view");
            }
        }

    }
    /// @endcond
}