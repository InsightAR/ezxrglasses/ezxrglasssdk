using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace EZXR.Glass.Runtime
{
    public class AssetsFileUtils
    {
        public static bool CopyAssetsToApplicationDir(string folderName) {
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject u3d_Activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaClass AssetUtils = new AndroidJavaClass("com.ezxr.ezglassarsdk.utils.AssetUtils");
            bool copyRes = AssetUtils.CallStatic<bool>("copyAssetsToApplicationDir", u3d_Activity, folderName);
            return copyRes;
        }
    }
}
