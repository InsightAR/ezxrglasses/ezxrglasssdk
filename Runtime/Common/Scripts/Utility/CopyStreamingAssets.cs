using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
namespace EZXR.Glass.Runtime
{
    public class CopyStreamingAssets : MonoBehaviour
    {
        public delegate void CopyCompletedDelegate(string assetPath);
        public event CopyCompletedDelegate OnCopyCompleted;

        /* iOS is not necessary 
        public string assetsDirectory_ios = "";
        */
        public string assetsDirectory_android = "";

        public Text m_CopyAssetInfoText;

        private bool isCopyFinished = false;
        IEnumerator Start()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                if (m_CopyAssetInfoText != null) m_CopyAssetInfoText.enabled = true;
                yield return CopyStreamingAssetsToPersistentAndroid(assetsDirectory_android);
                if (isCopyFinished)
                {
                    if (m_CopyAssetInfoText != null) m_CopyAssetInfoText.enabled = false;
                    string destinationPath = Path.Combine(Application.persistentDataPath, assetsDirectory_android);
                    OnCopyCompleted?.Invoke(destinationPath);
                }
                else
                {
                    OnCopyCompleted?.Invoke(null);
                }
            }
            /* iOS is not necessary
            else
            {
                yield return null;
                if(m_CopyAssetInfoText != null) m_CopyAssetInfoText.enabled = false;
                string assetPath = Path.Combine(Application.streamingAssetsPath, assetsDirectory_ios);
                if (Directory.Exists(assetPath))
                {
                    isCopyFinished = true;
                    OnCopyCompleted?.Invoke(assetPath);
                }
                else
                {
                    OnCopyCompleted?.Invoke(null);
                }
            }
            */
        }
        /// <summary>
        /// /// 从streamingAssetsPath复制到应用沙盒目录
        /// </summary>
        /// <param name="folderName"></param>
        /// <returns></returns>
        IEnumerator CopyStreamingAssetsToPersistentAndroid(string folderName)
        {
#if UNITY_ANDROID
            yield return new WaitForEndOfFrame();
            isCopyFinished = AssetsFileUtils.CopyAssetsToApplicationDir(folderName);
#else
            yield return null;
#endif

        }
    }
}
