namespace EZXR.Glass.Device
{
    public struct CameraResolution
    {
        public int width;
        public int height;
    }
}
