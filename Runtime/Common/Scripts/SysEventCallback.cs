﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    public class SysEventCallback : AndroidJavaProxy
    {
        public event Action<PermissionInfo[], int> callBack_OnPermissionEvent;
        public event Action<string, int> callBack_OnToast;
        public event Action callBack_OnProjectionDetected;
        public event Action callBack_OnGlassOn;
        public event Action callBack_OnGlassOff;

        public SysEventCallback() : base("com.ezxr.glass.nativelib.callback.SysEventCallback")
        {
        }

        public SysEventCallback(
            Action<PermissionInfo[], int> callBack_OnPermissionEvent = null,
            Action<string, int> callBack_OnToast = null,
            Action callBack_OnProjectionDetected = null,
            Action callBack_OnGlassOn = null,
            Action callBack_OnGlassOff = null
            ) : base("com.ezxr.glass.nativelib.callback.SysEventCallback")
        {
            this.callBack_OnPermissionEvent += callBack_OnPermissionEvent;
            this.callBack_OnToast += callBack_OnToast;
            this.callBack_OnProjectionDetected += callBack_OnProjectionDetected;
            this.callBack_OnGlassOn += callBack_OnGlassOn;
            this.callBack_OnGlassOff += callBack_OnGlassOff;
        }

        /// <summary>
        /// 用户请求权限事件,收到该回调请弹窗
        /// </summary>
        /// <param name="permissionInfos"></param>
        /// <param name="requestCode"></param>
        void onPermissionEvent(PermissionInfo[] permissionInfos, int requestCode)
        {
            Debug.Log("SysEventCallback--> onPermissionEvent, permissionInfos.Length:" + permissionInfos.Length + ", requestCode:" + requestCode);

            if (callBack_OnPermissionEvent != null)
            {
                callBack_OnPermissionEvent(permissionInfos, requestCode);
            }
        }

        /// <summary>
        /// 需要弹出toast
        /// </summary>
        /// <param name="content"></param>
        /// <param name="duration"></param>
        void onToast(string content, int duration)
        {
            Debug.Log("SysEventCallback--> onToast, content:" + content + ", duration:" + duration);

            if (callBack_OnToast != null)
            {
                callBack_OnToast(content, duration);
            }
        }

        void onProjectionDetected()
        {
            Debug.Log("SysEventCallback--> onProjectionDetected");

            if (callBack_OnProjectionDetected != null)
            {
                callBack_OnProjectionDetected();
            }
        }
        void onGlassOn()
        {
            Debug.Log("SysEventCallback--> onGlassOn");

            if (callBack_OnGlassOn != null)
            {
                callBack_OnGlassOn();
            }
        }
        void onGlassOff()
        {
            Debug.Log("SysEventCallback--> onGlassOff");

            if (callBack_OnGlassOff != null)
            {
                callBack_OnGlassOff();
            }
        }
    }
}