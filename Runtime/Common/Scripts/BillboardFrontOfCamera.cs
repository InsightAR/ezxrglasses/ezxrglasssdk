using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    public class BillboardFrontOfCamera : MonoBehaviour
    {
        public Transform head;
        public float initialDistance;

        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {

        }
        private void LateUpdate()
        {
            if (head == null) {
                SixDof.HMDPoseTracker hmd = FindObjectOfType<SixDof.HMDPoseTracker>();
                if (hmd != null){
                    head = hmd.transform;
                }
            }
            if (head != null)
            {
                Vector3 forward = head.forward;
                Vector3 up = head.up;
                this.transform.rotation = Quaternion.LookRotation(forward, up);
                this.transform.position = head.position + forward * initialDistance;
            }
        }
    }
}