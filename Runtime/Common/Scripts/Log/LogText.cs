﻿using System.IO;
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    internal class LogText
    {
        private string ECLogFilePath
        {
            get { return ECLog.LogTextFolder + "/ECLog_" + LogTime.GetDate() + ".txt"; }
        }

        public LogText()
        {
            LogScreen.OnClearButtonClick = OnClear;
        }

        /// <summary>
        /// Write log in text
        /// For editor path is dataPath, and for other platform path is persistentDataPath
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="stackTrace"></param>
        /// <param name="type"></param>
        public void Text(string condition, string stackTrace, LogType type)
        {
            var typeInfo = string.Format("[{0}]", type.ToString());
            var log = typeInfo + LogTime.GetTimeFormat() + condition;
            var sw = File.AppendText(ECLogFilePath);
            sw.WriteLine(log + "\n" + stackTrace);
            sw.Close();
        }

        private void OnClear()
        {
            File.WriteAllText(ECLogFilePath, string.Empty);
        }
    }
}