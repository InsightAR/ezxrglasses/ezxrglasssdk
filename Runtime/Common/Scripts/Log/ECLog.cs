﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    public static class ECLog
    {
        /// <summary>
        /// Set screen log's font size
        /// </summary>
        public static int FontSizeOnScreen = (int)(UnityEngine.Screen.width * 0.02f);

        /// <summary>
        /// Set log's foler
        /// </summary>
        public static string LogTextFolder = Application.persistentDataPath;

        /// <summary>
        /// Instance of view
        /// </summary>
        private static LogScreen _logScreen;
        private static LogText _logText;
        // 过滤tag集合
        private static List<string> s_filterTagList = new List<string>();

        private static List<string> debugLogList = new List<string>(10);
        /// <summary>
        /// Set if log is on
        /// </summary>
        public static void SetOn()
        {
            if (_logScreen == null)
            {
                _logScreen = new GameObject("ECLog").AddComponent<LogScreen>();
                _logText = new LogText();
                Application.logMessageReceivedThreaded += LogCallback;
                Debug.LogFormat("ECLog's file is here[{0}]", LogTextFolder);
            }
            Debug.unityLogger.logEnabled = true;
            _logScreen.enabled = true;
        }

        /// <summary>
        /// Set if log is off
        /// </summary>
        public static void SetOff()
        {
            Debug.unityLogger.logEnabled = false;
            _logScreen.enabled = false;
        }

        public static void Log(string tag, object message)
        {
            // 日志tag是被过滤的
            if (s_filterTagList.Contains(tag))
                return;
            Debug.unityLogger.Log(tag, message);
        }

        public static void LogWarning(string tag, object message)
        {
            // 日志tag是被过滤的
            if (s_filterTagList.Contains(tag))
                return;
            Debug.unityLogger.LogWarning(tag, message);
        }

        public static void LogError(string tag, object message)
        {
            // 日志tag是被过滤的
            if (s_filterTagList.Contains(tag))
                return;
            Debug.unityLogger.LogError(tag, message);
        }


        /// <summary>
        /// 清除所有屏蔽tag
        /// </summary>
        public static void ClearFilterTag()
        {
            s_filterTagList.Clear();
        }
        /// <summary>
        /// 增加屏蔽tag
        /// </summary>
        /// <param name="tag">tag</param>
        public static void AddFilterTag(string tag)
        {
            // 不加重复的tag
            if (s_filterTagList.Contains(tag))
                return;
            s_filterTagList.Add(tag);
        }
        /// <summary>
        /// 移除屏蔽tag
        /// </summary>
        /// <param name="tag">tag</param>
        public static void RemoveFilterTag(string tag)
        {
            if (s_filterTagList.Contains(tag))
                s_filterTagList.Remove(tag);
        }

        public static void SaveFile(byte[] bytes, string fileName)
        {
            string filePath = Path.Combine(LogTextFolder, fileName);
            Debug.Log("filePath" + filePath);
            File.WriteAllBytes(filePath, bytes);
        }

        public static void AddDebugLog(string line, int line_index)
        {
            if (debugLogList.Count < line_index + 1)
            {
                for (int i = debugLogList.Count; i < line_index + 1; i++)
                {
                    debugLogList.Add("");
                }   
            }
            debugLogList[line_index] = line;
        }

        public static string GetDebugLog()
        {
            string str = "";
            for (int i = 0; i < debugLogList.Count; i++)
            {
                str += debugLogList[i] + "\n";
            }
            return str;
        }

        private static void LogCallback(string condition, string stackTrace, LogType type)
        {
            // string tag = condition.Substring(0, condition.IndexOf(": "));
            // if (tag !=null && tag.Length > 0 && s_filterTagList.Contains(tag))
            //     return;
            Text(condition, stackTrace, type);
            Screen(condition, stackTrace, type);
        }

        private static void Text(string condition, string stackTrace, LogType type)
        {
            if (_logText != null)
            {
                _logText.Text(condition, stackTrace, type);
            }
        }

        /// <summary>
        /// Display log on Screen
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="stackTrace"></param>
        /// <param name="type"></param>
        private static void Screen(string condition, string stackTrace, LogType type)
        {
            if (_logScreen != null)
            {
                _logScreen.NewLog(new LogScreenInfo(condition, stackTrace, type));
            }
        }


    }
}