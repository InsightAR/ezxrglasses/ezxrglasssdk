﻿using System;

namespace EZXR.Glass.Runtime
{
    internal static class LogTime
    {
        internal static string GetTimeFormat()
        {
            var str = DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss");
            return string.Format("[{0}]", str);
        }

        internal static string GetDate()
        {
            return DateTime.Now.ToString("yyyyMMdd");
        }
    }
}