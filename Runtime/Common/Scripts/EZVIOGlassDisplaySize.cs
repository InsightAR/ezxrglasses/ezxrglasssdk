using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.InteropServices;

namespace EZXR.Glass.Device
{
    public struct DisplaySize
    {
        public int width;
        public int height;
    }
}
