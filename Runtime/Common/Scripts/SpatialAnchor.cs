
using EZXR.Glass.SixDof;
using EZXR.Glass.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wheels.Unity;
using System.Security.Cryptography;
using static UnityEngine.GraphicsBuffer;

public class SpatialAnchor : MonoBehaviour
{
    public enum Mode
    {
        SixDof,
        ThreeDof,
        ZeroDof,
        ThreeDofWithFollow
    }

    /// <summary>
    /// 物体的Z轴始终朝向头部
    /// </summary>
    public bool lookToHead = false;

    public Mode mode;

    RectTransform rectTransform;
    Vector3 reletivePosition;
    Vector3 reletivePosition_Local;
    Vector3 relativeVector;

    /// <summary>
    /// 当前是否已经超出边界条件
    /// </summary>
    public bool isOut;
    /// <summary>
    /// 是否启用横向出界判断
    /// </summary>
    public bool toggle_Hori = false;
    /// <summary>
    /// Head的朝向和Head到目标的向量的横向夹角如果超过此值，则认为出界
    /// </summary>
    public float angle_Hori = 180;
    /// <summary>
    /// 是否启用纵向出界判断
    /// </summary>
    public bool toggle_Vert = false;
    /// <summary>
    /// Head的朝向和Head到目标的向量的纵向夹角如果超过此值，则认为出界
    /// </summary>
    public float angle_Vert = 180;

    /// <summary>
    /// 目标平滑时间
    /// </summary>
    public float duration = 0.5f;
    /// <summary>
    /// 当前正在平滑移动
    /// </summary>
    private bool isSmoothMoving = false;
    /// <summary>
    /// 当前平滑速度
    /// </summary>
    private Vector3 currentVelocity;


    /// <summary>
    /// 设置位置和朝向目标（如果有的话）
    /// </summary>
    /// <param name="newPosition">新的位置</param>
    /// <param name="lookAtTarget">要朝向的目标</param>
    public void SetPosition(Vector3 newPosition, Transform lookAtTarget = null)
    {
        transform.position = newPosition;
        if (lookAtTarget != null)
        {
            transform.LookAt(lookAtTarget);
        }
        Refresh();
    }

    /// <summary>
    /// 同时设置位置和旋转
    /// </summary>
    /// <param name="newPosition">新的位置</param>
    /// <param name="rotation">新的旋转</param>
    public void SetPositionAndRotation(Vector3 newPosition, Quaternion rotation)
    {
        transform.position = newPosition;
        transform.rotation = rotation;
        Refresh();
    }

    void Refresh()
    {
        switch (mode)
        {
            case Mode.ThreeDof:
                reletivePosition = transform.position - XRMan.Head.position;
                break;
            case Mode.ZeroDof:
                transform.ActAsChild(XRMan.Head);
                break;
            case Mode.ThreeDofWithFollow:
                Debug.Log("refresh mode");
                reletivePosition = transform.position - XRMan.Head.position;
                relativeVector = XRMan.Head.InverseTransformDirection(reletivePosition);
                break;
        }
    }

    private void Start()
    {
        Refresh();
        rectTransform = GetComponent<RectTransform>();

#region 判断超出视野范围后移动到视野前（判断面积方法，局限于最多1个屏幕大的bound，很大的话计算面积会有问题）
        //bounds.center = center;
        //bounds.size = size;

        //IsRectOutOfViewport();
        //length = (maxX - minX) * (maxY - minY);
#endregion

        if (mode == Mode.ThreeDofWithFollow)
        {
            InvokeRepeating("CheckOutEachTime", 0, 0.5f);
        }
    }

    void Update()
    {
        //// 检测按键输入
        //if (Input.GetKeyDown(KeyCode.Space) && !isMoving)
        //{
        //    // 开始移动
        //    isMoving = true;
        //    StartCoroutine(MoveAndRotateToTarget());
        //}
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (mode == Mode.ThreeDof)
        {
            transform.position = XRMan.Head.position + reletivePosition;
        }
        else if (mode == Mode.ThreeDofWithFollow)
        {
            if (!isSmoothMoving)
            {
                transform.position = XRMan.Head.position + reletivePosition;
            }
            else
            {
                Vector3 targetPosition;
                if (toggle_Vert)
                {
                    if (toggle_Hori)
                    {
                        targetPosition = XRMan.Head.position + XRMan.Head.TransformDirection(relativeVector);
                    }
                    else
                    {
                        targetPosition = XRMan.Head.position + Vector3.ProjectOnPlane(XRMan.Head.TransformDirection(relativeVector), Vector3.right).normalized * relativeVector.magnitude;
                    }
                }
                else
                {
                    if (toggle_Hori)
                    {
                        targetPosition = XRMan.Head.position + Vector3.ProjectOnPlane(XRMan.Head.TransformDirection(relativeVector), Vector3.down).normalized * relativeVector.magnitude;
                    }
                    else
                    {
                        targetPosition = XRMan.Head.position + reletivePosition;
                    }
                }

                // 使用 SmoothDamp 函数实现追随效果，并在指定的时间内到达目标位置
                transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref currentVelocity, duration);
                if (lookToHead)
                {
                    transform.LookAt(XRMan.Head);
                }
                if (Vector3.Distance(transform.position, targetPosition) < 0.01f)
                {
                    isSmoothMoving = false;
                    reletivePosition = transform.position - XRMan.Head.position;
                    //SetPositionAndRotation(transform.position, transform.rotation);
                }
            }

            //Transform curCamera;
            //if (Application.isEditor)
            //{
            //    curCamera = XRMan.Eyes.Center.transform;
            //}
            //else
            //{
            //    curCamera = XRMan.Head;
            //}
            //float angle = Vector3.Angle(transform.position - curCamera.position, curCamera.forward);
            //if (angle > 40)
            //{
            //    transform.position = XRMan.Head.position + XRMan.Head.TransformVector(reletiveVector);
            //    reletivePosition = transform.position - XRMan.Head.position;
            //}

            //transform.LookAt(XRMan.Head);

            //if (!isMoving)
            {

            }
        }

        if (lookToHead)
        {
            transform.LookAt(XRMan.Head);
        }
    }

    void CheckOutEachTime()
    {
        if (!isSmoothMoving)
        {
            isOut = IsRectOutOfViewport();
            if (isOut)
            {
                isSmoothMoving = true;
                //iTween.StopByName(gameObject.GetInstanceID() + "moving");
                ////Debug.Log("reletiveVector: " + reletiveVector.ToString("0.###"));
                ////Debug.Log("reletiveVector 1: " + gameObject.name + ", " + XRMan.Head.TransformPoint(reletiveVector).ToString("0.###"));
                //Vector3 targetPosition;
                //if (toggle_Vert)
                //{
                //    if (toggle_Hori)
                //    {
                //        targetPosition = XRMan.Head.TransformPoint(reletiveVector);
                //    }
                //    else
                //    {
                //        targetPosition = Vector3.ProjectOnPlane(XRMan.Head.TransformPoint(reletiveVector), Vector3.right).normalized * reletiveVector.magnitude;
                //    }
                //}
                //else
                //{
                //    if (toggle_Hori)
                //    {
                //        targetPosition = Vector3.ProjectOnPlane(XRMan.Head.TransformPoint(reletiveVector), Vector3.down).normalized * reletiveVector.magnitude;
                //    }
                //    else
                //    {
                //        targetPosition = XRMan.Head.TransformPoint(reletiveVector);
                //    }
                //}
                //iTween.MoveTo(gameObject, iTween.Hash("name", "moving", "position", targetPosition, "looktarget", XRMan.Head, "time", duration, "easetype", "easeOutExpo", "oncomplete", "OnCompleted"));
                ////iTween.LookTo(gameObject, iTween.Hash("name", "moving", "looktarget", XRMan.Head, "time", duration, "easetype", "easeOutExpo", "oncomplete", "OnCompleted"));
            }
        }
    }

    void OnCompleted()
    {
        //Debug.Log("completed " + transform.name);
        reletivePosition = transform.position - XRMan.Head.position;

        isSmoothMoving = false;
    }

#region 判断超出视野范围后移动到视野前（判断面积方法，局限于最多1个屏幕大的bound，很大的话计算面积会有问题）
    //Bounds bounds;
    ///// <summary>
    ///// 重置到视野前的触发条件（BoundingBox投影到屏幕坐标系后，在屏幕上渲染的部分面积 / 本身面积得到的值如果低于此值触发）
    ///// </summary>
    //[Tooltip("重置到视野前的触发条件（BoundingBox折算为屏幕平面后，在屏幕上渲染的部分面积 / 本身面积得到的值如果低于此值触发）")]
    //public float recenterPercent = 0.333f;
    ///// <summary>
    ///// BoundingBox的中心位置（相对于物体自身坐标系，用于recenterPercent的计算）
    ///// </summary>
    //[Tooltip("BoundingBox的中心位置（相对于物体自身坐标系，用于recenterPercent的计算）")]
    //public Vector3 center;
    ///// <summary>
    ///// BoundingBox的尺寸（用于recenterPercent的计算）
    ///// </summary>
    //[Tooltip("BoundingBox的尺寸（用于recenterPercent的计算）")]
    //public Vector3 size = Vector3.one;

    //Vector3[] corners;
    //private void OnDrawGizmosSelected()
    //{
    //    if (mode == Mode.ThreeDofWithFollow)
    //    {
    //        if (bounds.size == Vector3.zero)
    //        {
    //            bounds = new Bounds(transform.position, Vector3.one);
    //        }
    //        bounds.center = center;
    //        bounds.size = size;
    //        Gizmos.color = Color.red;
    //        //Gizmos.DrawWireCube(bounds.center, bounds.size);

    //        if (!Application.isPlaying)
    //        {
    //            // 将物体的旋转应用到边界的顶点上
    //            Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);

    //            corners = GetObjectCorners(bounds);
    //            for (int i = 0; i < corners.Length; i++)
    //            {
    //                corners[i] = rotationMatrix.MultiplyPoint(corners[i]);
    //            }
    //        }
    //        DrawWireCube(corners);
    //    }
    //}

    //private void DrawWireCube(Vector3[] corners)
    //{
    //    Gizmos.DrawLine(corners[0], corners[1]);
    //    Gizmos.DrawLine(corners[1], corners[3]);
    //    Gizmos.DrawLine(corners[3], corners[2]);
    //    Gizmos.DrawLine(corners[2], corners[0]);

    //    Gizmos.DrawLine(corners[4], corners[5]);
    //    Gizmos.DrawLine(corners[5], corners[7]);
    //    Gizmos.DrawLine(corners[7], corners[6]);
    //    Gizmos.DrawLine(corners[6], corners[4]);

    //    Gizmos.DrawLine(corners[0], corners[4]);
    //    Gizmos.DrawLine(corners[1], corners[5]);
    //    Gizmos.DrawLine(corners[2], corners[6]);
    //    Gizmos.DrawLine(corners[3], corners[7]);
    //}

    //public float length;
    //bool IsRectOutOfViewport()
    //{
    //    //// 获取物体的屏幕坐标范围
    //    //Bounds objectBounds = GetComponent<Renderer>().bounds;

    //    // 将物体的旋转应用到边界的顶点上
    //    Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);

    //    // 获取物体的四个角在相机视口中的位置
    //    Vector3[] objectViewportPoints = new Vector3[8];
    //    corners = GetObjectCorners(bounds);
    //    for (int i = 0; i < corners.Length; i++)
    //    {
    //        corners[i] = rotationMatrix.MultiplyPoint(corners[i]);
    //        objectViewportPoints[i] = (Application.isEditor ? XRMan.Eyes.Center : XRMan.Eyes.Left).WorldToViewportPoint(corners[i]);
    //    }

    //    // 计算物体在相机视口中可见部分的矩形范围
    //    Rect visibleObjectRectInView = CalculateVisibleRect(objectViewportPoints, (Application.isEditor ? XRMan.Eyes.Center : XRMan.Eyes.Left).WorldToViewportPoint(rotationMatrix.MultiplyPoint(bounds.center)));

    //    // 输出物体在相机视口中可见部分的Rect信息
    //    //Debug.Log("Visible Object Rect in Camera Viewport: " + visibleObjectRectInView);

    //    float percent = visibleObjectRectInView.width * visibleObjectRectInView.height / (length);

    //    return percent < recenterPercent;
    //}

    //// 获取物体的八个角
    //Vector3[] GetObjectCorners(Bounds bounds)
    //{
    //    Vector3[] corners = new Vector3[8];
    //    corners[0] = bounds.center + new Vector3(bounds.extents.x, bounds.extents.y, bounds.extents.z);
    //    corners[1] = bounds.center + new Vector3(bounds.extents.x, bounds.extents.y, -bounds.extents.z);
    //    corners[2] = bounds.center + new Vector3(bounds.extents.x, -bounds.extents.y, bounds.extents.z);
    //    corners[3] = bounds.center + new Vector3(bounds.extents.x, -bounds.extents.y, -bounds.extents.z);
    //    corners[4] = bounds.center + new Vector3(-bounds.extents.x, bounds.extents.y, bounds.extents.z);
    //    corners[5] = bounds.center + new Vector3(-bounds.extents.x, bounds.extents.y, -bounds.extents.z);
    //    corners[6] = bounds.center + new Vector3(-bounds.extents.x, -bounds.extents.y, bounds.extents.z);
    //    corners[7] = bounds.center + new Vector3(-bounds.extents.x, -bounds.extents.y, -bounds.extents.z);
    //    return corners;
    //}

    ///// <summary>
    ///// 相机裁剪前的rect的四边
    ///// </summary>
    //public float minX = 1f, maxX = 0f, minY = 1f, maxY = 0f;
    ///// <summary>
    ///// 相机裁剪后的rect的四边
    ///// </summary>
    //public float minX_Cor = 1f, maxX_Cor = 0f, minY_Cor = 1f, maxY_Cor = 0f;
    ///// <summary>
    ///// 相机裁剪后的屏占区域
    ///// </summary>
    //public Rect result;
    //// 计算物体在相机视口中可见部分的矩形范围
    //Rect CalculateVisibleRect(Vector3[] viewportPoints, Vector3 centerPoints)
    //{
    //    float minX = 1f, maxX = 0f, minY = 1f, maxY = 0f;

    //    foreach (Vector3 viewportPoint in viewportPoints)
    //    {
    //        Vector3 tempPoint = viewportPoint;
    //        if (tempPoint.z < 0)////////////////////////////////////////TODO:当前视野朝向左侧的话，x=1，否则x=0，视野偏上的话y=0,否则y=1
    //        {
    //            if (centerPoints.x < 0.5f)
    //            {
    //                tempPoint.x = 0;
    //            }
    //            else
    //            {
    //                tempPoint.x = 1;
    //            }
    //        }
    //        if (tempPoint.x < minX) minX = tempPoint.x;
    //        if (tempPoint.x > maxX) maxX = tempPoint.x;
    //        if (tempPoint.y < minY) minY = tempPoint.y;
    //        if (tempPoint.y > maxY) maxY = tempPoint.y;
    //    }

    //    this.minX = minX;
    //    this.maxX = maxX;
    //    this.minY = minY;
    //    this.maxY = maxY;

    //    minX_Cor = Mathf.Clamp01(minX);
    //    maxX_Cor = Mathf.Clamp01(maxX);
    //    minY_Cor = Mathf.Clamp01(minY);
    //    maxY_Cor = Mathf.Clamp01(maxY);

    //    return result = new Rect(minX_Cor, minY_Cor, maxX_Cor - minX_Cor, maxY_Cor - minY_Cor);
    //}
#endregion

#region 判断超出视野范围后移动到视野前（角度判断法）
    float CalculateHorizontalAngle(Vector3 a, Vector3 b)
    {
        Vector3 aProject = new Vector3(a.x, 0, a.z).normalized;
        Vector3 bProject = new Vector3(b.x, 0, b.z).normalized;
        float dotProduct = Vector3.Dot(aProject, bProject);
        float thetaH = Mathf.Acos(dotProduct);
        return Mathf.Rad2Deg * thetaH;
    }

    float CalculateVerticalAngle(Vector3 a, Vector3 b)
    {
        float dotA = Vector3.Dot(a.normalized, Vector3.up);
        float dotB = Vector3.Dot(b.normalized, Vector3.up);

        float acosA = Mathf.Acos(dotA);
        float acosB = Mathf.Acos(dotB);

        float angleA0 = Mathf.Rad2Deg * acosA;
        float angleB0 = Mathf.Rad2Deg * acosB;

        float angleA = 90 - angleA0;
        float angleB = 90 - angleB0;

        //Debug.Log("angleA: " + angleA);
        //Debug.Log("angleB: " + angleB);

        return Mathf.Abs(angleA - angleB);
    }
    public bool IsRectOutOfViewport()
    {
        Transform cam = (Application.isEditor ? XRMan.Eyes.Center : XRMan.Eyes.Left).transform;

        Vector3 camToTar = transform.position - cam.position;

        // 水平夹角
        float horizontalAngle = CalculateHorizontalAngle(camToTar, cam.forward);
        //Debug.Log(transform.name + " Horizontal Angle: " + horizontalAngle);

        // 垂直夹角
        float verticalAngle = CalculateVerticalAngle(camToTar, cam.forward);
        //Debug.Log("Vertical Angle: " + verticalAngle);

        return (toggle_Hori && horizontalAngle > angle_Hori) || (toggle_Vert && verticalAngle > angle_Vert);
    }
#endregion

    IEnumerator MoveAndRotateToTarget()
    {
        Vector3 pos = XRMan.Head.position + XRMan.Head.TransformVector(relativeVector);
        Quaternion initialRotation = transform.rotation;

        float elapsedTime = 0f;
        while (elapsedTime < duration)
        {
            pos = XRMan.Head.position + XRMan.Head.TransformVector(relativeVector);

            float distance = Vector3.Distance(transform.position, pos);
            // 根据目标时间计算移动和旋转的速度
            float moveSpeed = distance / (duration - elapsedTime);

            // 计算移动方向和距离
            Vector3 direction = (pos - transform.position).normalized;

            // 计算旋转方向和角度
            Vector3 targetPosition = XRMan.Head.position;
            Vector3 directionToTarget = (targetPosition - transform.position).normalized;
            Quaternion targetRotation = Quaternion.LookRotation(directionToTarget);

            // 平滑移动
            transform.position += direction * moveSpeed * Time.deltaTime;

            // 平滑旋转
            transform.rotation = Quaternion.Slerp(initialRotation, targetRotation, elapsedTime / duration);

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        //// 确保最终位置和旋转正确
        //transform.position = pos;
        //transform.rotation = Quaternion.LookRotation(XRMan.Head.position - transform.position);

        // 移动完成
        isSmoothMoving = false;
    }
}
