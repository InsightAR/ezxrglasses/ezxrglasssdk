﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    public class NativeLib
    {
        static AndroidJavaObject instance;
        public static AndroidJavaObject Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AndroidJavaClass("com.ezxr.glass.nativelib.API").CallStatic<AndroidJavaObject>("getInstance");

                    AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                    AndroidJavaObject activity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                    Init(
                        activity,
                        new SysEventCallback(OnPermissionEvent, OnToast, OnProjectionDetected, OnGlassOn, OnGlassOff)
                        );
                }
                return instance;
            }
        }

        public static Action<PermissionInfo[], int> Callback_OnPermissionEvent;
        public static Action<string, float> Callback_OnToast;
        public static Action CallBack_OnProjectionDetected;
        public static Action CallBack_OnGlassOn;
        public static Action CallBack_OnGlassOff;


        static void OnPermissionEvent(PermissionInfo[] permissionInfos, int requestCode)
        {
            Debug.Log("SystemMenuUtils--> OnPermissionEvent, permissionInfos.Length:" + permissionInfos.Length + ", requestCode:" + requestCode);

            if (Callback_OnPermissionEvent != null)
            {
                Callback_OnPermissionEvent(permissionInfos, requestCode);
            }
        }

        /// <summary>
        /// 需要弹出toast
        /// </summary>
        /// <param name="content"></param>
        /// <param name="duration"></param>
        static void OnToast(string content, int duration)
        {
            Debug.Log("SystemMenuUtils--> onToast, content:" + content + ", duration:" + duration);

            if (Callback_OnToast != null)
            {
                Callback_OnToast(content, duration);
            }
        }

        static void OnProjectionDetected()
        {
            Debug.Log("SystemMenuUtils--> onProjectionDetected");

            if (CallBack_OnProjectionDetected != null)
            {
                CallBack_OnProjectionDetected();
            }
        }
        static void OnGlassOn()
        {
            Debug.Log("SystemMenuUtils--> onGlassOn");

            if (CallBack_OnGlassOn != null)
            {
                CallBack_OnGlassOn();
            }
        }
        static void OnGlassOff()
        {
            Debug.Log("SystemMenuUtils--> onGlassOff");

            if (CallBack_OnGlassOff != null)
            {
                CallBack_OnGlassOff();
            }
        }

        /// <summary>
        /// 进入应用时调用
        /// </summary>
        /// <param name="context"></param>
        /// <param name="keyCallback"></param>
        static void Init(
                        AndroidJavaObject context,
                        SysEventCallback keyCallback
            )
        {
#if UNITY_EDITOR

#else
            Instance.Call("init", context, keyCallback);
#endif
        }

        /// <summary>
        /// 退出应用时调用
        /// </summary>
        public static void DeInit()
        {
#if UNITY_EDITOR

#else
            Instance.Call("deInit");
#endif
        }

        // 应用从后台切回前台时调用
        public static void Resume()
        {
#if UNITY_EDITOR

#else
            Instance.Call("resume");
#endif
        }

        // 应用从前台切到后台时调用
        public static void Pause()
        {
#if UNITY_EDITOR

#else
            Instance.Call("pause");
#endif
        }

        /// <summary>
        /// 获取设备SN号
        /// </summary>
        /// <returns></returns>
        public static string GetDeviceId()
        {
#if UNITY_EDITOR
            return "";
#else
            return Instance.Call<string>("getDeviceId");
#endif
        }

        /// <summary>
        /// 保存完图片或视频后可以调用该接口添加到图库
        /// </summary>
        /// <returns></returns>
        /// <param name="path">绝对路径</param>
        public static void AddToAlbum(string path)
        {
#if UNITY_EDITOR

#else
            Instance.Call("addToAlbum", path);
#endif
        }

        /// <summary>
        /// 获取截屏录屏目录
        /// </summary>
        public static string GetScreenshotsPath()
        {
#if UNITY_EDITOR
            return "";
#else
            return Instance.Call<string>("getScreenshotsPath");
#endif
        }

        [Obsolete("Use \"GetGlassState\" instead!", true)]
        /// <summary>
        /// 当前眼镜是否佩戴在头上
        /// </summary>
        /// <returns></returns>
        public static bool IsGlassOn()
        {
#if UNITY_EDITOR
            return false;
#else
            int state = Instance.Call<int>("getGlassState");
            if (state == 1)
            {
                return true;
            }
            return false;
#endif
        }

        /// <summary>
        /// 当前眼镜是否佩戴在头上
        /// 0:未初始化 1：戴在头上 2：未佩戴
        /// </summary>
        /// <returns></returns>
        public static int GetGlassState()
        {
#if UNITY_EDITOR
            return 0;
#else
            return Instance.Call<int>("getGlassState");
#endif
        }

        /// <summary>
        /// 是否不允许进入待机，true为不允许，false为允许
        /// </summary>
        /// <param name="noSleepMode"></param>
        public static void SetNoSleepMode(bool noSleepMode)
        {
#if UNITY_EDITOR

#else
            Instance.Call("setNoSleepMode", noSleepMode);
#endif
        }

        /// <summary>
        /// 进入待机模式
        /// </summary>
        /// <returns>返回值：0(成功) -1(系统错误) -2（当前正在投录屏） -3(前台应用阻止) -4(当前正在OTA升级)</returns>
        public static int EnterSleepMode()
        {
#if UNITY_EDITOR
            return 0;
#else
            return Instance.Call<int>("enterSleepMode");
#endif
        }
    }
}