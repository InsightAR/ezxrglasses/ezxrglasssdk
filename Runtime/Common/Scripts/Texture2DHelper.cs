using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EZXR.Glass.Runtime
{
    public static class Texture2DHelper
    {
        public static Texture2D FilpY(Texture2D src)
        {
            int width = src.width;
            int height = src.height;
            Texture2D dst = new Texture2D(src.width, src.height, src.format, false);
            Color[] srcPixels = src.GetPixels();
            Color[] dstPixels = new Color[srcPixels.Length];
            for (int i = 0; i < height; i++)
            {
                Array.Copy(srcPixels, i*width, dstPixels, (height-i-1) * width , width);
            }
            // for (int i = 0; i < src.height; ++i)
            // {
            //     for (int j = 0; j < src.width; ++j)
            //     {
            //         dstPixels[i * src.width + j] = srcPixels[(src.height - i - 1) * src.width + j];
            //     }
            // }
            dst.SetPixels(dstPixels);
            dst.Apply();
            return dst;
        }
        public static byte[] FilpY(byte[] src, int width, int height, int channels)
        {
            byte[] dst = new byte[src.Length];
            int rowByteCount = width * channels;
            for (int i = 0; i < height; i++)
            {
                Array.Copy(src, i*rowByteCount, dst, (height-i-1) * rowByteCount , rowByteCount);
            }
            return dst;
        }
    }
}