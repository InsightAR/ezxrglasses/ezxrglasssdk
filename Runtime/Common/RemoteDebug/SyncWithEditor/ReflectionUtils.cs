using UnityEngine;
using System;
using System.Collections.Generic;
using System.Reflection;

public class ReflectionUtility : MonoBehaviour
{
    private static Dictionary<string, GameObject> gameObjectPool = new Dictionary<string, GameObject>();
    private static Dictionary<string, Component> componentPool = new Dictionary<string, Component>();
    private static Dictionary<string, MethodInfo> methodPool = new Dictionary<string, MethodInfo>();
    private static Dictionary<string, PropertyInfo> propertyPool = new Dictionary<string, PropertyInfo>();
    private static Dictionary<string, FieldInfo> fieldPool = new Dictionary<string, FieldInfo>();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="func">要调用的方法的路径，语法："GameObjectPath:ClassName:MethodName"</param>
    public static void InvokeMethod(string func)
    {
        string[] parts = func.Split(':');
        if (parts.Length != 3)
        {
            Debug.LogError("Invalid format: " + func);
        }

        string gameObjectPath = parts[0];
        string classInstanceStructInstanceProp = parts[1];
        string methodName = parts[2];

        GameObject obj = GetGameObject(gameObjectPath);
        if (obj == null)
        {
            Debug.LogError("GameObject not found: " + gameObjectPath);
        }

        object instance = GetComponentInstance(obj, classInstanceStructInstanceProp);
        if (instance == null)
        {
            Debug.LogError("Instance not found: " + classInstanceStructInstanceProp);
        }

        MethodInfo method = GetMethod(instance.GetType(), methodName);
        if (method == null)
        {
            Debug.LogError("Method not found: " + methodName);
        }
        else
        {
            method.Invoke(instance, null);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="prop">要获取的属性的路径，语法："GameObjectPath:ClassName:PropName"</param>
    /// <returns></returns>
    public static object GetProperty(string prop)
    {
        string[] parts = prop.Split(':');
        if (parts.Length != 3)
        {
            Debug.LogError("Invalid format: " + prop);
        }

        string gameObjectPath = parts[0];
        string classInstanceStructInstanceProp = parts[1];
        string propName = parts[2];

        GameObject obj = GetGameObject(gameObjectPath);
        if (obj == null)
        {
            Debug.LogError("GameObject not found: " + gameObjectPath);
        }

        object instance = GetComponentInstance(obj, classInstanceStructInstanceProp);
        if (instance == null)
        {
            Debug.LogError("Instance not found: " + classInstanceStructInstanceProp);
        }

        Type type = instance.GetType();
        PropertyInfo propertyInfo = GetProperty(type, propName);
        if (propertyInfo == null)
        {
            Debug.LogError("Property not found: " + propName);
        }

        object value = propertyInfo.GetValue(instance);
        Debug.Log("Property " + propName + " value: " + value);

        return value;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="field">要获取的字段的路径，语法："GameObjectPath:ClassNameA/ClassInstanceNameB/StructInstanceNameA/StructInstanceNameB:FieldName"</param>
    /// <returns></returns>
    public static object GetField(string field)
    {
        string[] parts = field.Split(':');
        if (parts.Length != 3)
        {
            Debug.LogError("Invalid format: " + field);
        }

        string gameObjectPath = parts[0];
        string classInstanceStructInstanceField = parts[1];
        string fieldName = parts[2];

        GameObject obj = GetGameObject(gameObjectPath);
        if (obj == null)
        {
            Debug.LogError("GameObject not found: " + gameObjectPath);
        }

        object instance = GetComponentInstance(obj, classInstanceStructInstanceField);
        if (instance == null)
        {
            Debug.LogError("Instance not found: " + classInstanceStructInstanceField);
        }

        Type type = instance.GetType();
        FieldInfo fieldInfo = GetField(type, fieldName);
        if (fieldInfo == null)
        {
            Debug.LogError("Field not found: " + fieldName);
        }

        object value = fieldInfo.GetValue(instance);
        Debug.Log("Field " + fieldName + " value: " + value);

        return value;
    }

    private static object GetComponentInstance(GameObject obj, string classInstanceStructInstanceField)
    {
        string[] parts = classInstanceStructInstanceField.Split('/');
        object instance = obj.GetComponent(parts[0]);

        // Traverse through nested instances
        for (int i = 1; i < parts.Length; i++)
        {
            if (instance == null)
            {
                Debug.LogError("Component instance not found: " + classInstanceStructInstanceField);
                return null;
            }

            Type type = instance.GetType();
            FieldInfo field = type.GetField(parts[i], BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            if (field != null)
            {
                instance = field.GetValue(instance);
                continue;
            }

            // If field is not found, try getting property
            PropertyInfo property = type.GetProperty(parts[i], BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            if (property != null)
            {
                instance = property.GetValue(instance);
                continue;
            }

            Debug.LogError("Instance field or property not found: " + parts[i]);
            return null;
        }

        return instance;
    }

    private static GameObject GetGameObject(string gameObjectPath)
    {
        if (gameObjectPool.ContainsKey(gameObjectPath))
            return gameObjectPool[gameObjectPath];

        GameObject obj = GameObject.Find(gameObjectPath);
        if (obj != null)
            gameObjectPool[gameObjectPath] = obj;

        return obj;
    }

    private static Component GetComponent(GameObject obj, string className)
    {
        string key = obj.name + ":" + className;
        if (componentPool.ContainsKey(key))
            return componentPool[key];

        Component component = obj.GetComponent(className);
        if (component != null)
            componentPool[key] = component;

        return component;
    }

    private static MethodInfo GetMethod(Type type, string methodName)
    {
        string key = type.FullName + ":" + methodName;
        if (methodPool.ContainsKey(key))
            return methodPool[key];

        MethodInfo method = type.GetMethod(methodName);
        if (method != null)
            methodPool[key] = method;

        return method;
    }

    private static PropertyInfo GetProperty(Type type, string propName)
    {
        string key = type.FullName + ":" + propName;
        if (propertyPool.ContainsKey(key))
            return propertyPool[key];

        PropertyInfo property = type.GetProperty(propName);
        if (property != null)
            propertyPool[key] = property;

        return property;
    }

    private static FieldInfo GetField(Type type, string fieldName)
    {
        string key = type.FullName + ":" + fieldName;
        if (fieldPool.ContainsKey(key))
            return fieldPool[key];

        FieldInfo field = type.GetField(fieldName);
        if (field != null)
            fieldPool[key] = field;

        return field;
    }
}
