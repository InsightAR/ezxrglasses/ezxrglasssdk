﻿using Wheels.Unity;
using System;
using System.Runtime.InteropServices;
using UnityEngine;
using System.Text;
using EZXR.Glass.Runtime;
using System.Reflection;

#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;

[ScriptExecutionOrder(32000)]
public class SyncGameObjects : MonoBehaviour
{
    public struct CustomTransform
    {
        public bool activeSelf;
        public Vector3 localPosition;
        public Quaternion localRotation;
        public Vector3 localScale;
        public void Set(bool activeSelf, Vector3 localPosition, Quaternion localRotation, Vector3 localScale)
        {
            this.activeSelf = activeSelf;
            this.localPosition = localPosition;
            this.localRotation = localRotation;
            this.localScale = localScale;
        }
    }

    public bool runOnStart;

    NetUtil_Server serverUDP = new NetUtil_Server();
    NetUtil_Client clientUDP = new NetUtil_Client();

    /// <summary>
    /// 专门用于同步控制指令
    /// </summary>
    NetUtil_Server serverTCP = new NetUtil_Server();
    NetUtil_Client clientTCP = new NetUtil_Client();

    /// <summary>
    /// Editor端IP
    /// </summary>
    [Tooltip("Editor所在的电脑IP")]
    public string editorIP;
    [Tooltip("Editor上用于同步数据的端口")]
    public int dataPort = 6611;
    [Tooltip("Editor上用于同步命令的端口")]
    public int commandPort = 6612;

    public enum SendTarget
    {
        SendToEditor,
        SendToClient
    }
    [Tooltip("数据的同步方向（支持运行时修改）：Client->Editor、Editor->Client")]
    public SendTarget sendToEditor;
    SendTarget lastSendToEditorState;

    /// <summary>
    /// 要同步的物体
    /// </summary>
    public Transform[] transformsToSync;
    CustomTransform[] customTransforms;
    IntPtr[] intPtrs;
    int perTransformBytesCount;
    byte[] syncData;
    byte[] dataIn;
    bool newDataIn;
    public static List<string> allowIPs = new List<string>();
    List<string> disAllowIPs = new List<string>();

    bool startSync;


    private void Start()
    {
        if (runOnStart)
        {
            Init();
        }
    }

    // Start is called before the first frame update
    public void Init()
    {
        intPtrs = new IntPtr[transformsToSync.Length];
        for (int i = 0; i < intPtrs.Length; i++)
        {
            int size = Marshal.SizeOf(typeof(CustomTransform));
            intPtrs[i] = Marshal.AllocHGlobal(size);
        }
        customTransforms = new CustomTransform[transformsToSync.Length];
        for (int i = 0; i < customTransforms.Length; i++)
        {
            customTransforms[i] = new CustomTransform();
        }

        //得到数据结构所占字节数
        perTransformBytesCount = Marshal.SizeOf(typeof(CustomTransform));
        //要同步的transform的数量*每个transform所占的字节数
        syncData = new byte[transformsToSync.Length * perTransformBytesCount];

        if (Application.isEditor)
        {
            serverUDP.StartServer(dataPort, NetUtil.NetType.UDP, GetDataUDP, true);
            serverTCP.StartServer(commandPort, NetUtil.NetType.TCP, GetDataTCP, true);
        }
        else
        {
            clientUDP.Connect(editorIP, dataPort, NetUtil.NetType.UDP, GetDataUDP);
            clientTCP.Connect(editorIP, commandPort, NetUtil.NetType.TCP, GetDataTCP);
        }

        startSync = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (startSync)
        {
            //if (Input.GetKeyDown(KeyCode.Alpha1))
            //{
            //    sendToEditor = true;
            //    serverTCP.SendToAll("SendToEditor");
            //}
            //if (Input.GetKeyDown(KeyCode.Alpha2))
            //{
            //    sendToEditor = false;
            //    serverTCP.SendToAll("SendToClient");
            //}

            if (Application.isEditor)
            {
                if (lastSendToEditorState != sendToEditor)
                {
                    lastSendToEditorState = sendToEditor;

                    if (sendToEditor == SendTarget.SendToEditor)
                    {
                        serverTCP.SendToAll("SendToEditor");
                    }
                    else
                    {
                        serverTCP.SendToAll("SendToClient");
                    }
                }

                if (sendToEditor == SendTarget.SendToClient)
                {
                    GetSyncData();
                    serverUDP.SendToAll(syncData);
                }
            }
            else
            {
                if (sendToEditor == SendTarget.SendToEditor)
                {
                    GetSyncData();
                    clientUDP.Send(syncData);
                }
            }
        }
    }

    private void LateUpdate()
    {
        if (newDataIn && ((Application.isEditor && sendToEditor == SendTarget.SendToEditor) || (!Application.isEditor && sendToEditor == SendTarget.SendToClient)))
        {
            //下面这行之所以屏蔽是因为同步的频率并不是每帧都同步，就会造成有数据的帧显示正常，没数据的帧会被其他脚本比如SpatialAnchor覆盖，造成物体的跳变的效果
            //newDataIn = false;

            //得到要同步的所有的Transform的数据
            int curTransformCount = 0;
            for (int i = 0; i < transformsToSync.Length; i++)
            {
                int curIndex = curTransformCount * perTransformBytesCount;
                Marshal.Copy(dataIn, curIndex, intPtrs[i], perTransformBytesCount);
                customTransforms[i] = Marshal.PtrToStructure<CustomTransform>(intPtrs[i]);

                transformsToSync[i].gameObject.SetActive(customTransforms[i].activeSelf);
                transformsToSync[i].localPosition = customTransforms[i].localPosition;
                transformsToSync[i].localRotation = customTransforms[i].localRotation;
                transformsToSync[i].localScale = customTransforms[i].localScale;

                curTransformCount++;
            }
        }
    }

    private void OnGUI()
    {
        GUI.skin.label.normal.textColor = Color.red;
        if (XRMan.Exist)
        {
            GUILayout.Label("当前正在同步调试模式，物体的状态会收到同步影响，如果要取消，请到\"XRMan - Extras\"禁用 \"RemoteDebug\"！");
        }
        else
        {
            GUILayout.Label("当前正在同步调试模式，物体的状态会收到同步影响，如果要取消，请全局搜索\"SyncGameObjects\"并禁用！");
        }
    }

    private void OnDisable()
    {
        if (Application.isEditor)
        {
            serverUDP.StopServer();
            serverTCP.StopServer();
        }
        else
        {
            clientUDP.DisConnect();
            clientTCP.DisConnect();
        }
    }

    void GetSyncData()
    {
        //得到要同步的所有的Transform的数据
        int curTransformCount = 0;
        for (int i = 0; i < transformsToSync.Length; i++)
        {
            customTransforms[i].Set(transformsToSync[i].gameObject.activeSelf, transformsToSync[i].localPosition, transformsToSync[i].localRotation, transformsToSync[i].localScale);
            Marshal.StructureToPtr(customTransforms[i], intPtrs[i], false);
            Marshal.Copy(intPtrs[i], syncData, curTransformCount * perTransformBytesCount, perTransformBytesCount);

            //byte[] forFloat = BitConverter.GetBytes(item.localPosition.x);
            //syncData[curTransformCount * perTransformBytesCount + 0] = forFloat[0];
            //syncData[curTransformCount * perTransformBytesCount + 1] = forFloat[1];
            //syncData[curTransformCount * perTransformBytesCount + 2] = forFloat[2];
            //syncData[curTransformCount * perTransformBytesCount + 3] = forFloat[3];

            //forFloat = BitConverter.GetBytes(item.localPosition.y);
            //syncData[curTransformCount * perTransformBytesCount + 4] = forFloat[0];
            //syncData[curTransformCount * perTransformBytesCount + 5] = forFloat[1];
            //syncData[curTransformCount * perTransformBytesCount + 6] = forFloat[2];
            //syncData[curTransformCount * perTransformBytesCount + 7] = forFloat[3];

            //forFloat = BitConverter.GetBytes(item.localPosition.z);
            //syncData[curTransformCount * perTransformBytesCount + 8] = forFloat[0];
            //syncData[curTransformCount * perTransformBytesCount + 9] = forFloat[1];
            //syncData[curTransformCount * perTransformBytesCount + 10] = forFloat[2];
            //syncData[curTransformCount * perTransformBytesCount + 11] = forFloat[3];

            //forFloat = BitConverter.GetBytes(item.localRotation.x);
            //syncData[curTransformCount * perTransformBytesCount + 12] = forFloat[0];
            //syncData[curTransformCount * perTransformBytesCount + 13] = forFloat[1];
            //syncData[curTransformCount * perTransformBytesCount + 14] = forFloat[2];
            //syncData[curTransformCount * perTransformBytesCount + 15] = forFloat[3];

            //forFloat = BitConverter.GetBytes(item.localRotation.y);
            //syncData[curTransformCount * perTransformBytesCount + 16] = forFloat[0];
            //syncData[curTransformCount * perTransformBytesCount + 17] = forFloat[1];
            //syncData[curTransformCount * perTransformBytesCount + 18] = forFloat[2];
            //syncData[curTransformCount * perTransformBytesCount + 19] = forFloat[3];

            //forFloat = BitConverter.GetBytes(item.localRotation.z);
            //syncData[curTransformCount * perTransformBytesCount + 20] = forFloat[0];
            //syncData[curTransformCount * perTransformBytesCount + 21] = forFloat[1];
            //syncData[curTransformCount * perTransformBytesCount + 22] = forFloat[2];
            //syncData[curTransformCount * perTransformBytesCount + 23] = forFloat[3];

            //forFloat = BitConverter.GetBytes(item.localRotation.w);
            //syncData[curTransformCount * perTransformBytesCount + 24] = forFloat[0];
            //syncData[curTransformCount * perTransformBytesCount + 25] = forFloat[1];
            //syncData[curTransformCount * perTransformBytesCount + 26] = forFloat[2];
            //syncData[curTransformCount * perTransformBytesCount + 27] = forFloat[3];

            curTransformCount++;
        }
    }

    public void GetDataUDP(byte[] data, string ip)
    {
        //Debug.Log("GetDataUDP: " + ip);

#if UNITY_EDITOR
        if (disAllowIPs.Contains(ip))
        {
            return;
        }
        else
        {
            if (!allowIPs.Contains(ip))
            {
                if (EditorUtility.DisplayDialog("新的远程调试请求", "有来自设备（IP:" + ip + "）的远程调试请求，是否允许？", "是", "否"))
                {
                    allowIPs.Add(ip);
                }
                else
                {
                    disAllowIPs.Add(ip);
                }
            }
        }

        if (allowIPs.Contains(ip))
#endif
        {
            //Debug.Log(syncData.Length);
            dataIn = data;
            newDataIn = true;
        }
    }

    public void GetDataTCP(byte[] data, string ip)
    {
        string command = Encoding.UTF8.GetString(data);
        Debug.Log("command: " + command);
        switch (command)
        {
            case "SendToEditor":
                sendToEditor = SendTarget.SendToEditor;
                break;
            case "SendToClient":
                sendToEditor = SendTarget.SendToClient;
                break;
        }
    }
}
