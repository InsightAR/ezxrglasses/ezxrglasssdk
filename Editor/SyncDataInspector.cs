using System.Reflection;
using System;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[CustomEditor(typeof(SyncData))]
public class SyncDataInspector : Editor
{
    SyncData syncData;
    SerializedProperty objProp;

    /// <summary>
    /// 元数据类型，不需要继续向内拆解的类型
    /// </summary>
    Type[] commonTypes = new Type[] { typeof(int), typeof(uint), typeof(short), typeof(ushort), typeof(byte), typeof(sbyte), typeof(long), typeof(ulong), typeof(float), typeof(double), typeof(string), typeof(char), typeof(bool) };

    Dictionary<PropertyInfo, PropertyInfo[]> propertyDic = new Dictionary<PropertyInfo, PropertyInfo[]>();
    Dictionary<FieldInfo, FieldInfo[]> fieldDic = new Dictionary<FieldInfo, FieldInfo[]>();
    bool propertyFold, fieldFold;
    bool[][] propertyCheckMatrix;

    private void OnEnable()
    {
        syncData = target as SyncData;

        objProp = serializedObject.FindProperty("obj");

        if (syncData.obj != null)
        {
            // 获取对象的类型
            Type type = syncData.obj.GetType();

            // 获取对象的所有属性
            PropertyInfo[] properties = type.GetProperties();

            Debug.Log("Properties:");
            for (int i = 0; i < properties.Length; i++)
            {
                try
                {
                    Debug.Log(properties[i].Name + " => PropertyType: " + properties[i].PropertyType + " ,Value: " + properties[i].GetValue(syncData.obj));

                    //不支持同类型嵌套（像单例模式就是个典型）
                    if (properties[i].PropertyType != syncData.obj.GetType())
                    {
                        PropertyInfo[] subProperties = GetProperties(properties[i].PropertyType);
                        propertyDic.Add(properties[i], subProperties);
                        propertyCheckMatrix[i] = (subProperties != null && subProperties.Length > 0) ? new bool[subProperties.Length] : null;
                    }
                }
                catch (Exception ex)
                {
                    //Debug.Log(property.Name + ": " + ex.Message);
                }
            }

            int num = 0;
            propertyCheckMatrix = new bool[propertyDic.Count][];
            foreach (PropertyInfo[] property in propertyDic.Values)
            {
                propertyCheckMatrix[num] = (property != null && property.Length > 0) ? new bool[property.Length] : null;
                num++;
            }


            // 获取对象的所有字段
            FieldInfo[] fields = type.GetFields();
            Debug.Log("Fields:");
            foreach (FieldInfo field in fields)
            {
                try
                {
                    Debug.Log(field.Name + " => FieldType: " + ": " + field.FieldType + " ,Value: " + field.GetValue(syncData.obj));
                    //不支持同类型嵌套（像单例模式就是个典型）
                    if (field.FieldType != syncData.obj.GetType())
                    {
                        fieldDic.Add(field, GetFields(field.FieldType));
                    }
                }
                catch (Exception ex)
                {
                    //Debug.Log(field.Name + ": " + ex.Message);
                }
            }
        }
    }

    PropertyInfo[] GetProperties(Type type)
    {
        if (commonTypes.Contains(type) || type.IsEnum)
        {
            return null;
        }

        return type.GetProperties();
    }

    FieldInfo[] GetFields(Type type)
    {
        if (commonTypes.Contains(type) || type.IsEnum)
        {
            return null;
        }

        return type.GetFields();
    }

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();

        serializedObject.Update();

        EditorGUILayout.ObjectField(objProp);

        propertyFold = EditorGUILayout.BeginFoldoutHeaderGroup(propertyFold, "Properties");
        {
            if (propertyFold)
            {
                //foreach (PropertyInfo property in properties)
                //{
                //    EditorGUILayout.Toggle(property.Name, false);
                //}
                int keyNum = 0;
                foreach (PropertyInfo property in propertyDic.Keys)
                {
                    if (propertyDic[property] != null)
                    {
                        EditorGUILayout.BeginToggleGroup(property.Name, true);
                        {
                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("", GUILayout.Width(15));
                                EditorGUILayout.BeginVertical();
                                {
                                    int valueNum = 0;
                                    foreach (PropertyInfo subProperty in propertyDic[property])
                                    {
                                        if (subProperty.CanWrite)
                                        {
                                            propertyCheckMatrix[keyNum][valueNum] = EditorGUILayout.ToggleLeft(subProperty.Name, propertyCheckMatrix[keyNum][valueNum]);
                                        }
                                        valueNum++;
                                    }
                                }
                                EditorGUILayout.EndVertical();
                            }
                            EditorGUILayout.EndHorizontal();
                        }
                        EditorGUILayout.EndToggleGroup();
                    }
                    else
                    {
                        EditorGUILayout.ToggleLeft(property.Name, false);
                    }
                    keyNum++;
                }
            }
        }
        EditorGUILayout.EndFoldoutHeaderGroup();

        fieldFold = EditorGUILayout.BeginFoldoutHeaderGroup(fieldFold, "Fields");
        {
            if (fieldFold)
            {
                //foreach (FieldInfo field in fields)
                //{
                //    EditorGUILayout.ToggleLeft(field.Name, false);
                //}
                foreach (FieldInfo field in fieldDic.Keys)
                {
                    if (fieldDic[field] != null && fieldDic[field].Length > 0)
                    {
                        EditorGUILayout.BeginToggleGroup(field.Name, true);
                        {
                            EditorGUILayout.BeginHorizontal();
                            {
                                EditorGUILayout.LabelField("", GUILayout.Width(15));
                                EditorGUILayout.BeginVertical();
                                {
                                    foreach (FieldInfo subField in fieldDic[field])
                                    {
                                        EditorGUILayout.ToggleLeft(subField.Name, false);
                                    }
                                }
                                EditorGUILayout.EndVertical();
                            }
                            EditorGUILayout.EndHorizontal();
                        }
                        EditorGUILayout.EndToggleGroup();
                    }
                    else
                    {
                        EditorGUILayout.ToggleLeft(field.Name, false);
                    }
                }
            }
        }
        EditorGUILayout.EndFoldoutHeaderGroup();

        //EditorGUILayout.PropertyField(modeProp);

        //EditorGUILayout.LabelField("Always look to head", EditorStyles.boldLabel);
        //EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        //{
        //    EditorGUILayout.PropertyField(lookToHeadProp);
        //}
        //EditorGUILayout.EndVertical();

        //if (spatialAnchor.mode == SpatialAnchor.Mode.ThreeDofWithFollow)
        //{
        //    EditorGUILayout.LabelField("Angles to trigger Follow", EditorStyles.boldLabel);
        //    EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        //    {
        //        EditorGUILayout.PropertyField(toggle_Hori, new GUIContent("Horizontal"));
        //        if (toggle_Hori.boolValue)
        //        {
        //            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
        //            {
        //                EditorGUILayout.PropertyField(angleHoriProp, new GUIContent("Angle:[0,180]"));
        //            }
        //            EditorGUILayout.EndHorizontal();
        //        }
        //        EditorGUILayout.PropertyField(toggle_Vert, new GUIContent("Vertical"));
        //        if (toggle_Vert.boolValue)
        //        {
        //            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
        //            {
        //                EditorGUILayout.PropertyField(angleVertProp, new GUIContent("Angle:[0,180]"));
        //            }
        //            EditorGUILayout.EndHorizontal();
        //        }
        //    }
        //    EditorGUILayout.EndVertical();

        //    EditorGUILayout.LabelField("Time to complete Follow", EditorStyles.boldLabel);
        //    EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        //    {
        //        EditorGUILayout.PropertyField(durationProp);
        //    }
        //    EditorGUILayout.EndVertical();
        //}

        serializedObject.ApplyModifiedProperties();
    }
}
