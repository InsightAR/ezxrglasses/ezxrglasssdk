using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SpatialAnchor))]
public class SpatialAnchorInspector : Editor
{
    SpatialAnchor spatialAnchor;

    private SerializedProperty modeProp;
    private SerializedProperty lookToHeadProp;
    private SerializedProperty toggle_Hori;
    private SerializedProperty angleHoriProp;
    private SerializedProperty toggle_Vert;
    private SerializedProperty angleVertProp;
    private SerializedProperty durationProp;

    private void OnEnable()
    {
        spatialAnchor = target as SpatialAnchor;

        modeProp = serializedObject.FindProperty("mode");
        lookToHeadProp = serializedObject.FindProperty("lookToHead");
        toggle_Hori = serializedObject.FindProperty("toggle_Hori");
        angleHoriProp = serializedObject.FindProperty("angle_Hori");
        toggle_Vert = serializedObject.FindProperty("toggle_Vert");
        angleVertProp = serializedObject.FindProperty("angle_Vert");
        durationProp = serializedObject.FindProperty("duration");
    }

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();

        serializedObject.Update();

        EditorGUILayout.PropertyField(modeProp);

        EditorGUILayout.LabelField("Always look to head", EditorStyles.boldLabel);
        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        {
            EditorGUILayout.PropertyField(lookToHeadProp);
        }
        EditorGUILayout.EndVertical();

        if (spatialAnchor.mode == SpatialAnchor.Mode.ThreeDofWithFollow)
        {
            EditorGUILayout.LabelField("Angles to trigger Follow", EditorStyles.boldLabel);
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            {
                EditorGUILayout.PropertyField(toggle_Hori, new GUIContent("Horizontal"));
                if (toggle_Hori.boolValue)
                {
                    EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                    {
                        EditorGUILayout.PropertyField(angleHoriProp, new GUIContent("Angle:[0,180]"));
                    }
                    EditorGUILayout.EndHorizontal();
                }
                EditorGUILayout.PropertyField(toggle_Vert, new GUIContent("Vertical"));
                if (toggle_Vert.boolValue)
                {
                    EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                    {
                        EditorGUILayout.PropertyField(angleVertProp, new GUIContent("Angle:[0,180]"));
                    }
                    EditorGUILayout.EndHorizontal();
                }
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.LabelField("Time to complete Follow", EditorStyles.boldLabel);
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            {
                EditorGUILayout.PropertyField(durationProp);
            }
            EditorGUILayout.EndVertical();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
