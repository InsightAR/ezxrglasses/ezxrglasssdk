using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public partial class AssetsImporter : AssetPostprocessor
{
    private static string[] targetModelNames = new string[] { "SpatialComputing", "Tracking2D", "Tracking3D" };


    static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths) {
        foreach (string importedAsset in importedAssets)
        {
            foreach (string targetName in targetModelNames) {
                string keyName = "Demos/" + targetName + "/AlgAssets";
                if (importedAsset.EndsWith(keyName))
                {
                    Debug.Log("-20002- OnPostprocessAllAssets importedAsset=" + importedAsset);
                    if (Directory.Exists(importedAsset))
                    {
                        string folderName = Path.GetFileName(importedAsset);
                        string sourceAssetsPath = Application.dataPath + "/" + importedAsset.Substring("Assets/".Length);
                        if (!Directory.Exists(sourceAssetsPath)) {
                            continue;
                        }
                        string streamingAssetsPath = Application.dataPath + "/StreamingAssets";
                        string destinationFolderPath = Path.Combine(streamingAssetsPath, targetName+"/"+folderName);
                        if (!Directory.Exists(destinationFolderPath))
                        {
                            Directory.CreateDirectory(destinationFolderPath);
                        }
                        Debug.Log("-20002- OnPostprocessAllAssets copy From=[ " + sourceAssetsPath + "] to [" + destinationFolderPath + "]");
                        CopyAll(new DirectoryInfo(sourceAssetsPath),new DirectoryInfo(destinationFolderPath));                        
                    }

                    // 刷新 AssetDatabase，使其能够在 Unity 编辑器中立即显示
                    AssetDatabase.Refresh();
                }
            }
        }
    }
    private static void CopyAll(DirectoryInfo source, DirectoryInfo target)
    {
        // 复制文件
        foreach (FileInfo file in source.GetFiles())
        {
            file.CopyTo(Path.Combine(target.FullName, file.Name), true);
        }

        // 递归复制子文件夹
        foreach (DirectoryInfo sourceSubDir in source.GetDirectories())
        {
            DirectoryInfo targetSubDir = target.CreateSubdirectory(sourceSubDir.Name);
            CopyAll(sourceSubDir, targetSubDir);
        }
    }
}
